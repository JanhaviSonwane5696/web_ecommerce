﻿using Repository.Model;
using System.Collections.Generic;

namespace Services.Interface
{
    public interface IProductItemServices
    {
        List<ProductItemModel> GetProductItems(int productId);
        List<ProductItemsModel> GetProductItem(int productId);
        List<ProductItemsModel> GetProductItemsByProductId(int productId);
        List<ProductItemModel> GetPreApprovalProductItem(int userId, int productId);
        List<ProductItemAttributesModel> GetProductItemAttributeList(int productId);
        List<ShortProductItemModel> GetProductItemShortList(int productId);
        bool AddProductItem(AddProductItemModel productItem, string createdBy);
        bool UpdateProductItem(AddProductItemModel productItem, string modifiedBy);
        bool DeleteProductItem(ProductItemModel productItem, string modifiedBy);
        bool UpdateStatusProductItem(ProductItemModel productItem, string modifiedBy);
        bool UpdateProductItemStatus(UpdateProductItemModel productItem);
    }
}