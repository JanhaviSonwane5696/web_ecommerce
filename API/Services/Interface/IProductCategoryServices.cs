﻿using Models;
using Repository.Model;
using System.Collections.Generic;

namespace Services.Interface
{
    public interface IProductCategoryServices
    {
        List<ProductCategoryModel> GetProductCategory();
        bool AddProductCategory(AddUpdateProductCategoryModel productCategory);
        bool UpdateProductCategory(AddUpdateProductCategoryModel productCategory);
        bool DeleteProductCategory(ProductCategoryModel productCategory);
        bool UpdateStatusProductCategory(ProductCategoryModel productCategory);
    }
}