﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interface
{
   public interface IUserAddressServices
    {
        List<UserAddressModel> GetUserAddress(int userId);
        Tuple<bool, string> AddUserAddress(AddUserAddressModel model);
        Tuple<bool, string> UpdateUserAddress(UpdateUserAddressModel model);
        Tuple<bool, string> DeleteUserAddress(DeleteUserAddressModel model);
    }
}
