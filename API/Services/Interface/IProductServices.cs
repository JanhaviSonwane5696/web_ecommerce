﻿using Repository.Model;
using System;
using System.Collections.Generic;

namespace Services.Interface
{
    public interface IProductServices
    {
        List<ProductsModel> GetProduct(int userId);
        List<ProductsModel> GetAllProduct(int userId);
        bool AddProduct(ProductModel product, int userId, string createdBy);
        Tuple<bool, string> AddProduct(ProductCloneModel product);
        bool UpdateProduct(ProductModel product, int userId, string modifiedBy);
        bool DeleteProduct(ProductModel product, int userId, string modifiedBy);
        bool UpdateStatusProduct(ProductModel product, int userId, string modifiedBy);
        bool UpdateStatusProduct(ProductUpdateModel product);
        List<ProductsModel> GetProductByCategoryId(int categoryId, int userId);
        List<ProductsModel> GetProductBySubCategoryId(int subCategoryId, int userId);
        List<ProductsModel> GetPreApprovalProduct(int userId);
        ProductAttributesModel GetProductAttributes(int productId);
    }
}