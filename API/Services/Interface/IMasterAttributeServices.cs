﻿using Models;
using System.Collections.Generic;

namespace Services.Interface
{
    public interface IMasterAttributeServices
    {
        List<MasterAttributeModel> GetMasterAttribute(int attributeId);
        List<MasterAttributeModel> GetMasterAllAttribute();
        bool AddMasterAttribute(MasterAttributeModel attributeItem);
        bool UpdateMasterAttribute(MasterAttributeModel attributeItem);
        bool DeleteMasterAttribute(MasterAttributeModel attributeItem);
        bool UpdateStatusMasterAttribute(MasterAttributeModel attributeItem);
    }
}