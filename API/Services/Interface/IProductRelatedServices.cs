﻿using Repository.Model;
using System.Collections.Generic;

namespace Services.Interface
{
    public interface IProductRelatedServices
    {
        List<ProductRelatedModel> GetProductRelated();
        bool AddProductRelated(AddUpdateProductRelatedModel productRelated);
        bool UpdateProductRelated(AddUpdateProductRelatedModel productRelated);
        bool DeleteProductRelated(ProductRelatedModel productRelated);
        bool UpdateStatusProductRelated(ProductRelatedModel productRelated);
    }
}