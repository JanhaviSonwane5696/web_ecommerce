﻿using Repository.Model;
using System;

namespace Services.Interface
{
    public interface IShoppingCartServices
    {
        ShoppingCartsModel GetCartDetails(string guid, int userId);
        bool DeleteProductCartItem(int cartItemID);
        bool AddShoppingCart(ShoppingCartModel cartData);
        bool AddShoppingCartItem(ShoppingCartItemModel cartItemData);
        Tuple<bool, string> UpdateProductCartItem(string guid, int userId);
    }
}
