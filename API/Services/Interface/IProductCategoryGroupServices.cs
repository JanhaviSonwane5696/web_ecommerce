﻿using Repository.Model;
using System.Collections.Generic;

namespace Services.Interface
{
    public interface IProductCategoryGroupServices
    {
        List<ProductCategoryGroupModel> GetProductCategoryGroup();
        bool AddProductCategoryGroup(AddUpdateProductCategoryGroupModel productCategory);
        bool UpdateProductCategoryGroup(AddUpdateProductCategoryGroupModel productCategory);
        bool DeleteProductCategoryGroup(ProductCategoryGroupModel productCategory);
        bool UpdateStatusProductCategoryGroup(ProductCategoryGroupModel productCategory);
    }
}