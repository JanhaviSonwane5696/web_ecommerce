﻿using Models;
using System;
using System.Collections.Generic;

namespace Services.Interface
{
    public interface IShoppingOrderServices
    {
        ShoppingOrderResponseModel PlaceOrder(ShoppingOrderModel model);
        List<ShoppingOrderDetailsModel> UserOrders(int userId);
        ShoppingOrderDetailModel UserOrderDetails(int userId, int id);
        List<ShoppingOrdersDetailsModel> UsersOrders(OrderReportModel model);
        List<ShoppingOrdersDetailsModel> UsersAcceptedOrders(int userId);
        Tuple<bool, string, int> UpdateOrder(UpdateOrderStatusModel model);
    }
}
