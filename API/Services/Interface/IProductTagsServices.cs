﻿using Models;
using Repository.Model;
using System.Collections.Generic;

namespace Services.Interface
{
    public interface IProductTagsServices
    {
        List<ProductTagsModel> GetProductTags();
        bool AddProductTags(ProductTagsModel productTags);
        bool UpdateProductTags(ProductTagsModel productTags);
        bool DeleteProductTags(ProductTagsModel productTags);
        bool UpdateStatusProductTags(ProductTagsModel productTags);
    }
}