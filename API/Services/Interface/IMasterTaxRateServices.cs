﻿using Models;
using System.Collections.Generic;

namespace Services.Interface
{
    public interface IMasterTaxRateServices
    {
        List<MasterTaxRatesModel> GetMasterTaxRate(int attributeId);
        List<MasterTaxRatesModel> GetAllMasterTaxRate();
        bool AddMasterTaxRate(MasterTaxRatesModel attribute);
        bool UpdateMasterTaxRate(MasterTaxRatesModel attribute);
        bool DeleteMasterTaxRate(MasterTaxRatesModel attribute);
        bool UpdateStatusMasterTaxRate(MasterTaxRatesModel attribute);
    }
}