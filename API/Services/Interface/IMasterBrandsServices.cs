﻿using Models;
using System.Collections.Generic;

namespace Services.Interface
{
    public interface IMasterBrandsServices
    {
        List<MasterBrandsModel> GetMasterBrands(int BrandsId);
        List<MasterBrandsModel> GetMasterAllBrands();
        bool AddMasterBrands(AddMasterBrandsModel BrandsItem);
        bool UpdateMasterBrands(AddMasterBrandsModel BrandsItem);
        bool DeleteMasterBrands(AddMasterBrandsModel BrandsItem);
        bool UpdateStatusMasterBrands(AddMasterBrandsModel BrandsItem);
    }
}