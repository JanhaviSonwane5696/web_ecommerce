﻿using Models;
using System;
using System.Collections.Generic;

namespace Services.Interface
{
    public interface IUserServices
    {
        UserDetailsModel AuthenticateUser(UserLoginModel loginDetails);
        UserDetailsModel GetUserByID(int userID);
        bool ResetPassword(ResetUserPasswordModel loginDetails);
        bool ForgotPassword(ResetUserPasswordModel loginDetails);
        Tuple<bool, string> ChangePassword(UserSettingModel loginDetails);
        Tuple<bool, int, string> AddUser(AddUserModel addUserDetails, string createdBy);
        Tuple<bool, int, string> UpdateUser(UpdateUserModel addUserDetails, string createdBy);
        Tuple<bool, int, string> UpdateUserStatus(UpdateUserStatusModel addUserDetails, string createdBy);
        List<UsersList> GetUserList();
        List<UsersList> GetUserByUserTypeList(short userType);
        List<UserTypeList> GetUserTypeList();
        Tuple<bool, string> UpdateUserDeviceId(UpdateUserDeviceIDModel model);


        #region Customer Method
        Tuple<bool, string> CustomerAddRequest(AddCustomerRequestModel addUserDetails);
        Tuple<bool, int, string> CustomerConfirmRequest(AddCustomerConfirmRequestModel addUserDetails, string createdBy);
        Tuple<bool, int, string> UpdateCustomerEmail(UpdateCustomerRequestModel updateUserDetails);
        #endregion

        #region Vendor Method
        Tuple<bool, string> VendorPhoneVerification(VerifyPhoneNumberModel verifyDetails);
        Tuple<bool, int, string> AddVendor(AddVendorModel addVendorDetails, string createdBy);
        PickUpAddressResponseModel GetPickupAddress(PickUpAddressModel pincodeDetails);
        Tuple<bool, int, string> UpdatePickupAddress(UpdatePickupPinAddressModel addVendorDetails);
        Tuple<bool, int, string> UpdateGSTIN(UpdateGSTINModel addVendorDetails);
        Tuple<bool, int, string> UpdateBankDetails(UpdateBankDetailsModel addVendorDetails);
        Tuple<bool, int, string> UpdateStoreDetails(StoreDetailsModel addVendorDetails);
        Tuple<bool, int, string> UpdateSignatureDetails(UpdateSignatureModel addVendorDetails);
        VendorDetailsModel VendorDetails(VendorStoredDetailsModel vendorDetails);
        Tuple<bool, int, string> UpdateProfileDetails(UpdateProfileDetailsModel vendorDetails);
        List<UsersList> GetVendorList();
        List<UsersListModel> GetVendorsList();
        Tuple<bool, int, string> VendorStatusRequest(UpdateVendorStatusModel vendorDetails);
        List<UserDocumentsListModel> UserDocumentList(VendorStoredDetailsModel vendorDetails);
        List<BanksDetailsModel> UserBankList(VendorStoredDetailsModel vendorDetails);
        #endregion
    }
}