﻿using Models;
using Repository.Model;
using System.Collections.Generic;

namespace Services.Interface
{
    public interface IProductSubCategoryServices
    {
        List<ProductSubCategoryModel> GetProductSubCategory();
        List<ProductSubCategoryModel> GetAllProductSubCategory();
        bool AddProductSubCategory(AddUpdateProductSubCategoryModel ProductSubCategory);
        List<ProductSubCategoryModel> GetProductSubCategoryByCategoryId(int productCategoryId);
        bool UpdateProductSubCategory(AddUpdateProductSubCategoryModel ProductSubCategory);
        bool DeleteProductSubCategory(ProductSubCategoryModel ProductSubCategory);
        bool UpdateStatusProductSubCategory(ProductSubCategoryModel ProductSubCategory);
    }
}