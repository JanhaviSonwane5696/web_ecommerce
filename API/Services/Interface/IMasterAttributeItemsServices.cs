﻿using Models;
using Repository.Model;
using System.Collections.Generic;

namespace Services.Interface
{
    public interface IMasterAttributeItemsServices
    {
        List<MasterAttributeItemsModel> GetMasterAttributeItems(int attributeId);
        MasterAttributeItemsModel GetMasterAttributeItemById(int attributeItemId);
        List<MasterAttributeItemsModel> GetMasterAllAttributeItems();
        bool AddMasterAttributeItems(MasterAttributeItemsModel attributeItem);
        bool UpdateMasterAttributeItems(MasterAttributeItemsModel attributeItem);
        bool DeleteMasterAttributeItems(MasterAttributeItemsModel attributeItem);
        bool UpdateStatusMasterAttributeItems(MasterAttributeItemsModel attributeItem);
    }
}