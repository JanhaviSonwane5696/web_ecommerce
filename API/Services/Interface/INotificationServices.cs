﻿using Models;
using System;
using System.Collections.Generic;

namespace Services.Interface
{
    public interface INotificationServices
    {
        List<MasterSMSApiListModel> SMSAPIList();
        Tuple<bool, int, string> AddSMSAPI(AddNotificationSMSAPIModel addSMSDetails, string createdBy);
        Tuple<bool, int, string> UpdateSMSAPI(UpdateNotificationSMSAPIModel addSMSDetails, string createdBy);
        Tuple<bool, int, string> UpdateSMSAPIStatus(UpdateNotificationSMSAPIStatusModel addSMSDetails, string createdBy);


        List<MasterSMSTemplatesListModel> SMSTemplateList();
        Tuple<bool, int, string> AddSMSTemplate(AddNotificationSMSModel addSMSDetails, string createdBy);
        Tuple<bool, int, string> UpdateSMSTemplate(UpdateNotificationSMSModel addSMSDetails, string createdBy);
        Tuple<bool, int, string> UpdateSMSTemplateStatus(UpdateStatusNotificationSMSModel addSMSDetails, string createdBy);
        Tuple<bool, int, string> DeleteSMSTemplate(DeleteNotificationSMSModel addSMSDetails, string createdBy);


        List<MasterEmailSettingsListModel> EmailAPIList();
        Tuple<bool, int, string> AddEmailAPI(AddNotificationEmailAPIModel addSMSDetails, string createdBy);
        Tuple<bool, int, string> UpdateEmailAPI(UpdateNotificationEmailAPIModel addSMSDetails, string createdBy);
        Tuple<bool, int, string> UpdateEmailAPIStatus(UpdateNotificationEmailAPIStatusModel addSMSDetails, string createdBy);


        List<MasterEmailTemplatesListModel> EmailTemplateList();
        Tuple<bool, int, string> AddEmailTemplate(AddNotificationEmailModel addEmailDetails, string createdBy);
        Tuple<bool, int, string> UpdateEmailTemplate(UpdateNotificationEmailModel emailDetails, string createdBy);
        Tuple<bool, int, string> UpdateEmailTemplateStatus(UpdateStatusNotificationEmailModel emailDetails, string createdBy);

        Tuple<bool, string> PramotionalSMSV1(PramotionalSMSV1Model model);
        Tuple<bool, string> PramotionalSMSV2(PramotionalSMSV2Model model);

        Tuple<bool, string> PramotionalEmailV1(PramotionalEmailV1Model model);
        Tuple<bool, string> PramotionalEmailV2(PramotionalEmailV2Model model);

        Tuple<bool, string, string> PushNotifications(PushNotificationModel model);
    }
}
