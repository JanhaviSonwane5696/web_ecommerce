﻿using Models;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interface
{
    public interface IBanksServices
    {
        List<string> BankNameList();
        List<string> BankStateList(BankStateModel bankdetails);
        List<string> BankDistrictList(BankDistrictModel bankdetails);
        List<string> BankBranchList(BankBranchModel bankdetails);
        AllBankName BankDetails(BankBranchDetailsModel bankdetails);
        AllBankName BankIfscDetails(BankDetailsModel bankdetails);
    }
}
