﻿using Helpers;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using Repository.Interface;
using Repository.Model;
using Services.Interface;
using System;
using System.Collections.Generic;

namespace Services.Service
{
    public class ProductServices : IProductServices
    {
        private readonly IProductRepository _productRepository;
        private readonly IProductItemRepository _productItemRepository;

        public ProductServices(IProductRepository productRepository, IProductItemRepository productItemRepository, ILoggerService loggerService)
        {
            _productRepository = productRepository;
            _productItemRepository = productItemRepository;
        }

        /// <summary>
        /// Get Product
        /// </summary>
        /// <returns></returns>
        public List<ProductsModel> GetProduct(int userId)
        {
            List<ProductsModel> product = _productRepository.GetAllProduct(userId);
            //List<ProductModel> productData = product.Select(x => new ProductModel
            //{
            //    Id = x.Id,
            //    Name = x.Name,
            //    UserId = x.UserId,
            //    ProductCategoryId = x.ProductCategoryId,
            //    ProductSubCategoryId = x.ProductSubCategoryId,
            //    Keywords = x.Keywords,
            //    SubProductFilterText = x.SubProductFilterText,
            //    HasMultipleProducts = x.HasMultipleProducts,
            //    IsActive = x.IsActive,
            //    ProductCategory
            //}).ToList();

            return product;
        }

        public List<ProductsModel> GetAllProduct(int userId)
        {
            List<ProductsModel> product = _productRepository.GetAllProduct(userId);
            return product;
        }

        public List<ProductsModel> GetProductByCategoryId(int categoryId, int userId)
        {
            List<ProductsModel> product = _productRepository.GetAllProductByCategoryId(userId, categoryId);
            //List<ProductModel> productData = product.Select(x => new ProductModel
            //{
            //    Id = x.Id,
            //    Name = x.Name,
            //    UserId = x.UserId,
            //    ProductCategoryId = x.ProductCategoryId,
            //    ProductSubCategoryId = x.ProductSubCategoryId,
            //    Keywords = x.Keywords,
            //    SubProductFilterText = x.SubProductFilterText,
            //    HasMultipleProducts = x.HasMultipleProducts,
            //    IsActive = x.IsActive
            //}).ToList();

            return product;
        }

        public List<ProductsModel> GetProductBySubCategoryId(int subCategoryId, int userId)
        {
            List<ProductsModel> product = _productRepository.GetAllProductBySubCategoryId(userId, subCategoryId);
            //List<ProductModel> productData = product.Select(x => new ProductModel
            //{
            //    Id = x.Id,
            //    Name = x.Name,
            //    UserId = x.UserId,
            //    ProductCategoryId = x.ProductCategoryId,
            //    ProductSubCategoryId = x.ProductSubCategoryId,
            //    Keywords = x.Keywords,
            //    SubProductFilterText = x.SubProductFilterText,
            //    HasMultipleProducts = x.HasMultipleProducts,
            //    IsActive = x.IsActive
            //}).ToList();

            return product;
        }

        public List<ProductsModel> GetPreApprovalProduct(int userId)
        {
            List<ProductsModel> product = _productRepository.GetProductByStatus(userId, Variables.ProductStatus.Accepted);
            return product;
        }

        public ProductAttributesModel GetProductAttributes(int productId)
        {
            ProductAttributesModel product = _productRepository.ProductAttributeList(productId);
            return product;
        }

        /// <summary>
        /// Add Product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public bool AddProduct(ProductModel product, int userId, string createdBy)
        {
            Product productData = new Product()
            {
                UserId = product.UserId,
                ProductCategoryId = product.ProductCategoryId,
                ProductSubCategoryId = product.ProductSubCategoryId,
                Name = product.Name,
                Keywords = product.Keywords,
                SubProductFilterText = product.SubProductFilterText,
                HasMultipleProducts = product.HasMultipleProducts,
                ProductAttribute = product.ProductAttribute,
                ImageURL = product.ImageURL,
                IsActive = product.IsActive,
                IsDeleted = false,
                CreatedOn = DateTime.Now,
                CreatedBy = createdBy,
                Status = Variables.ProductStatus.Approved
            };

            return _productRepository.AddProduct(productData).Item1;
        }

        public Tuple<bool, string> AddProduct(ProductCloneModel product)
        {
            Product productData = new Product()
            {
                UserId = product.UserId,
                ProductCategoryId = product.ProductCategoryId,
                ProductSubCategoryId = product.ProductSubCategoryId,
                Name = product.Name,
                Keywords = product.Keywords,
                SubProductFilterText = product.SubProductFilterText,
                HasMultipleProducts = product.HasMultipleProducts,
                ProductAttribute = product.ProductAttribute,
                ImageURL = product.ImageURL,
                IsActive = product.IsActive,
                IsDeleted = false,
                CreatedOn = DateTime.Now,
                CreatedBy = product.CreatedBy,
                Status = Variables.ProductStatus.Approved
            };

            var result = _productRepository.AddProduct(productData);
            if (result.Item1 && !string.IsNullOrEmpty(product.ProductItems))
            {
                string[] productItems = product.ProductItems.Split(',');
                foreach (string itemId in productItems)
                {
                    int productItemId = Convert.ToInt32(itemId);
                    ProductItem productItem = _productItemRepository.GetProductItemById(productItemId);
                    if (productItem != null)
                    {
                        ProductItem _add = new ProductItem()
                        {
                            ProductId = result.Item2,
                            Name = productItem.Name,
                            Price = productItem.Price,
                            DistributorPrice = productItem.DistributorPrice,
                            RetailPrice = productItem.RetailPrice,
                            SalePrice = productItem.SalePrice,
                            Weight = productItem.Weight,
                            Quantity = productItem.Quantity,
                            MaxQuantityForOrder = productItem.MaxQuantityForOrder,
                            Description = productItem.Description,
                            ShortDescription = productItem.ShortDescription,
                            Specification = productItem.Specification,
                            LegalDisclaimer = productItem.LegalDisclaimer,
                            RelatedProducts = productItem.RelatedProducts,
                            CompareProducts = productItem.CompareProducts,
                            SponsoredProducts = productItem.SponsoredProducts,
                            ImageURL1 = productItem.ImageURL1,
                            ImageThumbURL1 = productItem.ImageThumbURL1,
                            ImageURL2 = productItem.ImageURL2,
                            ImageThumbURL2 = productItem.ImageThumbURL2,
                            ImageURL3 = productItem.ImageURL3,
                            ImageThumbURL3 = productItem.ImageThumbURL3,
                            ImageURL4 = productItem.ImageURL4,
                            ImageThumbURL4 = productItem.ImageThumbURL4,
                            ImageURL5 = productItem.ImageURL5,
                            ImageThumbURL5 = productItem.ImageThumbURL5,
                            ImageURL6 = productItem.ImageURL6,
                            ImageThumbURL6 = productItem.ImageThumbURL6,
                            ImageURL7 = productItem.ImageURL7,
                            ImageThumbURL7 = productItem.ImageThumbURL7,
                            IsDefault = productItem.IsDefault,
                            IsActive = productItem.IsActive,
                            IsDeleted = false,
                            CreatedOn = DateTime.Now,
                            CreatedBy = product.CreatedBy,
                            SKU = productItem.SKU,
                            MetaTag = productItem.MetaTag,
                            Sale = productItem.Sale,
                            MinimumQuantity = productItem.MinimumQuantity,
                            ProductAttribute = productItem.ProductAttribute,
                            Brand = productItem.Brand,
                            Tax = productItem.Tax,
                            DeliveryCharge = productItem.DeliveryCharge,
                            RemainingQuantity = productItem.RemainingQuantity,
                            CountryOfOrigin = productItem.CountryOfOrigin,
                            Status = product.UserId == 1 ? Variables.ProductStatus.Approved : Variables.ProductStatus.Accepted
                        };
                        _productItemRepository.AddProductItem(_add);
                    }
                }
            }
            return new Tuple<bool, string>(true, "Product added successfully.");
        }

        /// <summary>
        /// Update Product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public bool UpdateProduct(ProductModel product, int userId, string modifiedBy)
        {
            Product productData = new Product();
            productData.Id = product.Id;
            productData.UserId = product.UserId;
            productData.ProductCategoryId = product.ProductCategoryId;
            productData.ProductSubCategoryId = product.ProductSubCategoryId;
            productData.Name = product.Name;
            productData.Keywords = product.Keywords;
            productData.SubProductFilterText = product.SubProductFilterText;
            productData.HasMultipleProducts = product.HasMultipleProducts;
            productData.ProductAttribute = product.ProductAttribute;
            productData.ImageURL = product.ImageURL;
            productData.ModifiedOn = DateTime.Now;
            productData.ModifiedBy = modifiedBy;
            return _productRepository.UpdateProduct(productData, userId);
        }

        /// <summary>
        /// Delete Product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public bool DeleteProduct(ProductModel product, int userId, string modifiedBy)
        {
            return _productRepository.DeleteProduct(product.Id, userId, modifiedBy);
        }

        /// <summary>
        /// Update Status Product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public bool UpdateStatusProduct(ProductModel product, int userId, string modifiedBy)
        {
            return _productRepository.UpdateStatusProduct(product.Id, userId, product.IsActive, modifiedBy);
        }

        /// <summary>
        /// Update Status Product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public bool UpdateStatusProduct(ProductUpdateModel product)
        {
            return _productRepository.UpdateStatusProduct(product.ProductId, product.UserId, product.Status, product.Remarks, product.ModifiedBy);
        }

        #region Private Members

        #endregion
    }
}
