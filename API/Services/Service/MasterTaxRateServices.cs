﻿using Models;
using Repository.Interface;
using Repository.Model;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services.Service
{
    public class MasterTaxRateServices : IMasterTaxRateServices
    {
        private readonly IMasterTaxRateRepository _attributeRepository;

        public MasterTaxRateServices(IMasterTaxRateRepository attributeRepository, ILoggerService loggerService)
        {
            _attributeRepository = attributeRepository;
        }

        /// <summary>
        /// Get MasterTaxRates
        /// </summary>
        /// <returns></returns>
        public List<MasterTaxRatesModel> GetMasterTaxRate(int attributeId)
        {
            List<MasterTaxRates> attribute = _attributeRepository.GetMasterTaxRate(attributeId);
            List<MasterTaxRatesModel> attributeData = attribute.Select(x => new MasterTaxRatesModel
            {
                Id = x.Id,
                Name = x.Name,
                Type = x.Type,
                Amount = x.Amount,
                CreatedBy = x.CreatedBy,
                CreatedOn = x.CreatedOn,
                ModifiedBy = x.ModifiedBy,
                ModifiedOn = x.ModifiedOn,
                IsActive = x.IsActive
            }).ToList();

            return attributeData;
        }

        /// <summary>
        /// Get MasterTaxRates
        /// </summary>
        /// <returns></returns>
        public List<MasterTaxRatesModel> GetAllMasterTaxRate()
        {
            List<MasterTaxRates> attribute = _attributeRepository.GetAllMasterTaxRate();
            List<MasterTaxRatesModel> attributeData = attribute.Select(x => new MasterTaxRatesModel
            {
                Id = x.Id,
                Name = x.Name,
                Type = x.Type,
                Amount = x.Amount,
                CreatedBy = x.CreatedBy,
                CreatedOn = x.CreatedOn,
                ModifiedBy = x.ModifiedBy,
                ModifiedOn = x.ModifiedOn,
                IsActive = x.IsActive
            }).ToList();

            return attributeData;
        }

        /// <summary>
        /// Add MasterTaxRates
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public bool AddMasterTaxRate(MasterTaxRatesModel attribute)
        {
            MasterTaxRates attributeData = new MasterTaxRates();
            attributeData.Name = attribute.Name;
            attributeData.Type = attribute.Type;
            attributeData.Amount = attribute.Amount;
            attributeData.IsActive = attribute.IsActive;
            attributeData.IsDeleted = false;
            attributeData.CreatedOn = DateTime.Now;
            attributeData.CreatedBy = attribute.CreatedBy;
            return _attributeRepository.AddMasterTaxRate(attributeData);
        }

        /// <summary>
        /// Update MasterTaxRates
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public bool UpdateMasterTaxRate(MasterTaxRatesModel attribute)
        {
            MasterTaxRates attributeData = new MasterTaxRates();
            attributeData.Id = attribute.Id;
            attributeData.Name = attribute.Name;
            attributeData.Type = attribute.Type;
            attributeData.Amount = attribute.Amount;
            attributeData.ModifiedOn = DateTime.Now;
            attributeData.ModifiedBy = attribute.ModifiedBy;
            return _attributeRepository.UpdateMasterTaxRate(attributeData);
        }

        /// <summary>
        /// Delete MasterTaxRates
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public bool DeleteMasterTaxRate(MasterTaxRatesModel attribute)
        {
            return _attributeRepository.DeleteMasterTaxRate(attribute.Id, attribute.ModifiedBy);
        }

        /// <summary>
        /// Update Status MasterTaxRates
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public bool UpdateStatusMasterTaxRate(MasterTaxRatesModel attribute)
        {
            return _attributeRepository.UpdateStatusMasterTaxRate(attribute.Id, attribute.IsActive, attribute.ModifiedBy);
        }

        #region Private Members

        #endregion
    }
}