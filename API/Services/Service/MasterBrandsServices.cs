﻿using Models;
using Repository.Interface;
using Repository.Model;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services.Service
{
    public class MasterBrandsServices : IMasterBrandsServices
    {
        private readonly IMasterBrandsRepository _BrandsRepository;

        public MasterBrandsServices(IMasterBrandsRepository BrandsRepository, ILoggerService loggerService)
        {
            _BrandsRepository = BrandsRepository;
        }

        /// <summary>
        /// Get MasterBrands
        /// </summary>
        /// <returns></returns>
        public List<MasterBrandsModel> GetMasterBrands(int BrandsId)
        {
            List<MasterBrands> Brands = _BrandsRepository.GetMasterBrands(BrandsId);
            List<MasterBrandsModel> BrandsData = Brands.Select(x => new MasterBrandsModel
            {
                Id = x.Id,
                BrandsName = x.BrandsName,
                BrandsDesciption = x.BrandsDesciption,
                CreatedBy = x.CreatedBy,
                CreatedOn = x.CreatedOn,
                ModifiedBy = x.ModifiedBy,
                ModifiedOn = x.ModifiedOn,
                IsActive = x.IsActive,
                BrandsLogoURL = x.BrandsLogoURL
            }).ToList();

            return BrandsData;
        }

        public List<MasterBrandsModel> GetMasterAllBrands()
        {
            List<MasterBrands> Brands = _BrandsRepository.GetMasterAllBrands();
            List<MasterBrandsModel> BrandsData = Brands.Select(x => new MasterBrandsModel
            {
                Id = x.Id,
                BrandsName = x.BrandsName,
                BrandsDesciption = x.BrandsDesciption,
                CreatedBy = x.CreatedBy,
                CreatedOn = x.CreatedOn,
                ModifiedBy = x.ModifiedBy,
                ModifiedOn = x.ModifiedOn,
                IsActive = x.IsActive,
                BrandsLogoURL = x.BrandsLogoURL
            }).ToList();

            return BrandsData;
        }

        /// <summary>
        /// Add MasterBrands
        /// </summary>
        /// <param name="Brands"></param>
        /// <returns></returns>
        public bool AddMasterBrands(AddMasterBrandsModel Brands)
        {
            MasterBrands BrandsData = new MasterBrands();
            BrandsData.BrandsName = Brands.BrandsName;
            BrandsData.BrandsDesciption = Brands.BrandsDesciption;
            BrandsData.IsActive = Brands.IsActive;
            BrandsData.BrandsLogoURL = Brands.BrandsLogoURL;
            BrandsData.IsDeleted = false;
            BrandsData.CreatedOn = DateTime.Now;
            BrandsData.CreatedBy = Brands.CreatedBy;
            return _BrandsRepository.AddMasterBrands(BrandsData);
        }

        /// <summary>
        /// Update MasterBrands
        /// </summary>
        /// <param name="Brands"></param>
        /// <returns></returns>
        public bool UpdateMasterBrands(AddMasterBrandsModel Brands)
        {
            MasterBrands BrandsData = new MasterBrands();
            BrandsData.Id = Brands.Id;
            BrandsData.BrandsName = Brands.BrandsName;
            BrandsData.BrandsDesciption = Brands.BrandsDesciption;
            BrandsData.BrandsLogoURL = Brands.BrandsLogoURL;
            BrandsData.ModifiedOn = DateTime.Now;
            BrandsData.ModifiedBy = Brands.ModifiedBy;
            return _BrandsRepository.UpdateMasterBrands(BrandsData);
        }

        /// <summary>
        /// Delete MasterBrands
        /// </summary>
        /// <param name="Brands"></param>
        /// <returns></returns>
        public bool DeleteMasterBrands(AddMasterBrandsModel Brands)
        {
            return _BrandsRepository.DeleteMasterBrands(Brands.Id, Brands.ModifiedBy);
        }

        /// <summary>
        /// Update Status MasterBrands
        /// </summary>
        /// <param name="Brands"></param>
        /// <returns></returns>
        public bool UpdateStatusMasterBrands(AddMasterBrandsModel Brands)
        {
            return _BrandsRepository.UpdateStatusMasterBrands(Brands.Id, Brands.IsActive, Brands.ModifiedBy);
        }

        #region Private Members

        #endregion
    }
}