﻿using Models;
using Repository.Interface;
using Repository.Model;
using Services.Interface;
using System.Collections.Generic;
using System.Linq;

namespace Services.Service
{
    public class ProductTagsServices : IProductTagsServices
    {
        private readonly IProductTagsRepository _productTagsRepository;

        public ProductTagsServices(IProductTagsRepository productTagsRepository, ILoggerService loggerService)
        {
            _productTagsRepository = productTagsRepository;
        }

        /// <summary>
        /// Get Product Tags
        /// </summary>
        /// <returns></returns>
        public List<ProductTagsModel> GetProductTags()
        {
            List<ProductTags> productTags = _productTagsRepository.GetProductTags();
            List<ProductTagsModel> productTagsData = productTags.Select(x => new ProductTagsModel
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                IsActive = x.IsActive,
                CreatedOn = x.CreatedOn,
                CreatedBy = x.CreatedBy,
                ModifiedOn = x.ModifiedOn,
                ModifiedBy = x.ModifiedBy
            }).ToList();

            return productTagsData;
        }

        /// <summary>
        /// Add Product Tags
        /// </summary>
        /// <param name="productTags"></param>
        /// <returns></returns>
        public bool AddProductTags(ProductTagsModel productTags)
        {
            ProductTags productTagsData = new ProductTags();
            productTagsData.Id = productTags.Id;
            productTagsData.Name = productTags.Name;
            productTagsData.Description = productTags.Description;
            productTagsData.IsActive = productTags.IsActive;
            productTagsData.IsDeleted = false;
            productTagsData.CreatedOn = productTags.CreatedOn;
            productTagsData.CreatedBy = productTags.CreatedBy;
            //productTagsData.ModifiedOn = productTags.ModifiedOn;
            //productTagsData.ModifiedBy = productTags.ModifiedBy;
            return _productTagsRepository.AddProductTags(productTagsData);
        }

        /// <summary>
        /// Update Product Tags
        /// </summary>
        /// <param name="productTags"></param>
        /// <returns></returns>
        public bool UpdateProductTags(ProductTagsModel productTags)
        {
            ProductTags productTagsData = new ProductTags();
            productTagsData.Id = productTags.Id;
            productTagsData.Name = productTags.Name;
            productTagsData.Description = productTags.Description;
            //productTagsData.IsActive = productTags.IsActive;
            //productTagsData.IsDeleted = productTags.IsDeleted;
            //productTagsData.CreatedOn = productTags.CreatedOn;
            //productTagsData.CreatedBy = productTags.CreatedBy;
            productTagsData.ModifiedOn = productTags.ModifiedOn;
            productTagsData.ModifiedBy = productTags.ModifiedBy;
            return _productTagsRepository.UpdateProductTags(productTagsData);
        }

        /// <summary>
        /// Delete Product Tags
        /// </summary>
        /// <param name="productTags"></param>
        /// <returns></returns>
        public bool DeleteProductTags(ProductTagsModel productTags)
        {
            return _productTagsRepository.DeleteProductTags(productTags.Id, productTags.ModifiedBy);
        }

        /// <summary>
        /// Update Status Product Tags
        /// </summary>
        /// <param name="productTags"></param>
        /// <returns></returns>
        public bool UpdateStatusProductTags(ProductTagsModel productTags)
        {
            return _productTagsRepository.UpdateStatusProductTags(productTags.Id, productTags.IsActive, productTags.ModifiedBy);
        }

        #region Private Members

        #endregion
    }
}