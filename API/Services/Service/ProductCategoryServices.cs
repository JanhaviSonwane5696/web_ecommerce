﻿using Models;
using Repository.Interface;
using Repository.Model;
using Services.Interface;
using System.Collections.Generic;
using System.Linq;

namespace Services.Service
{
    public class ProductCategoryServices : IProductCategoryServices
    {
        private readonly IProductCategoryRepository _productCategoryRepository;

        public ProductCategoryServices(IProductCategoryRepository productCategoryRepository, ILoggerService loggerService)
        {
            _productCategoryRepository = productCategoryRepository;
        }

        /// <summary>
        /// Get Product Category
        /// </summary>
        /// <returns></returns>
        public List<ProductCategoryModel> GetProductCategory()
        {
            List<ProductCategoryModel> productCategory = _productCategoryRepository.GetProductCategory();
            //List<ProductCategoryModel> productCategoryData = productCategory.Select(x => new ProductCategoryModel
            //{
            //    Id = x.Id,
            //    ProductCategoryGroupId = x.ProductCategoryGroupId,
            //    ProductCategoryGroup=x.ProductCategoryGroup,
            //    Name = x.Name,
            //    Description = x.Description,
            //    ImageURL = x.ImageURL,
            //    IsActive = x.IsActive,
            //    CreatedOn = x.CreatedOn,
            //    CreatedBy = x.CreatedBy,
            //    ModifiedOn = x.ModifiedOn,
            //    ModifiedBy = x.ModifiedBy,
            //    ShowHide = x.ShowHide,
            //    Priority = x.Priority,
            //    MetaTag = x.MetaTag,
            //    Status = x.Status
            //}).ToList();

            return productCategory;
        }

        /// <summary>
        /// Add Product Category
        /// </summary>
        /// <param name="productCategory"></param>
        /// <returns></returns>
        public bool AddProductCategory(AddUpdateProductCategoryModel productCategory)
        {
            ProductCategory productCategoryData = new ProductCategory();
            productCategoryData.Id = productCategory.Id;
            productCategoryData.ProductCategoryGroupId = productCategory.ProductCategoryGroupId;
            productCategoryData.Name = productCategory.Name;
            productCategoryData.ImageURL = productCategory.ImageURL;
            productCategoryData.Description = productCategory.Description;
            productCategoryData.IsActive = productCategory.IsActive;
            productCategoryData.IsDeleted = false;
            productCategoryData.CreatedOn = productCategory.CreatedOn;
            productCategoryData.CreatedBy = productCategory.CreatedBy;
            productCategoryData.ShowHide = productCategory.ShowHide;
            productCategoryData.Priority = productCategory.Priority;
            productCategoryData.MetaTag = productCategory.MetaTag;
            productCategoryData.Status = productCategory.Status;
            //productCategoryData.ModifiedOn = productCategory.ModifiedOn;
            //productCategoryData.ModifiedBy = productCategory.ModifiedBy;
            return _productCategoryRepository.AddProductCategory(productCategoryData);
        }

        /// <summary>
        /// Update Product Category
        /// </summary>
        /// <param name="productCategory"></param>
        /// <returns></returns>
        public bool UpdateProductCategory(AddUpdateProductCategoryModel productCategory)
        {
            ProductCategory productCategoryData = new ProductCategory();
            productCategoryData.Id = productCategory.Id;
            productCategoryData.ProductCategoryGroupId = productCategory.ProductCategoryGroupId;
            productCategoryData.Name = productCategory.Name;
            productCategoryData.ImageURL = productCategory.ImageURL;
            productCategoryData.Description = productCategory.Description;
            productCategoryData.ShowHide = productCategory.ShowHide;
            productCategoryData.Priority = productCategory.Priority;
            productCategoryData.MetaTag = productCategory.MetaTag;
            productCategoryData.Status = productCategory.Status;
            //productCategoryData.IsActive = productCategory.IsActive;
            //productCategoryData.IsDeleted = productCategory.IsDeleted;
            //productCategoryData.CreatedOn = productCategory.CreatedOn;
            //productCategoryData.CreatedBy = productCategory.CreatedBy;
            productCategoryData.ModifiedOn = productCategory.ModifiedOn;
            productCategoryData.ModifiedBy = productCategory.ModifiedBy;
            return _productCategoryRepository.UpdateProductCategory(productCategoryData);
        }

        /// <summary>
        /// Delete Product Category
        /// </summary>
        /// <param name="productCategory"></param>
        /// <returns></returns>
        public bool DeleteProductCategory(ProductCategoryModel productCategory)
        {
            return _productCategoryRepository.DeleteProductCategory(productCategory.Id, productCategory.ModifiedBy);
        }

        /// <summary>
        /// Update Status Product Category
        /// </summary>
        /// <param name="productCategory"></param>
        /// <returns></returns>
        public bool UpdateStatusProductCategory(ProductCategoryModel productCategory)
        {
            return _productCategoryRepository.UpdateStatusProductCategory(productCategory.Id, productCategory.IsActive, productCategory.ModifiedBy);
        }

        #region Private Members

        #endregion
    }
}