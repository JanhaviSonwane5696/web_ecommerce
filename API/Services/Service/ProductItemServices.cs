﻿using Helpers;
using Repository.Interface;
using Repository.Model;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services.Service
{
    public class ProductItemServices : IProductItemServices
    {
        private readonly IProductItemRepository _productItemRepository;

        public ProductItemServices(IProductItemRepository productItemRepository, ILoggerService loggerService)
        {
            _productItemRepository = productItemRepository;
        }

        /// <summary>
        /// Get ProductItem
        /// </summary>
        /// <returns></returns>
        public List<ProductItemModel> GetProductItems(int productId)
        {
            List<ProductItem> productItem = _productItemRepository.GetProductItems(productId);
            List<ProductItemModel> productItemData = productItem.Select(x => new ProductItemModel
            {
                Id = x.Id,
                Name = x.Name,
                ProductId = x.ProductId,
                Price = x.Price,
                DistributorPrice = x.DistributorPrice,
                RetailPrice = x.RetailPrice,
                SalePrice = x.SalePrice,
                Weight = x.Weight,
                Quantity = x.Quantity,
                MaxQuantityForOrder = x.MaxQuantityForOrder,
                Description = x.Description,
                ShortDescription = x.ShortDescription,
                Specification = x.Specification,
                LegalDisclaimer = x.LegalDisclaimer,
                RelatedProducts = x.RelatedProducts,
                CompareProducts = x.CompareProducts,
                SponsoredProducts = x.SponsoredProducts,
                ImageURL1 = x.ImageURL1,
                ImageThumbURL1 = x.ImageThumbURL1,
                ImageURL2 = x.ImageURL2,
                ImageThumbURL2 = x.ImageThumbURL2,
                ImageURL3 = x.ImageURL3,
                ImageThumbURL3 = x.ImageThumbURL3,
                ImageURL4 = x.ImageURL4,
                ImageThumbURL4 = x.ImageThumbURL4,
                ImageURL5 = x.ImageURL5,
                ImageThumbURL5 = x.ImageThumbURL5,
                ImageURL6 = x.ImageURL6,
                ImageThumbURL6 = x.ImageThumbURL6,
                ImageURL7 = x.ImageURL7,
                ImageThumbURL7 = x.ImageThumbURL7,
                IsDefault = x.IsDefault,
                IsActive = x.IsActive,
                SKU = x.SKU,
                Brand = x.Brand,
                DeliveryCharge = x.DeliveryCharge,
                MetaTag = x.MetaTag,
                MinimumQuantity = x.MinimumQuantity,
                ProductAttribute = x.ProductAttribute,
                RemainingQuantity = x.RemainingQuantity,
                Sale = x.Sale,
                Tax = x.Tax,
                CountryOfOrigin = x.CountryOfOrigin
            }).ToList();

            return productItemData;
        }

        /// <summary>
        /// Get ProductItem
        /// </summary>
        /// <returns></returns>
        public List<ProductItemsModel> GetProductItem(int productId)
        {
            List<ProductItemsModel> productItem = _productItemRepository.GetProductItem(productId);
            return productItem;
        }

        public List<ProductItemsModel> GetProductItemsByProductId(int productId)
        {
            List<ProductItemsModel> productItem = _productItemRepository.GetProductItems(productId, Variables.ProductStatus.Approved);
            return productItem;
        }

        /// <summary>
        /// Get ProductItem
        /// </summary>
        /// <returns></returns>
        public List<ProductItemModel> GetPreApprovalProductItem(int userId, int productId)
        {
            List<ProductItemModel> productItem = _productItemRepository.GetProductItemsByStatus(userId, productId, Variables.ProductStatus.Accepted);
            return productItem;
        }

        public List<ProductItemAttributesModel> GetProductItemAttributeList(int productId)
        {
            List<ProductItemAttributesModel> productItem = _productItemRepository.ProductItemAttributeList(productId);
            return productItem;
        }

        public List<ShortProductItemModel> GetProductItemShortList(int productId)
        {
            List<ShortProductItemModel> productItem = _productItemRepository.ProductItemShortList(productId);
            return productItem;
        }

        /// <summary>
        /// Add ProductItem
        /// </summary>
        /// <param name="productItem"></param>
        /// <returns></returns>
        public bool AddProductItem(AddProductItemModel productItem, string createdBy)
        {
            ProductItem productItemData = new ProductItem()
            {
                ProductId = productItem.ProductId,
                Name = productItem.Name,
                Price = productItem.Price,
                DistributorPrice = productItem.DistributorPrice,
                RetailPrice = productItem.RetailPrice,
                SalePrice = productItem.SalePrice,
                Weight = productItem.Weight,
                Quantity = productItem.Quantity,
                MaxQuantityForOrder = productItem.MaxQuantityForOrder,
                Description = productItem.Description,
                ShortDescription = productItem.ShortDescription,
                Specification = productItem.Specification,
                LegalDisclaimer = productItem.LegalDisclaimer,
                RelatedProducts = productItem.RelatedProducts,
                CompareProducts = productItem.CompareProducts,
                SponsoredProducts = productItem.SponsoredProducts,
                ImageURL1 = productItem.ImageURL1,
                ImageThumbURL1 = productItem.ImageThumbURL1,
                ImageURL2 = productItem.ImageURL2,
                ImageThumbURL2 = productItem.ImageThumbURL2,
                ImageURL3 = productItem.ImageURL3,
                ImageThumbURL3 = productItem.ImageThumbURL3,
                ImageURL4 = productItem.ImageURL4,
                ImageThumbURL4 = productItem.ImageThumbURL4,
                ImageURL5 = productItem.ImageURL5,
                ImageThumbURL5 = productItem.ImageThumbURL5,
                ImageURL6 = productItem.ImageURL6,
                ImageThumbURL6 = productItem.ImageThumbURL6,
                ImageURL7 = productItem.ImageURL7,
                ImageThumbURL7 = productItem.ImageThumbURL7,
                IsDefault = productItem.IsDefault,
                IsActive = productItem.IsActive,
                IsDeleted = false,
                CreatedOn = DateTime.Now,
                CreatedBy = createdBy,
                SKU = productItem.SKU,
                MetaTag = productItem.MetaTag,
                Sale = productItem.Sale,
                MinimumQuantity = productItem.MinimumQuantity,
                ProductAttribute = productItem.ProductAttribute,
                Brand = productItem.Brand,
                Tax = productItem.Tax,
                DeliveryCharge = productItem.DeliveryCharge,
                RemainingQuantity = productItem.RemainingQuantity,
                CountryOfOrigin = productItem.CountryOfOrigin,
                Status = productItem.UserID == 1 ? Variables.ProductStatus.Approved : Variables.ProductStatus.Accepted,
                DeliveryStart = 1,
                DeliveryEnd = 7
            };
            return _productItemRepository.AddProductItem(productItemData);
        }

        /// <summary>
        /// Update ProductItem
        /// </summary>
        /// <param name="productItem"></param>
        /// <returns></returns>
        public bool UpdateProductItem(AddProductItemModel productItem, string modifiedBy)
        {
            ProductItem productItemData = new ProductItem();
            productItemData.Id = productItem.Id;
            productItemData.ProductId = productItem.ProductId;
            productItemData.Name = productItem.Name;
            productItemData.Price = productItem.Price;
            productItemData.DistributorPrice = productItem.DistributorPrice;
            productItemData.RetailPrice = productItem.RetailPrice;
            productItemData.SalePrice = productItem.SalePrice;
            productItemData.Weight = productItem.Weight;
            productItemData.Quantity = productItem.Quantity;
            productItemData.MaxQuantityForOrder = productItem.MaxQuantityForOrder;
            productItemData.Description = productItem.Description;
            productItemData.ShortDescription = productItem.ShortDescription;
            productItemData.Specification = productItem.Specification;
            productItemData.LegalDisclaimer = productItem.LegalDisclaimer;
            productItemData.RelatedProducts = productItem.RelatedProducts;
            productItemData.CompareProducts = productItem.CompareProducts;
            productItemData.SponsoredProducts = productItem.SponsoredProducts;
            productItemData.ImageURL1 = productItem.ImageURL1;
            productItemData.ImageThumbURL1 = productItem.ImageThumbURL1;
            productItemData.ImageURL2 = productItem.ImageURL2;
            productItemData.ImageThumbURL2 = productItem.ImageThumbURL2;
            productItemData.ImageURL3 = productItem.ImageURL3;
            productItemData.ImageThumbURL3 = productItem.ImageThumbURL3;
            productItemData.ImageURL4 = productItem.ImageURL4;
            productItemData.ImageThumbURL4 = productItem.ImageThumbURL4;
            productItemData.ImageURL5 = productItem.ImageURL5;
            productItemData.ImageThumbURL5 = productItem.ImageThumbURL5;
            productItemData.ImageURL6 = productItem.ImageURL6;
            productItemData.ImageThumbURL6 = productItem.ImageThumbURL6;
            productItemData.ImageURL7 = productItem.ImageURL7;
            productItemData.ImageThumbURL7 = productItem.ImageThumbURL7;
            productItemData.IsDefault = productItem.IsDefault;
            productItemData.IsActive = productItem.IsActive;
            productItemData.ModifiedOn = DateTime.Now;
            productItemData.ModifiedBy = modifiedBy;
            productItemData.SKU = productItem.SKU;
            productItemData.MetaTag = productItem.MetaTag;
            productItemData.Sale = productItem.Sale;
            productItemData.MinimumQuantity = productItem.MinimumQuantity;
            productItemData.ProductAttribute = productItem.ProductAttribute;
            productItemData.Brand = productItem.Brand;
            productItemData.Tax = productItem.Tax;
            productItemData.DeliveryCharge = productItem.DeliveryCharge;
            productItemData.RemainingQuantity = productItem.RemainingQuantity;
            productItemData.CountryOfOrigin = productItem.CountryOfOrigin;
            return _productItemRepository.UpdateProductItem(productItemData);
        }

        /// <summary>
        /// Delete ProductItem
        /// </summary>
        /// <param name="productItem"></param>
        /// <returns></returns>
        public bool DeleteProductItem(ProductItemModel productItem, string modifiedBy)
        {
            return _productItemRepository.DeleteProductItem(productItem.Id, modifiedBy);
        }

        /// <summary>
        /// Update Status ProductItem
        /// </summary>
        /// <param name="productItem"></param>
        /// <returns></returns>
        public bool UpdateStatusProductItem(ProductItemModel productItem, string modifiedBy)
        {
            return _productItemRepository.UpdateStatusProductItem(productItem.Id, productItem.IsActive, modifiedBy);
        }

        /// <summary>
        /// Update Status ProductItem
        /// </summary>
        /// <param name="productItem"></param>
        /// <returns></returns>
        public bool UpdateProductItemStatus(UpdateProductItemModel productItem)
        {
            return _productItemRepository.UpdateStatusProductItem(productItem.ProductItemId, productItem.Status, productItem.Remarks, productItem.ModifiedBy);
        }

        #region Private Members

        #endregion
    }
}