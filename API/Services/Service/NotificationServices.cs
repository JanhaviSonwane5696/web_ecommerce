﻿using Models;
using Repository.Interface;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services.Service
{
    public class NotificationServices : INotificationServices
    {
        #region Private Variable
        private readonly INotificationRepository _notifyRepository;
        private readonly IUserRepository _userRepository;
        private readonly INotificationHelpers _notificationRepository;
        #endregion

        public NotificationServices(INotificationRepository notifyRepository, IUserRepository userRepository, INotificationHelpers notificationRepository)
        {
            _notifyRepository = notifyRepository;
            _userRepository = userRepository;
            _notificationRepository = notificationRepository;
        }

        public List<MasterSMSApiListModel> SMSAPIList()
        {
            List<MasterSMSApiListModel> result = _notifyRepository.SMSAPIList().Select(x => new MasterSMSApiListModel
            {
                Id = x.Id,
                APIName = x.APIName,
                Username = x.Username,
                Password = x.Password,
                Status = x.Status,
                SenderId = x.SenderId,
                CreatedOn = x.CreatedOn,
                CreatedBy = x.CreatedBy,
                ModifiedOn = x.ModifiedOn,
                ModifiedBy = x.ModifiedBy
            }).ToList();
            return result;
        }
        public Tuple<bool, int, string> AddSMSAPI(AddNotificationSMSAPIModel addSMSDetails, string createdBy)
        {
            var result = _notifyRepository.AddSMSAPI(addSMSDetails, createdBy);
            return result;
        }
        public Tuple<bool, int, string> UpdateSMSAPI(UpdateNotificationSMSAPIModel addSMSDetails, string createdBy)
        {
            var result = _notifyRepository.UpdateSMSAPI(addSMSDetails, createdBy);
            return result;
        }
        public Tuple<bool, int, string> UpdateSMSAPIStatus(UpdateNotificationSMSAPIStatusModel addSMSDetails, string createdBy)
        {
            var result = _notifyRepository.UpdateSMSAPIStatus(addSMSDetails, createdBy);
            return result;
        }



        public List<MasterSMSTemplatesListModel> SMSTemplateList()
        {
            List<MasterSMSTemplatesListModel> result = _notifyRepository.SMSAPITemplateList().Select(x => new MasterSMSTemplatesListModel
            {
                Id = x.Id,
                Type = x.Type,
                SMSContent = x.SMSContent,
                IsActive = x.IsActive,
                IsDeleted = x.IsDeleted,
                CreatedOn = x.CreatedOn,
                CreatedBy = x.CreatedBy,
                ModifiedOn = x.ModifiedOn,
                ModifiedBy = x.ModifiedBy
            }).ToList();
            return result;
        }
        public Tuple<bool, int, string> AddSMSTemplate(AddNotificationSMSModel addSMSDetails, string createdBy)
        {
            var result = _notifyRepository.AddSMS(addSMSDetails, createdBy);
            return result;
        }
        public Tuple<bool, int, string> UpdateSMSTemplate(UpdateNotificationSMSModel addSMSDetails, string createdBy)
        {
            var result = _notifyRepository.UpdateSMS(addSMSDetails, createdBy);
            return result;
        }
        public Tuple<bool, int, string> UpdateSMSTemplateStatus(UpdateStatusNotificationSMSModel addSMSDetails, string createdBy)
        {
            var result = _notifyRepository.UpdateSMSStatus(addSMSDetails, createdBy);
            return result;
        }
        public Tuple<bool, int, string> DeleteSMSTemplate(DeleteNotificationSMSModel addSMSDetails, string createdBy)
        {
            var result = _notifyRepository.DeleteSMS(addSMSDetails, createdBy);
            return result;
        }


        public List<MasterEmailSettingsListModel> EmailAPIList()
        {
            List<MasterEmailSettingsListModel> result = _notifyRepository.EmailAPIList().Select(x => new MasterEmailSettingsListModel
            {
                Id = x.Id,
                HostName = x.HostName,
                FromEmailAddress = x.FromEmailAddress,
                CC = x.CC,
                BCC = x.BCC,
                Port = x.Port,
                Password = x.Password,
                IsMailActive = x.IsMailActive,
                FromName = x.FromName,
                MailSSL = x.MailSSL,
                CreatedOn = x.CreatedOn,
                CreatedBy = x.CreatedBy,
                ModifiedOn = x.ModifiedOn,
                ModifiedBy = x.ModifiedBy
            }).ToList();
            return result;
        }
        public Tuple<bool, int, string> AddEmailAPI(AddNotificationEmailAPIModel addSMSDetails, string createdBy)
        {
            var result = _notifyRepository.AddEmailAPI(addSMSDetails, createdBy);
            return result;
        }
        public Tuple<bool, int, string> UpdateEmailAPI(UpdateNotificationEmailAPIModel addSMSDetails, string createdBy)
        {
            var result = _notifyRepository.UpdateEmailAPI(addSMSDetails, createdBy);
            return result;
        }
        public Tuple<bool, int, string> UpdateEmailAPIStatus(UpdateNotificationEmailAPIStatusModel addSMSDetails, string createdBy)
        {
            var result = _notifyRepository.UpdateEmailAPIStatus(addSMSDetails, createdBy);
            return result;
        }


        public List<MasterEmailTemplatesListModel> EmailTemplateList()
        {
            List<MasterEmailTemplatesListModel> result = _notifyRepository.EmailAPITemplateList().Select(x => new MasterEmailTemplatesListModel
            {
                Id = x.Id,
                Type = x.Type,
                Subject = x.Subject,
                Body = x.Body,
                IsActive = x.IsActive,
                CreatedOn = x.CreatedOn,
                CreatedBy = x.CreatedBy,
                ModifiedOn = x.ModifiedOn,
                ModifiedBy = x.ModifiedBy
            }).ToList();
            return result;
        }
        public Tuple<bool, int, string> AddEmailTemplate(AddNotificationEmailModel addEmailDetails, string createdBy)
        {
            var result = _notifyRepository.AddEmail(addEmailDetails, createdBy);
            return result;
        }
        public Tuple<bool, int, string> UpdateEmailTemplate(UpdateNotificationEmailModel emailDetails, string createdBy)
        {
            var result = _notifyRepository.UpdateEmail(emailDetails, createdBy);
            return result;
        }
        public Tuple<bool, int, string> UpdateEmailTemplateStatus(UpdateStatusNotificationEmailModel emailDetails, string createdBy)
        {
            var result = _notifyRepository.UpdateEmailStatus(emailDetails, createdBy);
            return result;
        }


        #region Pramotional SMS
        public Tuple<bool, string> PramotionalSMSV1(PramotionalSMSV1Model model)
        {
            var users = _userRepository.UserByUserType(model.UserType);
            if (users.Count > 0)
            {
                users = users.Where(x => x.PhoneNumber != null).ToList();
                foreach (Repository.Model.Users user in users)
                {
                    _notificationRepository.SendSMS(user.PhoneNumber, model.Content);
                }
                return new Tuple<bool, string>(true, "SMS sent successfully.");
            }
            else
                return new Tuple<bool, string>(false, "Users not found.");
        }
        public Tuple<bool, string> PramotionalSMSV2(PramotionalSMSV2Model model)
        {
            string[] mobileNos = model.MobileNumbers.Split(',');
            foreach (string mobileNo in mobileNos)
            {
                _notificationRepository.SendSMS(mobileNo, model.Content);
            }
            return new Tuple<bool, string>(true, "SMS sent successfully.");
        }
        #endregion

        #region Pramotional Email
        public Tuple<bool, string> PramotionalEmailV1(PramotionalEmailV1Model model)
        {
            var users = _userRepository.UserByUserType(model.UserType);
            if (users.Count > 0)
            {
                users = users.Where(x => x.Email != null).ToList();
                foreach (Repository.Model.Users user in users)
                {
                    _notificationRepository.SendEmail(model.Body, model.Subject, user.Email);
                }
                return new Tuple<bool, string>(true, "Email sent successfully.");
            }
            else
                return new Tuple<bool, string>(false, "Users not found.");
        }
        public Tuple<bool, string> PramotionalEmailV2(PramotionalEmailV2Model model)
        {
            string[] emailIds = model.EmailIds.Split(',');
            foreach (string email in emailIds)
            {
                _notificationRepository.SendEmail(model.Body, model.Subject, email);
            }
            return new Tuple<bool, string>(true, "Email sent successfully.");
        }
        #endregion

        #region Push Notification
        public Tuple<bool, string, string> PushNotifications(PushNotificationModel model)
        {
            var users = _userRepository.UserByUserType(model.UserType);
            if (users.Count > 0)
            {
                string result = "";
                users = users.Where(x => x.DeviceID != null).ToList();
                foreach (Repository.Model.Users user in users)
                {
                    result = _notificationRepository.PushNotification(user.DeviceID, model);
                }
                return new Tuple<bool, string, string>(true, "Notification sent successfully.", result);
            }
            else
                return new Tuple<bool, string, string>(false, "Users not found.", "");
        }
        #endregion
    }
}
