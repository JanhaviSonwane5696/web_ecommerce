﻿using Repository.Interface;
using Repository.Model;
using Services.Interface;
using System.Collections.Generic;
using System.Linq;

namespace Services.Service
{
    public class ProductRelatedServices : IProductRelatedServices
    {
        private readonly IProductRelatedRepository _productCategoryGroupRepository;

        public ProductRelatedServices(IProductRelatedRepository productRelatedRepository, ILoggerService loggerService)
        {
            _productCategoryGroupRepository = productRelatedRepository;
        }

        /// <summary>
        /// Get Product Category
        /// </summary>
        /// <returns></returns>
        public List<ProductRelatedModel> GetProductRelated()
        {
            List<ProductRelated> productRelated = _productCategoryGroupRepository.GetProductRelated();
            List<ProductRelatedModel> productCategoryData = productRelated.Select(x => new ProductRelatedModel
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                IsActive = x.IsActive,
                CreatedOn = x.CreatedOn,
                CreatedBy = x.CreatedBy,
                ModifiedOn = x.ModifiedOn,
                ModifiedBy = x.ModifiedBy,
                Status = x.Status
            }).ToList();

            return productCategoryData;
        }

        /// <summary>
        /// Add Product Category
        /// </summary>
        /// <param name="productRelated"></param>
        /// <returns></returns>
        public bool AddProductRelated(AddUpdateProductRelatedModel productRelated)
        {
            ProductRelated productCategoryData = new ProductRelated();
            productCategoryData.Id = productRelated.Id;
            productCategoryData.Name = productRelated.Name;
            productCategoryData.Description = productRelated.Description;
            productCategoryData.IsActive = productRelated.IsActive;
            productCategoryData.IsDeleted = false;
            productCategoryData.CreatedOn = productRelated.CreatedOn;
            productCategoryData.CreatedBy = productRelated.CreatedBy;
            productCategoryData.Status = productRelated.Status;
            return _productCategoryGroupRepository.AddProductRelated(productCategoryData);
        }

        /// <summary>
        /// Update Product Category
        /// </summary>
        /// <param name="productRelated"></param>
        /// <returns></returns>
        public bool UpdateProductRelated(AddUpdateProductRelatedModel productRelated)
        {
            ProductRelated productCategoryData = new ProductRelated();
            productCategoryData.Id = productRelated.Id;
            productCategoryData.Name = productRelated.Name;
            productCategoryData.Description = productRelated.Description;
            productCategoryData.Status = productRelated.Status;
            productCategoryData.ModifiedOn = productRelated.ModifiedOn;
            productCategoryData.ModifiedBy = productRelated.ModifiedBy;
            return _productCategoryGroupRepository.UpdateProductRelated(productCategoryData);
        }

        /// <summary>
        /// Delete Product Category
        /// </summary>
        /// <param name="productRelated"></param>
        /// <returns></returns>
        public bool DeleteProductRelated(ProductRelatedModel productRelated)
        {
            return _productCategoryGroupRepository.DeleteProductRelated(productRelated.Id, productRelated.ModifiedBy);
        }

        /// <summary>
        /// Update Status Product Category
        /// </summary>
        /// <param name="productRelated"></param>
        /// <returns></returns>
        public bool UpdateStatusProductRelated(ProductRelatedModel productRelated)
        {
            return _productCategoryGroupRepository.UpdateStatusProductRelated(productRelated.Id, productRelated.IsActive, productRelated.ModifiedBy);
        }

        #region Private Members

        #endregion
    }
}