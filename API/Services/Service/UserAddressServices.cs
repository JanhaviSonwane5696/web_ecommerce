﻿using Models;
using Repository.Interface;
using Repository.Model;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Service
{
    public class UserAddressServices : IUserAddressServices
    {
        private readonly IUserAddressRepository _userAddressRepository;

        public UserAddressServices(IUserAddressRepository userAddressRepository, ILoggerService loggerService)
        {
            _userAddressRepository = userAddressRepository;
        }

        /// <summary>
        /// Get MasterAttribute
        /// </summary>
        /// <returns></returns>
        public List<UserAddressModel> GetUserAddress(int userId)
        {
            List<UserAddress> address = _userAddressRepository.GetUserAddress(userId);
            List<UserAddressModel> userAddress = address.Select(x => new UserAddressModel
            {
                Id = x.Id,
                AddressType = x.AddressType,
                IsPrimary = x.IsPrimary,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Company = x.Company,
                AddressLine1 = x.AddressLine1,
                AddressLine2 = x.AddressLine2,
                Landmark = x.Landmark,
                ZipPostal = x.ZipPostal,
                Phone = x.Phone
            }).ToList();

            return userAddress;
        }

        public Tuple<bool, string> AddUserAddress(AddUserAddressModel model)
        {
            UserAddress _add = new UserAddress()
            {
                UserId = model.UserId,
                AddressType = model.AddressType,
                IsPrimary = model.IsPrimary,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Company = model.Company,
                AddressLine1 = model.AddressLine1,
                AddressLine2 = model.AddressLine2,
                Landmark = model.Landmark,
                Phone = model.Phone,
                ZipPostal = model.ZipPostal,
                IsDeleted = model.IsDeleted,
                CreatedBy = model.CreatedBy,
                CreatedOn = DateTime.Now
            };
            return _userAddressRepository.AddUserAddress(_add);
        }

        public Tuple<bool, string> UpdateUserAddress(UpdateUserAddressModel model)
        {
            UserAddress _add = new UserAddress()
            {
                Id = model.Id,
                UserId = model.UserId,
                AddressType = model.AddressType,
                IsPrimary = model.IsPrimary,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Company = model.Company,
                AddressLine1 = model.AddressLine1,
                AddressLine2 = model.AddressLine2,
                Landmark = model.Landmark,
                Phone = model.Phone,
                ZipPostal = model.ZipPostal,
                ModifiedBy = model.ModifiedBy,
                ModifiedOn = DateTime.Now
            };
            return _userAddressRepository.Update(_add);
        }

        public Tuple<bool, string> DeleteUserAddress(DeleteUserAddressModel model)
        {
            return _userAddressRepository.DeleteUserAddress(model.Id, model.UserId, model.ModifiedBy);
        }

    }
}
