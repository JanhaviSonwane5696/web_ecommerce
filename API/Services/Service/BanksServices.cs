﻿using Models;
using Repository.Interface;
using Repository.Model;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Service
{
   public class BanksServices : IBanksServices
    {
        private readonly IAllBankNameRepository _bankNameRepository;

        public BanksServices(IAllBankNameRepository bankNameRepository, ILoggerService loggerService)
        {
            _bankNameRepository = bankNameRepository;
        }

        public List<string> BankNameList()
        {
            var bankList=_bankNameRepository.BankNameList();
            return bankList;
        }

        public List<string> BankStateList(BankStateModel bankdetails)
        {
            var bankList = _bankNameRepository.BankStateNameList(bankdetails);
            return bankList;
        }

        public List<string> BankDistrictList(BankDistrictModel bankdetails)
        {
            var bankList = _bankNameRepository.BankDistrictNameList(bankdetails);
            return bankList;
        }

        public List<string> BankBranchList(BankBranchModel bankdetails)
        {
            var bankList = _bankNameRepository.BankBranchNameList(bankdetails);
            return bankList;
        }

        public AllBankName BankDetails(BankBranchDetailsModel bankdetails)
        {
            var bankList = _bankNameRepository.BankBranchDetails(bankdetails);
            return bankList;
        }

        public AllBankName BankIfscDetails(BankDetailsModel bankdetails)
        {
            var bankList = _bankNameRepository.IFSCDetails(bankdetails);
            return bankList;
        }
    }
}
