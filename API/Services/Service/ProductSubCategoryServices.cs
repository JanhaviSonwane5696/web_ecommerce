﻿using Models;
using Repository.Interface;
using Repository.Model;
using Services.Interface;
using System.Collections.Generic;
using System.Linq;

namespace Services.Service
{
    public class ProductSubCategoryServices : IProductSubCategoryServices
    {
        private readonly IProductSubCategoryRepository _productSubCategoryRepository;

        public ProductSubCategoryServices(IProductSubCategoryRepository productSubCategoryRepository, ILoggerService loggerService)
        {
            _productSubCategoryRepository = productSubCategoryRepository;
        }

        /// <summary>
        /// Get Product Category
        /// </summary>
        /// <returns></returns>
        public List<ProductSubCategoryModel> GetProductSubCategory()
        {
            List<ProductSubCategory> ProductSubCategory = _productSubCategoryRepository.GetProductSubCategory();
            List<ProductSubCategoryModel> ProductSubCategoryData = ProductSubCategory.Select(x => new ProductSubCategoryModel
            {
                Id = x.Id,
                ProductCategoryId = x.ProductCategoryId,
                Name = x.Name,
                Description = x.Description,
                ImageURL = x.ImageURL,
                IsActive = x.IsActive,
                CreatedOn = x.CreatedOn,
                CreatedBy = x.CreatedBy,
                ModifiedOn = x.ModifiedOn,
                ModifiedBy = x.ModifiedBy,
                ShowHide = x.ShowHide,
                Priority = x.Priority,
                MetaTag = x.MetaTag,
                Status = x.Status
            }).ToList();

            return ProductSubCategoryData;
        }

        public List<ProductSubCategoryModel> GetAllProductSubCategory()
        {
            List<ProductSubCategoryModel> ProductSubCategory = _productSubCategoryRepository.GetAllProductSubCategory();
            return ProductSubCategory;
        }

        public List<ProductSubCategoryModel> GetProductSubCategoryByCategoryId(int productCategoryId)
        {
            List<ProductSubCategory> ProductSubCategory = _productSubCategoryRepository.GetProductSubCategoryByCategoryId(productCategoryId);
            List<ProductSubCategoryModel> ProductSubCategoryData = ProductSubCategory.Select(x => new ProductSubCategoryModel
            {
                Id = x.Id,
                ProductCategoryId = x.ProductCategoryId,
                Name = x.Name,
                Description = x.Description,
                ImageURL = x.ImageURL,
                IsActive = x.IsActive,
                CreatedOn = x.CreatedOn,
                CreatedBy = x.CreatedBy,
                ModifiedOn = x.ModifiedOn,
                ModifiedBy = x.ModifiedBy,
                ShowHide = x.ShowHide,
                Priority = x.Priority,
                MetaTag = x.MetaTag,
                Status = x.Status
            }).ToList();

            return ProductSubCategoryData;
        }

        /// <summary>
        /// Add Product Category
        /// </summary>
        /// <param name="ProductSubCategory"></param>
        /// <returns></returns>
        public bool AddProductSubCategory(AddUpdateProductSubCategoryModel productSubCategory)
        {
            ProductSubCategory productSubCategoryData = new ProductSubCategory()
            {
                Id = productSubCategory.Id,
                ProductCategoryId = productSubCategory.ProductCategoryId,
                Name = productSubCategory.Name,
                Description = productSubCategory.Description,
                IsActive = productSubCategory.IsActive,
                ImageURL = productSubCategory.ImageURL,
                IsDeleted = false,
                CreatedOn = productSubCategory.CreatedOn,
                CreatedBy = productSubCategory.CreatedBy,
                ShowHide = productSubCategory.ShowHide,
                Priority = productSubCategory.Priority,
                MetaTag = productSubCategory.MetaTag,
                Status = productSubCategory.Status,
                ModifiedOn = productSubCategory.ModifiedOn,
                ModifiedBy = productSubCategory.ModifiedBy
            };
            return _productSubCategoryRepository.AddProductSubCategory(productSubCategoryData);
        }

        /// <summary>
        /// Update Product Category
        /// </summary>
        /// <param name="ProductSubCategory"></param>
        /// <returns></returns>
        public bool UpdateProductSubCategory(AddUpdateProductSubCategoryModel productSubCategory)
        {
            ProductSubCategory productSubCategoryData = new ProductSubCategory();
            productSubCategoryData.Id = productSubCategory.Id;
            productSubCategoryData.ProductCategoryId = productSubCategory.ProductCategoryId;
            productSubCategoryData.Name = productSubCategory.Name;
            productSubCategoryData.ImageURL = productSubCategory.ImageURL;
            productSubCategoryData.Description = productSubCategory.Description;
            productSubCategoryData.ModifiedOn = productSubCategory.ModifiedOn;
            productSubCategoryData.ModifiedBy = productSubCategory.ModifiedBy;
            productSubCategoryData.ShowHide = productSubCategory.ShowHide;
            productSubCategoryData.Priority = productSubCategory.Priority;
            productSubCategoryData.MetaTag = productSubCategory.MetaTag;
            productSubCategoryData.Status = productSubCategory.Status;
            return _productSubCategoryRepository.UpdateProductSubCategory(productSubCategoryData);
        }

        /// <summary>
        /// Delete Product Category
        /// </summary>
        /// <param name="ProductSubCategory"></param>
        /// <returns></returns>
        public bool DeleteProductSubCategory(ProductSubCategoryModel productSubCategory)
        {
            return _productSubCategoryRepository.DeleteProductSubCategory(productSubCategory.Id, productSubCategory.ModifiedBy);
        }

        /// <summary>
        /// Update Status Product Category
        /// </summary>
        /// <param name="productSubCategory"></param>
        /// <returns></returns>
        public bool UpdateStatusProductSubCategory(ProductSubCategoryModel productSubCategory)
        {
            return _productSubCategoryRepository.UpdateStatusProductSubCategory(productSubCategory.Id, productSubCategory.IsActive, productSubCategory.ModifiedBy);
        }

        #region Private Members

        #endregion
    }
}