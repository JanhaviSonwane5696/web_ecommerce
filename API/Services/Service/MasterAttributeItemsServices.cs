﻿using Models;
using Repository.Interface;
using Repository.Model;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services.Service
{
    public class MasterAttributeItemsServices : IMasterAttributeItemsServices
    {
        private readonly IMasterAttributeItemsRepository _attributeItemRepository;

        public MasterAttributeItemsServices(IMasterAttributeItemsRepository attributeItemRepository, ILoggerService loggerService)
        {
            _attributeItemRepository = attributeItemRepository;
        }

        /// <summary>
        /// Get MasterAttributeItems
        /// </summary>
        /// <returns></returns>
        public List<MasterAttributeItemsModel> GetMasterAttributeItems(int attributeId)
        {
            List<MasterAttributeItems> attributeItem = _attributeItemRepository.GetMasterAttributeItems(attributeId);
            List<MasterAttributeItemsModel> attributeItemData = attributeItem.Select(x => new MasterAttributeItemsModel
            {
                Id = x.Id,
                AttributeItem = x.AttributeItem,
                AttributeId = x.AttributeId,
                AttributeItemDesciption = x.AttributeItemDesciption,
                CreatedBy = x.CreatedBy,
                CreatedOn = x.CreatedOn,
                ModifiedBy = x.ModifiedBy,
                ModifiedOn = x.ModifiedOn,
                IsActive = x.IsActive
            }).ToList();

            return attributeItemData;
        }

        public MasterAttributeItemsModel GetMasterAttributeItemById(int attributeItemId)
        {
            List<MasterAttributeItems> attributeItem = _attributeItemRepository.GetMasterAttributeItemsById(attributeItemId);
            MasterAttributeItemsModel attributeItemData = attributeItem.Select(x => new MasterAttributeItemsModel
            {
                Id = x.Id,
                AttributeItem = x.AttributeItem,
                AttributeId = x.AttributeId,
                AttributeItemDesciption = x.AttributeItemDesciption,
                CreatedBy = x.CreatedBy,
                CreatedOn = x.CreatedOn,
                ModifiedBy = x.ModifiedBy,
                ModifiedOn = x.ModifiedOn,
                IsActive = x.IsActive
            }).FirstOrDefault();

            return attributeItemData;
        }

        /// <summary>
        /// Get MasterAttributeItems
        /// </summary>
        /// <returns></returns>
        public List<MasterAttributeItemsModel> GetMasterAllAttributeItems()
        {
            List<MasterAttributeItems> attributeItem = _attributeItemRepository.GetMasterAllAttributeItems();
            List<MasterAttributeItemsModel> attributeItemData = attributeItem.Select(x => new MasterAttributeItemsModel
            {
                Id = x.Id,
                AttributeItem = x.AttributeItem,
                AttributeId = x.AttributeId,
                AttributeItemDesciption = x.AttributeItemDesciption,
                CreatedBy = x.CreatedBy,
                CreatedOn = x.CreatedOn,
                ModifiedBy = x.ModifiedBy,
                ModifiedOn = x.ModifiedOn,
                IsActive = x.IsActive
            }).ToList();

            return attributeItemData;
        }

        /// <summary>
        /// Add MasterAttributeItems
        /// </summary>
        /// <param name="attributeItem"></param>
        /// <returns></returns>
        public bool AddMasterAttributeItems(MasterAttributeItemsModel attributeItem)
        {
            MasterAttributeItems attributeItemData = new MasterAttributeItems();
            attributeItemData.AttributeItem = attributeItem.AttributeItem;
            attributeItemData.AttributeId = attributeItem.AttributeId;
            attributeItemData.AttributeItemDesciption = attributeItem.AttributeItemDesciption;
            attributeItemData.IsActive = attributeItem.IsActive;
            attributeItemData.IsDeleted = false;
            attributeItemData.CreatedOn = DateTime.Now;
            attributeItemData.CreatedBy = attributeItem.CreatedBy;
            return _attributeItemRepository.AddMasterAttributeItems(attributeItemData);
        }

        /// <summary>
        /// Update MasterAttributeItems
        /// </summary>
        /// <param name="attributeItem"></param>
        /// <returns></returns>
        public bool UpdateMasterAttributeItems(MasterAttributeItemsModel attributeItem)
        {
            MasterAttributeItems attributeItemData = new MasterAttributeItems();
            attributeItemData.Id = attributeItem.Id;
            attributeItemData.AttributeItem = attributeItem.AttributeItem;
            attributeItemData.AttributeId = attributeItem.AttributeId;
            attributeItemData.AttributeItemDesciption = attributeItem.AttributeItemDesciption;
            attributeItemData.ModifiedOn = DateTime.Now;
            attributeItemData.ModifiedBy = attributeItem.ModifiedBy;
            return _attributeItemRepository.UpdateMasterAttributeItems(attributeItemData);
        }

        /// <summary>
        /// Delete MasterAttributeItems
        /// </summary>
        /// <param name="attributeItem"></param>
        /// <returns></returns>
        public bool DeleteMasterAttributeItems(MasterAttributeItemsModel attributeItem)
        {
            return _attributeItemRepository.DeleteMasterAttributeItems(attributeItem.Id, attributeItem.ModifiedBy);
        }

        /// <summary>
        /// Update Status MasterAttributeItems
        /// </summary>
        /// <param name="attributeItem"></param>
        /// <returns></returns>
        public bool UpdateStatusMasterAttributeItems(MasterAttributeItemsModel attributeItem)
        {
            return _attributeItemRepository.UpdateStatusMasterAttributeItems(attributeItem.Id, attributeItem.IsActive, attributeItem.ModifiedBy);
        }

        #region Private Members

        #endregion
    }
}