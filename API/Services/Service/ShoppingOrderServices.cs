﻿using Helpers;
using Microsoft.AspNetCore.Http;
using Models;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using OfficeOpenXml.FormulaParsing.Utilities;
using Repository.Interface;
using Repository.Model;
using Services.Helper;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace Services.Service
{
    public class ShoppingOrderServices : IShoppingOrderServices
    {
        private readonly IUserRepository _userRepository;
        private readonly IShoppingOrderRepository _shoppingOrderRepository;
        private readonly IUserAddressRepository _userAddressRepository;
        private readonly IShoppingCartRepository _shoppingCartRepository;
        private readonly INotificationHelpers _notificationRepository;

        public ShoppingOrderServices(IUserRepository userRepository, IShoppingOrderRepository shoppingOrderRepository, INotificationHelpers notificationRepository, IUserAddressRepository userAddressRepository, IShoppingCartRepository shoppingCartRepository)
        {
            _userRepository = userRepository;
            _shoppingOrderRepository = shoppingOrderRepository;
            _notificationRepository = notificationRepository;
            _userAddressRepository = userAddressRepository;
            _shoppingCartRepository = shoppingCartRepository;
        }

        public ShoppingOrderResponseModel PlaceOrder(ShoppingOrderModel model)
        {
            ShoppingOrderResponseModel _response = new ShoppingOrderResponseModel();
            try
            {
                var user = _userRepository.GetUserById(model.UserId);
                if (user != null)
                {
                    var billingAddress = _userAddressRepository.GetUserAddress(user.Id, model.BillingAddress);
                    if (billingAddress != null)
                    {
                        var shippingAddress = _userAddressRepository.GetUserAddress(user.Id, model.ShippingAddress);
                        if (shippingAddress != null)
                        {
                            List<ShoppingCartItemsListModel> listCart = _shoppingCartRepository.CartItemsListViaUserId(user.Id);
                            if (listCart.Count == 0)
                            {
                                _response.StatusCode = Variables.APIStatus.Failed;
                                _response.Message = "Please add product in cart.";
                                return _response;
                            }
                            else
                            {
                                decimal totalItemsAmount = 0;
                                foreach (ShoppingCartItemsListModel cart in listCart)
                                {
                                    totalItemsAmount += Convert.ToDecimal(cart.SalePrice) * cart.Quantity;
                                }
                                if (model.PaymentMode.ToUpper() == Variables.ShippingPaymentMode.WALLET)
                                {
                                    decimal balance = 0;
                                    if (balance < totalItemsAmount)
                                    {
                                        _response.StatusCode = Variables.APIStatus.Failed;
                                        _response.Message = "Your wallet balance is low.";
                                        return _response;
                                    }
                                }

                                #region Order Method
                                string orderRefNumber = ClsMethod.GetUniqueNo(8);
                                int uniqueNo = Convert.ToInt32(orderRefNumber);
                                int paymentId = _shoppingOrderRepository.AddPayment(user.Id, model.PaymentMode.ToUpper(), totalItemsAmount, uniqueNo);
                                foreach (ShoppingCartItemsListModel cart in listCart)
                                {
                                    int cnt = 1;
                                    decimal netAmount = Convert.ToDecimal(cart.SalePrice) * cart.Quantity;
                                    decimal productRate = Convert.ToDecimal(cart.SalePrice);
                                    #region Order Method
                                    decimal totalIGST = 0, totalCGST = 0, totalSGST = 0, IGST = 0, CGST = 0, SGST = 0, shippingCharges = 0;
                                    string billHTML = "<table class='shop_table woocommerce-checkout-review-order-table' id='billData'>"
                                                + "<thead>"
                                                    + "<tr>"
                                                        + "<th>SL.NO</th>"
                                                        + "<th>Description</th>"
                                                        + "<th>Quantity</th>"
                                                        + "<th>Rate</th>"
                                                        + "<th>Per</th>"
                                                        + "<th>Discount</th>"
                                                        + "<th>Amount</th>"
                                                    + "</tr>"
                                                + "</thead>"
                                            + "<tbody>";
                                    #region Tax Method
                                    string taxHTML = "";
                                    foreach (TaxRatesModel tax in cart.Tax)
                                    {
                                        #region Tax Calculation method
                                        //if (new[] { "IGST", "CGST", "SGST" }.Contains(tax.Name.ToUpper()))
                                        //{
                                        //    if (tax.Name == "IGST")
                                        //    { decimal Cgstamt = ((productRate * tax.Amount) / 100); productRate -= Cgstamt; }
                                        //    if (shippingAddress.State == Variables.ShippingState.Other && tax.Name == "IGST")
                                        //    {
                                        //        #region IGST
                                        //        if (tax.Type == "Percent")
                                        //        {
                                        //            totalIGST += (netAmount * tax.Amount) / 100;
                                        //            IGST = (netAmount * tax.Amount) / 100;
                                        //        }
                                        //        else
                                        //        {
                                        //            totalIGST += tax.Amount;
                                        //            IGST = tax.Amount;
                                        //        }
                                        //        #endregion
                                        //    }
                                        //    else if (shippingAddress.State == Variables.ShippingState.Maharashtra && new[] { "CGST", "SGST" }.Contains(tax.Name.ToUpper()))
                                        //    {
                                        //        #region SGST
                                        //        if (tax.Name == "SGST")
                                        //        {
                                        //            if (tax.Type == "Percent")
                                        //            {
                                        //                totalSGST += (netAmount * tax.Amount) / 100;
                                        //                SGST = (netAmount * tax.Amount) / 100;
                                        //            }
                                        //            else
                                        //            {
                                        //                totalSGST += tax.Amount;
                                        //                SGST = tax.Amount;
                                        //            }
                                        //        }
                                        //        #endregion
                                        //        #region CGST
                                        //        if (tax.Name == "CGST")
                                        //        {
                                        //            if (tax.Type == "Percent")
                                        //            {
                                        //                totalCGST += (netAmount * tax.Amount) / 100;
                                        //                CGST = (netAmount * tax.Amount) / 100;
                                        //            }
                                        //            else
                                        //            {
                                        //                totalCGST += tax.Amount;
                                        //                CGST = tax.Amount;
                                        //            }
                                        //        }
                                        //        #endregion
                                        //    }
                                        //}
                                        //else
                                        //{
                                        //    if (shippingAddress.State == Variables.ShippingState.Other && tax.Name == "Shipping Charges(O)" && tax.Amount > 0)
                                        //    {
                                        //        #region Shipping Charges
                                        //        if (tax.Type == "Percent")
                                        //        {
                                        //            shippingCharges += (netAmount * tax.Amount) / 100;
                                        //        }
                                        //        else
                                        //        {
                                        //            shippingCharges += tax.Amount;
                                        //        }
                                        //        #endregion
                                        //    }
                                        //    else if (shippingAddress.State == Variables.ShippingState.Maharashtra && tax.Name == "Shipping Charges(M)" && tax.Amount > 0)
                                        //    {
                                        //        #region Shipping Charges
                                        //        if (tax.Type == "Percent")
                                        //        {
                                        //            shippingCharges += (netAmount * tax.Amount) / 100;
                                        //        }
                                        //        else
                                        //        {
                                        //            shippingCharges += tax.Amount;
                                        //        }
                                        //        #endregion
                                        //    }
                                        //}
                                        #endregion
                                        #region Tax Calculation method
                                        if (new[] { "IGST", "CGST", "SGST" }.Contains(tax.Name.ToUpper()))
                                        {
                                            #region SGST
                                            if (tax.Name.ToUpper() == "SGST")
                                            {
                                                if (tax.Type == "Percent")
                                                {
                                                    totalSGST += (netAmount * tax.Amount) / 100;
                                                    SGST = (netAmount * tax.Amount) / 100;
                                                }
                                                else
                                                {
                                                    totalSGST += tax.Amount;
                                                    SGST = tax.Amount;
                                                }
                                                taxHTML += "<tr class='cart_item'>"
                                                        + "<td></td>"
                                                        + "<td class='product-name'>" + tax.Name.ToUpper() + "</td>"
                                                        + "<td></td>"
                                                        + "<td></td>"
                                                        + "<td></td>"
                                                        + "<td></td>"
                                                        + "<td>₹ " + totalSGST.ToString("0.00") + "</td>"
                                                        + "</tr>";
                                            }
                                            #endregion
                                            #region CGST
                                            if (tax.Name.ToUpper() == "CGST")
                                            {
                                                if (tax.Type == "Percent")
                                                {
                                                    totalCGST += (netAmount * tax.Amount) / 100;
                                                    CGST = (netAmount * tax.Amount) / 100;
                                                }
                                                else
                                                {
                                                    totalCGST += tax.Amount;
                                                    CGST = tax.Amount;
                                                }
                                                taxHTML += "<tr class='cart_item'>"
                                                        + "<td></td>"
                                                        + "<td class='product-name'>" + tax.Name.ToUpper() + "</td>"
                                                        + "<td></td>"
                                                        + "<td></td>"
                                                        + "<td></td>"
                                                        + "<td></td>"
                                                        + "<td>₹ " + totalCGST.ToString("0.00") + "</td>"
                                                        + "</tr>";
                                            }
                                            #endregion
                                            #region IGST
                                            if (tax.Name.ToUpper() == "IGST")
                                            {
                                                if (tax.Type == "Percent")
                                                {
                                                    totalIGST += (netAmount * tax.Amount) / 100;
                                                    IGST = (netAmount * tax.Amount) / 100;
                                                }
                                                else
                                                {
                                                    totalIGST += tax.Amount;
                                                    IGST = tax.Amount;
                                                }
                                                taxHTML += "<tr class='cart_item'>"
                                                        + "<td></td>"
                                                        + "<td class='product-name'>" + tax.Name.ToUpper() + "</td>"
                                                        + "<td></td>"
                                                        + "<td></td>"
                                                        + "<td></td>"
                                                        + "<td></td>"
                                                        + "<td>₹ " + totalIGST.ToString("0.00") + "</td>"
                                                        + "</tr>";
                                            }
                                            #endregion
                                        }
                                        #endregion
                                    }
                                    #endregion
                                    decimal subTotal = netAmount - IGST - CGST - SGST;
                                    billHTML += "<tr class='cart_item'>"
                                                + "<td>" + cnt + "</td>"
                                                + "<td class='product-name'>" + cart.ProductName + "</td>"
                                                + "<td>" + cart.Quantity + "</td>"
                                                + "<td>₹ " + cart.SalePrice + "</td>"
                                                + "<td>0.00</td>"
                                                + "<td>₹ 0.00</td>"
                                                + "<td>₹ " + subTotal.ToString("0.00") + "</td>"
                                            + "</tr>";

                                    if (cart.DeliveryCharge >= 0)
                                    {
                                        #region Shipping Charges
                                        billHTML += taxHTML;
                                        billHTML += "<tr class='cart_item'>"
                                                        + "<td></td>"
                                                        + "<td class='product-name'>SHIPPING CHARGES</td>"
                                                        + "<td></td>"
                                                        + "<td></td>"
                                                        + "<td>0.00</td>"
                                                        + "<td></td>"
                                                        + "<td>₹ " + cart.DeliveryCharge.ToString("0.00") + "</td>"
                                                        + "</tr>";
                                        #endregion
                                    }
                                    decimal grossAmount = netAmount + cart.DeliveryCharge;
                                    billHTML += "<tfoot>"
                                            + "<tr>"
                                                + "<td colspan ='6'></td>"
                                                + "<td><b>₹ " + grossAmount.ToString("0.00") + "</b></td>"
                                            + "</tr>"
                                            + "<tr>"
                                                + "<td colspan ='7'><b>Amount in words:</b><b>" + ClsMethod.NumberToWords(Convert.ToDouble(grossAmount)) + "</b></td>"
                                            + "</tr>"
                                            + "<tr>"
                                                + "<td colspan ='7'><b>GST Number:</b><b>" + user.GSTIN + "</b></td>"
                                            + "</tr>"
                                            + "</tfoot>"
                                            + "</table>";
                                    ShoppingOrder _addOrder = new ShoppingOrder()
                                    {
                                        UserId = user.Id,
                                        OrderId = ClsMethod.GuidToBigInteger(Guid.NewGuid()).ToString(),
                                        ProductId = cart.ProductItemId,
                                        Quantity = cart.Quantity,
                                        ItemPrice = Convert.ToDecimal(cart.SalePrice) - IGST - CGST - SGST,
                                        SubTotal = subTotal,
                                        Tax = IGST + CGST + SGST,
                                        Shipping = shippingCharges,
                                        TotalAmount = grossAmount,
                                        PrimePer = 0,
                                        PrimePoint = 0,
                                        DatePlaced = DateTime.Now,
                                        DateUpdated = DateTime.Now,
                                        BillingAddress = billingAddress.Id,
                                        ShipppingAddress = shippingAddress.Id,
                                        PaymentId = paymentId,
                                        BILLHTML = billHTML,
                                        IPAddress = "",
                                        Status = Variables.OrderStatus.Accepted,
                                        IsActive = true,
                                        ItemColor = "",
                                        ItemSize = "",
                                        BILLHTMLS = billHTML,
                                        AdminOffer = 0,
                                        Repurchase = 0
                                    };
                                    _addOrder = _shoppingOrderRepository.AddOrder(_addOrder);
                                    #endregion
                                    totalItemsAmount += shippingCharges;
                                }
                                #endregion
                                #region Payment Method
                                if (model.PaymentMode.ToUpper() == Variables.ShippingPaymentMode.COD)
                                {
                                    _shoppingOrderRepository.UpdatePayment(paymentId, Variables.OrderStatus.Pending, "Cash On Delivery", 0, 0, 0);
                                }
                                else if (model.PaymentMode.ToUpper() == Variables.ShippingPaymentMode.WALLET)
                                {
                                    _shoppingOrderRepository.UpdatePayment(paymentId, Variables.OrderStatus.Complete, "Amount Paid Via Wallet", totalItemsAmount, 0, 0);
                                }
                                else if (model.PaymentMode.ToUpper() == Variables.ShippingPaymentMode.PG)
                                {
                                    _shoppingOrderRepository.UpdatePayment(paymentId, Variables.OrderStatus.Pending, "Amount To Be Paid Via Payment Gateway", 0, totalItemsAmount, 0);
                                    string description = "Payment for order #" + paymentId + "#" + uniqueNo + " Amount : INR " + totalItemsAmount;
                                    long paymentProcId = _shoppingOrderRepository.AddProcessPay(user.Id, user.Email, user.PhoneNumber, paymentId, totalItemsAmount, uniqueNo, description);
                                    _response.StatusCode = Variables.APIStatus.Pending;
                                    _response.Message = "Order accepted.";
                                    _response.OrderId = uniqueNo;
                                    _response.PaymentId = paymentId;
                                    _response.GateWayId = paymentProcId;
                                    return _response;
                                }
                                else
                                {
                                    _shoppingOrderRepository.UpdatePayment(paymentId, Variables.OrderStatus.Pending, "Under Process", 0, 0, 0);
                                }
                                #endregion
                                #region Notification Method
                                foreach (ShoppingCartItemsListModel cart in listCart)
                                {
                                    _shoppingCartRepository.DeletesShoppingCartItem(cart.ShoppingCartID);
                                }
                                string addressBilling = "<div class='col-md-12'><h5>" + billingAddress.FirstName + " " + billingAddress.LastName + "</h5><p><b>Address :</b><br />  " + billingAddress.AddressLine1 + "</p> <p><b>Zip :</b>  " + billingAddress.ZipPostal + "</p><p><b>Phone :</b>  " + billingAddress.Phone + "</p></div>";
                                string addressShip = "<div class='col-md-12'><h5>" + shippingAddress.FirstName + " " + shippingAddress.LastName + "</h5><p><b>Address :</b><br />  " + shippingAddress.AddressLine1 + "</p> <p><b>Zip :</b>  " + shippingAddress.ZipPostal + "</p><p><b>Phone :</b>  " + shippingAddress.Phone + "</p></div>";
                                _notificationRepository.ProductPurchase(user.Email, user.PhoneNumber, user.Name, "#", "ORDER#" + paymentId, addressShip);
                                _notificationRepository.Invoice(user.Email, user.PhoneNumber, user.Name, "#", "ORDER#" + paymentId, addressBilling);

                                _response.StatusCode = Variables.APIStatus.Success;
                                _response.Message = "Order accepted.";
                                _response.OrderId = uniqueNo;
                                _response.PaymentId = paymentId;
                                #endregion
                            }
                        }
                        else
                        {
                            _response.StatusCode = Variables.APIStatus.Failed;
                            _response.Message = "Invalid shipping address.";
                        }
                    }
                    else
                    {
                        _response.StatusCode = Variables.APIStatus.Failed;
                        _response.Message = "Invalid billing address.";
                    }
                }
                else
                {
                    _response.StatusCode = Variables.APIStatus.Failed;
                    _response.Message = "Invalid user.";
                }
            }
            catch (Exception ex)
            {
                _response.StatusCode = Variables.APIStatus.Exception;
                _response.Message = "Something went wrong. " + ex.InnerException.Message;
            }
            return _response;
        }

        public List<ShoppingOrderDetailsModel> UserOrders(int userId)
        {
            return _shoppingOrderRepository.UserOrders(userId);
        }
        public ShoppingOrderDetailModel UserOrderDetails(int userId,int id)
        {
            return _shoppingOrderRepository.UserOrderDetails(userId, id);
        }

        public List<ShoppingOrdersDetailsModel> UsersOrders(OrderReportModel model)
        {
            return _shoppingOrderRepository.UsersOrders(model);
        }

        public List<ShoppingOrdersDetailsModel> UsersAcceptedOrders(int userId)
        {
            return _shoppingOrderRepository.UsersAcceptedOrders(userId);
        }

        public Tuple<bool, string, int> UpdateOrder(UpdateOrderStatusModel model)
        {
            var result = _shoppingOrderRepository.UpdateOrderStatus(model);
            if (result.Item1)
            {
                var user = _userRepository.GetUserById(result.Item4);
                _notificationRepository.ShippingUpdate(user.Email, user.PhoneNumber, user.Name, model.Status, result.Item5, result.Item6);
            }
            return new Tuple<bool, string, int>(result.Item1, result.Item2, result.Item3);
        }
    }
}
