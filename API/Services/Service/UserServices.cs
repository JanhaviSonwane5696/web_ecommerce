﻿using Helpers;
using Models;
using Repository.Interface;
using Services.Helper;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services.Service
{
    public class UserServices : IUserServices
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserTypesRepository _userTypeRepository;
        private readonly IUsersVerificationRepository _userVerificationRepository;
        private readonly IUsersBankRepository _usersBankRepository;
        private readonly INotificationHelpers _notificationRepository;

        public UserServices(IUserRepository userRepository, IUserTypesRepository userTypeRepository, IUsersVerificationRepository userVerificationRepository, IUsersBankRepository usersBankRepository, INotificationHelpers notificationRepository, ILoggerService loggerService)
        {
            _userRepository = userRepository;
            _userTypeRepository = userTypeRepository;
            _userVerificationRepository = userVerificationRepository;
            _usersBankRepository = usersBankRepository;
            _notificationRepository = notificationRepository;
        }

        /// <summary>
        ///     Authenticate User
        /// </summary>
        /// <param name="loginDetails"></param>
        /// <returns></returns>
        public UserDetailsModel AuthenticateUser(UserLoginModel loginDetails)
        {
            if (loginDetails != null && !string.IsNullOrEmpty(loginDetails.Username) &&
                !string.IsNullOrEmpty(loginDetails.Password))
            {
                var loginStatus = _userRepository.AuthenticateUser(loginDetails);

                return loginStatus.Item1 ? GetUserById(loginStatus.Item2) : null;
            }

            return null;
        }

        public bool ResetPassword(ResetUserPasswordModel loginDetails)
        {
            if (loginDetails != null && !string.IsNullOrEmpty(loginDetails.Username))
            {
                var loginStatus = _userRepository.ResetPassword(loginDetails);
                if (loginStatus.Item1)
                {
                    var user = _userRepository.GetUserById(loginStatus.Item2);
                    _notificationRepository.PasswordReset(user.Email, user.PhoneNumber, user.Name, user.PasswordHash);
                }
                return loginStatus.Item1;
            }

            return false;
        }

        public bool ForgotPassword(ResetUserPasswordModel loginDetails)
        {
            if (loginDetails != null && !string.IsNullOrEmpty(loginDetails.Username))
            {
                var loginStatus = _userRepository.ForgotPassword(loginDetails);
                if (loginStatus.Item1)
                {
                    var user = _userRepository.GetUserById(loginStatus.Item2);
                    _notificationRepository.PasswordReset(user.Email, user.PhoneNumber, user.Name, user.PasswordHash);
                }
                return loginStatus.Item1;
            }

            return false;
        }

        public Tuple<bool, string> ChangePassword(UserSettingModel loginDetails)
        {
            var loginStatus = _userRepository.ChangePassword(loginDetails);
            if (loginStatus.Item1)
            {
                //var user = _userRepository.GetUserById(loginStatus.Item2);
                //_notificationRepository.PasswordReset(user.Email, user.PhoneNumber, user.Name, user.PasswordHash);
            }
            return loginStatus;
        }

        public Tuple<bool, int, string> AddUser(AddUserModel addUserDetails, string createdBy)
        {
            var userDetails = _userRepository.AddUser(addUserDetails, Variables.UserStatus.Verified, createdBy);
            if (userDetails.Item1)
            {
                _notificationRepository.UserRegistration(addUserDetails.EmailID, addUserDetails.PhoneNumber, addUserDetails.Name);
            }
            return userDetails;
        }

        public Tuple<bool, int, string> UpdateUser(UpdateUserModel addUserDetails, string createdBy)
        {
            var userDetails = _userRepository.UpdateUser(addUserDetails, createdBy);
            return userDetails;
        }
        public Tuple<bool, int, string> UpdateUserStatus(UpdateUserStatusModel addUserDetails, string createdBy)
        {
            var userDetails = _userRepository.UpdateUserStatus(addUserDetails, createdBy);
            return userDetails;
        }

        /// <summary>
        ///     Get User By ID
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserDetailsModel GetUserByID(int userId)
        {
            return GetUserById(userId);
        }

        public List<UsersList> GetUserList()
        {
            var user = _userRepository.UserList();
            var userType = _userTypeRepository.UserTypeList();
            if (user != null)
            {
                List<UsersList> usersLists = user.Select(x => new UsersList
                {
                    Id = x.Id,
                    Name = x.Name,
                    Email = x.Email,
                    EmailVerification = x.EmailVerification,
                    PhoneNumber = x.PhoneNumber,
                    PhoneNumberVerification = x.PhoneNumberVerification,
                    AltPhoneNumber = x.AltPhoneNumber,
                    AltEmail = x.AltEmail,
                    UserType = (from u in userType where u.ID == x.UserType select u.UserType).FirstOrDefault(),
                    Status = x.Status,
                    IsLockAccount = x.IsLockAccount,
                    LoginFailedCount = x.LoginFailedCount,
                    CreatedOn = x.CreatedOn,
                    CreatedBy = x.CreatedBy,
                    ModifiedOn = x.ModifiedOn,
                    ModifiedBy = x.ModifiedBy,
                    Gender = x.Gender
                }).ToList();

                return usersLists;
            }

            return null;
        }

        public List<UsersList> GetUserByUserTypeList(short userType)
        {
            var user = _userRepository.UserByUserType(userType);
            var userTypes = _userTypeRepository.UserTypeList();
            if (user != null)
            {
                List<UsersList> usersLists = user.Select(x => new UsersList
                {
                    Id = x.Id,
                    Name = x.Name,
                    Email = x.Email,
                    EmailVerification = x.EmailVerification,
                    PhoneNumber = x.PhoneNumber,
                    PhoneNumberVerification = x.PhoneNumberVerification,
                    AltPhoneNumber = x.AltPhoneNumber,
                    AltEmail = x.AltEmail,
                    UserType = (from u in userTypes where u.ID == x.UserType select u.UserType).FirstOrDefault(),
                    Status = x.Status,
                    IsLockAccount = x.IsLockAccount,
                    LoginFailedCount = x.LoginFailedCount,
                    CreatedOn = x.CreatedOn,
                    CreatedBy = x.CreatedBy,
                    ModifiedOn = x.ModifiedOn,
                    ModifiedBy = x.ModifiedBy,
                    Gender = x.Gender
                }).ToList();

                return usersLists;
            }

            return null;
        }

        public List<UserTypeList> GetUserTypeList()
        {
            var userType = _userTypeRepository.UserTypeList();
            if (userType.Any())
            {
                List<UserTypeList> userTypeLists = userType.Select(x => new UserTypeList
                {
                    ID = x.ID,
                    UserType = x.UserType
                }).ToList();
                return userTypeLists;
            }
            else
                return null;
        }

        public Tuple<bool, string> UpdateUserDeviceId(UpdateUserDeviceIDModel model)
        {
            var result = _userRepository.UpdateDeviceID(model.UserID, model.DeviceID, model.ModifiedBy);
            return result;
        }

        #region Private Members

        /// <summary>
        ///     Get User By Id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>UserDetailsModel</returns>
        private UserDetailsModel GetUserById(int userId)
        {
            var usrDetails = new UserDetailsModel();
            var user = _userRepository.GetUserById(userId);
            if (user != null)
            {
                var userType = _userTypeRepository.GetUserTypeById(user.UserType);
                usrDetails.Id = user.Id;
                usrDetails.Name = user.Name;
                usrDetails.Username = user.Email;
                usrDetails.UserType = userType.UserType;
                usrDetails.EmailAddress = user.Email;
                usrDetails.Status = user.Status;
                usrDetails.Type = user.UserType;
                usrDetails.Gender = user.Gender;
            }

            return usrDetails;
        }

        #endregion


        #region Customer Method

        public Tuple<bool, string> CustomerAddRequest(AddCustomerRequestModel addUserDetails)
        {
            var userDetails = _userRepository.ValidateUserMobile(addUserDetails.PhoneNumber, 4);
            if (userDetails.Item1)
            {
                string otp = ClsMethod.GetUniqueNo(6);
                var result = _userVerificationRepository.AddUpdateVerification(addUserDetails.PhoneNumber, 4, otp);
                if (result.Item1)
                    _notificationRepository.UserVerification("", addUserDetails.PhoneNumber, otp);
            }
            return userDetails;
        }

        public Tuple<bool, int, string> CustomerConfirmRequest(AddCustomerConfirmRequestModel addUserDetails, string createdBy)
        {
            var result = _userVerificationRepository.GetUserVerificationByMobile(addUserDetails.PhoneNumber, 4);
            if (result != null)
            {
                if (result.ModifiedOn.AddMinutes(10) > DateTime.Now)
                {
                    if (result.VerifyData.Trim() == addUserDetails.OTP.Trim())
                    {
                        var userResult = _userRepository.AddCustomer(addUserDetails, Variables.UserStatus.Verified, createdBy);
                        var user = _userRepository.GetUserById(userResult.Item2);
                        _notificationRepository.UserRegistration(user.Email, user.PhoneNumber, user.Name);
                        return userResult;
                    }
                    else
                        return new Tuple<bool, int, string>(false, 0, "Invalid OTP.");
                }
                else
                    return new Tuple<bool, int, string>(false, 0, "OTP expired.");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "Phone number not exist.");
        }

        public Tuple<bool, int, string> UpdateCustomerEmail(UpdateCustomerRequestModel updateUserDetails)
        {
            var userResult = _userRepository.UpdateCustomerEmail(updateUserDetails);
            return userResult;
        }
        #endregion


        #region Vender Method
        public Tuple<bool, string> VendorPhoneVerification(VerifyPhoneNumberModel verifyDetails)
        {
            var userDetails = _userRepository.ValidateUserMobile(verifyDetails.PhoneNumber, 3);
            if (userDetails.Item1)
            {
                string otp = ClsMethod.GetUniqueNo(6);
                var result = _userVerificationRepository.AddUpdateVerification(verifyDetails.PhoneNumber, 3, otp);
                if (result.Item1)
                    _notificationRepository.UserVerification("", verifyDetails.PhoneNumber, otp);
            }
            return userDetails;
        }
        public Tuple<bool, int, string> AddVendor(AddVendorModel addVendorDetails, string createdBy)
        {
            var result = _userVerificationRepository.GetUserVerificationByMobile(addVendorDetails.PhoneNumber, 3);
            if (result != null)
            {
                if (result.ModifiedOn.AddMinutes(10) > DateTime.Now)
                {
                    if (result.VerifyData.Trim() == addVendorDetails.OTP.Trim())
                    {
                        var userResult = _userRepository.AddVendor(addVendorDetails, Variables.UserStatus.Registered, createdBy);
                        _notificationRepository.UserRegistration(addVendorDetails.EmailID, addVendorDetails.PhoneNumber, addVendorDetails.Name);
                        return userResult;
                    }
                    else
                        return new Tuple<bool, int, string>(false, 0, "Invalid OTP.");
                }
                else
                    return new Tuple<bool, int, string>(false, 0, "OTP expired.");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "Phone number not exist.");
        }
        public PickUpAddressResponseModel GetPickupAddress(PickUpAddressModel pincodeDetails)
        {
            var pinCode = _userRepository.PinCodeDetails(pincodeDetails);
            if (pinCode != null)
            {
                PickUpAddressResponseModel _response = new PickUpAddressResponseModel()
                {
                    PinCode = pincodeDetails.PinCode,
                    District = pinCode.DistrictName,
                    State = pinCode.StateName,
                    CircleName = pinCode.CircleName,
                    DivisionName = pinCode.DivisionName,
                    RegionName = pinCode.RegionName,
                    Taluka = pinCode.Taluk
                };
                return _response;
            }
            else
                return null;
        }
        public Tuple<bool, int, string> UpdatePickupAddress(UpdatePickupPinAddressModel addVendorDetails)
        {
            var result = _userRepository.UpdatePickUpAddress(addVendorDetails, Variables.UserStatus.AddressUpdated, addVendorDetails.ModifiedBy);
            return result;
        }
        public Tuple<bool, int, string> UpdateGSTIN(UpdateGSTINModel addVendorDetails)
        {
            var result = _userRepository.UpdateGSTIN(addVendorDetails, Variables.UserStatus.GSTINUpdated, addVendorDetails.ModifiedBy);
            return result;
        }
        public Tuple<bool, int, string> UpdateBankDetails(UpdateBankDetailsModel addVendorDetails)
        {
            var result = _usersBankRepository.UpdateBankDetails(addVendorDetails);
            if (result.Item1)
            {
                _userRepository.UpdatedUserStatus(addVendorDetails.ID, Variables.UserStatus.BankUpdated, addVendorDetails.ModifiedBy);
            }
            return result;
        }
        public Tuple<bool, int, string> UpdateStoreDetails(StoreDetailsModel addVendorDetails)
        {
            var result = _userRepository.UpdateStoreDetails(addVendorDetails, Variables.UserStatus.StoreUpdated, addVendorDetails.ModifiedBy);
            return result;
        }
        public Tuple<bool, int, string> UpdateSignatureDetails(UpdateSignatureModel addVendorDetails)
        {
            var result = _userRepository.UpdateUserDocument(addVendorDetails.ID, Variables.DocumentType.Signature, false, addVendorDetails.SignatureImageURL, string.Empty, addVendorDetails.ModifiedBy);
            if (result.Item1)
            {
                _userRepository.UpdatedUserStatus(addVendorDetails.ID, Variables.UserStatus.Submitted, addVendorDetails.ModifiedBy);
                return new Tuple<bool, int, string>(true, result.Item2, "Signature updated Successfully.");
            }
            return result;
        }

        public VendorDetailsModel VendorDetails(VendorStoredDetailsModel vendorDetails)
        {
            VendorDetailsModel _response = new VendorDetailsModel();
            var user = _userRepository.GetUserById(vendorDetails.ID);
            if (user == null)
                return null;

            var userDocuments = _userRepository.GetUserDocumentById(vendorDetails.ID);
            if (userDocuments.Count > 0)
            {
                var userDocument = userDocuments.FirstOrDefault(x => x.DocumentType == Variables.DocumentType.Signature);
                if (userDocument != null)
                    _response.SignatureImageURL = userDocument.DocumentFilePath;
                userDocument = userDocuments.FirstOrDefault(x => x.DocumentType == Variables.DocumentType.Profile);
                if (userDocument != null)
                    _response.ProfileImageURL = userDocument.DocumentFilePath;
            }

            _response.UserType = user.UserType;
            _response.PhoneNumber = user.PhoneNumber;
            _response.EmailID = user.Email;
            _response.Name = user.Name;
            _response.AltEmail = user.AltEmail;
            _response.AltPhoneNumber = user.AltPhoneNumber;
            _response.Active = user.Active;
            _response.Gender = user.Gender;
            _response.PinCode = user.PinCode;
            _response.Address = user.Address;
            _response.GSTIN = user.GSTIN;
            _response.StoreName = user.ShopName;
            _response.StoreDescription = user.ShopDescription;
            var bank = _usersBankRepository.UserBankList(user.Id);
            if (bank != null)
            {
                _response.Bank = bank.Select(x => new BanksDetailsModel
                {
                    BankName = x.BankName,
                    AccountHolderName = x.Name,
                    AccountNumber = x.AccountNo,
                    Ifsc = x.IfscCode,
                    BlankChequeImageURL = x.ChequeURL,
                    ID = x.ID
                }).ToList();
            }
            return _response;

        }
        public Tuple<bool, int, string> UpdateProfileDetails(UpdateProfileDetailsModel vendorDetails)
        {
            var result = _userRepository.UpdateProfileDetails(vendorDetails);
            if (result.Item1)
            {
                if (!string.IsNullOrEmpty(vendorDetails.SignatureImageURL))
                    _userRepository.UpdateUserDocument(vendorDetails.Id, Variables.DocumentType.Signature, false, vendorDetails.SignatureImageURL, string.Empty, vendorDetails.ModifiedBy);
                if (!string.IsNullOrEmpty(vendorDetails.ProfileImageURL))
                    _userRepository.UpdateUserDocument(vendorDetails.Id, Variables.DocumentType.Profile, false, vendorDetails.ProfileImageURL, string.Empty, vendorDetails.ModifiedBy);
            }
            return result;
        }
        public Tuple<bool, int, string> VendorStatusRequest(UpdateVendorStatusModel vendorDetails)
        {
            Tuple<bool, int, string> _response;
            if (vendorDetails.Status == Variables.UserStatus.Verified)
            {
                _response = _userRepository.UpdatedUserStatus(vendorDetails.VendorID, Variables.UserStatus.Verified, vendorDetails.ModifiedBy);
                if (_response.Item1)
                {
                    var user = _userRepository.GetUserById(_response.Item2);
                    _notificationRepository.UserKYCVerification(user.Email, user.PhoneNumber, user.Name, user.Active, vendorDetails.Remarks);
                }
                return _response;
            }
            else if (vendorDetails.Status == Variables.UserStatus.Rejected)
            {
                _response = _userRepository.UpdatedUserStatus(vendorDetails.VendorID, Variables.UserStatus.Rejected, vendorDetails.ModifiedBy);
                if (_response.Item1)
                {
                    var user = _userRepository.GetUserById(_response.Item2);
                    _notificationRepository.UserKYCVerification(user.Email, user.PhoneNumber, user.Name, user.Active, vendorDetails.Remarks);
                }
                return _response;
            }
            else if (vendorDetails.Status == Variables.UserStatus.Processing)
            {
                _response = _userRepository.UpdatedUserStatus(vendorDetails.VendorID, Variables.UserStatus.Processing, vendorDetails.ModifiedBy);
                if (_response.Item1)
                {
                    var user = _userRepository.GetUserById(_response.Item2);
                    _notificationRepository.UserKYCVerification(user.Email, user.PhoneNumber, user.Name, user.Active, vendorDetails.Remarks);
                }
                return _response;
            }
            else
                return new Tuple<bool, int, string>(false, 0, "Invalid requested status.");
        }
        public List<UsersList> GetVendorList()
        {
            var user = _userRepository.VendorsList();
            return user;
        }
        public List<UserDocumentsListModel> UserDocumentList(VendorStoredDetailsModel vendorDetails)
        {
            List<UserDocumentsListModel> _response = new List<UserDocumentsListModel>();
            var userDocuments = _userRepository.GetUserDocumentById(vendorDetails.ID);
            if (userDocuments.Count > 0)
            {
                _response = userDocuments.Select(x => new UserDocumentsListModel
                {
                    Id = x.Id,
                    DocumentType = x.DocumentType,
                    DocumentNumber = x.DocumentNumber,
                    DocumentFilePath = x.DocumentFilePath,
                    Extra1 = x.Extra1,
                    Extra2 = x.Extra2,
                    Extra3 = x.Extra3,
                    Extra4 = x.Extra4,
                    Extra5 = x.Extra5,
                    CreatedBy = x.CreatedBy,
                    CreatedOn = x.CreatedOn,
                    ModifiedBy = x.ModifiedBy,
                    ModifiedOn = x.ModifiedOn,
                    Remarks = x.Remarks
                }).ToList();
            }
            return _response;
        }
        public List<BanksDetailsModel> UserBankList(VendorStoredDetailsModel vendorDetails)
        {
            List<BanksDetailsModel> _response = new List<BanksDetailsModel>();
            var userDocuments = _usersBankRepository.UserBankList(vendorDetails.ID);
            if (userDocuments.Count > 0)
            {
                _response = userDocuments.Select(x => new BanksDetailsModel
                {
                    BankName = x.BankName,
                    AccountHolderName = x.Name,
                    AccountNumber = x.AccountNo,
                    Ifsc = x.IfscCode,
                    BlankChequeImageURL = x.ChequeURL,
                    ID = x.ID
                }).ToList();
            }
            return _response;
        }
        public List<UsersListModel> GetVendorsList()
        {
            var user = _userRepository.VendorsWithAdminList();
            return user;
        }
        #endregion

    }
}