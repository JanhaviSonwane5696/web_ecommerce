﻿using Models;
using Repository.Interface;
using Repository.Model;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services.Service
{
    public class MasterAttributeServices : IMasterAttributeServices
    {
        private readonly IMasterAttributeRepository _attributeRepository;

        public MasterAttributeServices(IMasterAttributeRepository attributeRepository, ILoggerService loggerService)
        {
            _attributeRepository = attributeRepository;
        }

        /// <summary>
        /// Get MasterAttribute
        /// </summary>
        /// <returns></returns>
        public List<MasterAttributeModel> GetMasterAttribute(int attributeId)
        {
            List<MasterAttribute> attribute = _attributeRepository.GetMasterAttribute(attributeId);
            List<MasterAttributeModel> attributeData = attribute.Select(x => new MasterAttributeModel
            {
                Id = x.Id,
                AttributeName = x.AttributeName,
                AttributeDesciption = x.AttributeDesciption,
                CreatedBy = x.CreatedBy,
                CreatedOn = x.CreatedOn,
                ModifiedBy = x.ModifiedBy,
                ModifiedOn = x.ModifiedOn,
                IsActive = x.IsActive,
                AttributeType = x.AttributeType
            }).ToList();

            return attributeData;
        }

        public List<MasterAttributeModel> GetMasterAllAttribute()
        {
            List<MasterAttribute> attribute = _attributeRepository.GetMasterAllAttribute();
            List<MasterAttributeModel> attributeData = attribute.Select(x => new MasterAttributeModel
            {
                Id = x.Id,
                AttributeName = x.AttributeName,
                AttributeDesciption = x.AttributeDesciption,
                CreatedBy = x.CreatedBy,
                CreatedOn = x.CreatedOn,
                ModifiedBy = x.ModifiedBy,
                ModifiedOn = x.ModifiedOn,
                IsActive = x.IsActive,
                AttributeType = x.AttributeType
            }).ToList();

            return attributeData;
        }

        /// <summary>
        /// Add MasterAttribute
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public bool AddMasterAttribute(MasterAttributeModel attribute)
        {
            MasterAttribute attributeData = new MasterAttribute();
            attributeData.AttributeName = attribute.AttributeName;
            attributeData.AttributeDesciption = attribute.AttributeDesciption;
            attributeData.IsActive = attribute.IsActive;
            attributeData.AttributeType = attribute.AttributeType;
            attributeData.IsDeleted = false;
            attributeData.CreatedOn = DateTime.Now;
            attributeData.CreatedBy = attribute.CreatedBy;
            return _attributeRepository.AddMasterAttribute(attributeData);
        }

        /// <summary>
        /// Update MasterAttribute
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public bool UpdateMasterAttribute(MasterAttributeModel attribute)
        {
            MasterAttribute attributeData = new MasterAttribute();
            attributeData.Id = attribute.Id;
            attributeData.AttributeName = attribute.AttributeName;
            attributeData.AttributeDesciption = attribute.AttributeDesciption;
            attributeData.AttributeType = attribute.AttributeType;
            attributeData.ModifiedOn = DateTime.Now;
            attributeData.ModifiedBy = attribute.ModifiedBy;
            return _attributeRepository.UpdateMasterAttribute(attributeData);
        }

        /// <summary>
        /// Delete MasterAttribute
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public bool DeleteMasterAttribute(MasterAttributeModel attribute)
        {
            return _attributeRepository.DeleteMasterAttribute(attribute.Id, attribute.ModifiedBy);
        }

        /// <summary>
        /// Update Status MasterAttribute
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public bool UpdateStatusMasterAttribute(MasterAttributeModel attribute)
        {
            return _attributeRepository.UpdateStatusMasterAttribute(attribute.Id, attribute.IsActive, attribute.ModifiedBy);
        }

        #region Private Members

        #endregion
    }
}