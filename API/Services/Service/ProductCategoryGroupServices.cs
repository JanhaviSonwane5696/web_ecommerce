﻿using Repository.Interface;
using Repository.Model;
using Services.Interface;
using System.Collections.Generic;
using System.Linq;

namespace Services.Service
{
    public class ProductCategoryGroupServices : IProductCategoryGroupServices
    {
        private readonly IProductCategoryGroupRepository _productCategoryGroupRepository;

        public ProductCategoryGroupServices(IProductCategoryGroupRepository productCategoryGroupRepository, ILoggerService loggerService)
        {
            _productCategoryGroupRepository = productCategoryGroupRepository;
        }

        /// <summary>
        /// Get Product Category
        /// </summary>
        /// <returns></returns>
        public List<ProductCategoryGroupModel> GetProductCategoryGroup()
        {
            List<ProductCategoryGroup> productCategory = _productCategoryGroupRepository.GetProductCategoryGroup();
            List<ProductCategoryGroupModel> productCategoryData = productCategory.Select(x => new ProductCategoryGroupModel
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                ImageURL = x.ImageURL,
                IsActive = x.IsActive,
                CreatedOn = x.CreatedOn,
                CreatedBy = x.CreatedBy,
                ModifiedOn = x.ModifiedOn,
                ModifiedBy = x.ModifiedBy,
                ShowHide = x.ShowHide,
                Priority = x.Priority,
                MetaTag = x.MetaTag,
                Status = x.Status
            }).ToList();

            return productCategoryData;
        }

        /// <summary>
        /// Add Product Category
        /// </summary>
        /// <param name="productCategory"></param>
        /// <returns></returns>
        public bool AddProductCategoryGroup(AddUpdateProductCategoryGroupModel productCategory)
        {
            ProductCategoryGroup productCategoryData = new ProductCategoryGroup();
            productCategoryData.Id = productCategory.Id;
            productCategoryData.Name = productCategory.Name;
            productCategoryData.ImageURL = productCategory.ImageURL;
            productCategoryData.Description = productCategory.Description;
            productCategoryData.IsActive = productCategory.IsActive;
            productCategoryData.IsDeleted = false;
            productCategoryData.CreatedOn = productCategory.CreatedOn;
            productCategoryData.CreatedBy = productCategory.CreatedBy;
            productCategoryData.ShowHide = productCategory.ShowHide;
            productCategoryData.Priority = productCategory.Priority;
            productCategoryData.MetaTag = productCategory.MetaTag;
            productCategoryData.Status = productCategory.Status;
            //productCategoryData.ModifiedOn = productCategory.ModifiedOn;
            //productCategoryData.ModifiedBy = productCategory.ModifiedBy;
            return _productCategoryGroupRepository.AddProductCategoryGroup(productCategoryData);
        }

        /// <summary>
        /// Update Product Category
        /// </summary>
        /// <param name="productCategory"></param>
        /// <returns></returns>
        public bool UpdateProductCategoryGroup(AddUpdateProductCategoryGroupModel productCategory)
        {
            ProductCategoryGroup productCategoryData = new ProductCategoryGroup();
            productCategoryData.Id = productCategory.Id;
            productCategoryData.Name = productCategory.Name;
            productCategoryData.ImageURL = productCategory.ImageURL;
            productCategoryData.Description = productCategory.Description;
            productCategoryData.ShowHide = productCategory.ShowHide;
            productCategoryData.Priority = productCategory.Priority;
            productCategoryData.MetaTag = productCategory.MetaTag;
            productCategoryData.Status = productCategory.Status;
            //productCategoryData.IsActive = productCategory.IsActive;
            //productCategoryData.IsDeleted = productCategory.IsDeleted;
            //productCategoryData.CreatedOn = productCategory.CreatedOn;
            //productCategoryData.CreatedBy = productCategory.CreatedBy;
            productCategoryData.ModifiedOn = productCategory.ModifiedOn;
            productCategoryData.ModifiedBy = productCategory.ModifiedBy;
            return _productCategoryGroupRepository.UpdateProductCategoryGroup(productCategoryData);
        }

        /// <summary>
        /// Delete Product Category
        /// </summary>
        /// <param name="productCategory"></param>
        /// <returns></returns>
        public bool DeleteProductCategoryGroup(ProductCategoryGroupModel productCategory)
        {
            return _productCategoryGroupRepository.DeleteProductCategoryGroup(productCategory.Id, productCategory.ModifiedBy);
        }

        /// <summary>
        /// Update Status Product Category
        /// </summary>
        /// <param name="productCategory"></param>
        /// <returns></returns>
        public bool UpdateStatusProductCategoryGroup(ProductCategoryGroupModel productCategory)
        {
            return _productCategoryGroupRepository.UpdateStatusProductCategoryGroup(productCategory.Id, productCategory.IsActive, productCategory.ModifiedBy);
        }

        #region Private Members

        #endregion
    }
}