﻿using Models;
using Repository.Interface;
using Repository.Model;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Service
{
    public class ShoppingCartServices : IShoppingCartServices
    {
        private readonly IShoppingCartRepository _shoppingCartRepository;

        public ShoppingCartServices(IShoppingCartRepository shoppingCartRepository, ILoggerService loggerService)
        {
            _shoppingCartRepository = shoppingCartRepository;
        }

        public ShoppingCartsModel GetCartDetails(string guid, int userId)
        {
            ShoppingCartsModel shoppingCartData = new ShoppingCartsModel();
            var cartData = _shoppingCartRepository.GetShoppingCart(guid, userId);
            if (cartData != null)
            {
                shoppingCartData.Id = cartData.Id;
                shoppingCartData.UserId = cartData.UserId;
                shoppingCartData.DateCreated = cartData.DateCreated;
                shoppingCartData.DateUpdated = cartData.DateUpdated;
                shoppingCartData.GuiD = cartData.GuiD;

                var cartList = _shoppingCartRepository.GetShoppingCartItemWithProduct(cartData.Id);
                if (cartList.Count > 0)
                {
                    shoppingCartData.cartItems = cartList;
                }
            }
            return shoppingCartData;
        }

        public bool DeleteProductCartItem(int cartItemID)
        {
            return _shoppingCartRepository.DeleteShoppingCartItem(cartItemID);
        }

        public bool AddShoppingCart(ShoppingCartModel cartData)
        {
            ShoppingCart cart = new ShoppingCart()
            {
                UserId = cartData.UserId,
                DateCreated = DateTime.Now,
                DateUpdated = DateTime.Now,
                GuiD = cartData.GuiD,
            };
            var result= _shoppingCartRepository.AddShoppingCart(cart);
            foreach(ShoppingCartItemModel row in cartData.cartItems)
            {
                row.ShoppingCardID = result.Item2;
                AddShoppingCartItem(row);
            }
            return result.Item1;
        }

        public bool AddShoppingCartItem(ShoppingCartItemModel cartItemData)
        {
            ShoppingCartItems cart = new ShoppingCartItems()
            {
                ShoppingCardID = cartItemData.ShoppingCardID,
                ProductId = cartItemData.ProductId,
                ProductItemId = cartItemData.ProductItemId,
                Quantity = cartItemData.Quantity,
                DateAdded = DateTime.Now,
                IsDeleted=false,
            };
            return _shoppingCartRepository.AddShoppingCartItem(cart);
        }

        public Tuple<bool, string> UpdateProductCartItem(string guid, int userId)
        {
            return _shoppingCartRepository.UpdateShoppingCart(guid, userId);
        }
    }
}
