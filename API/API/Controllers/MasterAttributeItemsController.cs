﻿using System;
using System.IO;
using System.Net;
using Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Repository.Model;
using Services.Interface;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterAttributeItemsController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly ILoggerService _loggerService;
        private readonly IMasterAttributeItemsServices _attributeItemsServices;
        private readonly IHostingEnvironment _hostingEnvironment;

        public MasterAttributeItemsController(IHostingEnvironment hostingEnvironment, IMasterAttributeItemsServices attributeItemsServices, IConfiguration config, ILoggerService loggerService)
        {
            _loggerService = loggerService;
            _hostingEnvironment = hostingEnvironment;
            _attributeItemsServices = attributeItemsServices;
            _config = config;
        }

        [HttpPost]
        [Route("get")]
        public IActionResult GetMasterAttributeItems(int attributeId)
        {
            try
            {
                var MasterAttributeItemsList = _attributeItemsServices.GetMasterAttributeItems(attributeId);
                return JsonResponse.Create(HttpStatusCode.OK, MasterAttributeItemsList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetMasterAttributeItems " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Authorize]
        [Route("getbyitemid")]
        public IActionResult GetMasterAttributeItemsById(int attributeItemId)
        {
            try
            {
                var MasterAttributeItemsList = _attributeItemsServices.GetMasterAttributeItemById(attributeItemId);
                return JsonResponse.Create(HttpStatusCode.OK, MasterAttributeItemsList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetMasterAttributeItems " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("getall")]
        public IActionResult GetAllMasterAttributeItems()
        {
            try
            {
                var MasterAttributeItemsList = _attributeItemsServices.GetMasterAllAttributeItems();
                return JsonResponse.Create(HttpStatusCode.OK, MasterAttributeItemsList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetMasterAttributeItems " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("add")]
        [Authorize]
        public IActionResult AddMasterAttributeItems([FromBody] MasterAttributeItemsModel attributeItemsModel)
        {
            if (attributeItemsModel == null) throw new ArgumentNullException(nameof(attributeItemsModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    attributeItemsModel.CreatedBy = tokenUserInformationModel.Name;
                    bool response = _attributeItemsServices.AddMasterAttributeItems(attributeItemsModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside AddMasterAttributeItems " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("update")]
        [Authorize]
        public IActionResult UpdateMasterAttributeItems([FromBody] MasterAttributeItemsModel attributeItemsModel)
        {
            if (attributeItemsModel == null) throw new ArgumentNullException(nameof(attributeItemsModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    attributeItemsModel.ModifiedBy = tokenUserInformationModel.Name;
                    var response = _attributeItemsServices.UpdateMasterAttributeItems(attributeItemsModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateMasterAttributeItems " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("delete")]
        [Authorize]
        public IActionResult DeleteMasterAttributeItems([FromBody] MasterAttributeItemsModel attributeItemsModel)
        {
            if (attributeItemsModel == null) throw new ArgumentNullException(nameof(attributeItemsModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    attributeItemsModel.ModifiedBy = tokenUserInformationModel.Name;

                    var response = _attributeItemsServices.DeleteMasterAttributeItems(attributeItemsModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside DeleteMasterAttributeItems " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("updatestatus")]
        [Authorize]
        public IActionResult UpdateStatusMasterAttributeItems([FromBody] MasterAttributeItemsModel attributeItemsModel)
        {
            if (attributeItemsModel == null) throw new ArgumentNullException(nameof(attributeItemsModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    attributeItemsModel.ModifiedBy = tokenUserInformationModel.Name;

                    var response = _attributeItemsServices.UpdateStatusMasterAttributeItems(attributeItemsModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateStatusMasterAttributeItems " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }


        private string UploadImage(string imageName, string imageBase64String, string imageFormat, string finalUrl)
        {
            try
            {
                var nametoupload = imageName + imageFormat;

                string webRootPath = _hostingEnvironment.ContentRootPath;

                var path = webRootPath + "/Content/MasterAttributeItemsImages/" + Guid.NewGuid() + "/";

                bool exists = Directory.Exists(path);

                if (!exists)
                    Directory.CreateDirectory(path);

                byte[] imageBytes = Convert.FromBase64String(imageBase64String);

                System.IO.File.WriteAllBytes(path + nametoupload, imageBytes);

                finalUrl = path.Replace(webRootPath, "") + nametoupload;
            }
            catch (Exception ex)
            {
            }

            return finalUrl;
        }


    }
}