﻿using System;
using System.IO;
using System.Net;
using Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Repository.Model;
using Services.Interface;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductCategoryController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly ILoggerService _loggerService;
        private readonly IProductCategoryServices _productCategoryServices;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ProductCategoryController(IHostingEnvironment hostingEnvironment, IProductCategoryServices productCategoryServices, IConfiguration config, ILoggerService loggerService)
        {
            _loggerService = loggerService;
            _hostingEnvironment = hostingEnvironment;
            _productCategoryServices = productCategoryServices;
            _config = config;
        }

        [HttpPost]
        [Route("get")]
        public IActionResult GetProductCategory()
        {
            try
            {
                var ProductCategoryList = _productCategoryServices.GetProductCategory();
                return JsonResponse.Create(HttpStatusCode.OK, ProductCategoryList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetProductCategory " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("add")]
        [Authorize]
        public IActionResult AddProductCategory([FromBody] AddUpdateProductCategoryModel productCategoryModel)
        {
            if (productCategoryModel == null) throw new ArgumentNullException(nameof(productCategoryModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    string imageName = "";
                    string imageBase64String = "";
                    string imageFormat = "";
                    string finalUrl = "";

                    if (!string.IsNullOrEmpty(productCategoryModel.ImageBytes))
                    {
                        imageName = "Image";
                        imageBase64String = productCategoryModel.ImageBytes;
                        imageFormat = productCategoryModel.ImageBytesFormat;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productCategoryModel.ImageURL = finalUrl;
                    }
                    productCategoryModel.CreatedBy = tokenUserInformationModel.Name;
                    bool response = _productCategoryServices.AddProductCategory(productCategoryModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside AddProductCategory " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("update")]
        [Authorize]
        public IActionResult UpdateProductCategory([FromBody] AddUpdateProductCategoryModel productCategoryModel)
        {
            if (productCategoryModel == null) throw new ArgumentNullException(nameof(productCategoryModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    string imageName = "";
                    string imageBase64String = "";
                    string imageFormat = "";
                    string finalUrl = "";

                    if (!string.IsNullOrEmpty(productCategoryModel.ImageBytes))
                    {
                        imageName = "Image";
                        imageBase64String = productCategoryModel.ImageBytes;
                        imageFormat = productCategoryModel.ImageBytesFormat;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productCategoryModel.ImageURL = finalUrl;
                    }
                    productCategoryModel.ModifiedBy = tokenUserInformationModel.Name;
                    var response = _productCategoryServices.UpdateProductCategory(productCategoryModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateProductCategory " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("delete")]
        [Authorize]
        public IActionResult DeleteProductCategory([FromBody] ProductCategoryModel productCategoryModel)
        {
            if (productCategoryModel == null) throw new ArgumentNullException(nameof(productCategoryModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    productCategoryModel.ModifiedBy = tokenUserInformationModel.Name;

                    var response = _productCategoryServices.DeleteProductCategory(productCategoryModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside DeleteProductCategory " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("updatestatus")]
        [Authorize]
        public IActionResult UpdateStatusProductCategory([FromBody] ProductCategoryModel productCategoryModel)
        {
            if (productCategoryModel == null) throw new ArgumentNullException(nameof(productCategoryModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    productCategoryModel.ModifiedBy = tokenUserInformationModel.Name;

                    var response = _productCategoryServices.UpdateStatusProductCategory(productCategoryModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateStatusProductCategory " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }


        private string UploadImage(string imageName, string imageBase64String, string imageFormat, string finalUrl)
        {
            try
            {
                var nametoupload = imageName + imageFormat;

                string webRootPath = _hostingEnvironment.ContentRootPath;

                var path = webRootPath + "/Content/ProductCategoryImages/" + Guid.NewGuid() + "/";

                bool exists = Directory.Exists(path);

                if (!exists)
                    Directory.CreateDirectory(path);

                byte[] imageBytes = Convert.FromBase64String(imageBase64String);

                System.IO.File.WriteAllBytes(path + nametoupload, imageBytes);

                finalUrl = path.Replace(webRootPath, "") + nametoupload;
            }
            catch (Exception ex)
            {
            }

            return finalUrl;
        }


    }
}