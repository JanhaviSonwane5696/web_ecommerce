﻿using System;
using System.IO;
using System.Net;
using Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Repository.Model;
using Services.Interface;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly ILoggerService _loggerService;
        private readonly IProductServices _productCategoryServices;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ProductController(IHostingEnvironment hostingEnvironment, IProductServices productCategoryServices, IConfiguration config, ILoggerService loggerService)
        {
            _loggerService = loggerService;
            _productCategoryServices = productCategoryServices;
            _config = config;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost]
        [Route("get")]
        public IActionResult GetProduct()
        {
            try
            {
                var ProductList = _productCategoryServices.GetProduct(1);
                return JsonResponse.Create(HttpStatusCode.OK, ProductList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetProduct " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("getall")]
        [Authorize]
        public IActionResult GetAllProduct()
        {
            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    var response = _productCategoryServices.GetAllProduct(Convert.ToInt32(tokenUserInformationModel.ID));

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside AddProduct " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("getproductbycategoryid")]
        public IActionResult GetProductByCategoryId(int categoryId)
        {
            try
            {
                var ProductList = _productCategoryServices.GetProductByCategoryId(categoryId, 1);
                return JsonResponse.Create(HttpStatusCode.OK, ProductList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetProduct " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }


        [HttpPost]
        [Route("getproductbysubcategoryid")]
        public IActionResult GetProductBySubCategoryId(int subCategoryId)
        {
            try
            {
                var ProductList = _productCategoryServices.GetProductBySubCategoryId(subCategoryId, 1);
                return JsonResponse.Create(HttpStatusCode.OK, ProductList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetProduct " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }


        [HttpPost]
        [Route("getProductByUserid")]
        [Authorize]
        public IActionResult GetProductByUserId(int userId)
        {
            try
            {
                var ProductList = _productCategoryServices.GetProduct(userId);
                return JsonResponse.Create(HttpStatusCode.OK, ProductList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetProduct " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("getpreapprovalproduct")]
        [Authorize]
        public IActionResult GetPreApprovalProduct()
        {
            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                var ProductList = _productCategoryServices.GetPreApprovalProduct(Convert.ToInt32(tokenUserInformationModel.ID));
                return JsonResponse.Create(HttpStatusCode.OK, ProductList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetProduct " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("productAttributes")]
        public IActionResult GetProductAttributes(int ProductId)
        {
            try
            {
                var ProductList = _productCategoryServices.GetProductAttributes(ProductId);
                return JsonResponse.Create(HttpStatusCode.OK, ProductList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetProduct " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("add")]
        [Authorize]
        public IActionResult AddProduct([FromBody] ProductModel productCategoryModel)
        {
            if (productCategoryModel == null) throw new ArgumentNullException(nameof(productCategoryModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    string imageName = "";
                    string imageBase64String = "";
                    string imageFormat = "";
                    string finalUrl = "";

                    #region Images Upload

                    if (!string.IsNullOrEmpty(productCategoryModel.ImageBytes))
                    {
                        imageName = "Image1";
                        imageBase64String = productCategoryModel.ImageBytes;
                        imageFormat = productCategoryModel.ImageBytesFormat;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productCategoryModel.ImageURL = finalUrl;
                    }
                    #endregion
                    if (productCategoryModel.UserId == 0)
                        productCategoryModel.UserId = Convert.ToInt32(tokenUserInformationModel.ID);
                    var response = _productCategoryServices.AddProduct(productCategoryModel, Convert.ToInt32(tokenUserInformationModel.ID), tokenUserInformationModel.Name);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside AddProduct " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("addClone")]
        [Authorize]
        public IActionResult AddProductClone([FromBody] ProductCloneModel productCategoryModel)
        {
            if (productCategoryModel == null) throw new ArgumentNullException(nameof(productCategoryModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    string imageName = "";
                    string imageBase64String = "";
                    string imageFormat = "";
                    string finalUrl = "";

                    #region Images Upload

                    if (!string.IsNullOrEmpty(productCategoryModel.ImageBytes))
                    {
                        imageName = "Image1";
                        imageBase64String = productCategoryModel.ImageBytes;
                        imageFormat = productCategoryModel.ImageBytesFormat;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productCategoryModel.ImageURL = finalUrl;
                    }
                    #endregion
                    if (productCategoryModel.UserId == 0)
                        productCategoryModel.UserId = Convert.ToInt32(tokenUserInformationModel.ID);
                    productCategoryModel.CreatedBy = tokenUserInformationModel.Name;
                    var response = _productCategoryServices.AddProduct(productCategoryModel);

                    return JsonResponse.Create(HttpStatusCode.OK, new
                    {
                        Message = response.Item2,
                        UserId = productCategoryModel.UserId,
                        Status = response.Item1
                    });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside AddProduct " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("update")]
        [Authorize]
        public IActionResult UpdateProduct([FromBody] ProductModel productCategoryModel)
        {
            if (productCategoryModel == null) throw new ArgumentNullException(nameof(productCategoryModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    string imageName = "";
                    string imageBase64String = "";
                    string imageFormat = "";
                    string finalUrl = "";

                    #region Images Upload

                    if (!string.IsNullOrEmpty(productCategoryModel.ImageBytes))
                    {
                        imageName = "Image1";
                        imageBase64String = productCategoryModel.ImageBytes;
                        imageFormat = productCategoryModel.ImageBytesFormat;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productCategoryModel.ImageURL = finalUrl;
                    }
                    #endregion
                    if (productCategoryModel.UserId == 0)
                        productCategoryModel.UserId = Convert.ToInt32(tokenUserInformationModel.ID);
                    var response = _productCategoryServices.UpdateProduct(productCategoryModel, Convert.ToInt32(tokenUserInformationModel.ID), tokenUserInformationModel.Name);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateProduct " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("delete")]
        [Authorize]
        public IActionResult DeleteProduct([FromBody] ProductModel productCategoryModel)
        {
            if (productCategoryModel == null) throw new ArgumentNullException(nameof(productCategoryModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    var response = _productCategoryServices.DeleteProduct(productCategoryModel, Convert.ToInt32(tokenUserInformationModel.ID), tokenUserInformationModel.Name);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside DeleteProduct " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("updatestatus")]
        [Authorize]
        public IActionResult UpdateStatusProduct([FromBody] ProductModel productCategoryModel)
        {
            if (productCategoryModel == null) throw new ArgumentNullException(nameof(productCategoryModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    var response = _productCategoryServices.UpdateStatusProduct(productCategoryModel, Convert.ToInt32(tokenUserInformationModel.ID), tokenUserInformationModel.Name);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateStatusProduct " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("updateproductstatus")]
        [Authorize]
        public IActionResult UpdateProductStatus([FromBody] ProductUpdateModel model)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    model.ModifiedBy = tokenUserInformationModel.Name;
                    model.UserId = Convert.ToInt32(tokenUserInformationModel.ID);
                    var response = _productCategoryServices.UpdateStatusProduct(model);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateStatusProduct " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        private string UploadImage(string imageName, string imageBase64String, string imageFormat, string finalUrl)
        {
            try
            {
                var nametoupload = imageName + imageFormat;

                string webRootPath = _hostingEnvironment.ContentRootPath;

                var path = webRootPath + "/Content/ProductsImages/" + Guid.NewGuid() + "/";

                bool exists = Directory.Exists(path);

                if (!exists)
                    Directory.CreateDirectory(path);

                byte[] imageBytes = Convert.FromBase64String(imageBase64String);

                System.IO.File.WriteAllBytes(path + nametoupload, imageBytes);

                finalUrl = path.Replace(webRootPath, "") + nametoupload;
            }
            catch (Exception)
            {
            }

            return finalUrl;
        }
    }
}