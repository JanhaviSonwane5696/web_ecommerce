﻿using System;
using System.IO;
using System.Net;
using Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Repository.Model;
using Services.Interface;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductItemController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly ILoggerService _loggerService;
        private readonly IProductItemServices _productItemCategoryServices;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ProductItemController(IHostingEnvironment hostingEnvironment, IProductItemServices productItemCategoryServices, IConfiguration config, ILoggerService loggerService)
        {
            _loggerService = loggerService;
            _hostingEnvironment = hostingEnvironment;
            _productItemCategoryServices = productItemCategoryServices;
            _config = config;
        }

        [HttpPost]
        [Route("get")]
        public IActionResult GetProductItems(int productId)
        {
            try
            {
                var ProductItemList = _productItemCategoryServices.GetProductItems(productId);
                return JsonResponse.Create(HttpStatusCode.OK, ProductItemList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetProductItem " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("getItem")]
        public IActionResult GetProductItem(int productItemId)
        {
            try
            {
                var ProductItemList = _productItemCategoryServices.GetProductItem(productItemId);
                return JsonResponse.Create(HttpStatusCode.OK, ProductItemList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetProductItem " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("getbyproductid")]
        public IActionResult GetProductItemsByProductId(int productId)
        {
            try
            {
                var ProductItemList = _productItemCategoryServices.GetProductItemsByProductId(productId);
                return JsonResponse.Create(HttpStatusCode.OK, ProductItemList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetProductItem " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("getpreapprovalproduct")]
        [Authorize]
        public IActionResult GetPreApprovalProduct(int productId)
        {
            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    int userId = Convert.ToInt32(tokenUserInformationModel.ID);
                    var ProductList = _productItemCategoryServices.GetPreApprovalProductItem(userId, productId);
                    return JsonResponse.Create(HttpStatusCode.OK, ProductList);
                }
                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetProduct " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("productItemsAttributes")]
        public IActionResult GetProductItemsAttributes(int productId)
        {
            try
            {
                var ProductItemList = _productItemCategoryServices.GetProductItemAttributeList(productId);
                return JsonResponse.Create(HttpStatusCode.OK, ProductItemList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetProductItem " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("productItemsList")]
        public IActionResult ProductItemsShortList(int productId)
        {
            try
            {
                var ProductItemList = _productItemCategoryServices.GetProductItemShortList(productId);
                return JsonResponse.Create(HttpStatusCode.OK, ProductItemList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetProductItem " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("add")]
        [Authorize]
        public IActionResult AddProductItem([FromBody] AddProductItemModel productItemCategoryModel)
        {
            if (productItemCategoryModel == null) throw new ArgumentNullException(nameof(productItemCategoryModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    string imageName = "";
                    string imageBase64String = "";
                    string imageFormat = "";
                    string finalUrl = "";

                    #region Images Upload

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageBytes1))
                    {
                        imageName = "Image1";
                        imageBase64String = productItemCategoryModel.ImageBytes1;
                        imageFormat = productItemCategoryModel.ImageBytes1Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageURL1 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageThumbBytes1))
                    {
                        imageName = "ImageThumb1";
                        imageBase64String = productItemCategoryModel.ImageThumbBytes1;
                        imageFormat = productItemCategoryModel.ImageThumbBytes1Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageThumbURL1 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageBytes2))
                    {
                        imageName = "Image2";
                        imageBase64String = productItemCategoryModel.ImageBytes2;
                        imageFormat = productItemCategoryModel.ImageBytes2Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageURL2 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageThumbBytes2))
                    {
                        imageName = "ImageThumb2";
                        imageBase64String = productItemCategoryModel.ImageThumbBytes2;
                        imageFormat = productItemCategoryModel.ImageThumbBytes2Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageThumbURL2 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageBytes3))
                    {
                        imageName = "Image3";
                        imageBase64String = productItemCategoryModel.ImageBytes3;
                        imageFormat = productItemCategoryModel.ImageBytes3Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageURL3 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageThumbBytes3))
                    {
                        imageName = "ImageThumb3";
                        imageBase64String = productItemCategoryModel.ImageThumbBytes3;
                        imageFormat = productItemCategoryModel.ImageThumbBytes3Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageThumbURL3 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageBytes4))
                    {
                        imageName = "Image4";
                        imageBase64String = productItemCategoryModel.ImageBytes4;
                        imageFormat = productItemCategoryModel.ImageBytes4Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageURL4 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageThumbBytes4))
                    {
                        imageName = "ImageThumb4";
                        imageBase64String = productItemCategoryModel.ImageThumbBytes4;
                        imageFormat = productItemCategoryModel.ImageThumbBytes4Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageThumbURL4 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageBytes5))
                    {
                        imageName = "Image5";
                        imageBase64String = productItemCategoryModel.ImageBytes5;
                        imageFormat = productItemCategoryModel.ImageBytes5Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageURL5 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageThumbBytes5))
                    {
                        imageName = "ImageThumb5";
                        imageBase64String = productItemCategoryModel.ImageThumbBytes5;
                        imageFormat = productItemCategoryModel.ImageThumbBytes5Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageThumbURL5 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageBytes6))
                    {
                        imageName = "Image6";
                        imageBase64String = productItemCategoryModel.ImageBytes6;
                        imageFormat = productItemCategoryModel.ImageBytes6Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageURL6 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageThumbBytes6))
                    {
                        imageName = "ImageThumb6";
                        imageBase64String = productItemCategoryModel.ImageThumbBytes6;
                        imageFormat = productItemCategoryModel.ImageThumbBytes6Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageThumbURL6 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageBytes7))
                    {
                        imageName = "Image7";
                        imageBase64String = productItemCategoryModel.ImageBytes7;
                        imageFormat = productItemCategoryModel.ImageBytes7Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageURL7 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageThumbBytes7))
                    {
                        imageName = "ImageThumb7";
                        imageBase64String = productItemCategoryModel.ImageThumbBytes7;
                        imageFormat = productItemCategoryModel.ImageThumbBytes7Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageThumbURL7 = finalUrl;
                    }

                    #endregion
                    productItemCategoryModel.UserID = Convert.ToInt32(tokenUserInformationModel.ID);
                    var response = _productItemCategoryServices.AddProductItem(productItemCategoryModel, tokenUserInformationModel.Name);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside AddProductItem " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("update")]
        [Authorize]
        public IActionResult UpdateProductItem([FromBody] AddProductItemModel productItemCategoryModel)
        {
            if (productItemCategoryModel == null) throw new ArgumentNullException(nameof(productItemCategoryModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    string imageName = "";
                    string imageBase64String = "";
                    string imageFormat = "";
                    string finalUrl = "";

                    #region Images Upload

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageBytes1))
                    {
                        imageName = "Image1";
                        imageBase64String = productItemCategoryModel.ImageBytes1;
                        imageFormat = productItemCategoryModel.ImageBytes1Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageURL1 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageThumbBytes1))
                    {
                        imageName = "ImageThumb1";
                        imageBase64String = productItemCategoryModel.ImageThumbBytes1;
                        imageFormat = productItemCategoryModel.ImageThumbBytes1Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageThumbURL1 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageBytes2))
                    {
                        imageName = "Image2";
                        imageBase64String = productItemCategoryModel.ImageBytes2;
                        imageFormat = productItemCategoryModel.ImageBytes2Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageURL2 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageThumbBytes2))
                    {
                        imageName = "ImageThumb2";
                        imageBase64String = productItemCategoryModel.ImageThumbBytes2;
                        imageFormat = productItemCategoryModel.ImageThumbBytes2Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageThumbURL2 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageBytes3))
                    {
                        imageName = "Image3";
                        imageBase64String = productItemCategoryModel.ImageBytes3;
                        imageFormat = productItemCategoryModel.ImageBytes3Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageURL3 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageThumbBytes3))
                    {
                        imageName = "ImageThumb3";
                        imageBase64String = productItemCategoryModel.ImageThumbBytes3;
                        imageFormat = productItemCategoryModel.ImageThumbBytes3Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageThumbURL3 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageBytes4))
                    {
                        imageName = "Image4";
                        imageBase64String = productItemCategoryModel.ImageBytes4;
                        imageFormat = productItemCategoryModel.ImageBytes4Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageURL4 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageThumbBytes4))
                    {
                        imageName = "ImageThumb4";
                        imageBase64String = productItemCategoryModel.ImageThumbBytes4;
                        imageFormat = productItemCategoryModel.ImageThumbBytes4Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageThumbURL4 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageBytes5))
                    {
                        imageName = "Image5";
                        imageBase64String = productItemCategoryModel.ImageBytes5;
                        imageFormat = productItemCategoryModel.ImageBytes5Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageURL5 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageThumbBytes5))
                    {
                        imageName = "ImageThumb5";
                        imageBase64String = productItemCategoryModel.ImageThumbBytes5;
                        imageFormat = productItemCategoryModel.ImageThumbBytes5Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageThumbURL5 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageBytes6))
                    {
                        imageName = "Image6";
                        imageBase64String = productItemCategoryModel.ImageBytes6;
                        imageFormat = productItemCategoryModel.ImageBytes6Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageURL6 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageThumbBytes6))
                    {
                        imageName = "ImageThumb6";
                        imageBase64String = productItemCategoryModel.ImageThumbBytes6;
                        imageFormat = productItemCategoryModel.ImageThumbBytes6Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageThumbURL6 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageBytes7))
                    {
                        imageName = "Image7";
                        imageBase64String = productItemCategoryModel.ImageBytes7;
                        imageFormat = productItemCategoryModel.ImageBytes7Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageURL7 = finalUrl;
                    }

                    if (!string.IsNullOrEmpty(productItemCategoryModel.ImageThumbBytes7))
                    {
                        imageName = "ImageThumb7";
                        imageBase64String = productItemCategoryModel.ImageThumbBytes7;
                        imageFormat = productItemCategoryModel.ImageThumbBytes7Format;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productItemCategoryModel.ImageThumbURL7 = finalUrl;
                    }

                    #endregion

                    var response = _productItemCategoryServices.UpdateProductItem(productItemCategoryModel, tokenUserInformationModel.Name);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateProductItem " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("delete")]
        [Authorize]
        public IActionResult DeleteProductItem([FromBody] ProductItemModel productItemCategoryModel)
        {
            if (productItemCategoryModel == null) throw new ArgumentNullException(nameof(productItemCategoryModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    var response = _productItemCategoryServices.DeleteProductItem(productItemCategoryModel, tokenUserInformationModel.Name);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside DeleteProductItem " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("updatestatus")]
        [Authorize]
        public IActionResult UpdateStatusProductItem([FromBody] ProductItemModel productItemCategoryModel)
        {
            if (productItemCategoryModel == null) throw new ArgumentNullException(nameof(productItemCategoryModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    var response = _productItemCategoryServices.UpdateStatusProductItem(productItemCategoryModel, tokenUserInformationModel.Name);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateStatusProductItem " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("updateproductstatus")]
        [Authorize]
        public IActionResult UpdateProductStatus([FromBody] UpdateProductItemModel model)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    model.ModifiedBy = tokenUserInformationModel.Name;
                    model.UserId = Convert.ToInt32(tokenUserInformationModel.ID);
                    var response = _productItemCategoryServices.UpdateProductItemStatus(model);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateStatusProduct " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        private string UploadImage(string imageName, string imageBase64String, string imageFormat, string finalUrl)
        {
            try
            {
                var nametoupload = imageName + imageFormat;

                string webRootPath = _hostingEnvironment.ContentRootPath;

                var path = webRootPath + "/Content/ProductImages/" + Guid.NewGuid() + "/";

                bool exists = Directory.Exists(path);

                if (!exists)
                    Directory.CreateDirectory(path);

                byte[] imageBytes = Convert.FromBase64String(imageBase64String);

                System.IO.File.WriteAllBytes(path + nametoupload, imageBytes);

                finalUrl = path.Replace(webRootPath, "") + nametoupload;
            }
            catch (Exception ex)
            {
            }

            return finalUrl;
        }

    }
}