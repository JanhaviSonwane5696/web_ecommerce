﻿using System;
using System.Net;
using Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Repository.Model;
using Services.Interface;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShoppingCartController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly ILoggerService _loggerService;
        private readonly IShoppingCartServices _shoppingCartServices;

        public ShoppingCartController(IShoppingCartServices shoppingCartServices, IConfiguration config, ILoggerService loggerService)
        {
            _loggerService = loggerService;
            _shoppingCartServices = shoppingCartServices;
            _config = config;
        }

        [HttpPost]
        [Route("get")]
        public IActionResult GetShoppingCart(string guid = "", int userId = 0)
        {
            try
            {
                var ShoppingCartList = _shoppingCartServices.GetCartDetails(guid, userId);
                return JsonResponse.Create(HttpStatusCode.OK, ShoppingCartList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetShoppingCart " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("add")]
        public IActionResult AddShoppingCart([FromBody] ShoppingCartModel ShoppingCartModel)
        {
            if (ShoppingCartModel == null) throw new ArgumentNullException(nameof(ShoppingCartModel));

            try
            {
                if (ModelState.IsValid)
                {
                    var response = _shoppingCartServices.AddShoppingCart(ShoppingCartModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside AddShoppingCart " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("delete")]
        public IActionResult DeleteShoppingCart(int cartItemID)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var response = _shoppingCartServices.DeleteProductCartItem(cartItemID);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside DeleteShoppingCart " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("update")]
        public IActionResult UpdateShoppingCart(string guid, int userId)
        {
            try
            {
                var ShoppingCartList = _shoppingCartServices.UpdateProductCartItem(guid, userId);
                return JsonResponse.Create(HttpStatusCode.OK, ShoppingCartList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetShoppingCart " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }
    }
}