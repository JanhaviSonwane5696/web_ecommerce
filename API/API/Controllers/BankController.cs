﻿using Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Services.Interface;
using System;
using System.Net;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BankController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly ILoggerService _loggerService;
        private readonly IBanksServices _bankServices;

        public BankController(IBanksServices bankServices, IConfiguration config, ILoggerService loggerService)
        {
            _loggerService = loggerService;
            _bankServices = bankServices;
            _config = config;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("get")]
        public IActionResult Get()
        {
            try
            {
                var status = _bankServices.BankNameList();

                if (status != null)
                {
                    return JsonResponse.Create(HttpStatusCode.OK, status
                    );
                }

                return JsonResponse.Create(HttpStatusCode.NotFound, null);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("bankState")]
        public IActionResult BankState([FromBody] BankStateModel addData)
        {
            if (addData == null) throw new ArgumentNullException(nameof(addData));
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _bankServices.BankStateList(addData);
                    if (status.Count > 0)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, status);
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound, null);
                }
                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("bankdistrict")]
        public IActionResult BankDistrict([FromBody] BankDistrictModel addData)
        {
            if (addData == null) throw new ArgumentNullException(nameof(addData));
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _bankServices.BankDistrictList(addData);
                    if (status.Count > 0)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, status);
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound, null);
                }
                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("bankdistrictbranch")]
        public IActionResult BankDistrictBranch([FromBody] BankBranchModel addData)
        {
            if (addData == null) throw new ArgumentNullException(nameof(addData));
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _bankServices.BankBranchList(addData);
                    if (status.Count > 0)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, status);
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound, null);
                }
                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("bankdistrictbranchdetails")]
        public IActionResult BankDistrictBranchDetails([FromBody] BankBranchDetailsModel addData)
        {
            if (addData == null) throw new ArgumentNullException(nameof(addData));
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _bankServices.BankDetails(addData);
                    if (status!=null)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, status);
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound, null);
                }
                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("findbankdetailsbyifsc")]
        public IActionResult FindBankDetailsbyIfsc([FromBody] BankDetailsModel addData)
        {
            if (addData == null) throw new ArgumentNullException(nameof(addData));
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _bankServices.BankIfscDetails(addData);
                    if (status != null)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, status);
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound, null);
                }
                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

    }
}
