﻿using System;
using System.Net;
using Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Repository.Model;
using Services.Interface;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductTagsController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly ILoggerService _loggerService;
        private readonly IProductTagsServices _productTagsServices;

        public ProductTagsController(IProductTagsServices productTagsServices, IConfiguration config, ILoggerService loggerService)
        {
            _loggerService = loggerService;
            _productTagsServices = productTagsServices;
            _config = config;
        }

        [HttpPost]
        [Route("get")]
        public IActionResult GetProductTags()
        {
            try
            {
                var ProductTagsList = _productTagsServices.GetProductTags();
                return JsonResponse.Create(HttpStatusCode.OK, ProductTagsList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetProductTags " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("add")]
        [Authorize]
        public IActionResult AddProductTags([FromBody] ProductTagsModel productTagsModel)
        {
            if (productTagsModel == null) throw new ArgumentNullException(nameof(productTagsModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    productTagsModel.CreatedBy = tokenUserInformationModel.Name;
                    var response = _productTagsServices.AddProductTags(productTagsModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside AddProductTags " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("update")]
        [Authorize]
        public IActionResult UpdateProductTags([FromBody] ProductTagsModel productTagsModel)
        {
            if (productTagsModel == null) throw new ArgumentNullException(nameof(productTagsModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    productTagsModel.ModifiedBy = tokenUserInformationModel.Name;
                    var response = _productTagsServices.UpdateProductTags(productTagsModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateProductTags " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("delete")]
        [Authorize]
        public IActionResult DeleteProductTags([FromBody] ProductTagsModel productTagsModel)
        {
            if (productTagsModel == null) throw new ArgumentNullException(nameof(productTagsModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    productTagsModel.ModifiedBy = tokenUserInformationModel.Name;
                    var response = _productTagsServices.DeleteProductTags(productTagsModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside DeleteProductTags " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("updatestatus")]
        [Authorize]
        public IActionResult UpdateStatusProductTags([FromBody] ProductTagsModel productTagsModel)
        {
            if (productTagsModel == null) throw new ArgumentNullException(nameof(productTagsModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    productTagsModel.ModifiedBy = tokenUserInformationModel.Name;
                    var response = _productTagsServices.UpdateStatusProductTags(productTagsModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateStatusProductTags " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }
    }
}