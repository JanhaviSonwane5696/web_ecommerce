﻿using System;
using System.IO;
using System.Net;
using Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Repository.Model;
using Services.Interface;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterBrandsController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly ILoggerService _loggerService;
        private readonly IMasterBrandsServices _BrandsServices;
        private readonly IHostingEnvironment _hostingEnvironment;

        public MasterBrandsController(IHostingEnvironment hostingEnvironment, IMasterBrandsServices BrandsServices, IConfiguration config, ILoggerService loggerService)
        {
            _loggerService = loggerService;
            _hostingEnvironment = hostingEnvironment;
            _BrandsServices = BrandsServices;
            _config = config;
        }

        [HttpPost]
        [Route("get")]
        public IActionResult GetMasterBrands(int BrandsId)
        {
            try
            {
                var MasterBrandsList = _BrandsServices.GetMasterBrands(BrandsId);
                return JsonResponse.Create(HttpStatusCode.OK, MasterBrandsList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetMasterBrands " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("getall")]
        public IActionResult GetAllMasterBrands()
        {
            try
            {
                var MasterBrandsList = _BrandsServices.GetMasterAllBrands();
                return JsonResponse.Create(HttpStatusCode.OK, MasterBrandsList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetMasterBrands " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("add")]
        [Authorize]
        public IActionResult AddMasterBrands([FromBody] AddMasterBrandsModel BrandsModel)
        {
            if (BrandsModel == null) throw new ArgumentNullException(nameof(BrandsModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    string imageName = "";
                    string imageBase64String = "";
                    string imageFormat = "";
                    string finalUrl = "";

                    if (!string.IsNullOrEmpty(BrandsModel.BrandsLogoBytes))
                    {
                        imageName = "Image";
                        imageBase64String = BrandsModel.BrandsLogoBytes;
                        imageFormat = BrandsModel.BrandsLogoFormat;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        BrandsModel.BrandsLogoURL = finalUrl;
                    }

                    BrandsModel.CreatedBy = tokenUserInformationModel.Name;
                    bool response = _BrandsServices.AddMasterBrands(BrandsModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside AddMasterBrands " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("update")]
        [Authorize]
        public IActionResult UpdateMasterBrands([FromBody] AddMasterBrandsModel BrandsModel)
        {
            if (BrandsModel == null) throw new ArgumentNullException(nameof(BrandsModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    string imageName = "";
                    string imageBase64String = "";
                    string imageFormat = "";
                    string finalUrl = "";

                    if (!string.IsNullOrEmpty(BrandsModel.BrandsLogoBytes))
                    {
                        imageName = "Image";
                        imageBase64String = BrandsModel.BrandsLogoBytes;
                        imageFormat = BrandsModel.BrandsLogoFormat;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        BrandsModel.BrandsLogoURL = finalUrl;
                    }
                    BrandsModel.ModifiedBy = tokenUserInformationModel.Name;
                    var response = _BrandsServices.UpdateMasterBrands(BrandsModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateMasterBrands " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("delete")]
        [Authorize]
        public IActionResult DeleteMasterBrands([FromBody] AddMasterBrandsModel BrandsModel)
        {
            if (BrandsModel == null) throw new ArgumentNullException(nameof(BrandsModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    BrandsModel.ModifiedBy = tokenUserInformationModel.Name;

                    var response = _BrandsServices.DeleteMasterBrands(BrandsModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside DeleteMasterBrands " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("updatestatus")]
        [Authorize]
        public IActionResult UpdateStatusMasterBrands([FromBody] AddMasterBrandsModel BrandsModel)
        {
            if (BrandsModel == null) throw new ArgumentNullException(nameof(BrandsModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    BrandsModel.ModifiedBy = tokenUserInformationModel.Name;

                    var response = _BrandsServices.UpdateStatusMasterBrands(BrandsModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateStatusMasterBrands " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        private string UploadImage(string imageName, string imageBase64String, string imageFormat, string finalUrl)
        {
            try
            {
                var nametoupload = imageName + imageFormat;

                string webRootPath = _hostingEnvironment.ContentRootPath;

                var path = webRootPath + "/Content/Content/MasterBrandsImages/" + Guid.NewGuid() + "/";

                bool exists = Directory.Exists(path);

                if (!exists)
                    Directory.CreateDirectory(path);

                byte[] imageBytes = Convert.FromBase64String(imageBase64String);

                System.IO.File.WriteAllBytes(path + nametoupload, imageBytes);

                finalUrl = path.Replace(webRootPath, "") + nametoupload;
            }
            catch (Exception ex)
            {
            }

            return finalUrl;
        }
    }
}