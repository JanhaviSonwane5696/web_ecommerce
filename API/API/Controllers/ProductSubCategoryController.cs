﻿using System;
using System.IO;
using System.Net;
using Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Repository.Model;
using Services.Interface;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductSubCategoryController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly ILoggerService _loggerService;
        private readonly IProductSubCategoryServices _productSubCategoryServices;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ProductSubCategoryController(IHostingEnvironment hostingEnvironment, IProductSubCategoryServices ProductSubCategoryServices, IConfiguration config, ILoggerService loggerService)
        {
            _loggerService = loggerService;
            _hostingEnvironment = hostingEnvironment;
            _productSubCategoryServices = ProductSubCategoryServices;
            _config = config;
        }

        [HttpPost]
        [Route("get")]
        public IActionResult GetProductSubCategory()
        {
            try
            {
                var ProductSubCategoryList = _productSubCategoryServices.GetProductSubCategory();
                return JsonResponse.Create(HttpStatusCode.OK, ProductSubCategoryList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetProductSubCategory " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("getall")]
        public IActionResult GetAllProductSubCategory()
        {
            try
            {
                var ProductSubCategoryList = _productSubCategoryServices.GetAllProductSubCategory();
                return JsonResponse.Create(HttpStatusCode.OK, ProductSubCategoryList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetProductSubCategory " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("getproductsubcategorybycategoryid")]
        public IActionResult GetProductSubCategoryByCategoryId(int productCategoryId)
        {
            try
            {
                var ProductSubCategoryList = _productSubCategoryServices.GetProductSubCategoryByCategoryId(productCategoryId);
                return JsonResponse.Create(HttpStatusCode.OK, ProductSubCategoryList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetProductSubCategory " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("add")]
        [Authorize]
        public IActionResult AddProductSubCategory([FromBody] AddUpdateProductSubCategoryModel productSubCategoryModel)
        {
            if (productSubCategoryModel == null) throw new ArgumentNullException(nameof(productSubCategoryModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    string imageName = "";
                    string imageBase64String = "";
                    string imageFormat = "";
                    string finalUrl = "";

                    if (!string.IsNullOrEmpty(productSubCategoryModel.ImageBytes))
                    {
                        imageName = "Image";
                        imageBase64String = productSubCategoryModel.ImageBytes;
                        imageFormat = productSubCategoryModel.ImageBytesFormat;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productSubCategoryModel.ImageURL = finalUrl;
                    }
                    productSubCategoryModel.CreatedBy = tokenUserInformationModel.Name;

                    var response = _productSubCategoryServices.AddProductSubCategory(productSubCategoryModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside AddProductSubCategory " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("update")]
        [Authorize]
        public IActionResult UpdateProductSubCategory([FromBody] AddUpdateProductSubCategoryModel productSubCategoryModel)
        {
            if (productSubCategoryModel == null) throw new ArgumentNullException(nameof(productSubCategoryModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    string imageName = "";
                    string imageBase64String = "";
                    string imageFormat = "";
                    string finalUrl = "";

                    if (!string.IsNullOrEmpty(productSubCategoryModel.ImageBytes))
                    {
                        imageName = "Image";
                        imageBase64String = productSubCategoryModel.ImageBytes;
                        imageFormat = productSubCategoryModel.ImageBytesFormat;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        productSubCategoryModel.ImageURL = finalUrl;
                    }
                    productSubCategoryModel.ModifiedBy = tokenUserInformationModel.Name;

                    var response = _productSubCategoryServices.UpdateProductSubCategory(productSubCategoryModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateProductSubCategory " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("delete")]
        [Authorize]
        public IActionResult DeleteProductSubCategory([FromBody] ProductSubCategoryModel productSubCategoryModel)
        {
            if (productSubCategoryModel == null) throw new ArgumentNullException(nameof(productSubCategoryModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    productSubCategoryModel.ModifiedBy = tokenUserInformationModel.Name;

                    var response = _productSubCategoryServices.DeleteProductSubCategory(productSubCategoryModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside DeleteProductSubCategory " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("updatestatus")]
        [Authorize]
        public IActionResult UpdateStatusProductSubCategory([FromBody] ProductSubCategoryModel productSubCategoryModel)
        {
            if (productSubCategoryModel == null) throw new ArgumentNullException(nameof(productSubCategoryModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    productSubCategoryModel.ModifiedBy = tokenUserInformationModel.Name;

                    var response = _productSubCategoryServices.UpdateStatusProductSubCategory(productSubCategoryModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateStatusProductSubCategory " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }



        private string UploadImage(string imageName, string imageBase64String, string imageFormat, string finalUrl)
        {
            try
            {
                var nametoupload = imageName + imageFormat;

                string webRootPath = _hostingEnvironment.ContentRootPath;

                var path = webRootPath + "/Content/ProductSubCategoryImages/" + Guid.NewGuid() + "/";

                bool exists = Directory.Exists(path);

                if (!exists)
                    Directory.CreateDirectory(path);

                byte[] imageBytes = Convert.FromBase64String(imageBase64String);

                System.IO.File.WriteAllBytes(path + nametoupload, imageBytes);

                finalUrl = path.Replace(webRootPath, "") + nametoupload;
            }
            catch (Exception ex)
            {
            }

            return finalUrl;
        }


    }
}