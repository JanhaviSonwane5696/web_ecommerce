﻿using Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Services.Interface;
using System;
using System.Net;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserAddressController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly ILoggerService _loggerService;
        private readonly IUserAddressServices _userAddressServices;
        private readonly IHostingEnvironment _hostingEnvironment;

        public UserAddressController(IHostingEnvironment hostingEnvironment, IUserAddressServices userAddressServices, IConfiguration config, ILoggerService loggerService)
        {
            _loggerService = loggerService;
            _hostingEnvironment = hostingEnvironment;
            _userAddressServices = userAddressServices;
            _config = config;
        }

        [HttpPost]
        [Route("get")]
        [Authorize]
        public IActionResult GetUserAddress()
        {
            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    int userId = Convert.ToInt32(tokenUserInformationModel.ID);
                    var response = _userAddressServices.GetUserAddress(userId);
                    if (response.Count > 0)
                        return JsonResponse.Create(HttpStatusCode.OK, response);
                    else
                        return JsonResponse.Create(HttpStatusCode.NotFound, "Not Found");
                }
                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside AddMasterAttribute " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("add")]
        [Authorize]
        public IActionResult AddUserAddress([FromBody] AddUserAddressModel model)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    model.CreatedBy = tokenUserInformationModel.Name;
                    model.UserId = Convert.ToInt32(tokenUserInformationModel.ID);
                    var response = _userAddressServices.AddUserAddress(model);
                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside AddMasterAttribute " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("update")]
        [Authorize]
        public IActionResult UpdateUserAddress([FromBody] UpdateUserAddressModel model)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    model.ModifiedBy = tokenUserInformationModel.Name;
                    model.UserId = Convert.ToInt32(tokenUserInformationModel.ID);
                    var response = _userAddressServices.UpdateUserAddress(model);
                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside AddMasterAttribute " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("delete")]
        [Authorize]
        public IActionResult DeleteUserAddress([FromBody] DeleteUserAddressModel model)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    model.ModifiedBy = tokenUserInformationModel.Name;
                    model.UserId = Convert.ToInt32(tokenUserInformationModel.ID);
                    var response = _userAddressServices.DeleteUserAddress(model);
                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside AddMasterAttribute " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }
    }
}
