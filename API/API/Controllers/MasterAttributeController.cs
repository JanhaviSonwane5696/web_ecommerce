﻿using System;
using System.IO;
using System.Net;
using Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Repository.Model;
using Services.Interface;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterAttributeController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly ILoggerService _loggerService;
        private readonly IMasterAttributeServices _attributeServices;
        private readonly IHostingEnvironment _hostingEnvironment;

        public MasterAttributeController(IHostingEnvironment hostingEnvironment, IMasterAttributeServices attributeServices, IConfiguration config, ILoggerService loggerService)
        {
            _loggerService = loggerService;
            _hostingEnvironment = hostingEnvironment;
            _attributeServices = attributeServices;
            _config = config;
        }

        [HttpPost]
        [Route("get")]
        public IActionResult GetMasterAttribute(int attributeId)
        {
            try
            {
                var MasterAttributeList = _attributeServices.GetMasterAttribute(attributeId);
                return JsonResponse.Create(HttpStatusCode.OK, MasterAttributeList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetMasterAttribute " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("getall")]
        public IActionResult GetAllMasterAttribute()
        {
            try
            {
                var MasterAttributeList = _attributeServices.GetMasterAllAttribute();
                return JsonResponse.Create(HttpStatusCode.OK, MasterAttributeList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetMasterAttribute " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("add")]
        [Authorize]
        public IActionResult AddMasterAttribute([FromBody] MasterAttributeModel attributeModel)
        {
            if (attributeModel == null) throw new ArgumentNullException(nameof(attributeModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    attributeModel.CreatedBy = tokenUserInformationModel.Name;
                    bool response = _attributeServices.AddMasterAttribute(attributeModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside AddMasterAttribute " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("update")]
        [Authorize]
        public IActionResult UpdateMasterAttribute([FromBody] MasterAttributeModel attributeModel)
        {
            if (attributeModel == null) throw new ArgumentNullException(nameof(attributeModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    attributeModel.ModifiedBy = tokenUserInformationModel.Name;
                    var response = _attributeServices.UpdateMasterAttribute(attributeModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateMasterAttribute " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("delete")]
        [Authorize]
        public IActionResult DeleteMasterAttribute([FromBody] MasterAttributeModel attributeModel)
        {
            if (attributeModel == null) throw new ArgumentNullException(nameof(attributeModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    attributeModel.ModifiedBy = tokenUserInformationModel.Name;

                    var response = _attributeServices.DeleteMasterAttribute(attributeModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside DeleteMasterAttribute " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("updatestatus")]
        [Authorize]
        public IActionResult UpdateStatusMasterAttribute([FromBody] MasterAttributeModel attributeModel)
        {
            if (attributeModel == null) throw new ArgumentNullException(nameof(attributeModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    attributeModel.ModifiedBy = tokenUserInformationModel.Name;

                    var response = _attributeServices.UpdateStatusMasterAttribute(attributeModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateStatusMasterAttribute " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }


        private string UploadImage(string imageName, string imageBase64String, string imageFormat, string finalUrl)
        {
            try
            {
                var nametoupload = imageName + imageFormat;

                string webRootPath = _hostingEnvironment.ContentRootPath;

                var path = webRootPath + "/Content/MasterAttributeImages/" + Guid.NewGuid() + "/";

                bool exists = Directory.Exists(path);

                if (!exists)
                    Directory.CreateDirectory(path);

                byte[] imageBytes = Convert.FromBase64String(imageBase64String);

                System.IO.File.WriteAllBytes(path + nametoupload, imageBytes);

                finalUrl = path.Replace(webRootPath, "") + nametoupload;
            }
            catch (Exception ex)
            {
            }

            return finalUrl;
        }


    }
}