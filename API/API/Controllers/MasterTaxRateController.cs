﻿using System;
using System.IO;
using System.Net;
using Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Repository.Model;
using Services.Interface;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterTaxRateController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly ILoggerService _loggerService;
        private readonly IMasterTaxRateServices _taxRateServices;
        private readonly IHostingEnvironment _hostingEnvironment;

        public MasterTaxRateController(IHostingEnvironment hostingEnvironment, IMasterTaxRateServices taxRateServices, IConfiguration config, ILoggerService loggerService)
        {
            _loggerService = loggerService;
            _hostingEnvironment = hostingEnvironment;
            _taxRateServices = taxRateServices;
            _config = config;
        }

        [HttpPost]
        [Route("get")]
        public IActionResult GetMasterTaxRate(int taxRateId)
        {
            try
            {
                var MasterTaxRateList = _taxRateServices.GetMasterTaxRate(taxRateId);
                return JsonResponse.Create(HttpStatusCode.OK, MasterTaxRateList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetMasterTaxRate " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("getall")]
        public IActionResult GetAllMasterTaxRate()
        {
            try
            {
                var MasterTaxRateList = _taxRateServices.GetAllMasterTaxRate();
                return JsonResponse.Create(HttpStatusCode.OK, MasterTaxRateList);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside GetMasterTaxRate " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("add")]
        [Authorize]
        public IActionResult AddMasterTaxRate([FromBody] MasterTaxRatesModel taxRateModel)
        {
            if (taxRateModel == null) throw new ArgumentNullException(nameof(taxRateModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    taxRateModel.CreatedBy = tokenUserInformationModel.Name;
                    bool response = _taxRateServices.AddMasterTaxRate(taxRateModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside AddMasterTaxRate " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("update")]
        [Authorize]
        public IActionResult UpdateMasterTaxRate([FromBody] MasterTaxRatesModel taxRateModel)
        {
            if (taxRateModel == null) throw new ArgumentNullException(nameof(taxRateModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    taxRateModel.ModifiedBy = tokenUserInformationModel.Name;
                    var response = _taxRateServices.UpdateMasterTaxRate(taxRateModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateMasterTaxRate " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("delete")]
        [Authorize]
        public IActionResult DeleteMasterTaxRate([FromBody] MasterTaxRatesModel taxRateModel)
        {
            if (taxRateModel == null) throw new ArgumentNullException(nameof(taxRateModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    taxRateModel.ModifiedBy = tokenUserInformationModel.Name;

                    var response = _taxRateServices.DeleteMasterTaxRate(taxRateModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside DeleteMasterTaxRate " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }

        [HttpPost]
        [Route("updatestatus")]
        [Authorize]
        public IActionResult UpdateStatusMasterTaxRate([FromBody] MasterTaxRatesModel taxRateModel)
        {
            if (taxRateModel == null) throw new ArgumentNullException(nameof(taxRateModel));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    taxRateModel.ModifiedBy = tokenUserInformationModel.Name;

                    var response = _taxRateServices.UpdateStatusMasterTaxRate(taxRateModel);

                    return JsonResponse.Create(HttpStatusCode.OK, response);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside UpdateStatusMasterTaxRate " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured"); //return error occured
            }
        }


        private string UploadImage(string imageName, string imageBase64String, string imageFormat, string finalUrl)
        {
            try
            {
                var nametoupload = imageName + imageFormat;

                string webRootPath = _hostingEnvironment.ContentRootPath;

                var path = webRootPath + "/Content/MasterTaxRateImages/" + Guid.NewGuid() + "/";

                bool exists = Directory.Exists(path);

                if (!exists)
                    Directory.CreateDirectory(path);

                byte[] imageBytes = Convert.FromBase64String(imageBase64String);

                System.IO.File.WriteAllBytes(path + nametoupload, imageBytes);

                finalUrl = path.Replace(webRootPath, "") + nametoupload;
            }
            catch (Exception ex)
            {
            }

            return finalUrl;
        }


    }
}