﻿using Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Services.Interface;
using System;
using System.IO;
using System.Net;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        #region Private Variable
        private readonly IConfiguration _config;
        private readonly ILoggerService _loggerService;
        private readonly INotificationServices _notificationServices;
        private readonly IHostingEnvironment _hostingEnvironment;
        #endregion
        public NotificationController(IHostingEnvironment hostingEnvironment, INotificationServices notificationServices, IConfiguration config, ILoggerService loggerService)
        {
            _loggerService = loggerService;
            _notificationServices = notificationServices;
            _config = config;
            _hostingEnvironment = hostingEnvironment;
        }

        [Authorize]
        [HttpPost]
        [Route("getSMS")]
        public IActionResult GetSMSAPI()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _notificationServices.SMSAPIList();
                    if (status.Count > 0)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, status);
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, status);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("addSMS")]
        public IActionResult AddSMSAPI([FromBody] AddNotificationSMSAPIModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _notificationServices.AddSMSAPI(addUserData, tokenUserInformationModel.Name);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, APIId = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item3, UserId = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("updateSMS")]
        public IActionResult UpdateSMSAPI([FromBody] UpdateNotificationSMSAPIModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _notificationServices.UpdateSMSAPI(addUserData, tokenUserInformationModel.Name);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, APIId = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item3, UserId = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("updateSMSStatus")]
        public IActionResult UpdateSMSAPIStatus([FromBody] UpdateNotificationSMSAPIStatusModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _notificationServices.UpdateSMSAPIStatus(addUserData, tokenUserInformationModel.Name);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, APIId = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item3, UserId = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }


        [HttpPost]
        [Route("getSMSTemplate")]
        public IActionResult GetSMSAPITemplate()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _notificationServices.SMSTemplateList();
                    if (status.Count > 0)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, status);
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, status);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("addSMSTemplate")]
        public IActionResult AddSMSAPITemplate([FromBody] AddNotificationSMSModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _notificationServices.AddSMSTemplate(addUserData, tokenUserInformationModel.Name);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, APIId = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item3, UserId = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("updateSMSTemplate")]
        public IActionResult UpdateSMSAPITemplate([FromBody] UpdateNotificationSMSModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _notificationServices.UpdateSMSTemplate(addUserData, tokenUserInformationModel.Name);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, APIId = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item3, UserId = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("addSMSTemplateStatus")]
        public IActionResult UpdateSMSAPITemplateStatus([FromBody] UpdateStatusNotificationSMSModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _notificationServices.UpdateSMSTemplateStatus(addUserData, tokenUserInformationModel.Name);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, APIId = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item3, UserId = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("deleteSMSTemplate")]
        public IActionResult DeleteSMSAPITemplate([FromBody] DeleteNotificationSMSModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _notificationServices.DeleteSMSTemplate(addUserData, tokenUserInformationModel.Name);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, APIId = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item3, UserId = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("pramotionalsmsv1")]
        public IActionResult PramotionalSMSV1([FromBody] PramotionalSMSV1Model model)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    model.ModifiedBy = tokenUserInformationModel.Name;
                    model.UserID = Convert.ToInt32(tokenUserInformationModel.ID);
                    var status = _notificationServices.PramotionalSMSV1(model);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("pramotionalsmsv2")]
        public IActionResult PramotionalSMSV2([FromBody] PramotionalSMSV2Model model)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    model.ModifiedBy = tokenUserInformationModel.Name;
                    model.UserID = Convert.ToInt32(tokenUserInformationModel.ID);
                    var status = _notificationServices.PramotionalSMSV2(model);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }






        [Authorize]
        [HttpPost]
        [Route("getEmail")]
        public IActionResult GetEmailAPI()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _notificationServices.EmailAPIList();
                    if (status.Count > 0)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, status);
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, status);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("addEmail")]
        public IActionResult AddEmailAPI([FromBody] AddNotificationEmailAPIModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _notificationServices.AddEmailAPI(addUserData, tokenUserInformationModel.Name);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, APIId = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item3, UserId = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("updateEmail")]
        public IActionResult UpdateEmailAPI([FromBody] UpdateNotificationEmailAPIModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _notificationServices.UpdateEmailAPI(addUserData, tokenUserInformationModel.Name);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, APIId = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item3, UserId = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("updateEmailStatus")]
        public IActionResult UpdateEmailAPIStatus([FromBody] UpdateNotificationEmailAPIStatusModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _notificationServices.UpdateEmailAPIStatus(addUserData, tokenUserInformationModel.Name);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, APIId = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item3, UserId = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }


        [Authorize]
        [HttpPost]
        [Route("getEmailTemplate")]
        public IActionResult GetEmailAPITemplate()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _notificationServices.EmailTemplateList();
                    if (status.Count > 0)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, status);
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, status);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("addEmailTemplate")]
        public IActionResult AddEmailAPITemplate([FromBody] AddNotificationEmailModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _notificationServices.AddEmailTemplate(addUserData, tokenUserInformationModel.Name);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, APIId = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item3, UserId = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("updateEmailTemplate")]
        public IActionResult UpdateEmailAPITemplate([FromBody] UpdateNotificationEmailModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _notificationServices.UpdateEmailTemplate(addUserData, tokenUserInformationModel.Name);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, APIId = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item3, UserId = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("UpdateEmailTemplateStatus")]
        public IActionResult UpdateEmailAPITemplateStatus([FromBody] UpdateStatusNotificationEmailModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _notificationServices.UpdateEmailTemplateStatus(addUserData, tokenUserInformationModel.Name);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, APIId = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item3, UserId = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("pramotionalemailv1")]
        public IActionResult PramotionalEmailV1([FromBody] PramotionalEmailV1Model model)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    model.ModifiedBy = tokenUserInformationModel.Name;
                    model.UserID = Convert.ToInt32(tokenUserInformationModel.ID);
                    var status = _notificationServices.PramotionalEmailV1(model);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("pramotionalemailv2")]
        public IActionResult PramotionalEmailV2([FromBody] PramotionalEmailV2Model model)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    model.ModifiedBy = tokenUserInformationModel.Name;
                    model.UserID = Convert.ToInt32(tokenUserInformationModel.ID);
                    var status = _notificationServices.PramotionalEmailV2(model);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }



        [Authorize]
        [HttpPost]
        [Route("pushnotification")]
        public IActionResult PushNotification([FromBody] PushNotificationModel model)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    #region Images Upload
                    string imageName = "";
                    string imageBase64String = "";
                    string imageFormat = "";
                    string finalUrl = "";
                    if (!string.IsNullOrEmpty(model.NotificationImageBytes))
                    {
                        imageName = "Notify";
                        imageBase64String = model.NotificationImageBytes;
                        imageFormat = model.NotificationImageBytesFormat;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        model.NotificationImageURL = finalUrl;
                    }
                    #endregion
                    model.ModifiedBy = tokenUserInformationModel.Name;
                    model.UserID = Convert.ToInt32(tokenUserInformationModel.ID);
                    var status = _notificationServices.PushNotifications(model);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item2, Status = true, Response = status.Item3 });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item2, Status = false, Response = status.Item3 });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }
        private string UploadImage(string imageName, string imageBase64String, string imageFormat, string finalUrl)
        {
            try
            {
                var nametoupload = imageName + imageFormat;

                string webRootPath = _hostingEnvironment.ContentRootPath;

                var path = webRootPath + "/Content/NotificationImages/" + Guid.NewGuid() + "/";

                bool exists = Directory.Exists(path);

                if (!exists)
                    Directory.CreateDirectory(path);

                byte[] imageBytes = Convert.FromBase64String(imageBase64String);

                System.IO.File.WriteAllBytes(path + nametoupload, imageBytes);

                finalUrl = path.Replace(webRootPath, "") + nametoupload;
            }
            catch (Exception ex)
            {
            }

            return finalUrl;
        }

    }
}
