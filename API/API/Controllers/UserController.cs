﻿using Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Models;
using Services.Interface;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly ILoggerService _loggerService;
        private readonly IUserServices _userServices;
        private readonly IHostingEnvironment _hostingEnvironment;
        public UserController(IHostingEnvironment hostingEnvironment, IUserServices userServices, IConfiguration config, ILoggerService loggerService)
        {
            _loggerService = loggerService;
            _userServices = userServices;
            _config = config;
            _hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        ///     Authenticate
        /// </summary>
        /// <param name="loginData"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public IActionResult AuthenticateUser([FromBody] UserLoginModel loginData)
        {
            if (loginData == null) throw new ArgumentNullException(nameof(loginData));
            try
            {
                if (ModelState.IsValid)
                {
                    var user = _userServices.AuthenticateUser(loginData);

                    if (user != null)
                    {
                        var jwtToken = GenerateJwtToken(user);
                        //user.Token = jwtToken;
                        return JsonResponse.Create(HttpStatusCode.OK,
                            new
                            {
                                UserDetails = user,
                                JwtToken = jwtToken
                            }
                        );
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound, "Not Found"); //return not found result
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside AuthenticateUser " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("resetpassword")]
        public IActionResult ResetPassword([FromBody] ResetUserPasswordModel loginData)
        {
            if (loginData == null) throw new ArgumentNullException(nameof(loginData));
            try
            {
                if (ModelState.IsValid)
                {
                    bool status = _userServices.ResetPassword(loginData);

                    if (status)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK,
                            new
                            {
                                Message = "Password reset successfull and has been sent to Email id / Mobile number",
                                Status = status
                            }
                        );
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound,
                            new
                            {
                                Message = "Password reset unsuccessfull",
                                Status = status
                            });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("forgotpassword")]
        public IActionResult ForgotPassword([FromBody] ResetUserPasswordModel loginData)
        {
            if (loginData == null) throw new ArgumentNullException(nameof(loginData));
            try
            {
                if (ModelState.IsValid)
                {
                    bool status = _userServices.ForgotPassword(loginData);

                    if (status)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK,
                            new
                            {
                                Message = "Password link sent successfull and has been sent to Email id / Mobile number",
                                Status = status
                            }
                        );
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound,
                            new
                            {
                                Message = "Forgot password unsuccessfull",
                                Status = status
                            });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Authorize]
        [Route("changepassword")]
        public IActionResult ChangeUserPassword([FromBody] UserSettingModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    addUserData.ModifyBy = tokenUserInformationModel.Name;
                    addUserData.UserID = Convert.ToInt32(tokenUserInformationModel.ID);
                    var status = _userServices.ChangePassword(addUserData);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK,
                            new
                            {
                                Message = status.Item2,
                                Status = true
                            }
                        );
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound,
                            new
                            {
                                Message = status.Item2,
                                Status = true
                            });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("add")]
        public IActionResult Add([FromBody] AddUserModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    string imageName = "";
                    string imageBase64String = "";
                    string imageFormat = "";
                    string finalUrl = "";
                    if (!string.IsNullOrEmpty(addUserData.ProfileImageBytes))
                    {
                        imageName = "Profile";
                        imageBase64String = addUserData.ProfileImageBytes;
                        imageFormat = addUserData.ProfileImageBytesFormat;
                        finalUrl = "";

                        finalUrl = UploadImageProfile(imageName, imageBase64String, imageFormat, finalUrl);

                        addUserData.ProfileImageURL = finalUrl;
                    }
                    var status = _userServices.AddUser(addUserData, "Online");

                    if (status.Item1)
                    {
                        if (addUserData.UserType == "3")
                        {
                            #region Vendor
                            UpdateSignatureModel _signatureModel = new UpdateSignatureModel()
                            {
                                ID = status.Item2,
                                SignatureImageBytes = addUserData.SignatureImageBytes,
                                SignatureImageBytesFormat = addUserData.SignatureImageBytesFormat,
                                ModifiedBy = tokenUserInformationModel.Name
                            };
                            #region Images Upload
                            if (!string.IsNullOrEmpty(addUserData.SignatureImageBytes))
                            {
                                imageName = "Image1";
                                imageBase64String = addUserData.SignatureImageBytes;
                                imageFormat = addUserData.SignatureImageBytesFormat;
                                finalUrl = "";

                                finalUrl = UploadImageSignature(imageName, imageBase64String, imageFormat, finalUrl);

                                _signatureModel.SignatureImageURL = finalUrl;
                            }
                            #endregion
                            var status1 = _userServices.UpdateSignatureDetails(_signatureModel);

                            UpdateBankDetailsModel _bankModel = new UpdateBankDetailsModel()
                            {
                                AccountNumber = addUserData.AccountNumber,
                                AccountHolderName = addUserData.AccountHolderName,
                                BankName = addUserData.BankName,
                                IfscCode = addUserData.Ifsc,
                                BlankChequeImageBytes = addUserData.BlankChequeImageBytes,
                                BlankChequeImageBytesFormat = addUserData.BlankChequeImageBytesFormat,
                                ModifiedBy = tokenUserInformationModel.Name,
                                ID = status.Item2
                            };
                            #region Images Upload
                            if (!string.IsNullOrEmpty(addUserData.BlankChequeImageBytes))
                            {
                                imageName = "Image1";
                                imageBase64String = addUserData.BlankChequeImageBytes;
                                imageFormat = addUserData.BlankChequeImageBytesFormat;
                                finalUrl = "";

                                finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                                _bankModel.BlankChequeImageURL = finalUrl;
                            }
                            #endregion
                            var status2 = _userServices.UpdateBankDetails(_bankModel);
                            #endregion
                        }
                        return JsonResponse.Create(HttpStatusCode.OK,
                            new
                            {
                                Message = status.Item3,
                                UserId = status.Item2,
                                Status = true
                            }
                        );
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound,
                            new
                            {
                                Message = status.Item3,
                                UserId = status.Item2,
                                Status = true
                            });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("update")]
        public IActionResult UpdateUserProfile([FromBody] UpdateUserModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _userServices.UpdateUser(addUserData, tokenUserInformationModel.Name);
                    if (status.Item1)
                    {
                        if (addUserData.UserType == "3")
                        {
                            #region Vendor
                            UpdateSignatureModel _signatureModel = new UpdateSignatureModel()
                            {
                                ID = addUserData.Id,
                                SignatureImageBytes = addUserData.SignatureImageBytes,
                                SignatureImageBytesFormat = addUserData.SignatureImageBytesFormat,
                                ModifiedBy = tokenUserInformationModel.Name
                            };
                            #region Images Upload
                            string imageName = "";
                            string imageBase64String = "";
                            string imageFormat = "";
                            string finalUrl = "";
                            if (!string.IsNullOrEmpty(addUserData.SignatureImageBytes))
                            {
                                imageName = "Image1";
                                imageBase64String = addUserData.SignatureImageBytes;
                                imageFormat = addUserData.SignatureImageBytesFormat;
                                finalUrl = "";

                                finalUrl = UploadImageSignature(imageName, imageBase64String, imageFormat, finalUrl);

                                _signatureModel.SignatureImageURL = finalUrl;
                            }
                            #endregion
                            var status1 = _userServices.UpdateSignatureDetails(_signatureModel);

                            UpdateBankDetailsModel _bankModel = new UpdateBankDetailsModel()
                            {
                                AccountNumber = addUserData.AccountNumber,
                                AccountHolderName = addUserData.AccountHolderName,
                                BankName = addUserData.BankName,
                                IfscCode = addUserData.Ifsc,
                                BlankChequeImageBytes = addUserData.BlankChequeImageBytes,
                                BlankChequeImageBytesFormat = addUserData.BlankChequeImageBytesFormat,
                                ModifiedBy = tokenUserInformationModel.Name,
                                ID = addUserData.Id
                            };
                            #region Images Upload
                            if (!string.IsNullOrEmpty(addUserData.BlankChequeImageBytes))
                            {
                                imageName = "Image1";
                                imageBase64String = addUserData.BlankChequeImageBytes;
                                imageFormat = addUserData.BlankChequeImageBytesFormat;
                                finalUrl = "";

                                finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                                _bankModel.BlankChequeImageURL = finalUrl;
                            }
                            #endregion
                            var status2 = _userServices.UpdateBankDetails(_bankModel);
                            #endregion
                        }
                        return JsonResponse.Create(HttpStatusCode.OK,
                            new
                            {
                                Message = status.Item3,
                                UserId = status.Item2,
                                Status = true
                            }
                        );
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound,
                            new
                            {
                                Message = status.Item3,
                                UserId = status.Item2,
                                Status = true
                            });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("updateStatus")]
        public IActionResult UpdateUserStatus([FromBody] UpdateUserStatusModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _userServices.UpdateUserStatus(addUserData, tokenUserInformationModel.Name);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK,
                            new
                            {
                                Message = status.Item3,
                                UserId = status.Item2,
                                Status = true
                            }
                        );
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound,
                            new
                            {
                                Message = status.Item3,
                                UserId = status.Item2,
                                Status = true
                            });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [AllowAnonymous]
        [HttpPost]
        [Route("get")]
        public IActionResult get()
        {
            try
            {
                var status = _userServices.GetUserList();

                if (status != null)
                {
                    return JsonResponse.Create(HttpStatusCode.OK, status
                    );
                }

                return JsonResponse.Create(HttpStatusCode.NotFound, null);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [AllowAnonymous]
        [HttpPost]
        [Route("getByType")]
        public IActionResult getByType(short userType)
        {
            try
            {
                var status = _userServices.GetUserByUserTypeList(userType);

                if (status != null)
                {
                    return JsonResponse.Create(HttpStatusCode.OK, status
                    );
                }

                return JsonResponse.Create(HttpStatusCode.NotFound, null);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("getusertypes")]
        public IActionResult GetuserTypes()
        {
            try
            {
                var result = _userServices.GetUserTypeList();
                if (result.Any())
                {
                    return JsonResponse.Create(HttpStatusCode.OK, result);
                }
                return JsonResponse.Create(HttpStatusCode.NotFound, null);
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("updatedeviceid")]
        public IActionResult UpdateUserDeviceID([FromBody] UpdateUserDeviceIDModel model)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    model.ModifiedBy = tokenUserInformationModel.Name;
                    model.UserID = Convert.ToInt32(tokenUserInformationModel.ID);
                    var status = _userServices.UpdateUserDeviceId(model);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK,
                            new
                            {
                                Message = status.Item2,
                                Status = true
                            }
                        );
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound,
                            new
                            {
                                Message = status.Item2,
                                Status = true
                            });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        /// <summary>
        ///     Generate Jwt Token
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private string GenerateJwtToken(UserDetailsModel user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, user.EmailAddress),
                new Claim(ClaimTypes.Name, user.Name),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
            };

            var jwtToken = new JwtSecurityToken(_config["Jwt:Issuer"],
                _config["Jwt:Audience"],
                claims,
                DateTime.UtcNow,
                user.UserType == "Customer" ? DateTime.UtcNow.AddDays(2) : DateTime.UtcNow.AddMinutes(int.Parse(_config["Jwt:accessTokenDurationInMinutes"])),
                credentials
            );
            return new JwtSecurityTokenHandler().WriteToken(jwtToken);
        }


        #region Customer Method
        [AllowAnonymous]
        [HttpPost]
        [Route("addcustomer")]
        public IActionResult addcustomer([FromBody] AddCustomerRequestModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _userServices.CustomerAddRequest(addUserData);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK,
                            new
                            {
                                Message = status.Item2,
                                Status = true
                            }
                        );
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound,
                            new
                            {
                                Message = status.Item2,
                                Status = true
                            });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("addcustomerconfirm")]
        public IActionResult AddCustomerConfirm([FromBody] AddCustomerConfirmRequestModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _userServices.CustomerConfirmRequest(addUserData, "Online");
                    if (status.Item1)
                    {
                        UserDetailsModel user = new UserDetailsModel()
                        {
                            EmailAddress = addUserData.PhoneNumber,
                            Id = status.Item2,
                            Name = addUserData.Name
                        };
                        var jwtToken = GenerateJwtToken(user);
                        return JsonResponse.Create(HttpStatusCode.OK,
                            new
                            {
                                Message = status.Item3,
                                UserId = status.Item2,
                                Status = true,
                                JwtToken = jwtToken
                            }
                        );
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound,
                            new
                            {
                                Message = status.Item3,
                                UserId = status.Item2,
                                Status = true
                            });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [AllowAnonymous]
        [HttpPost]
        [Route("updatecustomeremail")]
        public IActionResult UpdateCustomerEmail([FromBody] UpdateCustomerRequestModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _userServices.UpdateCustomerEmail(addUserData);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK,
                            new
                            {
                                Message = status.Item2,
                                Status = true
                            }
                        );
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound,
                            new
                            {
                                Message = status.Item2,
                                Status = true
                            });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }
        #endregion

        #region Vendor Method
        [AllowAnonymous]
        [HttpPost]
        [Route("verifymobileno")]
        public IActionResult VerifyMobileNo([FromBody] VerifyPhoneNumberModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _userServices.VendorPhoneVerification(addUserData);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK,
                            new
                            {
                                Message = status.Item2,
                                Status = true
                            }
                        );
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound,
                            new
                            {
                                Message = status.Item2,
                                Status = true
                            });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("addvendor")]
        public IActionResult AddVendor([FromBody] AddVendorModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _userServices.AddVendor(addUserData, "Online");
                    if (status.Item1)
                    {
                        UserDetailsModel user = new UserDetailsModel()
                        {
                            EmailAddress = addUserData.EmailID,
                            Id = status.Item2,
                            Name = addUserData.Name
                        };
                        var jwtToken = GenerateJwtToken(user);
                        return JsonResponse.Create(HttpStatusCode.OK,
                            new
                            {
                                Message = status.Item3,
                                UserId = status.Item2,
                                Status = true,
                                JwtToken = jwtToken
                            }
                        );
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound,
                            new
                            {
                                Message = status.Item3,
                                UserId = status.Item2,
                                Status = true
                            });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [HttpPost]
        [Route("pickupaddress")]
        public IActionResult PickUpAddress([FromBody] PickUpAddressModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _userServices.GetPickupAddress(addUserData);
                    if (status != null)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, status);
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound, null);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("updatepickupaddress")]
        public IActionResult UpdatePickupAddress([FromBody] UpdatePickupPinAddressModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    addUserData.ID = Convert.ToInt32(tokenUserInformationModel.ID);
                    addUserData.ModifiedBy = tokenUserInformationModel.Name;
                    var status = _userServices.UpdatePickupAddress(addUserData);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK,
                            new
                            {
                                Message = status.Item3,
                                UserId = status.Item2,
                                Status = true
                            }
                        );
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound,
                            new
                            {
                                Message = status.Item3,
                                UserId = status.Item2,
                                Status = true
                            });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("updategstin")]
        public IActionResult UpdateGSTIN([FromBody] UpdateGSTINModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    addUserData.ID = Convert.ToInt32(tokenUserInformationModel.ID);
                    addUserData.ModifiedBy = tokenUserInformationModel.Name;
                    var status = _userServices.UpdateGSTIN(addUserData);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK,
                            new
                            {
                                Message = status.Item3,
                                UserId = status.Item2,
                                Status = true
                            }
                        );
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound,
                            new
                            {
                                Message = status.Item3,
                                UserId = status.Item2,
                                Status = true
                            });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("updatebankdetails")]
        public IActionResult UpdateBankDetails([FromBody] UpdateBankDetailsModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    #region Images Upload
                    string imageName = "";
                    string imageBase64String = "";
                    string imageFormat = "";
                    string finalUrl = "";
                    if (!string.IsNullOrEmpty(addUserData.BlankChequeImageBytes))
                    {
                        imageName = "Image1";
                        imageBase64String = addUserData.BlankChequeImageBytes;
                        imageFormat = addUserData.BlankChequeImageBytesFormat;
                        finalUrl = "";

                        finalUrl = UploadImage(imageName, imageBase64String, imageFormat, finalUrl);

                        addUserData.BlankChequeImageURL = finalUrl;
                    }
                    #endregion
                    if (addUserData.ID == 0)
                        addUserData.ID = Convert.ToInt32(tokenUserInformationModel.ID);
                    addUserData.ModifiedBy = tokenUserInformationModel.Name;
                    var status = _userServices.UpdateBankDetails(addUserData);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, UserId = status.Item2, Status = true });
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item3, UserId = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("updatestoredetails")]
        public IActionResult UpdateStoreDetails([FromBody] StoreDetailsModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    addUserData.ID = Convert.ToInt32(tokenUserInformationModel.ID);
                    addUserData.ModifiedBy = tokenUserInformationModel.Name;
                    var status = _userServices.UpdateStoreDetails(addUserData);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK,
                            new
                            {
                                Message = status.Item3,
                                UserId = status.Item2,
                                Status = true
                            }
                        );
                    }

                    return JsonResponse.Create(HttpStatusCode.NotFound,
                            new
                            {
                                Message = status.Item3,
                                UserId = status.Item2,
                                Status = true
                            });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("updatesignaturedetails")]
        public IActionResult UpdateSignatureDetails([FromBody] UpdateSignatureModel addUserData)
        {
            if (addUserData == null) throw new ArgumentNullException(nameof(addUserData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    #region Images Upload
                    string imageName = "";
                    string imageBase64String = "";
                    string imageFormat = "";
                    string finalUrl = "";
                    if (!string.IsNullOrEmpty(addUserData.SignatureImageBytes))
                    {
                        imageName = "Image1";
                        imageBase64String = addUserData.SignatureImageBytes;
                        imageFormat = addUserData.SignatureImageBytesFormat;
                        finalUrl = "";

                        finalUrl = UploadImageSignature(imageName, imageBase64String, imageFormat, finalUrl);

                        addUserData.SignatureImageURL = finalUrl;
                    }
                    #endregion
                    addUserData.ID = Convert.ToInt32(tokenUserInformationModel.ID);
                    addUserData.ModifiedBy = tokenUserInformationModel.Name;
                    var status = _userServices.UpdateSignatureDetails(addUserData);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, UserId = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, new { Message = status.Item3, UserId = status.Item2, Status = true });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("getProfile")]
        public IActionResult VendorProfileDetails([FromBody] VendorStoredDetailsModel userData)
        {
            if (userData == null) throw new ArgumentNullException(nameof(userData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _userServices.VendorDetails(userData);
                    if (status != null)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, status);
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, null);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("updateProfile")]
        public IActionResult UpdateProfileDetails([FromBody] UpdateProfileDetailsModel userData)
        {
            if (userData == null) throw new ArgumentNullException(nameof(userData));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    #region Images Upload
                    string imageName = "";
                    string imageBase64String = "";
                    string imageFormat = "";
                    string finalUrl = "";
                    if (!string.IsNullOrEmpty(userData.SignatureImageBytes))
                    {
                        imageName = "Image1";
                        imageBase64String = userData.SignatureImageBytes;
                        imageFormat = userData.SignatureImageBytesFormat;
                        finalUrl = "";

                        finalUrl = UploadImageSignature(imageName, imageBase64String, imageFormat, finalUrl);

                        userData.SignatureImageURL = finalUrl;
                    }
                    if (!string.IsNullOrEmpty(userData.ProfileImageBytes))
                    {
                        imageName = "Profile";
                        imageBase64String = userData.ProfileImageBytes;
                        imageFormat = userData.ProfileImageBytesFormat;
                        finalUrl = "";

                        finalUrl = UploadImageProfile(imageName, imageBase64String, imageFormat, finalUrl);

                        userData.ProfileImageURL = finalUrl;
                    }
                    #endregion
                    userData.ModifiedBy = tokenUserInformationModel.Name;
                    var status = _userServices.UpdateProfileDetails(userData);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, UserId = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, UserId = status.Item2, Status = false });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("getallvendors")]
        public IActionResult GetAllVendors()
        {
            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _userServices.GetVendorList();
                    if (status.Count > 0)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, status);
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, null);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("getallvendorswithcompany")]
        public IActionResult GetAllVendorsWithCompany()
        {
            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _userServices.GetVendorsList();
                    if (status.Count > 0)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, status);
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, null);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("getuserdocumentlist")]
        public IActionResult GetUserDocuments([FromBody] VendorStoredDetailsModel model)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));
            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _userServices.UserDocumentList(model);
                    if (status.Count > 0)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, status);
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, null);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("getuserbanklist")]
        public IActionResult GetUserBanks([FromBody] VendorStoredDetailsModel model)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));
            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    var status = _userServices.UserBankList(model);
                    if (status.Count > 0)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, status);
                    }
                    return JsonResponse.Create(HttpStatusCode.NotFound, null);
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }

        [Authorize]
        [HttpPost]
        [Route("updatevendorstatus")]
        public IActionResult UpdateVendorStatus([FromBody] UpdateVendorStatusModel model)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));

            TokenUserInformationModel tokenUserInformationModel = new TokenUserInformationModel();
            var headers = Request.Headers;
            if (headers.ContainsKey("Authorization"))
            {
                TokenHelpers.GetTokenInformation(tokenUserInformationModel, headers);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    model.ModifiedBy = tokenUserInformationModel.Name;
                    var status = _userServices.VendorStatusRequest(model);
                    if (status.Item1)
                    {
                        return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, UserId = status.Item2, Status = true });
                    }
                    return JsonResponse.Create(HttpStatusCode.OK, new { Message = status.Item3, UserId = status.Item2, Status = false });
                }

                return JsonResponse.Create(HttpStatusCode.BadRequest, "Invalid Parameters");
            }
            catch (Exception ex)
            {
                _loggerService.LogError("Exception inside ResetPassword " + ex);
                return JsonResponse.Create(HttpStatusCode.InternalServerError, "Error Occured" + ex); //return error occured
            }
        }


        private string UploadImage(string imageName, string imageBase64String, string imageFormat, string finalUrl)
        {
            try
            {
                var nametoupload = imageName + imageFormat;

                string webRootPath = _hostingEnvironment.ContentRootPath;

                var path = webRootPath + "/Content/BankChequeImages/" + Guid.NewGuid() + "/";

                bool exists = Directory.Exists(path);

                if (!exists)
                    Directory.CreateDirectory(path);

                byte[] imageBytes = Convert.FromBase64String(imageBase64String);

                System.IO.File.WriteAllBytes(path + nametoupload, imageBytes);

                finalUrl = path.Replace(webRootPath, "") + nametoupload;
            }
            catch (Exception ex)
            {
            }

            return finalUrl;
        }
        private string UploadImageSignature(string imageName, string imageBase64String, string imageFormat, string finalUrl)
        {
            try
            {
                var nametoupload = imageName + imageFormat;

                string webRootPath = _hostingEnvironment.ContentRootPath;

                var path = webRootPath + "/Content/SignatureImages/" + Guid.NewGuid() + "/";

                bool exists = Directory.Exists(path);

                if (!exists)
                    Directory.CreateDirectory(path);

                byte[] imageBytes = Convert.FromBase64String(imageBase64String);

                System.IO.File.WriteAllBytes(path + nametoupload, imageBytes);

                finalUrl = path.Replace(webRootPath, "") + nametoupload;
            }
            catch (Exception ex)
            {
            }

            return finalUrl;
        }
        private string UploadImageProfile(string imageName, string imageBase64String, string imageFormat, string finalUrl)
        {
            try
            {
                var nametoupload = imageName + imageFormat;

                string webRootPath = _hostingEnvironment.ContentRootPath;

                var path = webRootPath + "/Content/ProfileImages/" + Guid.NewGuid() + "/";

                bool exists = Directory.Exists(path);

                if (!exists)
                    Directory.CreateDirectory(path);

                byte[] imageBytes = Convert.FromBase64String(imageBase64String);

                System.IO.File.WriteAllBytes(path + nametoupload, imageBytes);

                finalUrl = path.Replace(webRootPath, "") + nametoupload;
            }
            catch (Exception ex)
            {
            }

            return finalUrl;
        }
        #endregion
    }
}