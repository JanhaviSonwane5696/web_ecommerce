﻿using System.Collections.Generic;
using System.Text;
using Audit.Core;
using Audit.SqlServer;
using Audit.SqlServer.Providers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Models;
using Repository.Helpers;
using Repository.Interface;
using Repository.Repository;
using Services.Interface;
using Services.Service;

namespace API
{
    public static class ServiceConfiguration
    {
        public static IServiceCollection ConfigureServices(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.Configure<AppSettings>(configuration.GetSection("GlobalSettings"));

            #region Repository Dependency

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserTypesRepository, UserTypesRepository>();
            services.AddScoped<IUsersVerificationRepository, UsersVerificationRepository>();
            services.AddScoped<IProductCategoryGroupRepository, ProductCategoryGroupRepository>();
            services.AddScoped<IProductCategoryRepository, ProductCategoryRepository>();
            services.AddScoped<IProductSubCategoryRepository, ProductSubCategoryRepository>();
            services.AddScoped<IProductTagsRepository, ProductTagsRepository>();
            services.AddScoped<IProductItemRepository, ProductItemRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IAllBankNameRepository, AllBankNameRepository>();
            services.AddScoped<IUsersBankRepository, UsersBankRepository>();
            services.AddScoped<IMasterAttributeRepository, MasterAttributeRepository>();
            services.AddScoped<IMasterAttributeItemsRepository, MasterAttributeItemsRepository>();
            services.AddScoped<IMasterTaxRateRepository, MasterTaxRateRepository>();
            services.AddScoped<INotificationHelpers, NotificationHelpers>();
            services.AddScoped<IMasterBrandsRepository, MasterBrandsRepository>();
            services.AddScoped<INotificationRepository, NotificationRepository>();
            services.AddScoped<IShoppingCartRepository, ShoppingCartRepository>();
            services.AddScoped<IUserAddressRepository, UserAddressRepository>();
            services.AddScoped<IShoppingOrderRepository, ShoppingOrderRepository>();
            services.AddScoped<IProductRelatedRepository, ProductRelatedRepository>();
            #endregion

            #region Service Dependency

            services.AddTransient<IUserServices, UserServices>();
            services.AddTransient<IProductCategoryGroupServices, ProductCategoryGroupServices>();
            services.AddTransient<IProductCategoryServices, ProductCategoryServices>();
            services.AddTransient<IProductSubCategoryServices, ProductSubCategoryServices>();
            services.AddTransient<IProductTagsServices, ProductTagsServices>();
            services.AddTransient<IProductItemServices, ProductItemServices>();
            services.AddTransient<IProductServices, ProductServices>();
            services.AddTransient<IBanksServices, BanksServices>();
            services.AddTransient<IMasterAttributeServices, MasterAttributeServices>();
            services.AddTransient<IMasterAttributeItemsServices, MasterAttributeItemsServices>();
            services.AddTransient<IMasterTaxRateServices, MasterTaxRateServices>();
            services.AddTransient<IMasterBrandsServices, MasterBrandsServices>();
            services.AddTransient<INotificationServices, NotificationServices>();
            services.AddTransient<IShoppingCartServices, ShoppingCartServices>();
            services.AddTransient<IUserAddressServices, UserAddressServices>();
            services.AddTransient<IShoppingOrderServices, ShoppingOrderServices>();
            services.AddTransient<IProductRelatedServices, ProductRelatedServices>();
            #endregion

            return services;
        }

        public static void ConfigureLoggerService(this IServiceCollection services)
        {
            services.AddSingleton<ILoggerService, LoggerService>();

            services.Configure<FormOptions>(x =>
            {
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = int.MaxValue;
                x.MultipartHeadersLengthLimit = int.MaxValue;
            });
        }

        public static void ConfigureAuditService(IConfiguration configuration)
        {
            Configuration.DataProvider = new SqlDataProvider
            {
                ConnectionString = configuration.GetConnectionString("DefaultDatabase"),
                Schema = configuration["AuditNet:Schema"],
                TableName = configuration["AuditNet:TableName"],
                IdColumnName = configuration["AuditNet:IdColumnName"],
                JsonColumnName = configuration["AuditNet:JsonColumnName"],

                CustomColumns = new List<CustomColumn>
                {
                    new CustomColumn(configuration["AuditNet:EventType"], ev => ev.EventType),
                    new CustomColumn(configuration["AuditNet:ModifiedDate"], ev => ev.EndDate),
                    new CustomColumn(configuration["AuditNet:ReferenceID"],
                        ev => ev.CustomFields[configuration["AuditNet:ReferenceID"]])
                }
            };
        }

        public static void ConfigureJwt(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = configuration["Jwt:Issuer"],
                    ValidAudience = configuration["Jwt:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]))
                };
            });
        }
    }
}