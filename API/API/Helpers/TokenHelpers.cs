﻿using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Models;

namespace Helpers
{
    public class TokenHelpers
    {
        public static void GetTokenInformation(TokenUserInformationModel tokenUserInformationModel, IHeaderDictionary headers)
        {
            string tokenValue = "";
            StringValues token;
            headers.TryGetValue("Authorization", out token);
            tokenValue = token[0];
            var handler = new JwtSecurityTokenHandler();
            JwtSecurityToken tokenData = handler.ReadJwtToken(tokenValue.Replace("Bearer ", ""));
            var claimNameData = tokenData.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name);
            var claimEmailData = tokenData.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Email);
            var claimNameIdentifierData = tokenData.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

            tokenUserInformationModel.Email = claimEmailData.Value;
            tokenUserInformationModel.Name = claimNameData.Value;
            tokenUserInformationModel.ID = claimNameIdentifierData.Value;
        }
    }
}