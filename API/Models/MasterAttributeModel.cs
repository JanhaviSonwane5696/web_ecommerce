﻿using System;

namespace Models
{
    public class MasterAttributeModel
    {
        public int Id { get; set; }
        public string AttributeName { get; set; }
        public string AttributeType { get; set; }
        public string AttributeDesciption { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
