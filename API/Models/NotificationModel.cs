﻿using Newtonsoft.Json;
using System;

namespace Models
{
    public class MasterSMSApiListModel
    {
        public short Id { get; set; }
        public string APIName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool Status { get; set; }
        public string SenderId { get; set; }
        public Nullable<DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
    public class AddNotificationSMSAPIModel
    {
        public string SMSContent { get; set; }
        public bool IsActive { get; set; }
    }
    public class UpdateNotificationSMSAPIModel
    {
        public int Id { get; set; }
        public string SMSContent { get; set; }
        public bool IsActive { get; set; }
    }
    public class UpdateNotificationSMSAPIStatusModel
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
    }


    public class MasterSMSTemplatesListModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string SMSContent { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
    public class AddNotificationSMSModel
    {
        public string Type { get; set; }
        public string SMSContent { get; set; }
        public bool IsActive { get; set; }
    }
    public class UpdateNotificationSMSModel
    {
        public string Type { get; set; }
        public string SMSContent { get; set; }
        public bool IsActive { get; set; }
    }
    public class UpdateStatusNotificationSMSModel
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
    }
    public class DeleteNotificationSMSModel
    {
        public int Id { get; set; }
    }



    public class MasterEmailSettingsListModel
    {
        public int Id { get; set; }
        public string HostName { get; set; }
        public string FromEmailAddress { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public string Port { get; set; }
        public string Password { get; set; }
        public bool IsMailActive { get; set; }
        public string FromName { get; set; }
        public bool MailSSL { get; set; }
        public Nullable<DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
    public class AddNotificationEmailAPIModel
    {
        public string HostName { get; set; }
        public string FromEmailAddress { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public string Port { get; set; }
        public string Password { get; set; }
        public bool IsMailActive { get; set; }
        public string FromName { get; set; }
        public bool MailSSL { get; set; }
    }
    public class UpdateNotificationEmailAPIModel
    {
        public int Id { get; set; }
        public string HostName { get; set; }
        public string FromEmailAddress { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public string Port { get; set; }
        public string Password { get; set; }
        public string FromName { get; set; }
        public bool MailSSL { get; set; }
        public bool IsActive { get; set; }
    }
    public class UpdateNotificationEmailAPIStatusModel
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
    }


    public class MasterEmailTemplatesListModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsActive { get; set; }
        public Nullable<DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
    public class AddNotificationEmailModel
    {
        public string Type { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsActive { get; set; }
    }
    public class UpdateNotificationEmailModel
    {
        public string Type { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
    public class UpdateStatusNotificationEmailModel
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
    }


    public class PramotionalSMSV1Model
    {
        public short UserType { get; set; }
        public string Content { get; set; }
        [JsonIgnore] public int UserID { get; set; }
        [JsonIgnore] public string ModifiedBy { get; set; }
    }
    public class PramotionalSMSV2Model
    {
        public string MobileNumbers { get; set; }
        public string Content { get; set; }
        [JsonIgnore] public int UserID { get; set; }
        [JsonIgnore] public string ModifiedBy { get; set; }
    }


    public class PramotionalEmailV1Model
    {
        public short UserType { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        [JsonIgnore]public int UserID { get; set; }
        [JsonIgnore] public string ModifiedBy { get; set; }
    }
    public class PramotionalEmailV2Model
    {
        public string EmailIds { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        [JsonIgnore] public int UserID { get; set; }
        [JsonIgnore] public string ModifiedBy { get; set; }
    }

    public class PushNotificationModel
    {
        public short UserType { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Link { get; set; }
        public string NotificationImageBytes { get; set; }
        public string NotificationImageBytesFormat { get; set; }
        [JsonIgnore] public string NotificationImageURL { get; set; }
        [JsonIgnore] public int UserID { get; set; }
        [JsonIgnore] public string ModifiedBy { get; set; }
    }

}
