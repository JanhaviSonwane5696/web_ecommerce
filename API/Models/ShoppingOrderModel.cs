﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class ShoppingOrderModel
    {
        [Required]
        public int BillingAddress { get; set; }
        [Required]
        public int ShippingAddress { get; set; }
        [Required]
        public string PaymentMode { get; set; }
        [JsonIgnore] public int UserId { get; set; }
    }

    public class ShoppingOrderResponseModel
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public int OrderId { get; set; }
        public int PaymentId { get; set; }
        public long GateWayId { get; set; }
    }

    public class ShoppingOrderDetailsModel
    {
        public int Id { get; set; }
        public string OrderId { get; set; }
        public int Quantity { get; set; }
        public decimal Amount { get; set; }
        public string Orderdate { get; set; }
        public string Status { get; set; }
        public string Name { get; set; }
        public string ImageURL1 { get; set; }
        public string ImageThumbURL1 { get; set; }
        public decimal Discount { get; set; }
    }

    public class ShoppingOrdersDetailsModel
    {
        public int Id { get; set; }
        public string OrderId { get; set; }
        public string OrderStatus { get; set; }
        public int ProductItemId { get; set; }
        public bool IsBlock { get; set; }
        public string OrderDate { get; set; }
        public string OrderUpdate { get; set; }
        public decimal ItemPrice { get; set; }
        public int Quantity { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Tax { get; set; }
        public decimal Shipping { get; set; }
        public decimal PrimePoint { get; set; }
        public decimal TotalAmount { get; set; }
        public string PaymentMode { get; set; }
        public string PaymentStatus { get; set; }
        public string FullName { get; set; }
        public string EmailId { get; set; }
        public string Mobile { get; set; }
        public string ResellerComment { get; set; }
        public string AdminComment { get; set; }
        public string BillingAddress { get; set; }
        public string ShippingAddress { get; set; }
        public string BILLHTML { get; set; }
        public string BILLHTMLS { get; set; }
        public string SoldBy { get; set; }
        public string FirmName { get; set; }
        public string PerAddress { get; set; }
        public string SellerGSTNumber { get; set; }
        public string EstimateDelivery { get; set; }
        public Nullable<int> PaymentId { get; set; }
        public string ImageURL { get; set; }
    }

    public class OrderReportModel
    {
        [Required]
        public int UserId { get; set; }
        [Required]
        public string FromDate { get; set; }
        [Required]
        public string ToDate { get; set; }
        public string Status { get; set; }
    }

    public class UpdateOrderStatusModel
    {
        [JsonIgnore]
        public int UserId { get; set; }
        [Required]
        public int OrderId { get; set; }
        [Required]
        public string Comment { get; set; }
        [Required]
        public string Status { get; set; }
    }

    public class ShoppingOrderDetailModel
    {
        public int Id { get; set; }
        public string OrderId { get; set; }
        public int Quantity { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Tax { get; set; }
        public decimal DeliveryCharges { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal PrimePer { get; set; }
        public string Orderdate { get; set; }
        public string Status { get; set; }
        public string Name { get; set; }
        public string ImageURL1 { get; set; }
        public string ImageThumbURL1 { get; set; }
        public decimal Discount { get; set; }
        public string ShipppingAddress { get; set; }
        public Nullable<int> PaymentId { get; set; }
        public Nullable<System.DateTime> PayDate { get; set; }
        public string AdminComment { get; set; }
        public string ResellerComment { get; set; }
        public string PayStatus { get; set; }
        public string VendorName { get; set; }
    }
}
