﻿using System;
using System.Collections.Generic;

namespace Repository.Model
{
    public class ShoppingCartItemModel
    {
        public int Id { get; set; }
        public int ShoppingCardID { get; set; }
        public int ProductId { get; set; }
        public int ProductItemId { get; set; }
        public int Quantity { get; set; }
        public DateTime DateAdded { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class ShoppingCartItemsModel
    {
        public int Id { get; set; }
        public int ShoppingCardID { get; set; }
        public int ProductId { get; set; }
        public int ProductItemId { get; set; }
        public int Quantity { get; set; }
        public DateTime DateAdded { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal? DistributorPrice { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal Weight { get; set; }
        public int? ProductQuantity { get; set; }
        public string ImageURL1 { get; set; }
        public string ImageThumbURL1 { get; set; }
        public string ImageURL2 { get; set; }
        public string ImageThumbURL2 { get; set; }
        public string ImageURL3 { get; set; }
        public string ImageThumbURL3 { get; set; }
        public List<TaxRatesModel> Tax { get; set; }
        public decimal DeliveryCharge { get; set; }
        public decimal? PercentOff { get; set; }
        public string UserName { get; set; }
        public string ShopName { get; set; }
    }

    public class ShoppingCartItemsListModel
    {
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public decimal? DistributorPrice { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? SalePrice { get; set; }
        public int ShoppingCartID { get; set; }
        public string GuidID { get; set; }
        public int ProductId { get; set; }
        public int ProductItemId { get; set; }
        public int Quantity { get; set; }
        public DateTime DateAddedItems { get; set; }
        public DateTime DateCreatedCart { get; set; }
        public DateTime DateUpdatedCart { get; set; }
        public string HSNNumber { get; set; }
        public string SKUNumber { get; set; }
        public DateTime DeliveryStart { get; set; }
        public DateTime DeliveryEnd { get; set; }
        public string SoldBy { get; set; }
        public bool COD { get; set; }
        public decimal Weight { get; set; }
        public int? ProductQuantity { get; set; }
        public string ImageURL1 { get; set; }
        public decimal DeliveryCharge { get; set; }
        public List<TaxRatesModel> Tax { get; set; }
    }
}
