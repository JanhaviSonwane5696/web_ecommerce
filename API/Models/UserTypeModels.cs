﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
   public class UserTypeList
    {
        public int ID { get; set; }
        public string UserType { get; set; }
    }
}
