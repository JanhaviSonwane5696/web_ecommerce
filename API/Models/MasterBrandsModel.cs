﻿using Newtonsoft.Json;
using System;

namespace Models
{
    public class MasterBrandsModel
    {
        public int Id { get; set; }
        public string BrandsName { get; set; }
        public string BrandsLogoURL { get; set; }
        public string BrandsDesciption { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }

    public class AddMasterBrandsModel
    {
        public int Id { get; set; }
        public string BrandsName { get; set; }
        [JsonIgnore] public string BrandsLogoURL { get; set; }
        public string BrandsLogoBytes { get; set; }
        public string BrandsLogoFormat { get; set; }
        public string BrandsDesciption { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
