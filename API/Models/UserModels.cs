﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class UserLoginModel
    {
        [Required] [MaxLength(500)] public string Username { get; set; }

        [Required] [MaxLength(500)] public string Password { get; set; }
    }

    public partial class UsersList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool EmailVerification { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberVerification { get; set; }
        public string AltPhoneNumber { get; set; }
        public string AltEmail { get; set; }
        public string UserType { get; set; }
        public string Gender { get; set; }
        public bool Status { get; set; }
        public string PinCode { get; set; }
        public string Address { get; set; }
        public string GSTIN { get; set; }
        public string ShopName { get; set; }
        public string ShopDescription { get; set; }
        public string Active { get; set; }
        public bool IsLockAccount { get; set; }
        public int LoginFailedCount { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }

    public class UsersListModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string ShopName { get; set; }
    }

    public class ResetUserPasswordModel
    {
        [Required] [MaxLength(500)] public string Username { get; set; }
    }

    public class UserDetailsModel
    {
        [Required] public int Id { get; set; }
        [Required] public string Name { get; set; }
        [Required] public string Username { get; set; }
        [Required] public string UserType { get; set; }
        [Required] public string EmailAddress { get; set; }
        [Required] public bool Status { get; set; }
        public short Type { get; set; }
        public string Gender { get; set; }
    }

    public class AddUserModel
    {
        [Required(ErrorMessage = "UserType is required.")]
        [RegularExpression("[0-9]{1}", ErrorMessage = "UserType is required.")]
        public string UserType { get; set; }

        [MaxLength(10)]
        [Required(ErrorMessage = "Contact Number is required.")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("[0-9]{10}", ErrorMessage = "Contact no should be 10 digits.")]
        public string PhoneNumber { get; set; }

        [MaxLength(50)]
        [Required(ErrorMessage = "EmailID is required.")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Invalid email ID format.")]
        public string EmailID { get; set; }

        [MaxLength(80)]
        [Required(ErrorMessage = "Name is required.")]
        [RegularExpression("[a-zA-Z. ]{3,80}$", ErrorMessage = "Valid characters: Alphabets and min 3.")]
        public string Name { get; set; }

        [RegularExpression("[0-9]{10}", ErrorMessage = "Contact no should be 10 digits.")]
        public string AltPhoneNumber { get; set; }

        [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Invalid email ID format.")]
        public string AltEmail { get; set; }

        [MaxLength(10)]
        [Required(ErrorMessage = "Gender is required.")]
        public string Gender { get; set; }

        [RegularExpression("[0-9]{6}", ErrorMessage = "Pincode should be 6 digits.")]
        public string PinCode { get; set; }

        [RegularExpression("[a-zA-Z0-9. ]{3,500}$", ErrorMessage = "Valid characters: Alphabets and min 3.")]
        public string Address { get; set; }

        [RegularExpression("[a-zA-Z0-9]{3,80}$", ErrorMessage = "Valid characters: Alphabets and min 3.")]
        public string GSTIN { get; set; }

        [RegularExpression("[a-zA-Z0-9. ]{3,80}$", ErrorMessage = "Valid characters: Alphabets and min 3.")]
        public string StoreName { get; set; }

        [RegularExpression("[a-zA-Z0-9. ]{3,80}$", ErrorMessage = "Valid characters: Alphabets and min 3.")]
        public string StoreDescription { get; set; }
        [JsonIgnore]
        public string SignatureImageURL { get; set; }
        public string SignatureImageBytes { get; set; }
        public string SignatureImageBytesFormat { get; set; }

        [RegularExpression("[a-zA-Z0-9. ]{3,80}$", ErrorMessage = "Valid characters: Alphabets and min 3.")]
        public string BankName { get; set; }

        [RegularExpression("[a-zA-Z0-9. ]{3,80}$", ErrorMessage = "Valid characters: Alphabets and min 3.")]
        public string AccountHolderName { get; set; }

        [RegularExpression("[0-9]{9,20}", ErrorMessage = "Bank Account No between 9-20 digit.")]
        public string AccountNumber { get; set; }

        [RegularExpression("^[A-Za-z]{4}0[A-Z0-9a-z]{6}$", ErrorMessage = "Invalid IFSC Code format.")]
        public string Ifsc { get; set; }
        [JsonIgnore]
        public string BlankChequeImageURL { get; set; }
        public string BlankChequeImageBytes { get; set; }
        public string BlankChequeImageBytesFormat { get; set; }
        [JsonIgnore]
        public string ProfileImageURL { get; set; }
        public string ProfileImageBytes { get; set; }
        public string ProfileImageBytesFormat { get; set; }
        public string Active { get; set; }
    }

    public class UpdateUserModel
    {
        [Required] public int Id { get; set; }

        [Required(ErrorMessage = "UserType is required.")]
        [RegularExpression("[0-9]{1}", ErrorMessage = "UserType is required.")]
        public string UserType { get; set; }

        [MaxLength(10)]
        [Required(ErrorMessage = "Contact Number is required.")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("[0-9]{10}", ErrorMessage = "Contact no should be 10 digits.")]
        public string PhoneNumber { get; set; }

        [MaxLength(50)]
        [Required(ErrorMessage = "EmailID is required.")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Invalid email ID format.")]
        public string EmailID { get; set; }

        [MaxLength(80)]
        [Required(ErrorMessage = "Name is required.")]
        [RegularExpression("[a-zA-Z. ]{3,80}$", ErrorMessage = "Valid characters: Alphabets and min 3.")]
        public string Name { get; set; }

        [RegularExpression("[0-9]{10}", ErrorMessage = "Contact no should be 10 digits.")]
        public string AltPhoneNumber { get; set; }

        [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Invalid email ID format.")]
        public string AltEmail { get; set; }

        [MaxLength(10)]
        [Required(ErrorMessage = "Gender is required.")]
        public string Gender { get; set; }

        [RegularExpression("[0-9]{6}", ErrorMessage = "Pincode should be 6 digits.")]
        public string PinCode { get; set; }

        [RegularExpression("[a-zA-Z0-9. ]{3,500}$", ErrorMessage = "Valid characters: Alphabets and min 3.")]
        public string Address { get; set; }

        [RegularExpression("[a-zA-Z0-9]{3,80}$", ErrorMessage = "Valid characters: Alphabets and min 3.")]
        public string GSTIN { get; set; }

        [RegularExpression("[a-zA-Z0-9. ]{3,80}$", ErrorMessage = "Valid characters: Alphabets and min 3.")]
        public string StoreName { get; set; }

        [RegularExpression("[a-zA-Z0-9. ]{3,80}$", ErrorMessage = "Valid characters: Alphabets and min 3.")]
        public string StoreDescription { get; set; }
        [JsonIgnore]
        public string SignatureImageURL { get; set; }
        public string SignatureImageBytes { get; set; }
        public string SignatureImageBytesFormat { get; set; }

        [RegularExpression("[a-zA-Z0-9. ]{3,80}$", ErrorMessage = "Valid characters: Alphabets and min 3.")]
        public string BankName { get; set; }

        [RegularExpression("[a-zA-Z0-9. ]{3,80}$", ErrorMessage = "Valid characters: Alphabets and min 3.")]
        public string AccountHolderName { get; set; }

        [RegularExpression("[0-9]{9,20}", ErrorMessage = "Bank Account No between 9-20 digit.")]
        public string AccountNumber { get; set; }

        [RegularExpression("^[A-Za-z]{4}0[A-Z0-9a-z]{6}$", ErrorMessage = "Invalid IFSC Code format.")]
        public string Ifsc { get; set; }
        [JsonIgnore]
        public string BlankChequeImageURL { get; set; }
        public string BlankChequeImageBytes { get; set; }
        public string BlankChequeImageBytesFormat { get; set; }
        [JsonIgnore]
        public string ProfileImageURL { get; set; }
        public string ProfileImageBytes { get; set; }
        public string ProfileImageBytesFormat { get; set; }
    }

    public class UpdateUserStatusModel
    {
        [Required] public int Id { get; set; }
        [Required]
        public bool IsActive { get; set; }
    }

    public class TokenUserInformationModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }

    public class UserSettingModel
    {
        [MaxLength(30)]
        [Required(ErrorMessage = "Current password is required.")]
        [DataType(DataType.Password)]
        public string CurrentPassword { get; set; }

        [MaxLength(30)]
        [Required(ErrorMessage = "New password is required.")]
        [DataType(DataType.Password)]
        [RegularExpression("^((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])|(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[^a-zA-Z0-9])|(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])).{8,}$", ErrorMessage = "Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character")]
        public string NewPassword { get; set; }

        [MaxLength(30)]
        [Required(ErrorMessage = "Confirm password is required.")]
        [DataType(DataType.Password)]
        [RegularExpression("^((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])|(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[^a-zA-Z0-9])|(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])).{8,}$", ErrorMessage = "Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character")]
        [Compare("NewPassword", ErrorMessage = "Missmatch password.")]
        public string ConfirmPassword { get; set; }

        [JsonIgnore]
        public int UserID { get; set; }
        [JsonIgnore]
        public string ModifyBy { get; set; }
    }

    public class UpdateUserDeviceIDModel
    {
        [Required] public string DeviceID { get; set; }
        [JsonIgnore]public int UserID { get; set; }
        [JsonIgnore] public string ModifiedBy { get; set; }
    }


    #region Customer Models
    public class AddCustomerRequestModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
    }

    public class AddCustomerConfirmRequestModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string OTP { get; set; }
        [Required]
        [RegularExpression("^((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])|(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[^a-zA-Z0-9])|(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])).{8,}$", ErrorMessage = "Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character")]
        public string Password { get; set; }
        [Required]
        [RegularExpression("^((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])|(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[^a-zA-Z0-9])|(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])).{8,}$", ErrorMessage = "Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character")]
        [Compare("Password",ErrorMessage = "Missmatch password.")]
        public string ConfirmPassword { get; set; }
        public string Gender { get; set; }
    }

    public class UpdateCustomerRequestModel
    {
        public int ID { get; set; }
        [Required]
        public string EmailID { get; set; }
    }
    #endregion

    #region Vendor Models
    public class VerifyPhoneNumberModel
    {
        [Required]
        public string PhoneNumber { get; set; }
    }

    public class AddVendorModel
    {
        [Required]
        public string EmailID { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string OTP { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Password { get; set; }
        public string Gender { get; set; }
    }

    public class PickUpAddressModel
    {
        [Required]
        public string PinCode { get; set; }
    }

    public class PickUpAddressResponseModel
    {
        [Required] public string PinCode { get; set; }
        [Required] public string District { get; set; }
        [Required] public string State { get; set; }
        [Required] public string DivisionName { get; set; }
        [Required] public string RegionName { get; set; }
        [Required] public string CircleName { get; set; }
        [Required] public string Taluka { get; set; }
    }

    public class UpdatePickupPinAddressModel
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public string PinCode { get; set; }

        [Required]
        public string Address { get; set; }

        [JsonIgnore]
        public string ModifiedBy { get; set; }
    }

    public class UpdateGSTINModel
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public string GSTIN { get; set; }

        [JsonIgnore]
        public string ModifiedBy { get; set; }
    }

    public class UpdateBankDetailsModel
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public string BankName { get; set; }
        [Required]
        public string AccountHolderName { get; set; }
        [Required]
        public string AccountNumber { get; set; }
        [Required]
        public string IfscCode { get; set; }
        public string BlankChequeImageBytes { get; set; }
        public string BlankChequeImageBytesFormat { get; set; }
        [JsonIgnore] public string BlankChequeImageURL { get; set; }
        [JsonIgnore]
        public string ModifiedBy { get; set; }
    }

    public class StoreDetailsModel
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public string StoreName { get; set; }
        [Required]
        public string StoreDescription { get; set; }

        [JsonIgnore]
        public string ModifiedBy { get; set; }
    }

    public class UpdateSignatureModel
    {
        [Required]
        public int ID { get; set; }
        [Required]
        public string SignatureImageBytes { get; set; }
        [Required]
        public string SignatureImageBytesFormat { get; set; }
        [JsonIgnore] public string SignatureImageURL { get; set; }
        [JsonIgnore] public string ModifiedBy { get; set; }
    }

    public class VendorStoredDetailsModel
    {
        [Required]
        public int ID { get; set; }
    }

    public class VendorDetailsModel
    {
        public string PhoneNumber { get; set; }
        public string EmailID { get; set; }
        public string Name { get; set; }
        public string AltPhoneNumber { get; set; }
        public string AltEmail { get; set; }
        public string Gender { get; set; }
        public string PinCode { get; set; }
        public string Address { get; set; }
        public string GSTIN { get; set; }
        public string StoreName { get; set; }
        public string StoreDescription { get; set; }
        public string SignatureImageURL { get; set; }
        public string ProfileImageURL { get; set; }
        public string Active { get; set; }
        public int UserType { get; set; }
        public List<BanksDetailsModel> Bank { get; set; }
    }
    public class BanksDetailsModel
    {
        public int ID { get; set; }
        public string BankName { get; set; }
        public string AccountHolderName { get; set; }
        public string AccountNumber { get; set; }
        public string Ifsc { get; set; }
        public string BlankChequeImageURL { get; set; }
    }
    public class UpdateProfileDetailsModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string EmailID { get; set; }
        [Required]
        public string Name { get; set; }
        public string AltPhoneNumber { get; set; }
        public string AltEmail { get; set; }
        public string Gender { get; set; }
        public string PinCode { get; set; }
        public string Address { get; set; }
        public string GSTIN { get; set; }
        public string StoreName { get; set; }
        public string StoreDescription { get; set; }
        public string SignatureImageBytes { get; set; }
        public string SignatureImageBytesFormat { get; set; }
        [JsonIgnore] public string SignatureImageURL { get; set; }
        public string ProfileImageBytes { get; set; }
        public string ProfileImageBytesFormat { get; set; }
        [JsonIgnore] public string ProfileImageURL { get; set; }
        [JsonIgnore] public string ModifiedBy { get; set; }
    }
    public class UpdateVendorStatusModel
    {
        [Required]
        public int VendorID { get; set; }
        [Required]
        public string Status { get; set; }
        [Required]
        public string Remarks { get; set; }
        [JsonIgnore] public string ModifiedBy { get; set; }
    }
    public class UserDocumentsListModel
    {
        public int Id { get; set; }
        public string DocumentType { get; set; }
        public bool DocumentNumber { get; set; }
        public string DocumentFilePath { get; set; }
        public string Remarks { get; set; }
        public string Extra1 { get; set; }
        public string Extra2 { get; set; }
        public string Extra3 { get; set; }
        public string Extra4 { get; set; }
        public string Extra5 { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
    #endregion
}