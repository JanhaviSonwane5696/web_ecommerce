﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models
{
    public class BankStateModel
    {
        [Required] public string BankName { get; set; }
    }

    public class BankDistrictModel
    {
        [Required] public string BankName { get; set; }
        [Required] public string StateName { get; set; }
    }

    public class BankBranchModel
    {
        [Required] public string BankName { get; set; }
        [Required] public string StateName { get; set; }
        [Required] public string DistrictName { get; set; }
    }

    public class BankBranchDetailsModel
    {
        [Required] public string BankName { get; set; }
        [Required] public string StateName { get; set; }
        [Required] public string DistrictName { get; set; }
        [Required] public string BranchName { get; set; }
    }

    public class BankDetailsModel
    {
        [Required] public string IfscCode { get; set; }
    }
}
