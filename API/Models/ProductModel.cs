using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Repository.Model
{
    public partial class ProductModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ProductCategoryId { get; set; }
        public string ProductCategory { get; set; }
        public int ProductSubCategoryId { get; set; }
        public string ProductSubCategory { get; set; }
        public string Name { get; set; }
        public string Keywords { get; set; }
        public string SubProductFilterText { get; set; }
        public bool HasMultipleProducts { get; set; }
        public string ProductAttribute { get; set; }
        public string ImageBytes { get; set; }
        public string ImageBytesFormat { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        [JsonIgnore] public string ImageURL { get; set; }
    }

    public class ProductCloneModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ProductCategoryId { get; set; }
        public string ProductCategory { get; set; }
        public int ProductSubCategoryId { get; set; }
        public string ProductSubCategory { get; set; }
        public string Name { get; set; }
        public string Keywords { get; set; }
        public string SubProductFilterText { get; set; }
        public bool HasMultipleProducts { get; set; }
        public string ProductAttribute { get; set; }
        public string ProductItems { get; set; }
        public string ImageBytes { get; set; }
        public string ImageBytesFormat { get; set; }
        public bool IsActive { get; set; }
        [JsonIgnore] public string ImageURL { get; set; }
        [JsonIgnore] public string CreatedBy { get; set; }
    }

    public class ProductUpdateModel
    {
        public int ProductId { get; set; }
        [JsonIgnore] public int UserId { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        [JsonIgnore] public string ModifiedBy { get; set; }
    }

    public class ProductsModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int ProductCategoryId { get; set; }
        public string ProductCategory { get; set; }
        public int ProductSubCategoryId { get; set; }
        public string ProductSubCategory { get; set; }
        public string Name { get; set; }
        public string Keywords { get; set; }
        public string SubProductFilterText { get; set; }
        public bool HasMultipleProducts { get; set; }
        public List<AttributeItemsModel> ProductAttribute { get; set; }
        public string ImageURL { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public ShortProductItemsModel DefaultProductItem { get; set; }
    }

    public class ProductAttributesModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<AttributeItemsModel> ProductAttribute { get; set; }
    }

    public class ShortProductItemsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal? DistributorPrice { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? SalePrice { get; set; }
        public string ImageURL1 { get; set; }
        public string ImageURL2 { get; set; }
        public string ImageURL3 { get; set; }
    }
}