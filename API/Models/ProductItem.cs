using Newtonsoft.Json;
using System.Collections.Generic;

namespace Repository.Model
{
    public partial class ProductItemModel
    {
        public string UserName { get; set; }
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal? DistributorPrice { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal Weight { get; set; }
        public int? Quantity { get; set; }
        public int? MaxQuantityForOrder { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string Specification { get; set; }
        public string LegalDisclaimer { get; set; }
        public string RelatedProducts { get; set; }
        public string CompareProducts { get; set; }
        public string SponsoredProducts { get; set; }
        public string ImageURL1 { get; set; }
        public string ImageThumbURL1 { get; set; }
        public string ImageURL2 { get; set; }
        public string ImageThumbURL2 { get; set; }
        public string ImageURL3 { get; set; }
        public string ImageThumbURL3 { get; set; }
        public string ImageURL4 { get; set; }
        public string ImageThumbURL4 { get; set; }
        public string ImageURL5 { get; set; }
        public string ImageThumbURL5 { get; set; }
        public string ImageURL6 { get; set; }
        public string ImageThumbURL6 { get; set; }
        public string ImageURL7 { get; set; }
        public string ImageThumbURL7 { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }
        public string SKU { get; set; }
        public string MetaTag { get; set; }
        public bool Sale { get; set; }
        public int MinimumQuantity { get; set; }
        public string ProductAttribute { get; set; }
        public int Brand { get; set; }
        public string Tax { get; set; }
        public decimal DeliveryCharge { get; set; }
        public int RemainingQuantity { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public decimal? PercentOff { get; set; }
        public string CountryOfOrigin { get; set; }
    }

    public partial class AddProductItemModel
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal? DistributorPrice { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal Weight { get; set; }
        public int? Quantity { get; set; }
        public int? MaxQuantityForOrder { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string Specification { get; set; }
        public string LegalDisclaimer { get; set; }
        public string RelatedProducts { get; set; }
        public string CompareProducts { get; set; }
        public string SponsoredProducts { get; set; }
        public string ImageBytes1 { get; set; }
        public string ImageBytes1Format { get; set; }
        public string ImageThumbBytes1 { get; set; }
        public string ImageThumbBytes1Format { get; set; }
        public string ImageBytes2 { get; set; }
        public string ImageBytes2Format { get; set; }
        public string ImageThumbBytes2 { get; set; }
        public string ImageThumbBytes2Format { get; set; }
        public string ImageBytes3 { get; set; }
        public string ImageBytes3Format { get; set; }
        public string ImageThumbBytes3 { get; set; }
        public string ImageThumbBytes3Format { get; set; }
        public string ImageBytes4 { get; set; }
        public string ImageBytes4Format { get; set; }
        public string ImageThumbBytes4 { get; set; }
        public string ImageThumbBytes4Format { get; set; }
        public string ImageBytes5 { get; set; }
        public string ImageBytes5Format { get; set; }
        public string ImageThumbBytes5 { get; set; }
        public string ImageThumbBytes5Format { get; set; }
        public string ImageBytes6 { get; set; }
        public string ImageBytes6Format { get; set; }
        public string ImageThumbBytes6 { get; set; }
        public string ImageThumbBytes6Format { get; set; }
        public string ImageBytes7 { get; set; }
        public string ImageBytes7Format { get; set; }
        public string ImageThumbBytes7 { get; set; }
        public string ImageThumbBytes7Format { get; set; }
        public string SKU { get; set; }
        public string MetaTag { get; set; }
        public bool Sale { get; set; }
        public int MinimumQuantity { get; set; }
        public string ProductAttribute { get; set; }
        public int Brand { get; set; }
        public string Tax { get; set; }
        public decimal DeliveryCharge { get; set; }
        public int RemainingQuantity { get; set; }
        public string CountryOfOrigin { get; set; }

        [JsonIgnore] public string ImageURL1 { get; set; }
        [JsonIgnore] public string ImageThumbURL1 { get; set; }
        [JsonIgnore] public string ImageURL2 { get; set; }
        [JsonIgnore] public string ImageThumbURL2 { get; set; }
        [JsonIgnore] public string ImageURL3 { get; set; }
        [JsonIgnore] public string ImageThumbURL3 { get; set; }
        [JsonIgnore] public string ImageURL4 { get; set; }
        [JsonIgnore] public string ImageThumbURL4 { get; set; }
        [JsonIgnore] public string ImageURL5 { get; set; }
        [JsonIgnore] public string ImageThumbURL5 { get; set; }
        [JsonIgnore] public string ImageURL6 { get; set; }
        [JsonIgnore] public string ImageThumbURL6 { get; set; }
        [JsonIgnore] public string ImageURL7 { get; set; }
        [JsonIgnore] public string ImageThumbURL7 { get; set; }
        [JsonIgnore] public int UserID { get; set; }

        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }
    }

    public partial class UpdateProductItemModel
    {
        [JsonIgnore] public int UserId { get; set; }
        public int ProductItemId { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        [JsonIgnore] public string ModifiedBy { get; set; }
    }

    public partial class ProductItemsModel
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal? DistributorPrice { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal Weight { get; set; }
        public int? Quantity { get; set; }
        public int? MaxQuantityForOrder { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string Specification { get; set; }
        public string LegalDisclaimer { get; set; }
        public string RelatedProducts { get; set; }
        public string CompareProducts { get; set; }
        public string SponsoredProducts { get; set; }
        public string ImageURL1 { get; set; }
        public string ImageThumbURL1 { get; set; }
        public string ImageURL2 { get; set; }
        public string ImageThumbURL2 { get; set; }
        public string ImageURL3 { get; set; }
        public string ImageThumbURL3 { get; set; }
        public string ImageURL4 { get; set; }
        public string ImageThumbURL4 { get; set; }
        public string ImageURL5 { get; set; }
        public string ImageThumbURL5 { get; set; }
        public string ImageURL6 { get; set; }
        public string ImageThumbURL6 { get; set; }
        public string ImageURL7 { get; set; }
        public string ImageThumbURL7 { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }
        public string SKU { get; set; }
        public string MetaTag { get; set; }
        public bool Sale { get; set; }
        public int MinimumQuantity { get; set; }
        public List<AttributeItemsModel> ProductAttribute { get; set; }
        public BrandsModel Brand { get; set; }
        public List<TaxRatesModel> Tax { get; set; }
        public decimal DeliveryCharge { get; set; }
        public int RemainingQuantity { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public string UserName { get; set; }
        public decimal? PercentOff { get; set; }
        public string CountryOfOrigin { get; set; }
    }
    public class BrandsModel
    {
        public int Id { get; set; }
        public string BrandsName { get; set; }
        public string BrandsLogoURL { get; set; }
        public string BrandsDesciption { get; set; }
    }
    public class AttributeItemsModel
    {
        public int Id { get; set; }
        public int AttributeId { get; set; }
        public string AttributeItem { get; set; }
        public string AttributeItemDesciption { get; set; }
        public string AttributeName { get; set; }
        public string AttributeType { get; set; }
    }
    public class TaxRatesModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public string Type { get; set; }
    }

    public class ProductItemAttributesModel
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal? DistributorPrice { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? SalePrice { get; set; }
        public int? Quantity { get; set; }
        public List<AttributeItemsModel> ProductAttribute { get; set; }
    }

    public class ShortProductItemModel
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal? DistributorPrice { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? SalePrice { get; set; }
        public int? Quantity { get; set; }
    }
}