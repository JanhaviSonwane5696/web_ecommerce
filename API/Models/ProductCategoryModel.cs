using Newtonsoft.Json;
using System;

namespace Repository.Model
{
    public partial class ProductCategoryModel
    {
        public int Id { get; set; }
        public int ProductCategoryGroupId { get; set; }
        public string ProductCategoryGroup { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageURL { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool ShowHide { get; set; }
        public Nullable<short> Priority { get; set; }
        public string MetaTag { get; set; }
        public string Status { get; set; }
    }

    public partial class AddUpdateProductCategoryModel
    {
        public int Id { get; set; }
        public int ProductCategoryGroupId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageBytes { get; set; }
        public string ImageBytesFormat { get; set; }
        [JsonIgnore] public string ImageURL { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool ShowHide { get; set; }
        public short Priority { get; set; }
        public string MetaTag { get; set; }
        public string Status { get; set; }
    }
}