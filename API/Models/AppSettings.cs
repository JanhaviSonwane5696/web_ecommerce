﻿namespace Models
{
    public class AppSettings
    {
        public string ErrorLogPath { get; set; }
        public string ErrorLogFileName { get; set; }
    }
}