﻿using System;
using System.Collections.Generic;

namespace Repository.Model
{
    public class ShoppingCartModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public string GuiD { get; set; }
        public List<ShoppingCartItemModel> cartItems { get; set; }
    }

    public class ShoppingCartsModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public string GuiD { get; set; }
        public List<ShoppingCartItemsModel> cartItems { get; set; }
    }
}
