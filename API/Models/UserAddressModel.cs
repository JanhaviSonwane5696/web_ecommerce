﻿using Newtonsoft.Json;

namespace Models
{
    public class UserAddressModel
    {
        public int Id { get; set; }
        public string AddressType { get; set; }
        public bool IsPrimary { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Landmark { get; set; }
        public string ZipPostal { get; set; }
        public string Phone { get; set; }
    }

    public class AddUserAddressModel
    {
        [JsonIgnore]
        public int UserId { get; set; }
        public string AddressType { get; set; }
        public bool IsPrimary { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Landmark { get; set; }
        public string ZipPostal { get; set; }
        public string Phone { get; set; }
        public bool IsDeleted { get; set; }
        [JsonIgnore]
        public string CreatedBy { get; set; }
    }

    public class UpdateUserAddressModel
    {
        public int Id { get; set; }
        [JsonIgnore]
        public int UserId { get; set; }
        public string AddressType { get; set; }
        public bool IsPrimary { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Landmark { get; set; }
        public string ZipPostal { get; set; }
        public string Phone { get; set; }
        [JsonIgnore]
        public string ModifiedBy { get; set; }
    }

    public class DeleteUserAddressModel
    {
        public int Id { get; set; }
        [JsonIgnore]
        public int UserId { get; set; }
        public bool IsDeleted { get; set; }
        [JsonIgnore]
        public string ModifiedBy { get; set; }
    }
}
