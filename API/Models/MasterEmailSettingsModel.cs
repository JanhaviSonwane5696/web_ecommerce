﻿using System;

namespace Models
{
    public class MasterEmailSettingsModel
    {
        public int Id { get; set; }
        public string HostName { get; set; }
        public string FromEmailAddress { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public string Port { get; set; }
        public string Password { get; set; }
        public bool IsMailActive { get; set; }
        public string FromName { get; set; }
        public bool MailSSL { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
