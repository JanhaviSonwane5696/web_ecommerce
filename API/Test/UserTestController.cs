using API.Controllers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Models;
using Repository.Context;
using Repository.Helpers;
using Repository.Interface;
using Repository.Repository;
using Services.Interface;
using Services.Service;
using Xunit;

namespace Test
{
    public class UserTestController
    {
        static UserTestController()
        {
            Configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            var connectionString = Configuration.GetConnectionString("DefaultDatabase");
            DbContextOptions = new DbContextOptionsBuilder<DataContext>()
                .UseSqlServer(connectionString)
                .Options;
        }

        public UserTestController()
        {
            //var context = new DataContext(DbContextOptions);
            //IUserRepository userRepository = new UserRepository(context);
            //IUserTypesRepository userTypeRepository = new UserTypesRepository(context);
            //IUsersVerificationRepository userVerificationRepository = new UsersVerificationRepository(context);
            //IUsersBankRepository userbankRepository = new UsersBankRepository(context);
            //INotificationHelpers notificationRepository = new NotificationHelpers(context);
            //ILoggerService loggerService = new LoggerService();
            //IHostingEnvironment hostingEnvironment = new HostingEnvironment();
            //var userServices = new UserServices(userRepository, userTypeRepository, userVerificationRepository, userbankRepository, notificationRepository, loggerService);
            //_userController = new UserController(hostingEnvironment, userServices, Configuration, loggerService);
        }

        public static IConfiguration Configuration { get; }
        private static UserController _userController;
        public static DbContextOptions<DataContext> DbContextOptions { get; }

        [Fact]
        public void LoginUser()
        {
            //Prepare

            var userLoginModel = new UserLoginModel
            {
                Username = "TEST@TEST.COM",
                Password = "PWD"
            };

            //Act`
            var returnData = _userController.AuthenticateUser(userLoginModel);

            //Assert
            Assert.NotNull(returnData);
        }
    }
}