﻿namespace Helpers
{
    public class OutputResponse<T>
    {
        public int Code { get; set; }

        public string Msg { get; set; }

        public T Data { get; set; }

        public static OutputResponse<T> GetResult(int code, string msg, T data = default)
        {
            return new OutputResponse<T>
            {
                Code = code,
                Msg = msg,
                Data = data
            };
        }
    }
}