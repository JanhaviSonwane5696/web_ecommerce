﻿using System;
using System.Text;

namespace Helpers
{
    public static class Encryption
    {
        #region Public Method

        /// <summary>
        ///     To Decrypt
        /// </summary>
        /// <param name="encryptPwd"></param>
        /// <returns>string</returns>
        public static string DecryptData(string encryptPwd)
        {
            var encodePwd = new UTF8Encoding();
            var decode = encodePwd.GetDecoder();
            var todecodeByte = Convert.FromBase64String(encryptPwd);
            var charCount = decode.GetCharCount(todecodeByte, 0, todecodeByte.Length);
            var decodedChar = new char[charCount];
            decode.GetChars(todecodeByte, 0, todecodeByte.Length, decodedChar, 0);
            var decryptPwd = new string(decodedChar);
            return decryptPwd;
        }

        /// <summary>
        ///     To Encrypt
        /// </summary>
        /// <param name="password"></param>
        /// <returns>string</returns>
        public static string EncryptData(string password)
        {
            var encode = new byte[password.Length];
            encode = Encoding.UTF8.GetBytes(password);
            var strMsg = Convert.ToBase64String(encode);
            return strMsg;
        }

        #endregion
    }
}