﻿using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace Helpers
{
    public class JsonResponse
    {
        public static JsonResult Create(HttpStatusCode code, string message, dynamic body)
        {
            return new JsonResult(new Response {Code = code.ToString(), Message = message, Body = body});
        }

        public static JsonResult Create(HttpStatusCode code, string message)
        {
            return new JsonResult(new Response {Code = code.ToString(), Message = message});
        }

        public static JsonResult Create(HttpStatusCode code, dynamic body)
        {
            return new JsonResult(new Response {Code = code.ToString(), Body = body});
        }
    }

    public class Response : Status
    {
        public dynamic Body { get; set; }
    }

    public class Status
    {
        public string Message { get; set; }
        public string Code { get; set; }
    }
}