﻿namespace Helpers
{
    public static class Variables
    {
        public struct DocumentType
        {
            public static string Signature
            { get { return "Signature"; } }
            public static string Profile
            { get { return "Profile"; } }
            public static string AadhaarFront
            { get { return "Aadhaar Front"; } }
            public static string AadhaarBack
            { get { return "Aadhaar Back"; } }
            public static string PanCard
            { get { return "PanCard"; } }
        }

        public struct UserStatus
        {
            public static string Processing
            { get { return "Processing"; } }
            public static string Submitted
            { get { return "Submitted"; } }
            public static string StoreUpdated
            { get { return "Store Updated"; } }
            public static string BankUpdated
            { get { return "Bank Updated"; } }
            public static string Rejected
            { get { return "Rejected"; } }
            public static string GSTINUpdated
            { get { return "GSTIN Updated"; } }
            public static string AddressUpdated
            { get { return "Address Updated"; } }
            public static string Verified
            { get { return "Verified"; } }
            public static string Registered
            { get { return "Registered"; } }
        }

        public struct APIStatus
        {
            public static int Failed
            { get { return 0; } }
            public static int Success
            { get { return 1; } }
            public static int Pending
            { get { return 2; } }
            public static int Exception
            { get { return 400; } }
        }

        public struct ProductStatus
        {
            public static string Accepted
            { get { return "Accepted"; } }
            public static string Rejected
            { get { return "Rejected"; } }
            public static string Approved
            { get { return "Approved"; } }
        }

        public struct ShippingState
        {
            public static string Maharashtra
            { get { return "Maharashtra"; } }
            public static string Other
            { get { return "Other"; } }
        }

        public struct OrderStatus
        {
            public static string Pending
            { get { return "Pending"; } }
            public static string Accepted
            { get { return "Accepted"; } }
            public static string UnderProcess
            { get { return "Under Process"; } }
            public static string Dispatched
            { get { return "Dispatched"; } }
            public static string OutForDelivery
            { get { return "Out For Delivery"; } }
            public static string Complete
            { get { return "Complete"; } }
            public static string Failed
            { get { return "Failed"; } }
            public static string Cancel
            { get { return "Cancel"; } }
            public static string CancelltionUnderProcess
            { get { return "Cancelltion Under Process"; } }
            public static string RefundUnderProcess
            { get { return "Refund Under Process"; } }
        }

        public struct ShippingPaymentMode
        {
            public static string WALLET { get { return "WALLET"; } }
            public static string COD { get { return "COD"; } }
            public static string PG { get { return "PG"; } }
        }

        public struct PaymentStatus
        {
            public static string Pending
            { get { return "Pending"; } }
            public static string Complete
            { get { return "Complete"; } }
            public static string Canceled
            { get { return "Canceled"; } }
        }
    }
}