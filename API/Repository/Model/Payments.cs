﻿using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public class Payments
    {
        [Key] public int Id { get; set; }
        public int UserId { get; set; }
        public int OrderId { get; set; }
        public string PaymentMode { get; set; }
        public string Status { get; set; }
        public decimal Amount { get; set; }
        public System.DateTime Date { get; set; }
        public string Notes { get; set; }
        public decimal WalletAmount { get; set; }
        public decimal PGAmount { get; set; }
        public decimal ShopAmount { get; set; }
        public string BillHTML { get; set; }
    }
}
