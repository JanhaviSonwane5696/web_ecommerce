﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public class MasterEmailTemplates
    {
        [Key]
        public int Id { get; set; }
        public string Type { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}