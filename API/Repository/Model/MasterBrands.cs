﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public class MasterBrands
    {
        [Key]
        public int Id { get; set; }
        public string BrandsName { get; set; }
        public string BrandsLogoURL { get; set; }
        public string BrandsDesciption { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
