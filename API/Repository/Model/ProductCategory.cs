using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public partial class ProductCategory
    {
        [Key] public int Id { get; set; }
        public int ProductCategoryGroupId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageURL { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool ShowHide { get; set; }
        public Nullable<short> Priority { get; set; }
        public string MetaTag { get; set; }
        public string Status { get; set; }
    }
}