﻿using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public class PinCodes
    {
        [Key] public int Id { get; set; }
        public double PinCode { get; set; }
        public string DivisionName { get; set; }
        public string RegionName { get; set; }
        public string CircleName { get; set; }
        public string Taluk { get; set; }
        public string DistrictName { get; set; }
        public string StateName { get; set; }
    }
}
