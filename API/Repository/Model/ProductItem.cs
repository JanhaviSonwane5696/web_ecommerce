using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public partial class ProductItem
    {
        [Key] public int Id { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal? DistributorPrice { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal Weight { get; set; }
        public int? Quantity { get; set; }
        public int? MaxQuantityForOrder { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string Specification { get; set; }
        public string LegalDisclaimer { get; set; }
        public string RelatedProducts { get; set; }
        public string CompareProducts { get; set; }
        public string SponsoredProducts { get; set; }
        public string ImageURL1 { get; set; }
        public string ImageThumbURL1 { get; set; }
        public string ImageURL2 { get; set; }
        public string ImageThumbURL2 { get; set; }
        public string ImageURL3 { get; set; }
        public string ImageThumbURL3 { get; set; }
        public string ImageURL4 { get; set; }
        public string ImageThumbURL4 { get; set; }
        public string ImageURL5 { get; set; }
        public string ImageThumbURL5 { get; set; }
        public string ImageURL6 { get; set; }
        public string ImageThumbURL6 { get; set; }
        public string ImageURL7 { get; set; }
        public string ImageThumbURL7 { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public string SKU { get; set; }
        public string MetaTag { get; set; }
        public bool Sale { get; set; }
        public int MinimumQuantity { get; set; }
        public string ProductAttribute { get; set; }
        public int Brand { get; set; }
        public string Tax { get; set; }
        public decimal DeliveryCharge { get; set; }
        public int RemainingQuantity { get; set; }
        public string Status { get; set; }
        public string AdminRemarks { get; set; }
        public string CountryOfOrigin { get; set; }
        public int DeliveryStart { get; set; }
        public int DeliveryEnd { get; set; }
    }
}
