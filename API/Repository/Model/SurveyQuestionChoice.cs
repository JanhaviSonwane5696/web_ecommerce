using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public partial class SurveyQuestionChoice
    {
        [Key] public int Id { get; set; }
        public int ProductSurveyQuestionId { get; set; }
        public string ChoiceText { get; set; }
        public string ChoiceValue { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
