﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public partial class UsersType
    {
        [Key] public Int16 ID { get; set; }
        public string UserType { get; set; }
    }
}
