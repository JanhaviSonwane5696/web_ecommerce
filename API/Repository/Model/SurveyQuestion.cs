using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public partial class SurveyQuestion
    {
        [Key] public int Id { get; set; }
        public int ProductSurveyId { get; set; }
        public string Question { get; set; }
        public string QuestionType { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
