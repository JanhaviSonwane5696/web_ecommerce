﻿namespace Repository.Model
{
    public class ProcessPay
    {
        public long ID { get; set; }
        public int UserID { get; set; }
        public int PaymentID { get; set; }
        public int OrderID { get; set; }
        public decimal Amount { get; set; }
        public string TransactionDesc { get; set; }
        public bool Status { get; set; }
        public string OtherRemarks { get; set; }
        public string EmailID { get; set; }
        public string PhoneNumber { get; set; }
        public System.DateTime DateAdded { get; set; }
    }
}
