using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public partial class UserAddress
    {
        [Key] public int Id { get; set; }
        public int UserId { get; set; }
        public string AddressType { get; set; }
        public bool IsPrimary { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Landmark { get; set; }
        public string ZipPostal { get; set; }
        public string Phone { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
