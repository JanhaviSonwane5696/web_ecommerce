﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public class ShoppingCartItems
    {
        [Key]
        public int Id { get; set; }
        public int ShoppingCardID { get; set; }
        public int ProductId { get; set; }
        public int ProductItemId { get; set; }
        public int Quantity { get; set; }
        public DateTime DateAdded { get; set; }
        public bool IsDeleted { get; set; }
    }
}
