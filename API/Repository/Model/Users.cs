using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public partial class Users
    {
        [Key] public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool EmailVerification { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberVerification { get; set; }
        public string PasswordHash { get; set; }
        public string AltPhoneNumber { get; set; }
        public string AltEmail { get; set; }
        public short UserType { get; set; }
        public string Gender { get; set; }
        public bool Status { get; set; }
        public bool IsLockAccount { get; set; }
        public int LoginFailedCount { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public string VerifyData { get; set; }
        public Nullable<DateTime> VerifyDate { get; set; }
        public string PinCode { get; set; }
        public string Address { get; set; }
        public string GSTIN { get; set; }
        public string ShopName { get; set; }
        public string ShopDescription { get; set; }
        public string Active { get; set; }
        public string DeviceID { get; set; }
    }
}
