﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public class MasterAttributeItems
    {
        [Key]
        public int Id { get; set; }
        public Nullable<int> AttributeId { get; set; }
        public string AttributeItem { get; set; }
        public string AttributeItemDesciption { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
