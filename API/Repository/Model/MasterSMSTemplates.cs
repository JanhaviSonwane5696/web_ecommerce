﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public class MasterSMSTemplates
    {
        [Key]
        public int Id { get; set; }
        public string Type { get; set; }
        public string SMSContent { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}