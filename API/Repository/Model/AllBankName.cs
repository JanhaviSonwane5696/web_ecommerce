﻿using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public class AllBankName
    {
        [Key] public int ID { get; set; }
        public string BANK { get; set; }
        public string IFSC { get; set; }
        public string BRANCH { get; set; }
        public string ADDRESS { get; set; }
        public double CONTACT { get; set; }
        public string CITY { get; set; }
        public string DISTRICT { get; set; }
        public string STATE { get; set; }
    }
}
