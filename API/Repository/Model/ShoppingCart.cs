﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public class ShoppingCart
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public string GuiD { get; set; }
    }
}
