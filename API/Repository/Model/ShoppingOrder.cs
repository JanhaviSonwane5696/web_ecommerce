﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public class ShoppingOrder
    {
        [Key] public int Id { get; set; }
        public string OrderId { get; set; }
        public int UserId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Tax { get; set; }
        public decimal Shipping { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal PrimePer { get; set; }
        public decimal PrimePoint { get; set; }
        public System.DateTime DatePlaced { get; set; }
        public System.DateTime DateUpdated { get; set; }
        public int BillingAddress { get; set; }
        public int ShipppingAddress { get; set; }
        public Nullable<int> PaymentId { get; set; }
        public string BILLHTML { get; set; }
        public string IPAddress { get; set; }
        public string Status { get; set; }
        public bool IsActive { get; set; }
        public string ItemSize { get; set; }
        public string ItemColor { get; set; }
        public string AdminComment { get; set; }
        public string ResellerComment { get; set; }
        public decimal AdminOffer { get; set; }
        public decimal Repurchase { get; set; }
        public Nullable<System.DateTime> PayDate { get; set; }
        public bool PayStatus { get; set; }
        public string BILLHTMLS { get; set; }
    }
}
