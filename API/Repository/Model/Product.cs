using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public partial class Product
    {
        [Key] public int Id { get; set; }
        public int UserId { get; set; }
        public int ProductCategoryId { get; set; }
        public int ProductSubCategoryId { get; set; }
        public string Name { get; set; }
        public string Keywords { get; set; }
        public string SubProductFilterText { get; set; }
        public bool HasMultipleProducts { get; set; }
        public string ProductAttribute { get; set; }
        public string ImageURL { get; set; }
        public bool IsActive { get; set; }
        public string Status { get; set; }
        public string AdminRemarks { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}