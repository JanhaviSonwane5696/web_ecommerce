using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public partial class UserDocuments
    {
        [Key] public int Id { get; set; }
        public int UserId { get; set; }
        public string DocumentType { get; set; }
        public bool DocumentNumber { get; set; }
        public string DocumentFilePath { get; set; }
        public string Remarks { get; set; }
        public string Extra1 { get; set; }
        public string Extra2 { get; set; }
        public string Extra3 { get; set; }
        public string Extra4 { get; set; }
        public string Extra5 { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
