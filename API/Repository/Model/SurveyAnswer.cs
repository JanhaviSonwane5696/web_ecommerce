using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public partial class SurveyAnswer
    {
        [Key] public int Id { get; set; }
        public int ProductId { get; set; }
        public int SurveyId { get; set; }
        public string Question { get; set; }
        public string Value { get; set; }
        public string QuestionChoice { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
