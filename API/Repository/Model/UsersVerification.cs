﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public class UsersVerification
    {
        [Key] public int ID { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int UserType { get; set; }
        public DateTime AddOn { get; set; }
        public string VerifyData { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
