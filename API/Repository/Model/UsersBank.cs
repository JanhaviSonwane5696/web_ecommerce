﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Repository.Model
{
   public class UsersBank
    {
        [Key] public int ID { get; set; }
        public int UserID { get; set; }
        public string BankName { get; set; }
        public string AccountNo { get; set; }
        public string IfscCode { get; set; }
        public string Name { get; set; }
        public string ChequeURL { get; set; }
        public DateTime AddDate { get; set; }
        public DateTime EditDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    }
}
