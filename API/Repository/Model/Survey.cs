using System;
using System.ComponentModel.DataAnnotations;

namespace Repository.Model
{
    public partial class Survey
    {
        [Key] public int Id { get; set; }
        public string SurveyType { get; set; }
        public string SurveyDescription { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
