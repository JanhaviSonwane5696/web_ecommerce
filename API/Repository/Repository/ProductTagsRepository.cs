﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Models;
using Repository.Context;
using Repository.Interface;
using Repository.Model;

namespace Repository.Repository
{
    public class ProductTagsRepository : IProductTagsRepository
    {
        private readonly DataContext _context;

        /// <summary>
        /// Repository
        /// </summary>
        /// <param name="context"></param>
        public ProductTagsRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get Product Tags
        /// </summary>
        /// <returns></returns>
        public List<ProductTags> GetProductTags()
        {
            var productTags = _context.ProductTags.Where(x => x.IsDeleted == false).ToList();
            return productTags;
        }

        /// <summary>
        /// Add Product Tags
        /// </summary>
        /// <returns></returns>
        public bool AddProductTags(ProductTags productTags)
        {
            _context.ProductTags.Add(productTags);
            _context.SaveChanges();
            return true;
        }

        /// <summary>
        /// Update Product Tags
        /// </summary>
        /// <returns></returns>
        public bool UpdateProductTags(ProductTags productTags)
        {
            var getProductTags = _context.ProductTags.FirstOrDefault(x => x.Id == productTags.Id);
            if (getProductTags != null)
            {
                getProductTags.Name = productTags.Name;
                getProductTags.Description = productTags.Description;
                getProductTags.ModifiedBy = productTags.ModifiedBy;
                getProductTags.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Delete Product Tags
        /// </summary>
        /// <returns></returns>
        public bool DeleteProductTags(int id, string modifiedBy)
        {
            var productTags = _context.ProductTags.FirstOrDefault(x => x.Id == id);
            if (productTags != null)
            {
                productTags.IsDeleted = true;
                productTags.ModifiedBy = modifiedBy;
                productTags.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Activate / Deactivate Product Tags
        /// </summary>
        /// <returns></returns>
        public bool UpdateStatusProductTags(int id, bool status, string modifiedBy)
        {
            var productTags = _context.ProductTags.FirstOrDefault(x => x.Id == id);
            productTags.IsActive = status;
            productTags.ModifiedBy = modifiedBy;
            productTags.ModifiedOn = DateTime.Now;
            _context.SaveChanges();
            return true;
        }
    }
}