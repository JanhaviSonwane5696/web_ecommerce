﻿using System;
using System.Collections.Generic;
using System.Linq;
using Repository.Context;
using Repository.Interface;
using Repository.Model;

namespace Repository.Repository
{
    public class ProductRelatedRepository : IProductRelatedRepository
    {
        private readonly DataContext _context;

        /// <summary>
        /// Repository
        /// </summary>
        /// <param name="context"></param>
        public ProductRelatedRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get Product Category
        /// </summary>
        /// <returns></returns>
        public List<ProductRelated> GetProductRelated()
        {
            var productRelated = _context.ProductRelated.Where(x => x.IsDeleted == false).ToList();
            return productRelated;
        }

        /// <summary>
        /// Add Product Category
        /// </summary>
        /// <returns></returns>
        public bool AddProductRelated(ProductRelated productRelated)
        {
            ProductRelated productCategoryData = new ProductRelated()
            {
                Id = productRelated.Id,
                Name = productRelated.Name,
                Description = productRelated.Description,
                ModifiedOn = productRelated.ModifiedOn,
                ModifiedBy = productRelated.ModifiedBy,
                Status = productRelated.Status,
                CreatedBy = productRelated.CreatedBy,
                CreatedOn = productRelated.CreatedOn,
                IsActive = productRelated.IsActive,
                IsDeleted = productRelated.IsDeleted
            };
            _context.ProductRelated.Add(productCategoryData);
            _context.SaveChanges();
            return true;
        }

        /// <summary>
        /// Update Product Category
        /// </summary>
        /// <returns></returns>
        public bool UpdateProductRelated(ProductRelated productRelated)
        {
            var getProductRelated = _context.ProductRelated.FirstOrDefault(x => x.Id == productRelated.Id);
            if (getProductRelated != null)
            {
                getProductRelated.Name = productRelated.Name;
                getProductRelated.Description = productRelated.Description;
                getProductRelated.ModifiedBy = productRelated.ModifiedBy;
                getProductRelated.ModifiedOn = DateTime.Now;
                getProductRelated.Status = productRelated.Status;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Delete Product Category
        /// </summary>
        /// <returns></returns>
        public bool DeleteProductRelated(int id, string modifiedBy)
        {
            var productRelated = _context.ProductRelated.FirstOrDefault(x => x.Id == id);
            if (productRelated != null)
            {
                productRelated.IsDeleted = true;
                productRelated.ModifiedBy = modifiedBy;
                productRelated.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Activate / Deactivate Product Category
        /// </summary>
        /// <returns></returns>
        public bool UpdateStatusProductRelated(int id, bool status, string modifiedBy)
        {
            var productRelated = _context.ProductRelated.FirstOrDefault(x => x.Id == id);
            productRelated.IsActive = status;
            productRelated.ModifiedBy = modifiedBy;
            productRelated.ModifiedOn = DateTime.Now;
            _context.SaveChanges();
            return true;
        }
    }
}