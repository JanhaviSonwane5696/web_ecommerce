﻿using System;
using System.Collections.Generic;
using System.Linq;
using Repository.Context;
using Repository.Interface;
using Repository.Model;

namespace Repository.Repository
{
    public class MasterAttributeItemsRepository : IMasterAttributeItemsRepository
    {
        private readonly DataContext _context;

        /// <summary>
        /// Repository
        /// </summary>
        /// <param name="context"></param>
        public MasterAttributeItemsRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get MasterAttributeItems Category
        /// </summary>
        /// <returns></returns>
        public List<MasterAttributeItems> GetMasterAttributeItems(int attributeId)
        {
            var attributeItem = _context.MasterAttributeItems.Where(x => x.IsDeleted == false && x.AttributeId == attributeId).ToList();
            return attributeItem;
        }

        /// <summary>
        /// Get MasterAttributeItems Category
        /// </summary>
        /// <returns></returns>
        public List<MasterAttributeItems> GetMasterAttributeItemsById(int attributeItemId)
        {
            var attributeItem = _context.MasterAttributeItems.Where(x => x.IsDeleted == false && x.Id == attributeItemId).ToList();
            return attributeItem;
        }

        /// <summary>
        /// Get MasterAttributeItems Category
        /// </summary>
        /// <returns></returns>
        public List<MasterAttributeItems> GetMasterAllAttributeItems()
        {
            var attributeItem = _context.MasterAttributeItems.Where(x => x.IsDeleted == false).ToList();
            return attributeItem;
        }

        /// <summary>
        /// Add MasterAttributeItems Category
        /// </summary>
        /// <returns></returns>
        public bool AddMasterAttributeItems(MasterAttributeItems attributeItem)
        {
            _context.MasterAttributeItems.Add(attributeItem);
            _context.SaveChanges();
            return true;
        }

        /// <summary>
        /// Update MasterAttributeItems 
        /// </summary>
        /// <returns></returns>
        public bool UpdateMasterAttributeItems(MasterAttributeItems attributeItem)
        {
            var getMasterAttributeItems = _context.MasterAttributeItems.FirstOrDefault(x => x.Id == attributeItem.Id);
            if (getMasterAttributeItems != null)
            {
                getMasterAttributeItems.AttributeItem = attributeItem.AttributeItem;
                getMasterAttributeItems.AttributeId = attributeItem.AttributeId;
                getMasterAttributeItems.AttributeItemDesciption = attributeItem.AttributeItemDesciption;
                getMasterAttributeItems.ModifiedBy = attributeItem.ModifiedBy;
                getMasterAttributeItems.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Delete MasterAttributeItems 
        /// </summary>
        /// <returns></returns>
        public bool DeleteMasterAttributeItems(int id, string modifiedBy)
        {
            var attributeItem = _context.MasterAttributeItems.FirstOrDefault(x => x.Id == id);
            if (attributeItem != null)
            {
                attributeItem.IsDeleted = true;
                attributeItem.ModifiedBy = modifiedBy;
                attributeItem.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Activate / Deactivate MasterAttributeItems Category
        /// </summary>
        /// <returns></returns>
        public bool UpdateStatusMasterAttributeItems(int id, bool status, string modifiedBy)
        {
            var attributeItem = _context.MasterAttributeItems.FirstOrDefault(x => x.Id == id);
            attributeItem.IsActive = status;
            attributeItem.ModifiedBy = modifiedBy;
            attributeItem.ModifiedOn = DateTime.Now;
            _context.SaveChanges();
            return true;
        }
    }
}