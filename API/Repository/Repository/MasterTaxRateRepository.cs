﻿using System;
using System.Collections.Generic;
using System.Linq;
using Repository.Context;
using Repository.Interface;
using Repository.Model;

namespace Repository.Repository
{
    public class MasterTaxRateRepository : IMasterTaxRateRepository
    {
        private readonly DataContext _context;

        /// <summary>
        /// Repository
        /// </summary>
        /// <param name="context"></param>
        public MasterTaxRateRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get MasterTaxRate Category
        /// </summary>
        /// <returns></returns>
        public List<MasterTaxRates> GetMasterTaxRate(int taxId)
        {
            var taxRate = _context.MasterTaxRates.Where(x => x.IsDeleted == false && x.Id == taxId).ToList();
            return taxRate;
        }

        /// <summary>
        /// Get MasterTaxRate Category
        /// </summary>
        /// <returns></returns>
        public List<MasterTaxRates> GetAllMasterTaxRate()
        {
            var taxRate = _context.MasterTaxRates.Where(x => x.IsDeleted == false).ToList();
            return taxRate;
        }

        /// <summary>
        /// Add MasterTaxRate Category
        /// </summary>
        /// <returns></returns>
        public bool AddMasterTaxRate(MasterTaxRates taxRate)
        {
            _context.MasterTaxRates.Add(taxRate);
            _context.SaveChanges();
            return true;
        }

        /// <summary>
        /// Update MasterTaxRate 
        /// </summary>
        /// <returns></returns>
        public bool UpdateMasterTaxRate(MasterTaxRates taxRate)
        {
            var getMasterTaxRate = _context.MasterTaxRates.FirstOrDefault(x => x.Id == taxRate.Id);
            if (getMasterTaxRate != null)
            {
                getMasterTaxRate.Name = taxRate.Name;
                getMasterTaxRate.Type = taxRate.Type;
                getMasterTaxRate.Amount = taxRate.Amount;
                getMasterTaxRate.ModifiedBy = taxRate.ModifiedBy;
                getMasterTaxRate.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Delete MasterTaxRate 
        /// </summary>
        /// <returns></returns>
        public bool DeleteMasterTaxRate(int id, string modifiedBy)
        {
            var taxRate = _context.MasterTaxRates.FirstOrDefault(x => x.Id == id);
            if (taxRate != null)
            {
                taxRate.IsDeleted = true;
                taxRate.ModifiedBy = modifiedBy;
                taxRate.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Activate / Deactivate MasterTaxRate Category
        /// </summary>
        /// <returns></returns>
        public bool UpdateStatusMasterTaxRate(int id, bool status, string modifiedBy)
        {
            var taxRate = _context.MasterTaxRates.FirstOrDefault(x => x.Id == id);
            taxRate.IsActive = status;
            taxRate.ModifiedBy = modifiedBy;
            taxRate.ModifiedOn = DateTime.Now;
            _context.SaveChanges();
            return true;
        }
    }
}