﻿using Microsoft.EntityFrameworkCore;
using Models;
using Repository.Context;
using Repository.Interface;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repository.Repository
{
    public class NotificationRepository : INotificationRepository
    {
        private readonly DataContext _context;

        /// <summary>
        ///     Repository
        /// </summary>
        /// <param name="context"></param>
        public NotificationRepository(DataContext context)
        {
            _context = context;
        }

        #region Add SMS API Method
        public List<MasterSMSApi> SMSAPIList()
        {
            return _context.MasterSMSApi.AsNoTracking().OrderBy(x => x.Id).ToList();
        }

        public Tuple<bool, int, string> AddSMSAPI(AddNotificationSMSAPIModel addSMS, string createdBy)
        {
            MasterSMSApi _add = new MasterSMSApi()
            {
                APIName = "UNIVERSAL",
                Username = addSMS.SMSContent,
                Status = addSMS.IsActive,
                Password = null,
                SenderId = null,
                CreatedBy = createdBy,
                CreatedOn = DateTime.Now,
                ModifiedBy = null,
                ModifiedOn = null
            };
            _context.MasterSMSApi.Add(_add);
            _context.SaveChanges();

            return new Tuple<bool, int, string>(true, _add.Id, "SMS API Created Successfully");
        }

        public Tuple<bool, int, string> UpdateSMSAPI(UpdateNotificationSMSAPIModel updateSMS, string createdBy)
        {
            var smsAPI = _context.MasterSMSApi.AsNoTracking().FirstOrDefault(x => x.Id == updateSMS.Id);
            if (smsAPI != null)
            {
                smsAPI.Username = updateSMS.SMSContent;
                smsAPI.Status = updateSMS.IsActive;
                smsAPI.ModifiedOn = DateTime.Now;
                smsAPI.ModifiedBy = createdBy;
                _context.MasterSMSApi.Update(smsAPI);
                _context.SaveChanges();

                return new Tuple<bool, int, string>(true, smsAPI.Id, "SMS api updated Successfully.");
            }
            return new Tuple<bool, int, string>(false, 0, "SMS API not exist.");
        }

        public Tuple<bool, int, string> UpdateSMSAPIStatus(UpdateNotificationSMSAPIStatusModel updateSMS, string createdBy)
        {
            var smsAPI = _context.MasterSMSApi.AsNoTracking().FirstOrDefault(x => x.Id == updateSMS.Id);
            if (smsAPI != null)
            {
                smsAPI.Status = updateSMS.IsActive;
                smsAPI.ModifiedOn = DateTime.Now;
                smsAPI.ModifiedBy = createdBy;
                _context.MasterSMSApi.Update(smsAPI);
                _context.SaveChanges();

                return new Tuple<bool, int, string>(true, smsAPI.Id, "SMS api status updated Successfully.");
            }
            return new Tuple<bool, int, string>(false, 0, "SMS API not exist.");
        }
        #endregion

        #region Add SMS Template Method
        public List<MasterSMSTemplates> SMSAPITemplateList()
        {
            return _context.MasterSMSTemplates.AsNoTracking().Where(x => !x.IsDeleted).OrderBy(x => x.Type).ToList();
        }

        public Tuple<bool, int, string> AddSMS(AddNotificationSMSModel addSMS, string createdBy)
        {
            var smsTemplate = _context.MasterSMSTemplates.AsNoTracking().FirstOrDefault(x => x.Type.ToUpper() == addSMS.Type);
            if (smsTemplate == null)
            {
                MasterSMSTemplates _add = new MasterSMSTemplates()
                {
                    Type = addSMS.Type,
                    SMSContent = addSMS.SMSContent,
                    IsActive = addSMS.IsActive,
                    IsDeleted = false,
                    CreatedBy = createdBy,
                    CreatedOn = DateTime.Now,
                    ModifiedBy = null,
                    ModifiedOn = null
                };
                _context.MasterSMSTemplates.Add(_add);
                _context.SaveChanges();

                return new Tuple<bool, int, string>(true, _add.Id, "SMS template Created Successfully");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "SMS type already exist.");
        }

        public Tuple<bool, int, string> UpdateSMS(UpdateNotificationSMSModel updateSMS, string createdBy)
        {
            var smsTemplate = _context.MasterSMSTemplates.AsNoTracking().FirstOrDefault(x => x.Type.ToUpper() == updateSMS.Type.ToUpper());
            if (smsTemplate != null)
            {
                smsTemplate.SMSContent = updateSMS.SMSContent;
                smsTemplate.IsActive = updateSMS.IsActive;
                smsTemplate.ModifiedOn = DateTime.Now;
                smsTemplate.ModifiedBy = createdBy;
                _context.MasterSMSTemplates.Update(smsTemplate);
                _context.SaveChanges();

                return new Tuple<bool, int, string>(true, smsTemplate.Id, "SMS template updated Successfully");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "SMS template not exist.");
        }

        public Tuple<bool, int, string> UpdateSMSStatus(UpdateStatusNotificationSMSModel updateSMS, string createdBy)
        {
            var smsTemplate = _context.MasterSMSTemplates.AsNoTracking().FirstOrDefault(x => x.Id == updateSMS.Id);
            if (smsTemplate != null)
            {
                smsTemplate.IsActive = updateSMS.IsActive;
                smsTemplate.ModifiedOn = DateTime.Now;
                smsTemplate.ModifiedBy = createdBy;
                _context.MasterSMSTemplates.Update(smsTemplate);
                _context.SaveChanges();

                return new Tuple<bool, int, string>(true, smsTemplate.Id, "SMS template status updated Successfully");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "SMS template not exist.");
        }

        public Tuple<bool, int, string> DeleteSMS(DeleteNotificationSMSModel updateSMS, string createdBy)
        {
            var smsTemplate = _context.MasterSMSTemplates.AsNoTracking().FirstOrDefault(x => x.Id == updateSMS.Id);
            if (smsTemplate != null)
            {
                smsTemplate.IsDeleted = true;
                smsTemplate.ModifiedOn = DateTime.Now;
                smsTemplate.ModifiedBy = createdBy;
                _context.MasterSMSTemplates.Update(smsTemplate);
                _context.SaveChanges();

                return new Tuple<bool, int, string>(true, smsTemplate.Id, "SMS template deleted Successfully");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "SMS template not exist.");
        }
        #endregion

        #region Add Email API Method
        public List<MasterEmailSettings> EmailAPIList()
        {
            return _context.MasterEmailSettings.AsNoTracking().OrderBy(x => x.Id).ToList();
        }

        public Tuple<bool, int, string> AddEmailAPI(AddNotificationEmailAPIModel addEmail, string createdBy)
        {
            MasterEmailSettings _add = new MasterEmailSettings()
            {
                HostName = addEmail.HostName,
                FromEmailAddress = addEmail.FromEmailAddress,
                CC = addEmail.CC,
                BCC = addEmail.BCC,
                Port = addEmail.Port,
                Password = addEmail.Password,
                IsMailActive = addEmail.IsMailActive,
                FromName = addEmail.FromName,
                MailSSL = addEmail.MailSSL,
                CreatedBy = createdBy,
                CreatedOn = DateTime.Now,
                ModifiedBy = null,
                ModifiedOn = null
            };
            _context.MasterEmailSettings.Add(_add);
            _context.SaveChanges();

            return new Tuple<bool, int, string>(true, _add.Id, "Email API Created Successfully");
        }

        public Tuple<bool, int, string> UpdateEmailAPI(UpdateNotificationEmailAPIModel updateEmail, string createdBy)
        {
            var emailAPI = _context.MasterEmailSettings.AsNoTracking().FirstOrDefault(x => x.Id == updateEmail.Id);
            if (emailAPI != null)
            {
                emailAPI.HostName = updateEmail.HostName;
                emailAPI.FromEmailAddress = updateEmail.FromEmailAddress;
                emailAPI.CC = updateEmail.CC;
                emailAPI.BCC = updateEmail.BCC;
                emailAPI.Port = updateEmail.Port;
                emailAPI.Password = updateEmail.Password;
                emailAPI.FromName = updateEmail.FromName;
                emailAPI.MailSSL = updateEmail.MailSSL;
                emailAPI.IsMailActive = updateEmail.IsActive;
                emailAPI.ModifiedOn = DateTime.Now;
                emailAPI.ModifiedBy = createdBy;
                _context.MasterEmailSettings.Update(emailAPI);
                _context.SaveChanges();

                return new Tuple<bool, int, string>(true, emailAPI.Id, "Email api updated Successfully.");
            }
            return new Tuple<bool, int, string>(false, 0, "Email API not exist.");
        }

        public Tuple<bool, int, string> UpdateEmailAPIStatus(UpdateNotificationEmailAPIStatusModel updateEmail, string createdBy)
        {
            var emailAPI = _context.MasterEmailSettings.AsNoTracking().FirstOrDefault(x => x.Id == updateEmail.Id);
            if (emailAPI != null)
            {
                emailAPI.IsMailActive = updateEmail.IsActive;
                emailAPI.ModifiedOn = DateTime.Now;
                emailAPI.ModifiedBy = createdBy;
                _context.MasterEmailSettings.Update(emailAPI);
                _context.SaveChanges();

                return new Tuple<bool, int, string>(true, emailAPI.Id, "Email api status updated Successfully.");
            }
            return new Tuple<bool, int, string>(false, 0, "Email API not exist.");
        }
        #endregion

        #region Add Email Template Method
        public List<MasterEmailTemplates> EmailAPITemplateList()
        {
            return _context.MasterEmailTemplates.AsNoTracking().OrderBy(x => x.Type).ToList();
        }

        public Tuple<bool, int, string> AddEmail(AddNotificationEmailModel addEmail, string createdBy)
        {
            var smsTemplate = _context.MasterEmailTemplates.AsNoTracking().FirstOrDefault(x => x.Type.ToUpper() == addEmail.Type);
            if (smsTemplate == null)
            {
                MasterEmailTemplates _add = new MasterEmailTemplates()
                {
                    Type = addEmail.Type,
                    Subject = addEmail.Subject,
                    Body = addEmail.Body,
                    IsActive = addEmail.IsActive,
                    CreatedBy = createdBy,
                    CreatedOn = DateTime.Now,
                    ModifiedBy = null,
                    ModifiedOn = null
                };
                _context.MasterEmailTemplates.Add(_add);
                _context.SaveChanges();

                return new Tuple<bool, int, string>(true, _add.Id, "Email template Created Successfully");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "Email type already exist.");
        }

        public Tuple<bool, int, string> UpdateEmail(UpdateNotificationEmailModel updateEmail, string createdBy)
        {
            var emailTemplate = _context.MasterEmailTemplates.AsNoTracking().FirstOrDefault(x => x.Type.ToUpper() == updateEmail.Type.ToUpper());
            if (emailTemplate != null)
            {
                emailTemplate.Subject = updateEmail.Subject;
                emailTemplate.Body = updateEmail.Body;
                emailTemplate.ModifiedOn = DateTime.Now;
                emailTemplate.ModifiedBy = createdBy;
                _context.MasterEmailTemplates.Update(emailTemplate);
                _context.SaveChanges();

                return new Tuple<bool, int, string>(true, emailTemplate.Id, "Email template updated Successfully");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "Email template not exist.");
        }

        public Tuple<bool, int, string> UpdateEmailStatus(UpdateStatusNotificationEmailModel updateEmail, string createdBy)
        {
            var emailTemplate = _context.MasterEmailTemplates.AsNoTracking().FirstOrDefault(x => x.Id == updateEmail.Id);
            if (emailTemplate != null)
            {
                emailTemplate.IsActive = updateEmail.IsActive;
                emailTemplate.ModifiedOn = DateTime.Now;
                emailTemplate.ModifiedBy = createdBy;
                _context.MasterEmailTemplates.Update(emailTemplate);
                _context.SaveChanges();

                return new Tuple<bool, int, string>(true, emailTemplate.Id, "Email template status updated Successfully");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "Email template not exist.");
        }
        #endregion
    }
}
