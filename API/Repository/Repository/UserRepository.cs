﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Models;
using Repository.Context;
using Repository.Interface;
using Repository.Model;

namespace Repository.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _context;

        /// <summary>
        ///     Repository
        /// </summary>
        /// <param name="context"></param>
        public UserRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        ///     Authenticate User
        /// </summary>
        /// <param name="loginDetails"></param>
        /// <returns></returns>
        public Tuple<bool, int> AuthenticateUser(UserLoginModel loginDetails)
        {
            var user = _context.Users.AsNoTracking().FirstOrDefault(x =>
                (x.PhoneNumber.ToLower() == loginDetails.Username.ToLower() || x.Email.ToLower() == loginDetails.Username.ToLower()) && x.PasswordHash == loginDetails.Password &&
                x.Status);

            return user != null ? new Tuple<bool, int>(true, user.Id) : new Tuple<bool, int>(false, 0);
        }

        public Tuple<bool, int> ResetPassword(ResetUserPasswordModel loginDetails)
        {
            var user = _context.Users.FirstOrDefault(x => (x.Email.ToLower() == loginDetails.Username.ToLower() || x.PhoneNumber.ToLower() == loginDetails.Username.ToLower()) && x.Status);
            if (user != null)
            {
                user.PasswordHash = GeneratePassword(6, 1);
                _context.SaveChanges();
                return user != null ? new Tuple<bool, int>(true, user.Id) : new Tuple<bool, int>(false, 0);
            }
            else
            {
                return new Tuple<bool, int>(false, 0);
            }
        }

        public Tuple<bool, int> ForgotPassword(ResetUserPasswordModel loginDetails)
        {
            var user = _context.Users.FirstOrDefault(x => (x.Email.ToLower() == loginDetails.Username.ToLower() || x.PhoneNumber.ToLower() == loginDetails.Username.ToLower()) && x.Status);
            return user != null ? new Tuple<bool, int>(true, user.Id) : new Tuple<bool, int>(false, 0);
        }
        public Tuple<bool, string> ChangePassword(UserSettingModel userData)
        {
            var user = _context.Users.AsNoTracking().FirstOrDefault(x => x.Id == userData.UserID && x.IsDeleted == false);
            if (user != null)
            {
                if (user.PasswordHash.Trim() == userData.CurrentPassword.Trim())
                {
                    if (user.PasswordHash.Trim() == userData.CurrentPassword.Trim())
                    {
                        user.PasswordHash = userData.ConfirmPassword;
                        user.ModifiedBy = userData.ModifyBy;
                        user.ModifiedOn = DateTime.Now;
                        _context.Users.Update(user);
                        _context.SaveChanges();

                        return new Tuple<bool, string>(true, "Password updated Successfully.");
                    }
                    else
                        return new Tuple<bool, string>(false, "Mismatch new and confirm password.");
                }
                else
                    return new Tuple<bool, string>(false, "Invalid current password.");
            }
            else
                return new Tuple<bool, string>(false, "Invalid Id.");
        }

        public Tuple<bool, int, string> AddUser(AddUserModel model, string status, string createdBy)
        {
            int userType = Convert.ToInt32(model.UserType);
            var userTypes = _context.UsersType.AsNoTracking().FirstOrDefault(x => x.ID == userType);
            if (userTypes != null)
            {
                var user = _context.Users.AsNoTracking().FirstOrDefault(x =>
                    x.Email.ToLower() == model.EmailID.ToLower() ||
                    x.PhoneNumber.ToLower() == model.PhoneNumber.ToLower());
                if (user == null)
                {
                    Users users = new Users()
                    {
                        Name = model.Name,
                        Email = model.EmailID,
                        PhoneNumber = model.PhoneNumber,
                        AltPhoneNumber = model.AltPhoneNumber,
                        AltEmail = model.AltEmail,
                        UserType = userTypes.ID,
                        PasswordHash = GeneratePassword(8, 1),
                        CreatedBy = createdBy,
                        CreatedOn = DateTime.Now,
                        Gender = model.Gender,
                        Active = status,
                        Address = model.Address,
                        EmailVerification = false,
                        GSTIN = model.GSTIN,
                        IsDeleted = false,
                        IsLockAccount = false,
                        LoginFailedCount = 0,
                        ModifiedBy = null,
                        ModifiedOn = null,
                        PhoneNumberVerification = true,
                        PinCode = model.PinCode,
                        ShopDescription = model.StoreDescription,
                        ShopName = model.StoreName,
                        Status = true,
                        VerifyData = null,
                        VerifyDate = null
                    };
                    _context.Users.Add(users);
                    _context.SaveChanges();

                    UpdateUserDocument(users.Id, "Profile", false, model.ProfileImageURL, "", createdBy);
                    return new Tuple<bool, int, string>(true, users.Id, "User Created Successfully");
                }
                else
                    return new Tuple<bool, int, string>(false, 0, "User Creation Failed Email / Phone number already exist.");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "Invalid usertype.");
        }

        public Tuple<bool, int, string> UpdateUser(UpdateUserModel model, string createdBy)
        {
            var user = _context.Users.AsNoTracking().FirstOrDefault(x => x.Id == model.Id);
            if (user != null)
            {
                var userMobile = _context.Users.AsNoTracking().FirstOrDefault(x => x.Id != user.Id && x.PhoneNumber == model.PhoneNumber);
                if (userMobile == null)
                {
                    if (!string.IsNullOrEmpty(model.Name))
                        user.Name = model.Name;
                    if (!string.IsNullOrEmpty(model.EmailID))
                        user.Email = model.EmailID;
                    if (!string.IsNullOrEmpty(model.PhoneNumber))
                        user.PhoneNumber = model.PhoneNumber;
                    if (!string.IsNullOrEmpty(model.AltPhoneNumber))
                        user.AltPhoneNumber = model.AltPhoneNumber;
                    if (!string.IsNullOrEmpty(model.AltEmail))
                        user.AltEmail = model.AltEmail;
                    user.ModifiedOn = DateTime.Now;
                    user.ModifiedBy = createdBy;
                    if (!string.IsNullOrEmpty(model.Gender))
                        user.Gender = model.Gender;
                    if (!string.IsNullOrEmpty(model.GSTIN))
                        user.GSTIN = model.GSTIN;
                    if (!string.IsNullOrEmpty(model.Address))
                        user.Address = model.Address;
                    if (!string.IsNullOrEmpty(model.PinCode))
                        user.PinCode = model.PinCode;
                    if (!string.IsNullOrEmpty(model.StoreName))
                        user.ShopName = model.StoreName;
                    if (!string.IsNullOrEmpty(model.StoreDescription))
                        user.ShopDescription = model.StoreDescription;
                    _context.Users.Update(user);
                    _context.SaveChanges();

                    UpdateUserDocument(user.Id, "Profile", false, model.ProfileImageURL, "", createdBy);

                    return new Tuple<bool, int, string>(true, user.Id, "User updated Successfully");
                }
                else
                    return new Tuple<bool, int, string>(false, 0, "Phone number already exist.");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "Invalid user Id.");
        }

        public Tuple<bool, int, string> UpdateUserStatus(UpdateUserStatusModel addUserDetails, string createdBy)
        {
            var user = _context.Users.AsNoTracking().FirstOrDefault(x => x.Id == addUserDetails.Id);
            if (user != null)
            {
                user.ModifiedOn = DateTime.Now;
                user.ModifiedBy = createdBy;
                user.Status = addUserDetails.IsActive;
                _context.Users.Update(user);
                _context.SaveChanges();

                return new Tuple<bool, int, string>(true, user.Id, "User status updated Successfully");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "Invalid user Id.");
        }

        public List<Users> UserList()
        {
            return _context.Users.AsNoTracking().Where(x => x.IsDeleted == false && x.UserType != 1).ToList();
        }

        public List<Users> UserByUserType(short userType)
        {
            return _context.Users.AsNoTracking().Where(x => x.IsDeleted == false && x.UserType != 1 && x.UserType == userType).ToList();
        }

        /// <summary>
        ///     Get User By Id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Users GetUserById(int userId)
        {
            var user = _context.Users.FirstOrDefault(x => x.Id == userId);
            return user;
        }

        private static string GeneratePassword(int Lenght, int NonAlphaNumericChars)
        {
            var allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            var allowedNonAlphaNum = "!@$%*()";
            var rd = new Random();

            if (NonAlphaNumericChars > Lenght || Lenght <= 0 || NonAlphaNumericChars < 0)
                throw new ArgumentOutOfRangeException();

            var pass = new char[Lenght];
            var pos = new int[Lenght];
            var i = 0;

            //Random the position values of the pos array for the string Pass
            while (i < Lenght - 1)
            {
                var flag = false;
                var temp = rd.Next(0, Lenght);
                int j;
                for (j = 0; j < Lenght; j++)
                    if (temp == pos[j])
                    {
                        flag = true;
                        j = Lenght;
                    }

                if (!flag)
                {
                    pos[i] = temp;
                    i++;
                }
            }

            //Random the AlphaNumericChars
            for (i = 0; i < Lenght - NonAlphaNumericChars; i++)
                pass[i] = allowedChars[rd.Next(0, allowedChars.Length)];

            //Random the NonAlphaNumericChars
            for (i = Lenght - NonAlphaNumericChars; i < Lenght; i++)
                pass[i] = allowedNonAlphaNum[rd.Next(0, allowedNonAlphaNum.Length)];

            //Set the sorted array values by the pos array for the rigth posistion
            var sorted = new char[Lenght];
            for (i = 0; i < Lenght; i++)
                sorted[i] = pass[pos[i]];

            var Pass = new string(sorted);

            return Pass;
        }
        public Tuple<bool, string> UpdateDeviceID(int userID, string deviceID, string modifiedBy)
        {
            var user = _context.Users.AsNoTracking().FirstOrDefault(x => x.Id == userID);
            if (user != null)
            {
                user.DeviceID = deviceID;
                user.ModifiedBy = modifiedBy;
                user.ModifiedOn = DateTime.Now;
                _context.Users.Update(user);
                _context.SaveChanges();

                return new Tuple<bool, string>(true, "Device id updated successfully.");
            }
            else
                return new Tuple<bool, string>(false, "Invalid userID");
        }


        public Tuple<bool, string> ValidateUserMobile(string mobileNumber, short userType)
        {
            var user = _context.Users.AsNoTracking().FirstOrDefault(x =>
                x.PhoneNumber == mobileNumber && x.UserType == userType);
            return user == null ? new Tuple<bool, string>(true, "OTP sent on mobile number") : new Tuple<bool, string>(false, "Mobile number alredy exist");
        }
        public Tuple<bool, int, string> AddCustomer(AddCustomerConfirmRequestModel addUserDetails, string status, string createdBy)
        {
            var user = _context.Users.AsNoTracking().FirstOrDefault(x => x.PhoneNumber.ToLower() == addUserDetails.PhoneNumber.ToLower() && x.UserType == 4);
            if (user == null)
            {
                Users users = new Users()
                {
                    Name = addUserDetails.Name,
                    Email = null,
                    PhoneNumber = addUserDetails.PhoneNumber,
                    AltPhoneNumber = null,
                    AltEmail = null,
                    UserType = 4,
                    PasswordHash = addUserDetails.Password,
                    CreatedBy = createdBy,
                    CreatedOn = DateTime.Now,
                    EmailVerification = false,
                    IsDeleted = false,
                    IsLockAccount = false,
                    LoginFailedCount = 0,
                    PhoneNumberVerification = true,
                    Status = true,
                    ModifiedBy = null,
                    ModifiedOn = null,
                    VerifyData = null,
                    VerifyDate = null,
                    Gender = addUserDetails.Gender,
                    ShopName = null,
                    ShopDescription = null,
                    PinCode = null,
                    Active = status,
                    Address = null,
                    GSTIN = null
                };
                _context.Users.Add(users);
                _context.SaveChanges();
                //Send Password Logic

                return new Tuple<bool, int, string>(true, users.Id, "User Created Successfully");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "User Creation Failed Email / Phone number already exist.");
        }
        public Tuple<bool, int, string> UpdateCustomerEmail(UpdateCustomerRequestModel updateUserDetails)
        {
            var userEmail = _context.Users.AsNoTracking().FirstOrDefault(x => x.Email.ToLower() == updateUserDetails.EmailID.ToLower() && x.UserType == 4);
            if (userEmail == null)
            {
                var user = _context.Users.AsNoTracking().FirstOrDefault(x => x.Id == updateUserDetails.ID);
                if (user != null)
                {
                    user.Email = updateUserDetails.EmailID;
                    _context.Users.Update(user);
                    _context.SaveChanges();

                    return new Tuple<bool, int, string>(true, user.Id, "Email ID updated successfully.");
                }
                else
                    return new Tuple<bool, int, string>(false, 0, "Failed to update.");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "Email ID alredy exist");
        }


        public Tuple<bool, int, string> AddVendor(AddVendorModel addVendorDetails, string status, string createdBy)
        {
            var user = _context.Users.AsNoTracking().FirstOrDefault(x => x.PhoneNumber.ToLower() == addVendorDetails.PhoneNumber.ToLower() && x.UserType == 3);
            if (user == null)
            {
                Users users = new Users()
                {
                    Name = addVendorDetails.Name,
                    Email = addVendorDetails.EmailID,
                    PhoneNumber = addVendorDetails.PhoneNumber,
                    AltPhoneNumber = null,
                    AltEmail = null,
                    UserType = 3,
                    PasswordHash = addVendorDetails.Password,
                    CreatedBy = createdBy,
                    CreatedOn = DateTime.Now,
                    EmailVerification = false,
                    IsDeleted = false,
                    IsLockAccount = false,
                    LoginFailedCount = 0,
                    PhoneNumberVerification = true,
                    Status = true,
                    ModifiedBy = null,
                    ModifiedOn = null,
                    VerifyData = null,
                    VerifyDate = null,
                    Gender = addVendorDetails.Gender,
                    GSTIN = null,
                    Address = null,
                    Active = status,
                    PinCode = null,
                    ShopDescription = null,
                    ShopName = null
                };
                _context.Users.Add(users);
                _context.SaveChanges();

                return new Tuple<bool, int, string>(true, users.Id, "User Created Successfully");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "User Creation Failed Email / Phone number already exist.");
        }
        public Tuple<bool, int, string> UpdatePickUpAddress(UpdatePickupPinAddressModel addVendorDetails, string status, string modifyBy)
        {
            var user = _context.Users.AsNoTracking().FirstOrDefault(x => x.Id == addVendorDetails.ID && x.UserType == 3);
            if (user != null)
            {
                user.PinCode = addVendorDetails.PinCode;
                user.Address = addVendorDetails.Address;
                user.Active = status;
                user.ModifiedBy = modifyBy;
                _context.Users.Update(user);
                _context.SaveChanges();

                return new Tuple<bool, int, string>(true, user.Id, "Address updated Successfully.");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "Invalid Id.");
        }
        public Tuple<bool, int, string> UpdateGSTIN(UpdateGSTINModel addVendorDetails, string status, string modifyBy)
        {
            var user = _context.Users.AsNoTracking().FirstOrDefault(x => x.Id == addVendorDetails.ID && x.UserType == 3);
            if (user != null)
            {
                user.GSTIN = addVendorDetails.GSTIN;
                user.Active = status;
                user.ModifiedBy = modifyBy;
                _context.Users.Update(user);
                _context.SaveChanges();

                return new Tuple<bool, int, string>(true, user.Id, "GSTIN updated Successfully.");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "Invalid Id.");
        }
        public PinCodes PinCodeDetails(PickUpAddressModel pickupDetails)
        {
            double zipCode = Convert.ToDouble(pickupDetails.PinCode);
            var pinCode = _context.PinCodes.AsNoTracking().FirstOrDefault(x => x.PinCode == zipCode);
            return pinCode;
        }
        public Tuple<bool, int, string> UpdateStoreDetails(StoreDetailsModel addVendorStoreDetails, string status, string modifyBy)
        {
            var user = _context.Users.AsNoTracking().FirstOrDefault(x => x.Id == addVendorStoreDetails.ID && x.UserType == 3);
            if (user != null)
            {
                user.ShopName = addVendorStoreDetails.StoreName;
                user.ShopDescription = addVendorStoreDetails.StoreDescription;
                user.Active = status;
                user.ModifiedBy = modifyBy;
                user.ModifiedOn = DateTime.Now;
                _context.Users.Update(user);
                _context.SaveChanges();

                return new Tuple<bool, int, string>(true, user.Id, "Store updated Successfully.");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "Invalid Id.");
        }
        public Tuple<bool, int, string> UpdatedUserStatus(int userID, string status, string modifyBy)
        {
            var user = _context.Users.AsNoTracking().FirstOrDefault(x => x.Id == userID);
            if (user != null)
            {
                user.Active = status;
                user.ModifiedBy = modifyBy;
                user.ModifiedOn = DateTime.Now;
                //_context.Users.Update(user);
                _context.SaveChanges();

                return new Tuple<bool, int, string>(true, user.Id, "Status updated Successfully.");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "Invalid Id.");
        }
        public List<UsersList> VendorsList()
        {
            var result = (from x in _context.Users
                          join y in _context.UsersType on x.UserType equals y.ID
                          where x.UserType == 3
                          orderby x.Id descending
                          select new UsersList
                          {
                              Id = x.Id,
                              Name = x.Name,
                              Email = x.Email,
                              EmailVerification = x.EmailVerification,
                              PhoneNumber = x.PhoneNumber,
                              PhoneNumberVerification = x.PhoneNumberVerification,
                              AltPhoneNumber = x.AltPhoneNumber,
                              AltEmail = x.AltEmail,
                              UserType = y.UserType,
                              Status = x.Status,
                              IsLockAccount = x.IsLockAccount,
                              LoginFailedCount = x.LoginFailedCount,
                              CreatedOn = x.CreatedOn,
                              CreatedBy = x.CreatedBy,
                              ModifiedOn = x.ModifiedOn,
                              ModifiedBy = x.ModifiedBy,
                              Gender = x.Gender,
                              PinCode = x.PinCode,
                              Address = x.Address,
                              GSTIN = x.GSTIN,
                              ShopName = x.ShopName,
                              ShopDescription = x.ShopDescription,
                              Active = x.Active
                          }).ToList();
            return result;
        }
        public Tuple<bool, int, string> UpdateProfileDetails(UpdateProfileDetailsModel userDetails)
        {
            var user = _context.Users.AsNoTracking().FirstOrDefault(x => x.Id == userDetails.Id);
            if (user != null)
            {
                var userMobile = _context.Users.AsNoTracking().FirstOrDefault(x => x.Id != userDetails.Id && x.PhoneNumber == userDetails.PhoneNumber);
                if (userMobile == null)
                {
                    user.PhoneNumber = userDetails.PhoneNumber;
                    user.Email = userDetails.EmailID;
                    user.Name = userDetails.Name;
                    if (!string.IsNullOrEmpty(userDetails.Gender))
                        user.Gender = userDetails.Gender;
                    if (!string.IsNullOrEmpty(userDetails.PinCode))
                        user.PinCode = userDetails.PinCode;
                    if (!string.IsNullOrEmpty(userDetails.Address))
                        user.Address = userDetails.Address;
                    if (!string.IsNullOrEmpty(userDetails.GSTIN))
                        user.GSTIN = userDetails.GSTIN;
                    if (!string.IsNullOrEmpty(userDetails.StoreName))
                        user.ShopName = userDetails.StoreName;
                    if (!string.IsNullOrEmpty(userDetails.StoreDescription))
                        user.ShopDescription = userDetails.StoreDescription;
                    user.ModifiedBy = userDetails.ModifiedBy;
                    user.ModifiedOn = DateTime.Now;
                    _context.Users.Update(user);
                    _context.SaveChanges();
                    return new Tuple<bool, int, string>(true, user.Id, "Profile updated Successfully.");
                }
                else
                    return new Tuple<bool, int, string>(false, 0, "Mobile number already exist.");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "Invalid Id.");
        }
        public Tuple<bool, int, string> UpdateUserDocument(int userID, string documentType, bool documentNumber, string documentURL, string docNumber, string createdBy)
        {
            var user = _context.UserDocuments.AsNoTracking().FirstOrDefault(x => x.UserId == userID && x.DocumentType == documentType);
            if (user != null)
            {
                user.DocumentFilePath = documentURL;
                user.DocumentNumber = documentNumber;
                if (!string.IsNullOrEmpty(docNumber))
                    user.Extra1 = docNumber;
                user.ModifiedBy = createdBy;
                user.ModifiedOn = DateTime.Now;
                _context.UserDocuments.Update(user);
                _context.SaveChanges();
                return new Tuple<bool, int, string>(true, user.Id, "Document added Successfully.");
            }
            else if (user == null)
            {
                UserDocuments _add = new UserDocuments()
                {
                    UserId = userID,
                    DocumentType = documentType,
                    DocumentNumber = documentNumber,
                    DocumentFilePath = documentURL,
                    CreatedBy = createdBy,
                    CreatedOn = DateTime.Now,
                    IsDeleted = false,
                    Remarks = "Document submitted.",
                    Extra1 = docNumber
                };
                _context.UserDocuments.Add(_add);
                _context.SaveChanges();
                return new Tuple<bool, int, string>(true, _add.Id, "Document updated Successfully.");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "Invalid Id or type.");
        }
        public UserDocuments GetUserDocumentById(int userId, string documentType)
        {
            var user = _context.UserDocuments.AsNoTracking().FirstOrDefault(x => x.UserId == userId && x.DocumentType == documentType && x.IsDeleted == false);
            return user;
        }
        public List<UsersListModel> VendorsWithAdminList()
        {
            short[] userType = { 1, 3 };
            var result = (from x in _context.Users
                          join y in _context.UsersType on x.UserType equals y.ID
                          where userType.Contains(x.UserType)
                          orderby x.Name descending
                          select new UsersListModel
                          {
                              Id = x.Id,
                              Name = x.Name,
                              PhoneNumber = x.PhoneNumber,
                              ShopName = x.ShopName,
                          }).ToList();
            return result;
        }

        public List<UserDocuments> GetUserDocumentById(int userId)
        {
            var user = _context.UserDocuments.AsNoTracking().Where(x => x.UserId == userId && x.IsDeleted == false).ToList();
            return user;
        }
    }
}