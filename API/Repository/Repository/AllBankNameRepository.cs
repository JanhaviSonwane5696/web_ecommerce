﻿using Microsoft.EntityFrameworkCore;
using Models;
using Repository.Context;
using Repository.Interface;
using Repository.Model;
using System.Collections.Generic;
using System.Linq;

namespace Repository.Repository
{
    public class AllBankNameRepository : IAllBankNameRepository
    {
        private readonly DataContext _context;

        /// <summary>
        ///     Repository
        /// </summary>
        /// <param name="context"></param>
        public AllBankNameRepository(DataContext context)
        {
            _context = context;
        }

        public List<string> BankNameList()
        {
            var bankList = _context.AllBankName.AsNoTracking().GroupBy(x => x.BANK).OrderBy(x => x.Key).Select(x => x.Key).ToList();
            return bankList;
        }

        public List<string> BankStateNameList(BankStateModel bankDetails)
        {
            var stateList = _context.AllBankName.AsNoTracking().Where(x => x.BANK == bankDetails.BankName).GroupBy(x => x.STATE).OrderBy(x => x.Key).Select(x => x.Key).ToList();
            return stateList;
        }

        public List<string> BankDistrictNameList(BankDistrictModel bankDetails)
        {
            var districtList = _context.AllBankName.AsNoTracking().Where(x => x.BANK == bankDetails.BankName && x.STATE == bankDetails.StateName).GroupBy(x => x.DISTRICT).OrderBy(x => x.Key).Select(x => x.Key).ToList();
            return districtList;
        }

        public List<string> BankBranchNameList(BankBranchModel bankDetails)
        {
            var branchList = _context.AllBankName.AsNoTracking().Where(x => x.BANK == bankDetails.BankName && x.STATE == bankDetails.StateName && x.DISTRICT == bankDetails.DistrictName).GroupBy(x => x.BRANCH).OrderBy(x => x.Key).Select(x => x.Key).ToList();
            return branchList;
        }

        public AllBankName BankBranchDetails(BankBranchDetailsModel bankDetail)
        {
            var bankDetails = _context.AllBankName.AsNoTracking().FirstOrDefault(x => x.BANK == bankDetail.BankName && x.STATE == bankDetail.StateName && x.DISTRICT == bankDetail.DistrictName && x.BRANCH == bankDetail.BranchName);
            return bankDetails;
        }

        public AllBankName IFSCDetails(BankDetailsModel bankDetail)
        {
            var bankDetails = _context.AllBankName.AsNoTracking().FirstOrDefault(x => x.IFSC == bankDetail.IfscCode);
            return bankDetails;
        }

    }
}
