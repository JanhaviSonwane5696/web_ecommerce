﻿using Repository.Context;
using Repository.Interface;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repository.Repository
{
    public class UserAddressRepository : IUserAddressRepository
    {
        private readonly DataContext _context;

        /// <summary>
        /// Repository
        /// </summary>
        /// <param name="context"></param>
        public UserAddressRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get MasterAttribute Category
        /// </summary>
        /// <returns></returns>
        public List<UserAddress> GetUserAddress(int userId)
        {
            var attribute = _context.UserAddress.Where(x => x.IsDeleted == false && x.UserId == userId).ToList();
            return attribute;
        }

        public UserAddress GetUserAddress(int userId, int addressId)
        {
            var attribute = _context.UserAddress.SingleOrDefault(x => x.IsDeleted == false && x.UserId == userId && x.Id == addressId);
            return attribute;
        }

        /// <summary>
        /// Add MasterAttribute Category
        /// </summary>
        /// <returns></returns>
        public Tuple<bool, string> AddUserAddress(UserAddress model)
        {
            _context.UserAddress.Add(model);
            _context.SaveChanges();
            return new Tuple<bool, string>(true, "Address added successfully.");
        }

        public Tuple<bool, string> Update(UserAddress model)
        {
            var address = _context.UserAddress.FirstOrDefault(x => x.UserId == model.UserId && x.Id == model.Id && x.IsDeleted == false);
            if (address != null)
            {
                address.AddressType = model.AddressType;
                address.IsPrimary = model.IsPrimary;
                address.FirstName = model.FirstName;
                address.LastName = model.LastName;
                address.Company = model.Company;
                address.AddressLine1 = model.AddressLine1;
                address.AddressLine2 = model.AddressLine2;
                address.Landmark = model.Landmark;
                address.ZipPostal = model.ZipPostal;
                address.Phone = model.Phone;
                address.ModifiedBy = model.ModifiedBy;
                address.ModifiedOn = DateTime.Now;
                _context.UserAddress.Update(address);
                _context.SaveChanges();
                return new Tuple<bool, string>(true, "Address updated successfully.");
            }
            else
                return new Tuple<bool, string>(false, "Address not found.");
        }

        public Tuple<bool, string> DeleteUserAddress(int id, int userId, string modifiedBy)
        {
            var userAddress = _context.UserAddress.FirstOrDefault(x => x.Id == id && x.UserId == userId);
            if (userAddress != null)
            {
                userAddress.IsDeleted = true;
                userAddress.ModifiedBy = modifiedBy;
                userAddress.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return new Tuple<bool, string>(true, "Address deleted successfully.");
            }
            else
                return new Tuple<bool, string>(false, "Address not found.");
        }
    }
}
