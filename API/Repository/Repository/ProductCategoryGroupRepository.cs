﻿using System;
using System.Collections.Generic;
using System.Linq;
using Repository.Context;
using Repository.Interface;
using Repository.Model;

namespace Repository.Repository
{
    public class ProductCategoryGroupRepository : IProductCategoryGroupRepository
    {
        private readonly DataContext _context;

        /// <summary>
        /// Repository
        /// </summary>
        /// <param name="context"></param>
        public ProductCategoryGroupRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get Product Category
        /// </summary>
        /// <returns></returns>
        public List<ProductCategoryGroup> GetProductCategoryGroup()
        {
            var productCategoryGroup = _context.ProductCategoryGroup.Where(x => x.IsDeleted == false).ToList();
            return productCategoryGroup;
        }

        /// <summary>
        /// Add Product Category
        /// </summary>
        /// <returns></returns>
        public bool AddProductCategoryGroup(ProductCategoryGroup productCategory)
        {
            ProductCategoryGroup productCategoryData = new ProductCategoryGroup()
            {
                Id = productCategory.Id,
                Name = productCategory.Name,
                ImageURL = productCategory.ImageURL,
                Description = productCategory.Description,
                ModifiedOn = productCategory.ModifiedOn,
                ModifiedBy = productCategory.ModifiedBy,
                ShowHide = productCategory.ShowHide,
                Priority = productCategory.Priority,
                MetaTag = productCategory.MetaTag,
                Status = productCategory.Status,
                CreatedBy = productCategory.CreatedBy,
                CreatedOn = productCategory.CreatedOn,
                IsActive = productCategory.IsActive,
                IsDeleted = productCategory.IsDeleted
            };
            _context.ProductCategoryGroup.Add(productCategoryData);
            _context.SaveChanges();
            return true;
        }

        /// <summary>
        /// Update Product Category
        /// </summary>
        /// <returns></returns>
        public bool UpdateProductCategoryGroup(ProductCategoryGroup productCategory)
        {
            var getProductCategory = _context.ProductCategoryGroup.FirstOrDefault(x => x.Id == productCategory.Id);
            if (getProductCategory != null)
            {
                getProductCategory.Name = productCategory.Name;
                getProductCategory.Description = productCategory.Description;
                if (!string.IsNullOrEmpty(productCategory.ImageURL))
                    getProductCategory.ImageURL = productCategory.ImageURL;
                getProductCategory.ModifiedBy = productCategory.ModifiedBy;
                getProductCategory.ModifiedOn = DateTime.Now;
                getProductCategory.ShowHide = productCategory.ShowHide;
                getProductCategory.Priority = productCategory.Priority;
                getProductCategory.MetaTag = productCategory.MetaTag;
                getProductCategory.Status = productCategory.Status;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Delete Product Category
        /// </summary>
        /// <returns></returns>
        public bool DeleteProductCategoryGroup(int id, string modifiedBy)
        {
            var productCategory = _context.ProductCategoryGroup.FirstOrDefault(x => x.Id == id);
            if (productCategory != null)
            {
                productCategory.IsDeleted = true;
                productCategory.ModifiedBy = modifiedBy;
                productCategory.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Activate / Deactivate Product Category
        /// </summary>
        /// <returns></returns>
        public bool UpdateStatusProductCategoryGroup(int id, bool status, string modifiedBy)
        {
            var productCategory = _context.ProductCategoryGroup.FirstOrDefault(x => x.Id == id);
            productCategory.IsActive = status;
            productCategory.ModifiedBy = modifiedBy;
            productCategory.ModifiedOn = DateTime.Now;
            _context.SaveChanges();
            return true;
        }
    }
}