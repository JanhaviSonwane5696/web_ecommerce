﻿using Microsoft.EntityFrameworkCore;
using Repository.Context;
using Repository.Interface;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository.Repository
{
    public class UsersVerificationRepository : IUsersVerificationRepository
    {
        private readonly DataContext _context;

        /// <summary>
        ///     Repository
        /// </summary>
        /// <param name="context"></param>
        public UsersVerificationRepository(DataContext context)
        {
            _context = context;
        }

        public UsersVerification GetUserVerificationByMobile(string mobileNo, short userType)
        {
            var user = _context.UsersVerification.FirstOrDefault(x => x.PhoneNumber == mobileNo);
            return user;
        }

        public Tuple<bool> AddUpdateVerification(string phoneNumber, short userType, string otp)
        {
            var user = _context.UsersVerification.AsNoTracking().FirstOrDefault(x => x.PhoneNumber.ToLower() == phoneNumber.ToLower() && x.UserType == userType);
            if (user == null)
            {
                UsersVerification users = new UsersVerification()
                {
                    PhoneNumber = phoneNumber,
                    Email = null,
                    AddOn = DateTime.Now,
                    UserType = userType,
                    ModifiedOn = DateTime.Now,
                    VerifyData = otp
                };
                _context.UsersVerification.Add(users);
                _context.SaveChanges();

                return new Tuple<bool>(true);
            }
            else if (user != null)
            {
                user.VerifyData = otp;
                user.ModifiedOn = DateTime.Now;
                _context.UsersVerification.Update(user);
                _context.SaveChanges();

                return new Tuple<bool>(true);
            }
            else
                return new Tuple<bool>(false);
        }

    }
}
