﻿using Repository.Context;
using Repository.Interface;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repository.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly DataContext _context;

        /// <summary>
        /// Repository
        /// </summary>
        /// <param name="context"></param>
        public ProductRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get Product Category
        /// </summary>
        /// <returns></returns>
        public List<Product> GetProduct(int userId)
        {
            List<Product> product = new List<Product>();
            if (userId == 1)
            {
                product = _context.Product.Where(x => x.IsDeleted == false && x.Status == "Approved").ToList();
            }
            else
            {
                product = _context.Product.Where(x => x.IsDeleted == false && x.UserId == userId && x.Status == "Approved").ToList();
            }
            return product;
        }

        public List<ProductsModel> GetAllProduct(int userId)
        {
            List<ProductsModel> product = new List<ProductsModel>();
            if (userId == 1)
            {
                product = (from x in _context.Product
                           join c in _context.ProductCategory on x.ProductCategoryId equals c.Id
                           join s in _context.ProductSubCategory on x.ProductSubCategoryId equals s.Id
                           join u in _context.Users on x.UserId equals u.Id
                           where x.IsDeleted == false && x.Status == "Approved"
                           orderby x.Id descending
                           select new ProductsModel
                           {
                               Id = x.Id,
                               UserId = x.UserId,
                               UserName = u.Name + "-" + u.ShopName,
                               ProductCategoryId = x.ProductCategoryId,
                               ProductCategory = c.Name,
                               ProductSubCategoryId = x.ProductSubCategoryId,
                               ProductSubCategory = s.Name,
                               Name = x.Name,
                               Keywords = x.Keywords,
                               SubProductFilterText = x.SubProductFilterText,
                               HasMultipleProducts = x.HasMultipleProducts,
                               ProductAttribute = !string.IsNullOrEmpty(x.ProductAttribute) ? (from a in _context.MasterAttributeItems join b in _context.MasterAttribute on a.AttributeId equals b.Id where ("," + x.ProductAttribute + ",").Contains("," + a.Id.ToString() + ",") select new AttributeItemsModel { Id = a.Id, AttributeItem = a.AttributeItem, AttributeItemDesciption = a.AttributeItemDesciption, AttributeId = b.Id, AttributeName = b.AttributeName, AttributeType = b.AttributeType }).ToList() : null,
                               ImageURL = x.ImageURL,
                               IsActive = x.IsActive,
                               Status = x.Status,
                               CreatedOn = x.CreatedOn,
                               CreatedBy = x.CreatedBy,
                               ModifiedBy = x.ModifiedBy,
                               ModifiedOn = x.ModifiedOn
                           }).ToList();
            }
            else
            {
                product = (from x in _context.Product
                           join c in _context.ProductCategory on x.ProductCategoryId equals c.Id
                           join s in _context.ProductSubCategory on x.ProductSubCategoryId equals s.Id
                           join u in _context.Users on x.UserId equals u.Id
                           where x.IsDeleted == false && x.UserId == userId && x.Status == "Approved"
                           orderby x.Id descending
                           select new ProductsModel
                           {
                               Id = x.Id,
                               UserId = x.UserId,
                               UserName = u.Name + "-" + u.ShopName,
                               ProductCategoryId = x.ProductCategoryId,
                               ProductCategory = c.Name,
                               ProductSubCategoryId = x.ProductSubCategoryId,
                               ProductSubCategory = s.Name,
                               Name = x.Name,
                               Keywords = x.Keywords,
                               SubProductFilterText = x.SubProductFilterText,
                               HasMultipleProducts = x.HasMultipleProducts,
                               ProductAttribute = !string.IsNullOrEmpty(x.ProductAttribute) ? (from a in _context.MasterAttributeItems join b in _context.MasterAttribute on a.AttributeId equals b.Id where ("," + x.ProductAttribute + ",").Contains("," + a.Id.ToString() + ",") select new AttributeItemsModel { Id = a.Id, AttributeItem = a.AttributeItem, AttributeItemDesciption = a.AttributeItemDesciption, AttributeId = b.Id, AttributeName = b.AttributeName, AttributeType = b.AttributeType }).ToList() : null,
                               ImageURL = x.ImageURL,
                               IsActive = x.IsActive,
                               Status = x.Status,
                               CreatedOn = x.CreatedOn,
                               CreatedBy = x.CreatedBy,
                               ModifiedBy = x.ModifiedBy,
                               ModifiedOn = x.ModifiedOn
                           }).ToList();
            }
            return product;
        }

        public List<ProductsModel> GetProductByStatus(int userId, string status)
        {
            List<ProductsModel> product = new List<ProductsModel>();
            if (userId == 1)
            {
                product = (from x in _context.Product
                           join c in _context.ProductCategory on x.ProductCategoryId equals c.Id
                           join s in _context.ProductSubCategory on x.ProductSubCategoryId equals s.Id
                           join u in _context.Users on x.UserId equals u.Id
                           where x.IsDeleted == false && x.Status == status
                           orderby x.Id descending
                           select new ProductsModel
                           {
                               Id = x.Id,
                               UserId = x.UserId,
                               UserName = u.Name + "-" + u.ShopName,
                               ProductCategoryId = x.ProductCategoryId,
                               ProductCategory = c.Name,
                               ProductSubCategoryId = x.ProductSubCategoryId,
                               ProductSubCategory = s.Name,
                               Name = x.Name,
                               Keywords = x.Keywords,
                               SubProductFilterText = x.SubProductFilterText,
                               HasMultipleProducts = x.HasMultipleProducts,
                               ProductAttribute = !string.IsNullOrEmpty(x.ProductAttribute) ? (from a in _context.MasterAttributeItems join b in _context.MasterAttribute on a.AttributeId equals b.Id where ("," + x.ProductAttribute + ",").Contains("," + a.Id.ToString() + ",") select new AttributeItemsModel { Id = a.Id, AttributeItem = a.AttributeItem, AttributeItemDesciption = a.AttributeItemDesciption, AttributeId = b.Id, AttributeName = b.AttributeName, AttributeType = b.AttributeType }).ToList() : null,
                               ImageURL = x.ImageURL,
                               IsActive = x.IsActive,
                               Status = x.Status,
                               Remarks = x.AdminRemarks,
                               CreatedOn = x.CreatedOn,
                               CreatedBy = x.CreatedBy,
                               ModifiedBy = x.ModifiedBy,
                               ModifiedOn = x.ModifiedOn
                           }).ToList();
            }
            else
            {
                product = (from x in _context.Product
                           join c in _context.ProductCategory on x.ProductCategoryId equals c.Id
                           join s in _context.ProductSubCategory on x.ProductSubCategoryId equals s.Id
                           join u in _context.Users on x.UserId equals u.Id
                           where x.IsDeleted == false && x.UserId == userId && x.Status == status
                           orderby x.Id descending
                           select new ProductsModel
                           {
                               Id = x.Id,
                               UserId = x.UserId,
                               UserName = u.Name + "-" + u.ShopName,
                               ProductCategoryId = x.ProductCategoryId,
                               ProductCategory = c.Name,
                               ProductSubCategoryId = x.ProductSubCategoryId,
                               ProductSubCategory = s.Name,
                               Name = x.Name,
                               Keywords = x.Keywords,
                               SubProductFilterText = x.SubProductFilterText,
                               HasMultipleProducts = x.HasMultipleProducts,
                               ProductAttribute = !string.IsNullOrEmpty(x.ProductAttribute) ? (from a in _context.MasterAttributeItems join b in _context.MasterAttribute on a.AttributeId equals b.Id where ("," + x.ProductAttribute + ",").Contains("," + a.Id.ToString() + ",") select new AttributeItemsModel { Id = a.Id, AttributeItem = a.AttributeItem, AttributeItemDesciption = a.AttributeItemDesciption, AttributeId = b.Id, AttributeName = b.AttributeName, AttributeType = b.AttributeType }).ToList() : null,
                               ImageURL = x.ImageURL,
                               IsActive = x.IsActive,
                               Status = x.Status,
                               Remarks = x.AdminRemarks,
                               CreatedOn = x.CreatedOn,
                               CreatedBy = x.CreatedBy,
                               ModifiedBy = x.ModifiedBy,
                               ModifiedOn = x.ModifiedOn
                           }).ToList();
            }
            return product;
        }

        public List<ProductsModel> GetAllProductByCategoryId(int userId, int categoryId)
        {
            List<ProductsModel> product = new List<ProductsModel>();
            if (userId == 1)
            {
                product = (from x in _context.Product
                           join c in _context.ProductCategory on x.ProductCategoryId equals c.Id
                           join s in _context.ProductSubCategory on x.ProductSubCategoryId equals s.Id
                           join u in _context.Users on x.UserId equals u.Id
                           where x.IsDeleted == false && x.ProductCategoryId == categoryId && x.Status == "Approved"
                           orderby x.Id descending
                           select new ProductsModel
                           {
                               Id = x.Id,
                               UserId = x.UserId,
                               UserName = u.Name + "-" + u.ShopName,
                               ProductCategoryId = x.ProductCategoryId,
                               ProductCategory = c.Name,
                               ProductSubCategoryId = x.ProductSubCategoryId,
                               ProductSubCategory = s.Name,
                               Name = x.Name,
                               Keywords = x.Keywords,
                               SubProductFilterText = x.SubProductFilterText,
                               HasMultipleProducts = x.HasMultipleProducts,
                               ProductAttribute = !string.IsNullOrEmpty(x.ProductAttribute) ? (from a in _context.MasterAttributeItems join b in _context.MasterAttribute on a.AttributeId equals b.Id where ("," + x.ProductAttribute + ",").Contains("," + a.Id.ToString() + ",") select new AttributeItemsModel { Id = a.Id, AttributeItem = a.AttributeItem, AttributeItemDesciption = a.AttributeItemDesciption, AttributeId = b.Id, AttributeName = b.AttributeName, AttributeType = b.AttributeType }).ToList() : null,
                               ImageURL = x.ImageURL,
                               IsActive = x.IsActive,
                               Status = x.Status,
                               CreatedOn = x.CreatedOn,
                               CreatedBy = x.CreatedBy,
                               ModifiedBy = x.ModifiedBy,
                               ModifiedOn = x.ModifiedOn,
                               DefaultProductItem = (from p in _context.ProductItem where p.ProductId == x.Id && p.IsDefault == true && p.IsDeleted == false && p.Status == "Approved" select new ShortProductItemsModel { Id = p.Id, Name = p.Name, Price = p.Price, DistributorPrice = p.DistributorPrice, RetailPrice = p.RetailPrice, SalePrice = p.SalePrice, ImageURL1 = p.ImageURL1, ImageURL2 = p.ImageURL2, ImageURL3 = p.ImageURL3 }).FirstOrDefault()
                           }).ToList();
            }
            else
            {
                product = (from x in _context.Product
                           join c in _context.ProductCategory on x.ProductCategoryId equals c.Id
                           join s in _context.ProductSubCategory on x.ProductSubCategoryId equals s.Id
                           join u in _context.Users on x.UserId equals u.Id
                           where x.IsDeleted == false && x.UserId == userId && x.ProductCategoryId == categoryId && x.Status == "Approved"
                           orderby x.Id descending
                           select new ProductsModel
                           {
                               Id = x.Id,
                               UserId = x.UserId,
                               UserName = u.Name + "-" + u.ShopName,
                               ProductCategoryId = x.ProductCategoryId,
                               ProductCategory = c.Name,
                               ProductSubCategoryId = x.ProductSubCategoryId,
                               ProductSubCategory = s.Name,
                               Name = x.Name,
                               Keywords = x.Keywords,
                               SubProductFilterText = x.SubProductFilterText,
                               HasMultipleProducts = x.HasMultipleProducts,
                               ProductAttribute = !string.IsNullOrEmpty(x.ProductAttribute) ? (from a in _context.MasterAttributeItems join b in _context.MasterAttribute on a.AttributeId equals b.Id where ("," + x.ProductAttribute + ",").Contains("," + a.Id.ToString() + ",") select new AttributeItemsModel { Id = a.Id, AttributeItem = a.AttributeItem, AttributeItemDesciption = a.AttributeItemDesciption, AttributeId = b.Id, AttributeName = b.AttributeName, AttributeType = b.AttributeType }).ToList() : null,
                               ImageURL = x.ImageURL,
                               IsActive = x.IsActive,
                               Status = x.Status,
                               CreatedOn = x.CreatedOn,
                               CreatedBy = x.CreatedBy,
                               ModifiedBy = x.ModifiedBy,
                               ModifiedOn = x.ModifiedOn,
                               DefaultProductItem = (from p in _context.ProductItem where p.ProductId == x.Id && p.IsDefault == true && p.IsDeleted == false && p.Status == "Approved" select new ShortProductItemsModel { Id = p.Id, Name = p.Name, Price = p.Price, DistributorPrice = p.DistributorPrice, RetailPrice = p.RetailPrice, SalePrice = p.SalePrice, ImageURL1 = p.ImageURL1, ImageURL2 = p.ImageURL2, ImageURL3 = p.ImageURL3 }).FirstOrDefault()
                           }).ToList();
            }
            return product;
        }

        public List<ProductsModel> GetAllProductBySubCategoryId(int userId, int subCategoryId)
        {
            List<ProductsModel> product = new List<ProductsModel>();
            if (userId == 1)
            {
                product = (from x in _context.Product
                           join c in _context.ProductCategory on x.ProductCategoryId equals c.Id
                           join s in _context.ProductSubCategory on x.ProductSubCategoryId equals s.Id
                           join u in _context.Users on x.UserId equals u.Id
                           where x.IsDeleted == false && x.ProductSubCategoryId == subCategoryId && x.Status == "Approved"
                           orderby x.Id descending
                           select new ProductsModel
                           {
                               Id = x.Id,
                               UserId = x.UserId,
                               UserName = u.Name + "-" + u.ShopName,
                               ProductCategoryId = x.ProductCategoryId,
                               ProductCategory = c.Name,
                               ProductSubCategoryId = x.ProductSubCategoryId,
                               ProductSubCategory = s.Name,
                               Name = x.Name,
                               Keywords = x.Keywords,
                               SubProductFilterText = x.SubProductFilterText,
                               HasMultipleProducts = x.HasMultipleProducts,
                               ProductAttribute = !string.IsNullOrEmpty(x.ProductAttribute) ? (from a in _context.MasterAttributeItems join b in _context.MasterAttribute on a.AttributeId equals b.Id where ("," + x.ProductAttribute + ",").Contains("," + a.Id.ToString() + ",") select new AttributeItemsModel { Id = a.Id, AttributeItem = a.AttributeItem, AttributeItemDesciption = a.AttributeItemDesciption, AttributeId = b.Id, AttributeName = b.AttributeName, AttributeType = b.AttributeType }).ToList() : null,
                               ImageURL = x.ImageURL,
                               IsActive = x.IsActive,
                               Status = x.Status,
                               CreatedOn = x.CreatedOn,
                               CreatedBy = x.CreatedBy,
                               ModifiedBy = x.ModifiedBy,
                               ModifiedOn = x.ModifiedOn,
                               DefaultProductItem = (from p in _context.ProductItem where p.ProductId == x.Id && p.IsDefault == true && p.IsDeleted == false && p.Status == "Approved" select new ShortProductItemsModel { Id = p.Id, Name = p.Name, Price = p.Price, DistributorPrice = p.DistributorPrice, RetailPrice = p.RetailPrice, SalePrice = p.SalePrice, ImageURL1 = p.ImageURL1, ImageURL2 = p.ImageURL2, ImageURL3 = p.ImageURL3 }).FirstOrDefault()
                           }).ToList();
            }
            else
            {
                product = (from x in _context.Product
                           join c in _context.ProductCategory on x.ProductCategoryId equals c.Id
                           join s in _context.ProductSubCategory on x.ProductSubCategoryId equals s.Id
                           join u in _context.Users on x.UserId equals u.Id
                           where x.IsDeleted == false && x.UserId == userId && x.ProductSubCategoryId == subCategoryId && x.Status == "Approved"
                           orderby x.Id descending
                           select new ProductsModel
                           {
                               Id = x.Id,
                               UserId = x.UserId,
                               UserName = u.Name + "-" + u.ShopName,
                               ProductCategoryId = x.ProductCategoryId,
                               ProductCategory = c.Name,
                               ProductSubCategoryId = x.ProductSubCategoryId,
                               ProductSubCategory = s.Name,
                               Name = x.Name,
                               Keywords = x.Keywords,
                               SubProductFilterText = x.SubProductFilterText,
                               HasMultipleProducts = x.HasMultipleProducts,
                               ProductAttribute = !string.IsNullOrEmpty(x.ProductAttribute) ? (from a in _context.MasterAttributeItems join b in _context.MasterAttribute on a.AttributeId equals b.Id where ("," + x.ProductAttribute + ",").Contains("," + a.Id.ToString() + ",") select new AttributeItemsModel { Id = a.Id, AttributeItem = a.AttributeItem, AttributeItemDesciption = a.AttributeItemDesciption, AttributeId = b.Id, AttributeName = b.AttributeName, AttributeType = b.AttributeType }).ToList() : null,
                               ImageURL = x.ImageURL,
                               IsActive = x.IsActive,
                               Status = x.Status,
                               CreatedOn = x.CreatedOn,
                               CreatedBy = x.CreatedBy,
                               ModifiedBy = x.ModifiedBy,
                               ModifiedOn = x.ModifiedOn,
                               DefaultProductItem = (from p in _context.ProductItem where p.ProductId == x.Id && p.IsDefault == true && p.IsDeleted == false && p.Status == "Approved" select new ShortProductItemsModel { Id = p.Id, Name = p.Name, Price = p.Price, DistributorPrice = p.DistributorPrice, RetailPrice = p.RetailPrice, SalePrice = p.SalePrice, ImageURL1 = p.ImageURL1, ImageURL2 = p.ImageURL2, ImageURL3 = p.ImageURL3 }).FirstOrDefault()
                           }).ToList();
            }
            return product;
        }

        public List<Product> GetProductByCategoryId(int categoryId, int userId)
        {
            List<Product> product = new List<Product>();
            if (userId == 1)
            {
                product = _context.Product.Where(x => x.IsDeleted == false && x.ProductCategoryId == categoryId && x.Status == "Approved").ToList();
            }
            else
            {
                product = _context.Product.Where(x => x.IsDeleted == false && x.ProductCategoryId == categoryId && x.UserId == userId && x.Status == "Approved").ToList();
            }
            return product;
        }

        public List<Product> GetProductBySubCategoryId(int subCategoryId, int userId)
        {
            List<Product> product = new List<Product>();
            if (userId == 1)
            {
                product = _context.Product.Where(x => x.IsDeleted == false && x.ProductSubCategoryId == subCategoryId && x.Status == "Approved").ToList();
            }
            else
            {
                product = _context.Product.Where(x => x.IsDeleted == false && x.ProductSubCategoryId == subCategoryId && x.UserId == userId && x.Status == "Approved").ToList();
            }
            return product;
        }

        /// <summary>
        /// Add Product Category
        /// </summary>
        /// <returns></returns>
        public Tuple<bool, int> AddProduct(Product product)
        {
            _context.Product.Add(product);
            _context.SaveChanges();
            return new Tuple<bool, int>(true, product.Id);
        }

        /// <summary>
        /// Update Product 
        /// </summary>
        /// <returns></returns>
        public bool UpdateProduct(Product product,int userId)
        {
            Product getProduct = new Product();
            if (userId == 1)
            {
                getProduct = _context.Product.FirstOrDefault(x => x.Id == product.Id && x.IsDeleted == false);
            }
            else
            {
                getProduct = _context.Product.FirstOrDefault(x => x.Id == product.Id && x.IsDeleted == false && x.UserId == userId);
            }

            //var getProduct = _context.Product.FirstOrDefault(x => x.Id == product.Id);
            if (getProduct != null)
            {
                getProduct.Name = product.Name;
                getProduct.UserId = product.UserId;
                getProduct.ProductCategoryId = product.ProductCategoryId;
                getProduct.ProductSubCategoryId = product.ProductSubCategoryId;
                getProduct.Name = product.Name;
                getProduct.Keywords = product.Keywords;
                getProduct.SubProductFilterText = product.SubProductFilterText;
                getProduct.HasMultipleProducts = product.HasMultipleProducts;
                getProduct.ProductAttribute = product.ProductAttribute;
                if (!string.IsNullOrEmpty(product.ImageURL))
                    getProduct.ImageURL = product.ImageURL;
                getProduct.ModifiedBy = product.ModifiedBy;
                getProduct.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Delete Product 
        /// </summary>
        /// <returns></returns>
        public bool DeleteProduct(int id, int userId, string modifiedBy)
        {
            Product product = new Product();
            if (userId == 1)
            {
                product = _context.Product.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            }
            else
            {
                product = _context.Product.FirstOrDefault(x => x.Id == id && x.IsDeleted == false && x.UserId == userId);
            }
            if (product != null)
            {
                product.IsDeleted = true;
                product.ModifiedBy = modifiedBy;
                product.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Activate / Deactivate Product Category
        /// </summary>
        /// <returns></returns>
        public bool UpdateStatusProduct(int id, int userId, bool status, string modifiedBy)
        {
            Product product = new Product();
            if (userId == 1)
            {
                product = _context.Product.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            }
            else
            {
                product = _context.Product.FirstOrDefault(x => x.Id == id && x.IsDeleted == false && x.UserId == userId);
            }
            if (product != null)
            {
                product.IsActive = status;
                product.ModifiedBy = modifiedBy;
                product.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Activate / Deactivate Product Category
        /// </summary>
        /// <returns></returns>
        public bool UpdateStatusProduct(int id, int userId, string status, string remarks, string modifiedBy)
        {
            Product product = new Product();
            if (userId == 1)
            {
                product = _context.Product.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
            }
            else
            {
                product = _context.Product.FirstOrDefault(x => x.Id == id && x.IsDeleted == false && x.UserId == userId);
            }
            if (product != null)
            {
                product.Status = status;
                product.AdminRemarks = remarks;
                product.ModifiedBy = modifiedBy;
                product.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        public ProductAttributesModel ProductAttributeList(int productId)
        {
            ProductAttributesModel product = new ProductAttributesModel();
            product = (from x in _context.Product
                       where x.IsDeleted == false && x.Id == productId && x.Status == "Approved"
                       orderby x.Id descending
                       select new ProductAttributesModel
                       {
                           Id = x.Id,
                           Name = x.Name,
                           ProductAttribute = !string.IsNullOrEmpty(x.ProductAttribute) ? (from a in _context.MasterAttributeItems join b in _context.MasterAttribute on a.AttributeId equals b.Id where ("," + x.ProductAttribute + ",").Contains("," + a.Id.ToString() + ",") select new AttributeItemsModel { Id = a.Id, AttributeItem = a.AttributeItem, AttributeItemDesciption = a.AttributeItemDesciption, AttributeId = b.Id, AttributeName = b.AttributeName, AttributeType = b.AttributeType }).ToList() : null,
                       }).FirstOrDefault();

            return product;
        }

    }
}