﻿using System;
using System.Collections.Generic;
using System.Linq;
using Repository.Context;
using Repository.Interface;
using Repository.Model;

namespace Repository.Repository
{
    public class MasterBrandsRepository : IMasterBrandsRepository
    {
        private readonly DataContext _context;

        /// <summary>
        /// Repository
        /// </summary>
        /// <param name="context"></param>
        public MasterBrandsRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get MasterBrands Category
        /// </summary>
        /// <returns></returns>
        public List<MasterBrands> GetMasterBrands(int BrandsId)
        {
            var Brands = _context.MasterBrands.Where(x => x.IsDeleted == false && x.Id == BrandsId).ToList();
            return Brands;
        }

        /// <summary>
        /// Get MasterBrands Category
        /// </summary>
        /// <returns></returns>
        public List<MasterBrands> GetMasterAllBrands()
        {
            var Brands = _context.MasterBrands.Where(x => x.IsDeleted == false).ToList();
            return Brands;
        }

        /// <summary>
        /// Add MasterBrands Category
        /// </summary>
        /// <returns></returns>
        public bool AddMasterBrands(MasterBrands Brands)
        {
            _context.MasterBrands.Add(Brands);
            _context.SaveChanges();
            return true;
        }

        /// <summary>
        /// Update MasterBrands 
        /// </summary>
        /// <returns></returns>
        public bool UpdateMasterBrands(MasterBrands Brands)
        {
            var getMasterBrands = _context.MasterBrands.FirstOrDefault(x => x.Id == Brands.Id);
            if (getMasterBrands != null)
            {
                getMasterBrands.BrandsName = Brands.BrandsName;
                if (!string.IsNullOrEmpty(Brands.BrandsLogoURL))
                    getMasterBrands.BrandsLogoURL = Brands.BrandsLogoURL;
                getMasterBrands.BrandsDesciption = Brands.BrandsDesciption;
                getMasterBrands.ModifiedBy = Brands.ModifiedBy;
                getMasterBrands.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Delete MasterBrands 
        /// </summary>
        /// <returns></returns>
        public bool DeleteMasterBrands(int id, string modifiedBy)
        {
            var Brands = _context.MasterBrands.FirstOrDefault(x => x.Id == id);
            if (Brands != null)
            {
                Brands.IsDeleted = true;
                Brands.ModifiedBy = modifiedBy;
                Brands.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Activate / Deactivate MasterBrands Category
        /// </summary>
        /// <returns></returns>
        public bool UpdateStatusMasterBrands(int id, bool status, string modifiedBy)
        {
            var Brands = _context.MasterBrands.FirstOrDefault(x => x.Id == id);
            Brands.IsActive = status;
            Brands.ModifiedBy = modifiedBy;
            Brands.ModifiedOn = DateTime.Now;
            _context.SaveChanges();
            return true;
        }
    }
}