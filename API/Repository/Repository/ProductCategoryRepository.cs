﻿using System;
using System.Collections.Generic;
using System.Linq;
using Repository.Context;
using Repository.Interface;
using Repository.Model;

namespace Repository.Repository
{
    public class ProductCategoryRepository : IProductCategoryRepository
    {
        private readonly DataContext _context;

        /// <summary>
        /// Repository
        /// </summary>
        /// <param name="context"></param>
        public ProductCategoryRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get Product Category
        /// </summary>
        /// <returns></returns>
        public List<ProductCategoryModel> GetProductCategory()
        {
            var productCategory = (from x in _context.ProductCategory
                                      join y in _context.ProductCategoryGroup on x.ProductCategoryGroupId equals y.Id
                                      where x.IsDeleted == false
                                      orderby x.ProductCategoryGroupId, x.Name
                                      select new ProductCategoryModel
                                      {
                                          Id = x.Id,
                                          ProductCategoryGroupId = x.ProductCategoryGroupId,
                                          ProductCategoryGroup = y.Name,
                                          Name = x.Name,
                                          Description = x.Description,
                                          ImageURL = x.ImageURL,
                                          IsActive = x.IsActive,
                                          CreatedOn = x.CreatedOn,
                                          CreatedBy = x.CreatedBy,
                                          ModifiedOn = x.ModifiedOn,
                                          ModifiedBy = x.ModifiedBy,
                                          ShowHide = x.ShowHide,
                                          MetaTag = x.MetaTag,
                                          Priority = x.Priority,
                                          Status = x.Status
                                      }).ToList();
            //var productCategory = _context.ProductCategory.Where(x => x.IsDeleted == false).ToList();
            return productCategory;
        }

        /// <summary>
        /// Add Product Category
        /// </summary>
        /// <returns></returns>
        public bool AddProductCategory(ProductCategory productCategory)
        {
            ProductCategory productCategoryData = new ProductCategory()
            {
                Id = productCategory.Id,
                ProductCategoryGroupId = productCategory.ProductCategoryGroupId,
                Name = productCategory.Name,
                ImageURL = productCategory.ImageURL,
                Description = productCategory.Description,
                ModifiedOn = productCategory.ModifiedOn,
                ModifiedBy = productCategory.ModifiedBy,
                ShowHide = productCategory.ShowHide,
                Priority = productCategory.Priority,
                MetaTag = productCategory.MetaTag,
                Status = productCategory.Status,
                CreatedBy = productCategory.CreatedBy,
                CreatedOn = productCategory.CreatedOn,
                IsActive = productCategory.IsActive,
                IsDeleted = productCategory.IsDeleted
            };
            _context.ProductCategory.Add(productCategoryData);
            _context.SaveChanges();
            return true;
        }

        /// <summary>
        /// Update Product Category
        /// </summary>
        /// <returns></returns>
        public bool UpdateProductCategory(ProductCategory productCategory)
        {
            var getProductCategory = _context.ProductCategory.FirstOrDefault(x => x.Id == productCategory.Id);
            if (getProductCategory != null)
            {
                getProductCategory.ProductCategoryGroupId = productCategory.ProductCategoryGroupId;
                getProductCategory.Name = productCategory.Name;
                getProductCategory.Description = productCategory.Description;
                if (!string.IsNullOrEmpty(productCategory.ImageURL))
                    getProductCategory.ImageURL = productCategory.ImageURL;
                getProductCategory.ModifiedBy = productCategory.ModifiedBy;
                getProductCategory.ModifiedOn = DateTime.Now;
                getProductCategory.ShowHide = productCategory.ShowHide;
                getProductCategory.Priority = productCategory.Priority;
                getProductCategory.MetaTag = productCategory.MetaTag;
                getProductCategory.Status = productCategory.Status;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Delete Product Category
        /// </summary>
        /// <returns></returns>
        public bool DeleteProductCategory(int id, string modifiedBy)
        {
            var productCategory = _context.ProductCategory.FirstOrDefault(x => x.Id == id);
            if (productCategory != null)
            {
                productCategory.IsDeleted = true;
                productCategory.ModifiedBy = modifiedBy;
                productCategory.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Activate / Deactivate Product Category
        /// </summary>
        /// <returns></returns>
        public bool UpdateStatusProductCategory(int id, bool status, string modifiedBy)
        {
            var productCategory = _context.ProductCategory.FirstOrDefault(x => x.Id == id);
            productCategory.IsActive = status;
            productCategory.ModifiedBy = modifiedBy;
            productCategory.ModifiedOn = DateTime.Now;
            _context.SaveChanges();
            return true;
        }
    }
}