﻿using System;
using System.Collections.Generic;
using System.Linq;
using Repository.Context;
using Repository.Interface;
using Repository.Model;

namespace Repository.Repository
{
    public class ProductItemRepository : IProductItemRepository
    {
        private readonly DataContext _context;

        /// <summary>
        /// Repository
        /// </summary>
        /// <param name="context"></param>
        public ProductItemRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get ProductItem Category
        /// </summary>
        /// <returns></returns>
        public ProductItem GetProductItemById(int id)
        {
            var product = _context.ProductItem.SingleOrDefault(x => x.IsDeleted == false && x.Id == id && x.Status == "Approved");
            return product;
        }
        public List<ProductItem> GetProductItems(int productId)
        {
            var product = _context.ProductItem.Where(x => x.IsDeleted == false && x.ProductId == productId && x.Status == "Approved").ToList();
            return product;
        }

        public List<ProductItem> GetProductItems(int productId, int productItemId)
        {
            var product = _context.ProductItem.Where(x => x.IsDeleted == false && x.ProductId == productId && x.Id == productItemId && x.Status == "Approved").ToList();
            return product;
        }

        /// <summary>
        /// Get ProductItem Category
        /// </summary>
        /// <returns></returns>
        public List<ProductItemsModel> GetProductItem(int productItemId)
        {
            var product = (from x in _context.ProductItem
                           join y in _context.Product on x.ProductId equals y.Id
                           join z in _context.Users on y.UserId equals z.Id
                           where x.IsDeleted == false && x.Id == productItemId && x.Status == "Approved"
                           select new ProductItemsModel
                           {
                               UserName = z.Name,
                               Id = x.Id,
                               Name = x.Name,
                               ProductId = x.ProductId,
                               Price = x.Price,
                               DistributorPrice = x.DistributorPrice,
                               RetailPrice = x.RetailPrice,
                               SalePrice = x.SalePrice,
                               Weight = x.Weight,
                               Quantity = x.Quantity,
                               MaxQuantityForOrder = x.MaxQuantityForOrder,
                               Description = x.Description,
                               ShortDescription = x.ShortDescription,
                               Specification = x.Specification,
                               LegalDisclaimer = x.LegalDisclaimer,
                               RelatedProducts = x.RelatedProducts,
                               CompareProducts = x.CompareProducts,
                               SponsoredProducts = x.SponsoredProducts,
                               ImageURL1 = x.ImageURL1,
                               ImageThumbURL1 = x.ImageThumbURL1,
                               ImageURL2 = x.ImageURL2,
                               ImageThumbURL2 = x.ImageThumbURL2,
                               ImageURL3 = x.ImageURL3,
                               ImageThumbURL3 = x.ImageThumbURL3,
                               ImageURL4 = x.ImageURL4,
                               ImageThumbURL4 = x.ImageThumbURL4,
                               ImageURL5 = x.ImageURL5,
                               ImageThumbURL5 = x.ImageThumbURL5,
                               ImageURL6 = x.ImageURL6,
                               ImageThumbURL6 = x.ImageThumbURL6,
                               ImageURL7 = x.ImageURL7,
                               ImageThumbURL7 = x.ImageThumbURL7,
                               IsDefault = x.IsDefault,
                               IsActive = x.IsActive,
                               SKU = x.SKU,
                               Brand = x.Brand > 0 ? (from b in _context.MasterBrands where b.Id == x.Brand select new BrandsModel { Id = b.Id, BrandsName = b.BrandsName, BrandsLogoURL = b.BrandsLogoURL, BrandsDesciption = b.BrandsDesciption }).FirstOrDefault() : null,
                               DeliveryCharge = x.DeliveryCharge,
                               MetaTag = x.MetaTag,
                               MinimumQuantity = x.MinimumQuantity,
                               ProductAttribute = !string.IsNullOrEmpty(x.ProductAttribute) ? (from a in _context.MasterAttributeItems join b in _context.MasterAttribute on a.AttributeId equals b.Id where ("," + x.ProductAttribute + ",").Contains("," + a.Id.ToString() + ",") select new AttributeItemsModel { Id = a.Id, AttributeItem = a.AttributeItem, AttributeItemDesciption = a.AttributeItemDesciption, AttributeId = b.Id, AttributeName = b.AttributeName, AttributeType = b.AttributeType }).ToList() : null,
                               Tax = !string.IsNullOrEmpty(x.Tax) ? (from a in _context.MasterTaxRates where ("," + x.Tax + ",").Contains("," + a.Id.ToString() + ",") select new TaxRatesModel { Id = a.Id, Name = a.Name, Amount = a.Amount, Type = a.Type }).ToList() : null,
                               RemainingQuantity = x.RemainingQuantity,
                               Sale = x.Sale,
                               Status = x.Status,
                               Remarks = x.AdminRemarks,
                               PercentOff = (x.Price - x.SalePrice) / x.Price * 100,
                               CountryOfOrigin = x.CountryOfOrigin
                           }).ToList();
            return product;
        }
        public List<ProductItemsModel> GetProductItems(int productId, string status)
        {
            var product = (from x in _context.ProductItem
                           join y in _context.Product on x.ProductId equals y.Id
                           join z in _context.Users on y.UserId equals z.Id
                           where x.IsDeleted == false && x.ProductId == productId && x.Status == status
                           select new ProductItemsModel
                           {
                               UserName = z.Name,
                               Id = x.Id,
                               Name = x.Name,
                               ProductId = x.ProductId,
                               Price = x.Price,
                               DistributorPrice = x.DistributorPrice,
                               RetailPrice = x.RetailPrice,
                               SalePrice = x.SalePrice,
                               Weight = x.Weight,
                               Quantity = x.Quantity,
                               MaxQuantityForOrder = x.MaxQuantityForOrder,
                               Description = x.Description,
                               ShortDescription = x.ShortDescription,
                               Specification = x.Specification,
                               LegalDisclaimer = x.LegalDisclaimer,
                               RelatedProducts = x.RelatedProducts,
                               CompareProducts = x.CompareProducts,
                               SponsoredProducts = x.SponsoredProducts,
                               ImageURL1 = x.ImageURL1,
                               ImageThumbURL1 = x.ImageThumbURL1,
                               ImageURL2 = x.ImageURL2,
                               ImageThumbURL2 = x.ImageThumbURL2,
                               ImageURL3 = x.ImageURL3,
                               ImageThumbURL3 = x.ImageThumbURL3,
                               ImageURL4 = x.ImageURL4,
                               ImageThumbURL4 = x.ImageThumbURL4,
                               ImageURL5 = x.ImageURL5,
                               ImageThumbURL5 = x.ImageThumbURL5,
                               ImageURL6 = x.ImageURL6,
                               ImageThumbURL6 = x.ImageThumbURL6,
                               ImageURL7 = x.ImageURL7,
                               ImageThumbURL7 = x.ImageThumbURL7,
                               IsDefault = x.IsDefault,
                               IsActive = x.IsActive,
                               SKU = x.SKU,
                               Brand = x.Brand > 0 ? (from b in _context.MasterBrands where b.Id == x.Brand select new BrandsModel { Id = b.Id, BrandsName = b.BrandsName, BrandsLogoURL = b.BrandsLogoURL, BrandsDesciption = b.BrandsDesciption }).FirstOrDefault() : null,
                               DeliveryCharge = x.DeliveryCharge,
                               MetaTag = x.MetaTag,
                               MinimumQuantity = x.MinimumQuantity,
                               ProductAttribute = !string.IsNullOrEmpty(x.ProductAttribute) ? (from a in _context.MasterAttributeItems join b in _context.MasterAttribute on a.AttributeId equals b.Id where ("," + x.ProductAttribute + ",").Contains("," + a.Id.ToString() + ",") select new AttributeItemsModel { Id = a.Id, AttributeItem = a.AttributeItem, AttributeItemDesciption = a.AttributeItemDesciption, AttributeId = b.Id, AttributeName = b.AttributeName, AttributeType = b.AttributeType }).ToList() : null,
                               Tax = !string.IsNullOrEmpty(x.Tax) ? (from a in _context.MasterTaxRates where ("," + x.Tax + ",").Contains("," + a.Id.ToString() + ",") select new TaxRatesModel { Id = a.Id, Name = a.Name, Amount = a.Amount, Type = a.Type }).ToList() : null,
                               RemainingQuantity = x.RemainingQuantity,
                               Sale = x.Sale,
                               Status = x.Status,
                               Remarks = x.AdminRemarks,
                               PercentOff = (x.Price - x.SalePrice) / x.Price * 100,
                               CountryOfOrigin = x.CountryOfOrigin
                           }).ToList();
            return product;
        }

        public List<ProductItemModel> GetProductItemsByStatus(int userId, int productId, string status)
        {
            if (userId == 1)
            {
                if (productId == 0)
                {
                    var product = (from x in _context.ProductItem
                                   join y in _context.Product on x.ProductId equals y.Id
                                   join z in _context.Users on y.UserId equals z.Id
                                   where x.IsDeleted == false && x.Status == status
                                   select new ProductItemModel
                                   {
                                       UserName = z.Name,
                                       Id = x.Id,
                                       Name = x.Name,
                                       ProductId = x.ProductId,
                                       Price = x.Price,
                                       DistributorPrice = x.DistributorPrice,
                                       RetailPrice = x.RetailPrice,
                                       SalePrice = x.SalePrice,
                                       Weight = x.Weight,
                                       Quantity = x.Quantity,
                                       MaxQuantityForOrder = x.MaxQuantityForOrder,
                                       Description = x.Description,
                                       ShortDescription = x.ShortDescription,
                                       Specification = x.Specification,
                                       LegalDisclaimer = x.LegalDisclaimer,
                                       RelatedProducts = x.RelatedProducts,
                                       CompareProducts = x.CompareProducts,
                                       SponsoredProducts = x.SponsoredProducts,
                                       ImageURL1 = x.ImageURL1,
                                       ImageThumbURL1 = x.ImageThumbURL1,
                                       ImageURL2 = x.ImageURL2,
                                       ImageThumbURL2 = x.ImageThumbURL2,
                                       ImageURL3 = x.ImageURL3,
                                       ImageThumbURL3 = x.ImageThumbURL3,
                                       ImageURL4 = x.ImageURL4,
                                       ImageThumbURL4 = x.ImageThumbURL4,
                                       ImageURL5 = x.ImageURL5,
                                       ImageThumbURL5 = x.ImageThumbURL5,
                                       ImageURL6 = x.ImageURL6,
                                       ImageThumbURL6 = x.ImageThumbURL6,
                                       ImageURL7 = x.ImageURL7,
                                       ImageThumbURL7 = x.ImageThumbURL7,
                                       IsDefault = x.IsDefault,
                                       IsActive = x.IsActive,
                                       SKU = x.SKU,
                                       Brand = x.Brand,
                                       DeliveryCharge = x.DeliveryCharge,
                                       MetaTag = x.MetaTag,
                                       MinimumQuantity = x.MinimumQuantity,
                                       ProductAttribute = x.ProductAttribute,
                                       RemainingQuantity = x.RemainingQuantity,
                                       Sale = x.Sale,
                                       Tax = x.Tax,
                                       Status = x.Status,
                                       Remarks = x.AdminRemarks,
                                       PercentOff = (x.Price - x.SalePrice) / x.Price * 100,
                                       CountryOfOrigin = x.CountryOfOrigin
                                   }).ToList();
                    return product;
                }
                else if (status == "Accepted")
                {
                    string[] statusName = { "Accepted", "Rejected" };
                    var product = (from x in _context.ProductItem
                                   join y in _context.Product on x.ProductId equals y.Id
                                   join z in _context.Users on y.UserId equals z.Id
                                   where x.IsDeleted == false && x.ProductId == productId && statusName.Contains(x.Status)
                                   select new ProductItemModel
                                   {
                                       UserName = z.Name,
                                       Id = x.Id,
                                       Name = x.Name,
                                       ProductId = x.ProductId,
                                       Price = x.Price,
                                       DistributorPrice = x.DistributorPrice,
                                       RetailPrice = x.RetailPrice,
                                       SalePrice = x.SalePrice,
                                       Weight = x.Weight,
                                       Quantity = x.Quantity,
                                       MaxQuantityForOrder = x.MaxQuantityForOrder,
                                       Description = x.Description,
                                       ShortDescription = x.ShortDescription,
                                       Specification = x.Specification,
                                       LegalDisclaimer = x.LegalDisclaimer,
                                       RelatedProducts = x.RelatedProducts,
                                       CompareProducts = x.CompareProducts,
                                       SponsoredProducts = x.SponsoredProducts,
                                       ImageURL1 = x.ImageURL1,
                                       ImageThumbURL1 = x.ImageThumbURL1,
                                       ImageURL2 = x.ImageURL2,
                                       ImageThumbURL2 = x.ImageThumbURL2,
                                       ImageURL3 = x.ImageURL3,
                                       ImageThumbURL3 = x.ImageThumbURL3,
                                       ImageURL4 = x.ImageURL4,
                                       ImageThumbURL4 = x.ImageThumbURL4,
                                       ImageURL5 = x.ImageURL5,
                                       ImageThumbURL5 = x.ImageThumbURL5,
                                       ImageURL6 = x.ImageURL6,
                                       ImageThumbURL6 = x.ImageThumbURL6,
                                       ImageURL7 = x.ImageURL7,
                                       ImageThumbURL7 = x.ImageThumbURL7,
                                       IsDefault = x.IsDefault,
                                       IsActive = x.IsActive,
                                       SKU = x.SKU,
                                       Brand = x.Brand,
                                       DeliveryCharge = x.DeliveryCharge,
                                       MetaTag = x.MetaTag,
                                       MinimumQuantity = x.MinimumQuantity,
                                       ProductAttribute = x.ProductAttribute,
                                       RemainingQuantity = x.RemainingQuantity,
                                       Sale = x.Sale,
                                       Tax = x.Tax,
                                       Status = x.Status,
                                       Remarks = x.AdminRemarks,
                                       PercentOff = (x.Price - x.SalePrice) / x.Price * 100,
                                       CountryOfOrigin = x.CountryOfOrigin
                                   }).ToList();
                    return product;
                }
                else
                {
                    var product = (from x in _context.ProductItem
                                   join y in _context.Product on x.ProductId equals y.Id
                                   join z in _context.Users on y.UserId equals z.Id
                                   where x.IsDeleted == false && x.ProductId == productId && x.Status == status
                                   select new ProductItemModel
                                   {
                                       UserName = z.Name,
                                       Id = x.Id,
                                       Name = x.Name,
                                       ProductId = x.ProductId,
                                       Price = x.Price,
                                       DistributorPrice = x.DistributorPrice,
                                       RetailPrice = x.RetailPrice,
                                       SalePrice = x.SalePrice,
                                       Weight = x.Weight,
                                       Quantity = x.Quantity,
                                       MaxQuantityForOrder = x.MaxQuantityForOrder,
                                       Description = x.Description,
                                       ShortDescription = x.ShortDescription,
                                       Specification = x.Specification,
                                       LegalDisclaimer = x.LegalDisclaimer,
                                       RelatedProducts = x.RelatedProducts,
                                       CompareProducts = x.CompareProducts,
                                       SponsoredProducts = x.SponsoredProducts,
                                       ImageURL1 = x.ImageURL1,
                                       ImageThumbURL1 = x.ImageThumbURL1,
                                       ImageURL2 = x.ImageURL2,
                                       ImageThumbURL2 = x.ImageThumbURL2,
                                       ImageURL3 = x.ImageURL3,
                                       ImageThumbURL3 = x.ImageThumbURL3,
                                       ImageURL4 = x.ImageURL4,
                                       ImageThumbURL4 = x.ImageThumbURL4,
                                       ImageURL5 = x.ImageURL5,
                                       ImageThumbURL5 = x.ImageThumbURL5,
                                       ImageURL6 = x.ImageURL6,
                                       ImageThumbURL6 = x.ImageThumbURL6,
                                       ImageURL7 = x.ImageURL7,
                                       ImageThumbURL7 = x.ImageThumbURL7,
                                       IsDefault = x.IsDefault,
                                       IsActive = x.IsActive,
                                       SKU = x.SKU,
                                       Brand = x.Brand,
                                       DeliveryCharge = x.DeliveryCharge,
                                       MetaTag = x.MetaTag,
                                       MinimumQuantity = x.MinimumQuantity,
                                       ProductAttribute = x.ProductAttribute,
                                       RemainingQuantity = x.RemainingQuantity,
                                       Sale = x.Sale,
                                       Tax = x.Tax,
                                       Status = x.Status,
                                       Remarks = x.AdminRemarks,
                                       PercentOff = (x.Price - x.SalePrice) / x.Price * 100,
                                       CountryOfOrigin = x.CountryOfOrigin
                                   }).ToList();
                    return product;
                }
            }
            else
            {
                if (productId == 0)
                {
                    string[] statusName = { "Accepted", "Rejected" };
                    var product = (from x in _context.ProductItem
                                   join y in _context.Product on x.ProductId equals y.Id
                                   join z in _context.Users on y.UserId equals z.Id
                                   where x.IsDeleted == false && statusName.Contains(x.Status)
                                   select new ProductItemModel
                                   {
                                       UserName = z.Name,
                                       Id = x.Id,
                                       Name = x.Name,
                                       ProductId = x.ProductId,
                                       Price = x.Price,
                                       DistributorPrice = x.DistributorPrice,
                                       RetailPrice = x.RetailPrice,
                                       SalePrice = x.SalePrice,
                                       Weight = x.Weight,
                                       Quantity = x.Quantity,
                                       MaxQuantityForOrder = x.MaxQuantityForOrder,
                                       Description = x.Description,
                                       ShortDescription = x.ShortDescription,
                                       Specification = x.Specification,
                                       LegalDisclaimer = x.LegalDisclaimer,
                                       RelatedProducts = x.RelatedProducts,
                                       CompareProducts = x.CompareProducts,
                                       SponsoredProducts = x.SponsoredProducts,
                                       ImageURL1 = x.ImageURL1,
                                       ImageThumbURL1 = x.ImageThumbURL1,
                                       ImageURL2 = x.ImageURL2,
                                       ImageThumbURL2 = x.ImageThumbURL2,
                                       ImageURL3 = x.ImageURL3,
                                       ImageThumbURL3 = x.ImageThumbURL3,
                                       ImageURL4 = x.ImageURL4,
                                       ImageThumbURL4 = x.ImageThumbURL4,
                                       ImageURL5 = x.ImageURL5,
                                       ImageThumbURL5 = x.ImageThumbURL5,
                                       ImageURL6 = x.ImageURL6,
                                       ImageThumbURL6 = x.ImageThumbURL6,
                                       ImageURL7 = x.ImageURL7,
                                       ImageThumbURL7 = x.ImageThumbURL7,
                                       IsDefault = x.IsDefault,
                                       IsActive = x.IsActive,
                                       SKU = x.SKU,
                                       Brand = x.Brand,
                                       DeliveryCharge = x.DeliveryCharge,
                                       MetaTag = x.MetaTag,
                                       MinimumQuantity = x.MinimumQuantity,
                                       ProductAttribute = x.ProductAttribute,
                                       RemainingQuantity = x.RemainingQuantity,
                                       Sale = x.Sale,
                                       Tax = x.Tax,
                                       Status = x.Status,
                                       Remarks = x.AdminRemarks,
                                       PercentOff = (x.Price - x.SalePrice) / x.Price * 100,
                                       CountryOfOrigin = x.CountryOfOrigin
                                   }).ToList();
                    return product;
                }
                else if (status == "Accepted")
                {
                    string[] statusName = { "Accepted", "Rejected" };
                    var product = (from x in _context.ProductItem
                                   join y in _context.Product on x.ProductId equals y.Id
                                   join z in _context.Users on y.UserId equals z.Id
                                   where x.IsDeleted == false && x.ProductId == productId && statusName.Contains(x.Status)
                                   select new ProductItemModel
                                   {
                                       UserName = z.Name,
                                       Id = x.Id,
                                       Name = x.Name,
                                       ProductId = x.ProductId,
                                       Price = x.Price,
                                       DistributorPrice = x.DistributorPrice,
                                       RetailPrice = x.RetailPrice,
                                       SalePrice = x.SalePrice,
                                       Weight = x.Weight,
                                       Quantity = x.Quantity,
                                       MaxQuantityForOrder = x.MaxQuantityForOrder,
                                       Description = x.Description,
                                       ShortDescription = x.ShortDescription,
                                       Specification = x.Specification,
                                       LegalDisclaimer = x.LegalDisclaimer,
                                       RelatedProducts = x.RelatedProducts,
                                       CompareProducts = x.CompareProducts,
                                       SponsoredProducts = x.SponsoredProducts,
                                       ImageURL1 = x.ImageURL1,
                                       ImageThumbURL1 = x.ImageThumbURL1,
                                       ImageURL2 = x.ImageURL2,
                                       ImageThumbURL2 = x.ImageThumbURL2,
                                       ImageURL3 = x.ImageURL3,
                                       ImageThumbURL3 = x.ImageThumbURL3,
                                       ImageURL4 = x.ImageURL4,
                                       ImageThumbURL4 = x.ImageThumbURL4,
                                       ImageURL5 = x.ImageURL5,
                                       ImageThumbURL5 = x.ImageThumbURL5,
                                       ImageURL6 = x.ImageURL6,
                                       ImageThumbURL6 = x.ImageThumbURL6,
                                       ImageURL7 = x.ImageURL7,
                                       ImageThumbURL7 = x.ImageThumbURL7,
                                       IsDefault = x.IsDefault,
                                       IsActive = x.IsActive,
                                       SKU = x.SKU,
                                       Brand = x.Brand,
                                       DeliveryCharge = x.DeliveryCharge,
                                       MetaTag = x.MetaTag,
                                       MinimumQuantity = x.MinimumQuantity,
                                       ProductAttribute = x.ProductAttribute,
                                       RemainingQuantity = x.RemainingQuantity,
                                       Sale = x.Sale,
                                       Tax = x.Tax,
                                       Status = x.Status,
                                       Remarks = x.AdminRemarks,
                                       PercentOff = (x.Price - x.SalePrice) / x.Price * 100,
                                       CountryOfOrigin = x.CountryOfOrigin
                                   }).ToList();
                    return product;
                }
                else
                {
                    var product = (from x in _context.ProductItem
                                   join y in _context.Product on x.ProductId equals y.Id
                                   join z in _context.Users on y.UserId equals z.Id
                                   where x.IsDeleted == false && x.ProductId == productId && x.Status == status
                                   select new ProductItemModel
                                   {
                                       UserName = z.Name,
                                       Id = x.Id,
                                       Name = x.Name,
                                       ProductId = x.ProductId,
                                       Price = x.Price,
                                       DistributorPrice = x.DistributorPrice,
                                       RetailPrice = x.RetailPrice,
                                       SalePrice = x.SalePrice,
                                       Weight = x.Weight,
                                       Quantity = x.Quantity,
                                       MaxQuantityForOrder = x.MaxQuantityForOrder,
                                       Description = x.Description,
                                       ShortDescription = x.ShortDescription,
                                       Specification = x.Specification,
                                       LegalDisclaimer = x.LegalDisclaimer,
                                       RelatedProducts = x.RelatedProducts,
                                       CompareProducts = x.CompareProducts,
                                       SponsoredProducts = x.SponsoredProducts,
                                       ImageURL1 = x.ImageURL1,
                                       ImageThumbURL1 = x.ImageThumbURL1,
                                       ImageURL2 = x.ImageURL2,
                                       ImageThumbURL2 = x.ImageThumbURL2,
                                       ImageURL3 = x.ImageURL3,
                                       ImageThumbURL3 = x.ImageThumbURL3,
                                       ImageURL4 = x.ImageURL4,
                                       ImageThumbURL4 = x.ImageThumbURL4,
                                       ImageURL5 = x.ImageURL5,
                                       ImageThumbURL5 = x.ImageThumbURL5,
                                       ImageURL6 = x.ImageURL6,
                                       ImageThumbURL6 = x.ImageThumbURL6,
                                       ImageURL7 = x.ImageURL7,
                                       ImageThumbURL7 = x.ImageThumbURL7,
                                       IsDefault = x.IsDefault,
                                       IsActive = x.IsActive,
                                       SKU = x.SKU,
                                       Brand = x.Brand,
                                       DeliveryCharge = x.DeliveryCharge,
                                       MetaTag = x.MetaTag,
                                       MinimumQuantity = x.MinimumQuantity,
                                       ProductAttribute = x.ProductAttribute,
                                       RemainingQuantity = x.RemainingQuantity,
                                       Sale = x.Sale,
                                       Tax = x.Tax,
                                       Status = x.Status,
                                       Remarks = x.AdminRemarks,
                                       PercentOff = (x.Price - x.SalePrice) / x.Price * 100,
                                       CountryOfOrigin = x.CountryOfOrigin
                                   }).ToList();
                    return product;
                }
            }
        }

        /// <summary>
        /// Add ProductItem Category
        /// </summary>
        /// <returns></returns>
        public bool AddProductItem(ProductItem product)
        {
            _context.ProductItem.Add(product);
            _context.SaveChanges();
            return true;
        }

        /// <summary>
        /// Update ProductItem 
        /// </summary>
        /// <returns></returns>
        public bool UpdateProductItem(ProductItem product)
        {
            var getProductItem = _context.ProductItem.FirstOrDefault(x => x.Id == product.Id);
            if (getProductItem != null)
            {
                getProductItem.Name = product.Name;
                getProductItem.ProductId = product.ProductId;
                getProductItem.Name = product.Name;
                getProductItem.Price = product.Price;
                getProductItem.DistributorPrice = product.DistributorPrice;
                getProductItem.RetailPrice = product.RetailPrice;
                getProductItem.SalePrice = product.SalePrice;
                getProductItem.Weight = product.Weight;
                getProductItem.Quantity = product.Quantity;
                getProductItem.MaxQuantityForOrder = product.MaxQuantityForOrder;
                getProductItem.Description = product.Description;
                getProductItem.ShortDescription = product.ShortDescription;
                getProductItem.Specification = product.Specification;
                getProductItem.LegalDisclaimer = product.LegalDisclaimer;
                getProductItem.RelatedProducts = product.RelatedProducts;
                getProductItem.CompareProducts = product.CompareProducts;
                getProductItem.SponsoredProducts = product.SponsoredProducts;
                if (!string.IsNullOrEmpty(product.ImageURL1))
                    getProductItem.ImageURL1 = product.ImageURL1;
                if (!string.IsNullOrEmpty(product.ImageThumbURL1))
                    getProductItem.ImageThumbURL1 = product.ImageThumbURL1;
                if (!string.IsNullOrEmpty(product.ImageURL2))
                    getProductItem.ImageURL2 = product.ImageURL2;
                if (!string.IsNullOrEmpty(product.ImageThumbURL2))
                    getProductItem.ImageThumbURL2 = product.ImageThumbURL2;
                if (!string.IsNullOrEmpty(product.ImageURL3))
                    getProductItem.ImageURL3 = product.ImageURL3;
                if (!string.IsNullOrEmpty(product.ImageThumbURL3))
                    getProductItem.ImageThumbURL3 = product.ImageThumbURL3;
                if (!string.IsNullOrEmpty(product.ImageURL4))
                    getProductItem.ImageURL4 = product.ImageURL4;
                if (!string.IsNullOrEmpty(product.ImageThumbURL4))
                    getProductItem.ImageThumbURL4 = product.ImageThumbURL4;
                if (!string.IsNullOrEmpty(product.ImageURL5))
                    getProductItem.ImageURL5 = product.ImageURL5;
                if (!string.IsNullOrEmpty(product.ImageThumbURL5))
                    getProductItem.ImageThumbURL5 = product.ImageThumbURL5;
                if (!string.IsNullOrEmpty(product.ImageURL6))
                    getProductItem.ImageURL6 = product.ImageURL6;
                if (!string.IsNullOrEmpty(product.ImageThumbURL6))
                    getProductItem.ImageThumbURL6 = product.ImageThumbURL6;
                if (!string.IsNullOrEmpty(product.ImageURL7))
                    getProductItem.ImageURL7 = product.ImageURL7;
                if (!string.IsNullOrEmpty(product.ImageThumbURL7))
                    getProductItem.ImageThumbURL7 = product.ImageThumbURL7;
                getProductItem.IsDefault = product.IsDefault;
                getProductItem.IsActive = product.IsActive;
                getProductItem.IsDeleted = product.IsDeleted;
                getProductItem.ModifiedBy = product.ModifiedBy;
                getProductItem.ModifiedOn = DateTime.Now;
                getProductItem.SKU = product.SKU;
                getProductItem.MetaTag = product.MetaTag;
                getProductItem.Sale = product.Sale;
                getProductItem.MinimumQuantity = product.MinimumQuantity;
                getProductItem.ProductAttribute = product.ProductAttribute;
                getProductItem.Brand = product.Brand;
                getProductItem.Tax = product.Tax;
                getProductItem.DeliveryCharge = product.DeliveryCharge;
                getProductItem.RemainingQuantity = product.RemainingQuantity;
                getProductItem.CountryOfOrigin = product.CountryOfOrigin;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Delete ProductItem 
        /// </summary>
        /// <returns></returns>
        public bool DeleteProductItem(int id, string modifiedBy)
        {
            var product = _context.ProductItem.FirstOrDefault(x => x.Id == id);
            if (product != null)
            {
                product.IsDeleted = true;
                product.ModifiedBy = modifiedBy;
                product.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Activate / Deactivate ProductItem Category
        /// </summary>
        /// <returns></returns>
        public bool UpdateStatusProductItem(int id, bool status, string modifiedBy)
        {
            var product = _context.ProductItem.FirstOrDefault(x => x.Id == id);
            product.IsActive = status;
            product.ModifiedBy = modifiedBy;
            product.ModifiedOn = DateTime.Now;
            _context.SaveChanges();
            return true;
        }

        public bool UpdateStatusProductItem(int id, string status, string remarks, string modifiedBy)
        {
            var product = _context.ProductItem.FirstOrDefault(x => x.Id == id);
            product.Status = status;
            product.AdminRemarks = remarks;
            product.ModifiedBy = modifiedBy;
            product.ModifiedOn = DateTime.Now;
            _context.SaveChanges();
            return true;
        }

        public List<ProductItemAttributesModel> ProductItemAttributeList(int productId)
        {
            List<ProductItemAttributesModel> product = new List<ProductItemAttributesModel>();
            product = (from x in _context.ProductItem
                       where x.IsDeleted == false && x.ProductId == productId && x.Status == "Approved"
                       orderby x.Id descending
                       select new ProductItemAttributesModel
                       {
                           Id = x.Id,
                           ProductId = x.ProductId,
                           Name = x.Name,
                           Price = x.Price,
                           DistributorPrice = x.DistributorPrice,
                           RetailPrice = x.RetailPrice,
                           SalePrice = x.SalePrice,
                           ProductAttribute = !string.IsNullOrEmpty(x.ProductAttribute) ? (from a in _context.MasterAttributeItems join b in _context.MasterAttribute on a.AttributeId equals b.Id where ("," + x.ProductAttribute + ",").Contains("," + a.Id.ToString() + ",") select new AttributeItemsModel { Id = a.Id, AttributeItem = a.AttributeItem, AttributeItemDesciption = a.AttributeItemDesciption, AttributeId = b.Id, AttributeName = b.AttributeName, AttributeType = b.AttributeType }).ToList() : null,
                           Quantity = x.Quantity
                       }).ToList();

            return product;
        }

        public List<ShortProductItemModel> ProductItemShortList(int productId)
        {
            List<ShortProductItemModel> product = new List<ShortProductItemModel>();
            product = (from x in _context.ProductItem
                       where x.IsDeleted == false && x.ProductId == productId && x.Status == "Approved"
                       orderby x.Id descending
                       select new ShortProductItemModel
                       {
                           Id = x.Id,
                           ProductId = x.ProductId,
                           Name = x.Name,
                           Price = x.Price,
                           DistributorPrice = x.DistributorPrice,
                           RetailPrice = x.RetailPrice,
                           SalePrice = x.SalePrice,
                           Quantity = x.Quantity
                       }).ToList();

            return product;
        }
    }
}