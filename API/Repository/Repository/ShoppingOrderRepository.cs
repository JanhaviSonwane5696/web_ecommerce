﻿using Models;
using Repository.Context;
using Repository.Interface;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repository.Repository
{
    public class ShoppingOrderRepository : IShoppingOrderRepository
    {
        private readonly DataContext _context;

        /// <summary>
        ///     Repository
        /// </summary>
        /// <param name="context"></param>
        public ShoppingOrderRepository(DataContext context)
        {
            _context = context;
        }

        public int AddPayment(int userId, string payment, decimal amount, int orderId)
        {
            Payments _add = new Payments()
            {
                UserId = userId,
                OrderId = orderId,
                PaymentMode = payment,
                Status = "Pending",
                Amount = amount,
                Date = DateTime.Now,
                Notes = "Pending",
                PGAmount = 0,
                ShopAmount = 0,
                WalletAmount = 0,
                BillHTML = "NA"
            };
            _context.Payments.Add(_add);
            _context.SaveChanges();
            return _add.Id;
        }

        public bool UpdatePayment(int paymentId, string status, string note, decimal walletAmount, decimal gatewayAmount, decimal discountAmount)
        {
            var payment = _context.Payments.SingleOrDefault(x => x.Id == paymentId);
            payment.Status = status;
            payment.Notes = note;
            payment.WalletAmount = walletAmount;
            payment.PGAmount = gatewayAmount;
            payment.ShopAmount = discountAmount;
            _context.SaveChanges();
            return true;
        }

        public ShoppingOrder AddOrder(ShoppingOrder model)
        {
            _context.ShoppingOrder.Add(model);
            _context.SaveChanges();

            var product = _context.ProductItem.SingleOrDefault(x => x.Id == model.ProductId);
            product.Quantity -= model.Quantity;
            _context.SaveChanges();

            return model;
        }

        public long AddProcessPay(int userId, string emailId, string mobileNo, int paymentId, decimal amount, int orderId, string description)
        {
            ProcessPay _add = new ProcessPay()
            {
                UserID = userId,
                EmailID = emailId,
                PhoneNumber = mobileNo,
                OrderID = orderId,
                PaymentID = paymentId,
                Status = false,
                Amount = amount,
                DateAdded = DateTime.Now,
                TransactionDesc = description
            };
            _context.ProcessPay.Add(_add);
            _context.SaveChanges();
            return _add.ID;
        }

        public List<ShoppingOrderDetailsModel> UserOrders(int userId)
        {
            List<ShoppingOrderDetailsModel> orders = (from x in _context.ShoppingOrder
                                                      join p in _context.ProductItem on x.ProductId equals p.Id
                                                      where x.UserId == userId
                                                      orderby x.Id descending
                                                      select new ShoppingOrderDetailsModel
                                                      {
                                                          Id = x.Id,
                                                          OrderId = x.OrderId,
                                                          Quantity = x.Quantity,
                                                          Amount = x.TotalAmount,
                                                          Status = x.Status,
                                                          Name = p.Name,
                                                          ImageURL1 = p.ImageURL1,
                                                          ImageThumbURL1 = p.ImageThumbURL1,
                                                          Discount = x.PrimePoint,
                                                          Orderdate = x.DatePlaced.ToLongDateString()
                                                      }).ToList();
            return orders;
        }

        public ShoppingOrderDetailModel UserOrderDetails(int userId, int id)
        {
            ShoppingOrderDetailModel order = new ShoppingOrderDetailModel();
            if (userId == 1)
            {
                order = (from x in _context.ShoppingOrder
                         join p in _context.ProductItem on x.ProductId equals p.Id
                         join p1 in _context.Product on p.ProductId equals p1.Id
                         join pay in _context.Payments on x.PaymentId equals pay.Id
                         join a in _context.UserAddress on x.ShipppingAddress equals a.Id
                         join v in _context.Users on p1.UserId equals v.Id
                         where x.Id == id
                         select new ShoppingOrderDetailModel
                         {
                             Id = x.Id,
                             OrderId = x.OrderId,
                             Quantity = x.Quantity,
                             ItemPrice = x.ItemPrice,
                             Tax = x.Tax,
                             SubTotal = x.SubTotal,
                             DeliveryCharges = x.Shipping,
                             TotalAmount = x.TotalAmount,
                             Status = x.Status,
                             Name = p.Name,
                             ImageURL1 = p.ImageURL1,
                             ImageThumbURL1 = p.ImageThumbURL1,
                             Discount = x.PrimePoint,
                             Orderdate = x.DatePlaced.ToLongDateString(),
                             AdminComment = x.AdminComment,
                             PayDate = pay.Date,
                             PaymentId = x.PaymentId,
                             PayStatus = pay.Status,
                             PrimePer = x.PrimePer,
                             ResellerComment = x.ResellerComment,
                             ShipppingAddress = a.FirstName + " " + a.LastName + "</br>" + a.AddressLine1 + "</br>" + a.AddressLine2 + "</br>" + a.Landmark + "</br>" + a.ZipPostal + "</br>" + a.Phone,
                             VendorName = v.Name + " " + v.ShopName
                         }).FirstOrDefault();
            }
            else
            {
                order = (from x in _context.ShoppingOrder
                         join p in _context.ProductItem on x.ProductId equals p.Id
                         join p1 in _context.Product on p.ProductId equals p1.Id
                         join pay in _context.Payments on x.PaymentId equals pay.Id
                         join a in _context.UserAddress on x.ShipppingAddress equals a.Id
                         join v in _context.Users on p1.UserId equals v.Id
                         where x.UserId == userId && x.Id == id
                         select new ShoppingOrderDetailModel
                         {
                             Id = x.Id,
                             OrderId = x.OrderId,
                             Quantity = x.Quantity,
                             ItemPrice = x.ItemPrice,
                             Tax = x.Tax,
                             SubTotal = x.SubTotal,
                             DeliveryCharges = x.Shipping,
                             TotalAmount = x.TotalAmount,
                             Status = x.Status,
                             Name = p.Name,
                             ImageURL1 = p.ImageURL1,
                             ImageThumbURL1 = p.ImageThumbURL1,
                             Discount = x.PrimePoint,
                             Orderdate = x.DatePlaced.ToLongDateString(),
                             AdminComment = x.AdminComment,
                             PayDate = pay.Date,
                             PaymentId = x.PaymentId,
                             PayStatus = pay.Status,
                             PrimePer = x.PrimePer,
                             ResellerComment = x.ResellerComment,
                             ShipppingAddress = a.FirstName + " " + a.LastName + "</br>" + a.AddressLine1 + "</br>" + a.AddressLine2 + "</br>" + a.Landmark + "</br>" + a.ZipPostal + "</br>" + a.Phone,
                             VendorName = v.Name + " " + v.ShopName
                         }).FirstOrDefault();
            }
            return order;
        }

        public List<ShoppingOrdersDetailsModel> UsersOrders(OrderReportModel model)
        {
            List<ShoppingOrdersDetailsModel> orders = new List<ShoppingOrdersDetailsModel>();
            DateTime fromDate = Convert.ToDateTime(model.FromDate);
            DateTime toDate = Convert.ToDateTime(model.ToDate).AddDays(1);
            if (model.UserId == 1)
            {
                orders = (from x in _context.ShoppingOrder
                          join u in _context.Users on x.UserId equals u.Id
                          join p in _context.ProductItem on x.ProductId equals p.Id
                          join p1 in _context.Product on p.ProductId equals p1.Id
                          join pay in _context.Payments on x.PaymentId equals pay.Id
                          join a in _context.UserAddress on x.ShipppingAddress equals a.Id
                          join v in _context.Users on p1.UserId equals v.Id
                          where x.DatePlaced >= fromDate && x.DatePlaced <= toDate
                          orderby x.Id descending
                          select new ShoppingOrdersDetailsModel
                          {
                              Id = x.Id,
                              OrderId = x.OrderId,
                              OrderStatus = x.Status,
                              ProductItemId = x.ProductId,
                              IsBlock = x.IsActive,
                              OrderDate = x.DatePlaced.ToString(),
                              OrderUpdate = x.DateUpdated.ToString(),
                              ItemPrice = x.ItemPrice,
                              Quantity = x.Quantity,
                              SubTotal = x.SubTotal,
                              Tax = x.Tax,
                              Shipping = x.Shipping,
                              PrimePoint = x.PrimePoint,
                              TotalAmount = x.TotalAmount,
                              ShippingAddress = a.FirstName + " " + a.LastName + "</br>" + a.AddressLine1 + "</br>" + a.AddressLine2 + "</br>" + a.Landmark + "</br>" + a.ZipPostal + "</br>" + a.Phone,
                              BillingAddress = a.FirstName + " " + a.LastName + "</br>" + a.AddressLine1 + "</br>" + a.AddressLine2 + "</br>" + a.Landmark + "</br>" + a.ZipPostal + "</br>" + a.Phone,
                              PaymentMode = pay.PaymentMode,
                              PaymentStatus = pay.Status,
                              FullName = u.Name,
                              EmailId = u.Email,
                              Mobile = u.PhoneNumber,
                              ResellerComment = x.ResellerComment,
                              AdminComment = x.AdminComment,
                              BILLHTML = x.BILLHTML,
                              BILLHTMLS = x.BILLHTMLS,
                              SoldBy = v.Name,
                              FirmName = v.ShopName,
                              EstimateDelivery = x.DatePlaced.AddDays(p.DeliveryStart).ToShortDateString() + " To " + x.DatePlaced.AddDays(p.DeliveryEnd).ToShortDateString(),
                              PaymentId = x.PaymentId,
                              PerAddress = v.ShopDescription,
                              SellerGSTNumber = v.GSTIN,
                              ImageURL = p.ImageURL1
                          }).ToList();
            }
            else
            {
                orders = (from x in _context.ShoppingOrder
                          join u in _context.Users on x.UserId equals u.Id
                          join p in _context.ProductItem on x.ProductId equals p.Id
                          join p1 in _context.Product on p.ProductId equals p1.Id
                          join pay in _context.Payments on x.PaymentId equals pay.Id
                          join a in _context.UserAddress on x.ShipppingAddress equals a.Id
                          join v in _context.Users on p1.UserId equals v.Id
                          where x.UserId == model.UserId && x.DatePlaced >= fromDate && x.DatePlaced <= toDate
                          orderby x.Id descending
                          select new ShoppingOrdersDetailsModel
                          {
                              Id = x.Id,
                              OrderId = x.OrderId,
                              OrderStatus = x.Status,
                              ProductItemId = x.ProductId,
                              IsBlock = x.IsActive,
                              OrderDate = x.DatePlaced.ToString(),
                              OrderUpdate = x.DateUpdated.ToString(),
                              ItemPrice = x.ItemPrice,
                              Quantity = x.Quantity,
                              SubTotal = x.SubTotal,
                              Tax = x.Tax,
                              Shipping = x.Shipping,
                              PrimePoint = x.PrimePoint,
                              TotalAmount = x.TotalAmount,
                              ShippingAddress = a.FirstName + " " + a.LastName + "</br>" + a.AddressLine1 + "</br>" + a.AddressLine2 + "</br>" + a.Landmark + "</br>" + a.ZipPostal + "</br>" + a.Phone,
                              BillingAddress = a.FirstName + " " + a.LastName + "</br>" + a.AddressLine1 + "</br>" + a.AddressLine2 + "</br>" + a.Landmark + "</br>" + a.ZipPostal + "</br>" + a.Phone,
                              PaymentMode = pay.PaymentMode,
                              PaymentStatus = pay.Status,
                              FullName = u.Name,
                              EmailId = u.Email,
                              Mobile = u.PhoneNumber,
                              ResellerComment = x.ResellerComment,
                              AdminComment = x.AdminComment,
                              BILLHTML = x.BILLHTML,
                              BILLHTMLS = x.BILLHTMLS,
                              SoldBy = v.Name,
                              FirmName = v.ShopName,
                              EstimateDelivery = x.DatePlaced.AddDays(p.DeliveryStart).ToShortDateString() + " To " + x.DatePlaced.AddDays(p.DeliveryEnd).ToShortDateString(),
                              PaymentId = x.PaymentId,
                              PerAddress = v.ShopDescription,
                              SellerGSTNumber = v.GSTIN,
                              ImageURL = p.ImageURL1
                          }).ToList();
            }
            return orders;
        }

        public List<ShoppingOrdersDetailsModel> UsersAcceptedOrders(int userId)
        {
            List<ShoppingOrdersDetailsModel> orders = new List<ShoppingOrdersDetailsModel>();
            if (userId == 1)
            {
                orders = (from x in _context.ShoppingOrder
                          join u in _context.Users on x.UserId equals u.Id
                          join p in _context.ProductItem on x.ProductId equals p.Id
                          join p1 in _context.Product on p.ProductId equals p1.Id
                          join pay in _context.Payments on x.PaymentId equals pay.Id
                          join a in _context.UserAddress on x.ShipppingAddress equals a.Id
                          join v in _context.Users on p1.UserId equals v.Id
                          where x.Status == "Accepted"
                          orderby x.Id descending
                          select new ShoppingOrdersDetailsModel
                          {
                              Id = x.Id,
                              OrderId = x.OrderId,
                              OrderStatus = x.Status,
                              ProductItemId = x.ProductId,
                              IsBlock = x.IsActive,
                              OrderDate = x.DatePlaced.ToString(),
                              OrderUpdate = x.DateUpdated.ToString(),
                              ItemPrice = x.ItemPrice,
                              Quantity = x.Quantity,
                              SubTotal = x.SubTotal,
                              Tax = x.Tax,
                              Shipping = x.Shipping,
                              PrimePoint = x.PrimePoint,
                              TotalAmount = x.TotalAmount,
                              ShippingAddress = a.FirstName + " " + a.LastName + "</br>" + a.AddressLine1 + "</br>" + a.AddressLine2 + "</br>" + a.Landmark + "</br>" + a.ZipPostal + "</br>" + a.Phone,
                              BillingAddress = a.FirstName + " " + a.LastName + "</br>" + a.AddressLine1 + "</br>" + a.AddressLine2 + "</br>" + a.Landmark + "</br>" + a.ZipPostal + "</br>" + a.Phone,
                              PaymentMode = pay.PaymentMode,
                              PaymentStatus = pay.Status,
                              FullName = u.Name,
                              EmailId = u.Email,
                              Mobile = u.PhoneNumber,
                              ResellerComment = x.ResellerComment,
                              AdminComment = x.AdminComment,
                              BILLHTML = x.BILLHTML,
                              BILLHTMLS = x.BILLHTMLS,
                              SoldBy = v.Name,
                              FirmName = v.ShopName,
                              EstimateDelivery = x.DatePlaced.AddDays(p.DeliveryStart).ToShortDateString() + " To " + x.DatePlaced.AddDays(p.DeliveryEnd).ToShortDateString(),
                              PaymentId = x.PaymentId,
                              PerAddress = v.ShopDescription,
                              SellerGSTNumber = v.GSTIN,
                              ImageURL = p.ImageURL1
                          }).ToList();
            }
            else
            {
                orders = (from x in _context.ShoppingOrder
                          join u in _context.Users on x.UserId equals u.Id
                          join p in _context.ProductItem on x.ProductId equals p.Id
                          join p1 in _context.Product on p.ProductId equals p1.Id
                          join pay in _context.Payments on x.PaymentId equals pay.Id
                          join a in _context.UserAddress on x.ShipppingAddress equals a.Id
                          join v in _context.Users on p1.UserId equals v.Id
                          where x.UserId == userId && x.Status == "Accepted"
                          orderby x.Id descending
                          select new ShoppingOrdersDetailsModel
                          {
                              Id = x.Id,
                              OrderId = x.OrderId,
                              OrderStatus = x.Status,
                              ProductItemId = x.ProductId,
                              IsBlock = x.IsActive,
                              OrderDate = x.DatePlaced.ToString(),
                              OrderUpdate = x.DateUpdated.ToString(),
                              ItemPrice = x.ItemPrice,
                              Quantity = x.Quantity,
                              SubTotal = x.SubTotal,
                              Tax = x.Tax,
                              Shipping = x.Shipping,
                              PrimePoint = x.PrimePoint,
                              TotalAmount = x.TotalAmount,
                              ShippingAddress = a.FirstName + " " + a.LastName + "</br>" + a.AddressLine1 + "</br>" + a.AddressLine2 + "</br>" + a.Landmark + "</br>" + a.ZipPostal + "</br>" + a.Phone,
                              BillingAddress = a.FirstName + " " + a.LastName + "</br>" + a.AddressLine1 + "</br>" + a.AddressLine2 + "</br>" + a.Landmark + "</br>" + a.ZipPostal + "</br>" + a.Phone,
                              PaymentMode = pay.PaymentMode,
                              PaymentStatus = pay.Status,
                              FullName = u.Name,
                              EmailId = u.Email,
                              Mobile = u.PhoneNumber,
                              ResellerComment = x.ResellerComment,
                              AdminComment = x.AdminComment,
                              BILLHTML = x.BILLHTML,
                              BILLHTMLS = x.BILLHTMLS,
                              SoldBy = v.Name,
                              FirmName = v.ShopName,
                              EstimateDelivery = x.DatePlaced.AddDays(p.DeliveryStart).ToShortDateString() + " To " + x.DatePlaced.AddDays(p.DeliveryEnd).ToShortDateString(),
                              PaymentId = x.PaymentId,
                              PerAddress = v.ShopDescription,
                              SellerGSTNumber = v.GSTIN,
                              ImageURL = p.ImageURL1
                          }).ToList();
            }
            return orders;
        }

        public Tuple<bool, string, int, int, string, string> UpdateOrderStatus(UpdateOrderStatusModel model)
        {
            var order = _context.ShoppingOrder.SingleOrDefault(x => x.Id == model.OrderId);
            if (order != null)
            {
                if (model.UserId == 1)
                {
                    if (string.IsNullOrEmpty(order.AdminComment))
                        order.AdminComment = DateTime.Now + " :- " + model.Comment;
                    else
                        order.AdminComment += "<br><hr>" + DateTime.Now + " :- " + model.Comment;
                }
                else
                {
                    if (string.IsNullOrEmpty(order.ResellerComment))
                        order.ResellerComment = DateTime.Now + " :- " + model.Comment;
                    else
                        order.ResellerComment += "<br><hr>" + DateTime.Now + " :- " + model.Comment;
                }
                order.Status = model.Status;
                _context.SaveChanges();

                var userAddress = _context.UserAddress.SingleOrDefault(x => x.Id == order.ShipppingAddress);
                string addressShipping = "<div class='col-md-12'><h5>" + userAddress.FirstName + " " + userAddress.LastName + "</h5><p><b>Address :</b><br />  " + userAddress.AddressLine1 + " " + userAddress.AddressLine2 + "</p> <p><b>Zip :</b>  " + userAddress.ZipPostal + "</p><p><b>Phone :</b>  " + userAddress.Phone + "</p></div>";

                return new Tuple<bool, string, int, int, string, string>(false, "Order updated sucessfully.", model.OrderId, order.UserId, "ORDER#" + order.OrderId, addressShipping);
            }
            else
                return new Tuple<bool, string, int, int, string, string>(false, "Order id not found.", model.OrderId, 0, "", "");
        }
    }
}
