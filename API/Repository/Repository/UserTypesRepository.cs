﻿using Microsoft.EntityFrameworkCore;
using Repository.Context;
using Repository.Interface;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository.Repository
{
    public class UserTypesRepository : IUserTypesRepository
    {
        private readonly DataContext _context;

        /// <summary>
        ///     Repository
        /// </summary>
        /// <param name="context"></param>
        public UserTypesRepository(DataContext context)
        {
            _context = context;
        }

        public List<UsersType> UserTypeList()
        {
            return _context.UsersType.Where(x => x.ID != 1).OrderBy(x => x.UserType).ToList();
        }

        /// <summary>
        ///     Get User By Id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UsersType GetUserTypeById(short typeId)
        {
            var user = _context.UsersType.FirstOrDefault(x => x.ID == typeId);
            return user;
        }
    }
}
