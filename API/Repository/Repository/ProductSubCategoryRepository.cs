﻿using System;
using System.Collections.Generic;
using System.Linq;
using Repository.Context;
using Repository.Interface;
using Repository.Model;

namespace Repository.Repository
{
    public class ProductSubCategoryRepository : IProductSubCategoryRepository
    {
        private readonly DataContext _context;

        /// <summary>
        /// Repository
        /// </summary>
        /// <param name="context"></param>
        public ProductSubCategoryRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get Product Sub Category
        /// </summary>
        /// <returns></returns>
        public List<ProductSubCategory> GetProductSubCategory()
        {
            var ProductSubCategory = _context.ProductSubCategory.Where(x => x.IsDeleted == false).ToList();
            return ProductSubCategory;
        }

        public List<ProductSubCategoryModel> GetAllProductSubCategory()
        {
            var ProductSubCategory = (from x in _context.ProductSubCategory
                                      join y in _context.ProductCategory on x.ProductCategoryId equals y.Id
                                      where x.IsDeleted == false
                                      orderby x.ProductCategoryId, x.Name
                                      select new ProductSubCategoryModel
                                      {
                                          Id = x.Id,
                                          ProductCategoryId = x.ProductCategoryId,
                                          ProductCategory = y.Name,
                                          Name = x.Name,
                                          Description = x.Description,
                                          ImageURL = x.ImageURL,
                                          IsActive = x.IsActive,
                                          CreatedOn = x.CreatedOn,
                                          CreatedBy = x.CreatedBy,
                                          ModifiedOn = x.ModifiedOn,
                                          ModifiedBy = x.ModifiedBy,
                                          ShowHide = x.ShowHide,
                                          MetaTag = x.MetaTag,
                                          Priority = x.Priority,
                                          Status = x.Status
                                      }).ToList();
            return ProductSubCategory;
        }

        public List<ProductSubCategory> GetProductSubCategoryByCategoryId(int productCategoryId)
        {
            var ProductSubCategory = _context.ProductSubCategory.Where(x => x.IsDeleted == false && x.ProductCategoryId == productCategoryId).ToList();
            return ProductSubCategory;
        }

        /// <summary>
        /// Add Product Sub Category
        /// </summary>
        /// <returns></returns>
        public bool AddProductSubCategory(ProductSubCategory productSubCategory)
        {
            var validateProductCategoryId = _context.ProductCategory.FirstOrDefault(x => x.Id == productSubCategory.ProductCategoryId);
            if (validateProductCategoryId != null)
            {
                _context.ProductSubCategory.Add(productSubCategory);
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Update Product Sub Category
        /// </summary>
        /// <returns></returns>
        public bool UpdateProductSubCategory(ProductSubCategory productSubCategory)
        {
            var getProductSubCategory = _context.ProductSubCategory.FirstOrDefault(x => x.Id == productSubCategory.Id);
            if (getProductSubCategory != null)
            {
                getProductSubCategory.ProductCategoryId = productSubCategory.ProductCategoryId;
                getProductSubCategory.Name = productSubCategory.Name;
                if (!string.IsNullOrEmpty(productSubCategory.ImageURL))
                    getProductSubCategory.ImageURL = productSubCategory.ImageURL;
                getProductSubCategory.Description = productSubCategory.Description;
                getProductSubCategory.ModifiedBy = productSubCategory.ModifiedBy;
                getProductSubCategory.ModifiedOn = DateTime.Now;
                getProductSubCategory.ShowHide = productSubCategory.ShowHide;
                getProductSubCategory.Priority = productSubCategory.Priority;
                getProductSubCategory.MetaTag = productSubCategory.MetaTag;
                getProductSubCategory.Status = productSubCategory.Status;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Delete Product Sub Category
        /// </summary>
        /// <returns></returns>
        public bool DeleteProductSubCategory(int id, string modifiedBy)
        {
            var ProductSubCategory = _context.ProductSubCategory.FirstOrDefault(x => x.Id == id);
            if (ProductSubCategory != null)
            {
                ProductSubCategory.IsDeleted = true;
                ProductSubCategory.ModifiedBy = modifiedBy;
                ProductSubCategory.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Activate / Deactivate Product Sub Category
        /// </summary>
        /// <returns></returns>
        public bool UpdateStatusProductSubCategory(int id, bool status, string modifiedBy)
        {
            var ProductSubCategory = _context.ProductSubCategory.FirstOrDefault(x => x.Id == id);
            ProductSubCategory.IsActive = status;
            ProductSubCategory.ModifiedBy = modifiedBy;
            ProductSubCategory.ModifiedOn = DateTime.Now;
            _context.SaveChanges();
            return true;
        }
    }
}