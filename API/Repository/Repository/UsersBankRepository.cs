﻿using Microsoft.EntityFrameworkCore;
using Models;
using Repository.Context;
using Repository.Interface;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository.Repository
{
    public class UsersBankRepository : IUsersBankRepository
    {
        private readonly DataContext _context;

        /// <summary>
        ///     Repository
        /// </summary>
        /// <param name="context"></param>
        public UsersBankRepository(DataContext context)
        {
            _context = context;
        }

        public Tuple<bool, int, string> UpdateBankDetails(UpdateBankDetailsModel addVendorBankDetails)
        {
            var user = _context.Users.AsNoTracking().FirstOrDefault(x => x.Id == addVendorBankDetails.ID && x.UserType == 3);
            if (user != null)
            {
                var userBank = _context.UsersBank.AsNoTracking().FirstOrDefault(x => x.UserID == user.Id && x.AccountNo == addVendorBankDetails.AccountNumber && x.IfscCode == addVendorBankDetails.IfscCode);
                if (userBank == null)
                {
                    UsersBank _add = new UsersBank()
                    {
                        UserID = user.Id,
                        BankName = addVendorBankDetails.BankName,
                        AccountNo = addVendorBankDetails.AccountNumber,
                        IfscCode = addVendorBankDetails.IfscCode,
                        ChequeURL = addVendorBankDetails.BlankChequeImageURL,
                        AddDate = DateTime.Now,
                        EditDate = DateTime.Now,
                        IsActive = true,
                        IsDelete = false,
                        Name = addVendorBankDetails.AccountHolderName
                    };
                    _context.UsersBank.Add(_add);
                    _context.SaveChanges();

                    return new Tuple<bool, int, string>(true, _add.ID, "Bank details added Successfully.");
                }
                else if (userBank.IsDelete)
                {
                    userBank.BankName = addVendorBankDetails.BankName;
                    userBank.AccountNo = addVendorBankDetails.AccountNumber;
                    userBank.IfscCode = addVendorBankDetails.IfscCode;
                    userBank.ChequeURL = addVendorBankDetails.BlankChequeImageURL;
                    userBank.EditDate = DateTime.Now;
                    userBank.IsActive = true;
                    userBank.IsDelete = false;
                    userBank.Name = addVendorBankDetails.AccountHolderName;
                    _context.UsersBank.Update(userBank);
                    _context.SaveChanges();

                    return new Tuple<bool, int, string>(true, userBank.ID, "Bank details added Successfully.");
                }
                else if (userBank != null)
                {
                    userBank.BankName = addVendorBankDetails.BankName;
                    userBank.AccountNo = addVendorBankDetails.AccountNumber;
                    userBank.IfscCode = addVendorBankDetails.IfscCode;
                    userBank.ChequeURL = addVendorBankDetails.BlankChequeImageURL;
                    userBank.EditDate = DateTime.Now;
                    userBank.IsActive = true;
                    userBank.IsDelete = false;
                    userBank.Name = addVendorBankDetails.AccountHolderName;
                    _context.UsersBank.Update(userBank);
                    _context.SaveChanges();

                    return new Tuple<bool, int, string>(true, userBank.ID, "Bank details added Successfully.");
                }
                else
                {
                    return new Tuple<bool, int, string>(false, 0, "Failed to updated.");
                }
            }
            else
                return new Tuple<bool, int, string>(false, 0, "Invalid userId.");
        }

        public List<UsersBank> UserBankList(int userID)
        {
            var userBank = _context.UsersBank.AsNoTracking().Where(x => x.UserID == userID && x.IsActive && !x.IsDelete).ToList();
            return userBank;
        }
    }
}
