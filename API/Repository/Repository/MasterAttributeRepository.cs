﻿using System;
using System.Collections.Generic;
using System.Linq;
using Repository.Context;
using Repository.Interface;
using Repository.Model;

namespace Repository.Repository
{
    public class MasterAttributeRepository : IMasterAttributeRepository
    {
        private readonly DataContext _context;

        /// <summary>
        /// Repository
        /// </summary>
        /// <param name="context"></param>
        public MasterAttributeRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get MasterAttribute Category
        /// </summary>
        /// <returns></returns>
        public List<MasterAttribute> GetMasterAttribute(int attributeId)
        {
            var attribute = _context.MasterAttribute.Where(x => x.IsDeleted == false && x.Id == attributeId).ToList();
            return attribute;
        }

        /// <summary>
        /// Get MasterAttribute Category
        /// </summary>
        /// <returns></returns>
        public List<MasterAttribute> GetMasterAllAttribute()
        {
            var attribute = _context.MasterAttribute.Where(x => x.IsDeleted == false).ToList();
            return attribute;
        }

        /// <summary>
        /// Add MasterAttribute Category
        /// </summary>
        /// <returns></returns>
        public bool AddMasterAttribute(MasterAttribute attribute)
        {
            _context.MasterAttribute.Add(attribute);
            _context.SaveChanges();
            return true;
        }

        /// <summary>
        /// Update MasterAttribute 
        /// </summary>
        /// <returns></returns>
        public bool UpdateMasterAttribute(MasterAttribute attribute)
        {
            var getMasterAttribute = _context.MasterAttribute.FirstOrDefault(x => x.Id == attribute.Id);
            if (getMasterAttribute != null)
            {
                getMasterAttribute.AttributeName = attribute.AttributeName;
                getMasterAttribute.AttributeType = attribute.AttributeType;
                getMasterAttribute.AttributeDesciption = attribute.AttributeDesciption;
                getMasterAttribute.ModifiedBy = attribute.ModifiedBy;
                getMasterAttribute.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Delete MasterAttribute 
        /// </summary>
        /// <returns></returns>
        public bool DeleteMasterAttribute(int id, string modifiedBy)
        {
            var attribute = _context.MasterAttribute.FirstOrDefault(x => x.Id == id);
            if (attribute != null)
            {
                attribute.IsDeleted = true;
                attribute.ModifiedBy = modifiedBy;
                attribute.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Activate / Deactivate MasterAttribute Category
        /// </summary>
        /// <returns></returns>
        public bool UpdateStatusMasterAttribute(int id, bool status, string modifiedBy)
        {
            var attribute = _context.MasterAttribute.FirstOrDefault(x => x.Id == id);
            attribute.IsActive = status;
            attribute.ModifiedBy = modifiedBy;
            attribute.ModifiedOn = DateTime.Now;
            _context.SaveChanges();
            return true;
        }
    }
}