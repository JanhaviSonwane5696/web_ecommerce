﻿using Repository.Context;
using Repository.Interface;
using Repository.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repository.Repository
{
    public class ShoppingCartRepository : IShoppingCartRepository
    {
        private readonly DataContext _context;

        /// <summary>
        /// Repository
        /// </summary>
        /// <param name="context"></param>
        public ShoppingCartRepository(DataContext context)
        {
            _context = context;
        }

        public Tuple<bool, int, string> AddShoppingCart(ShoppingCart cart)
        {
            if (cart.UserId > 0)
            {
                var shoppingCart = _context.ShoppingCart.FirstOrDefault(x => x.UserId == cart.UserId);
                if (shoppingCart == null)
                {
                    _context.ShoppingCart.Add(cart);
                    _context.SaveChanges();
                    return new Tuple<bool, int, string>(true, cart.Id, "Added to Cart.");
                }
                else
                    return new Tuple<bool, int, string>(true, shoppingCart.Id, "Added to Cart.");
            }
            else if (!string.IsNullOrEmpty(cart.GuiD))
            {
                var shoppingCart = _context.ShoppingCart.FirstOrDefault(x => x.GuiD == cart.GuiD);
                if (shoppingCart == null)
                {
                    _context.ShoppingCart.Add(cart);
                    _context.SaveChanges();
                    return new Tuple<bool, int, string>(true, cart.Id, "Added to Cart.");
                }
                else
                    return new Tuple<bool, int, string>(true, shoppingCart.Id, "Added to Cart.");
            }
            else
                return new Tuple<bool, int, string>(false, 0, "Guid or userid not found.");
        }

        public ShoppingCart GetShoppingCart(string guid, int userId)
        {
            if (!string.IsNullOrEmpty(guid))
            {
                return _context.ShoppingCart.FirstOrDefault(x => x.GuiD == guid);
            }
            else
            {
                return _context.ShoppingCart.FirstOrDefault(x => x.UserId == userId);
            }
        }

        public bool AddShoppingCartItem(ShoppingCartItems cartItem)
        {
            var shoppingCartData = _context.ShoppingCartItems.FirstOrDefault(x => x.ShoppingCardID == cartItem.ShoppingCardID && x.ProductId == cartItem.ProductId && x.ProductItemId == cartItem.ProductItemId && x.IsDeleted == false);
            if (shoppingCartData != null)
            {
                shoppingCartData.Quantity = shoppingCartData.Quantity + 1;
                _context.SaveChanges();
            }
            else
            {
                _context.ShoppingCartItems.Add(cartItem);
                _context.SaveChanges();
            }
            return true;
        }

        public bool DeleteShoppingCartItem(int cartItemID)
        {
            var shoppingCartData = _context.ShoppingCartItems.FirstOrDefault(x => x.Id == cartItemID);
            if (shoppingCartData != null)
            {
                if (shoppingCartData.Quantity == 0)
                {
                    shoppingCartData.IsDeleted = true;
                    _context.SaveChanges();
                    return true;
                }
                else
                {
                    shoppingCartData.Quantity = shoppingCartData.Quantity - 1;
                    _context.SaveChanges();
                    return true;
                }
            }
            else
                return false;
        }

        public bool DeletesShoppingCartItem(int cartItemID)
        {
            var shoppingCartData = _context.ShoppingCartItems.FirstOrDefault(x => x.Id == cartItemID);
            if (shoppingCartData != null)
            {
                shoppingCartData.IsDeleted = true;
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }

        public List<ShoppingCartItems> GetShoppingCartItem(int shoppingCardID)
        {
            return _context.ShoppingCartItems.Where(x => x.ShoppingCardID == shoppingCardID && x.IsDeleted == false).ToList();
        }

        public List<ShoppingCartItemsModel> GetShoppingCartItemWithProduct(int shoppingCardID)
        {
            return (from x in _context.ShoppingCartItems
                    join y in _context.ProductItem on x.ProductItemId equals y.Id
                    join z in _context.Product on y.ProductId equals z.Id
                    join u in _context.Users on z.UserId equals u.Id
                    where x.ShoppingCardID == shoppingCardID && x.IsDeleted == false
                    select new ShoppingCartItemsModel
                    {
                        Id = x.Id,
                        ShoppingCardID = x.ShoppingCardID,
                        ProductId = x.ProductId,
                        ProductItemId = x.ProductItemId,
                        Quantity = x.Quantity,
                        DateAdded = x.DateAdded,
                        Name = y.Name,
                        Price = y.Price,
                        DistributorPrice = y.DistributorPrice,
                        RetailPrice = y.RetailPrice,
                        SalePrice = y.SalePrice,
                        Weight = y.Weight,
                        ProductQuantity = y.Quantity,
                        ImageURL1 = y.ImageURL1,
                        ImageThumbURL1 = y.ImageThumbURL1,
                        ImageURL2 = y.ImageURL2,
                        ImageThumbURL2 = y.ImageThumbURL2,
                        ImageURL3 = y.ImageURL3,
                        ImageThumbURL3 = y.ImageThumbURL3,
                        Tax = !string.IsNullOrEmpty(y.Tax) ? (from a in _context.MasterTaxRates where y.Tax.Contains(a.Id.ToString()) select new TaxRatesModel { Id = a.Id, Name = a.Name, Amount = a.Amount, Type = a.Type }).ToList() : null,
                        PercentOff = (y.Price - y.SalePrice) / y.Price * 100,
                        DeliveryCharge = y.DeliveryCharge,
                        ShopName = u.ShopName,
                        UserName = u.Name
                    }).ToList();
        }

        public Tuple<bool, string> UpdateShoppingCart(string guid, int userId)
        {
            var shoppingCartData = _context.ShoppingCart.FirstOrDefault(x => x.GuiD == guid);
            if (shoppingCartData != null)
            {
                shoppingCartData.UserId = userId;
                _context.SaveChanges();
                return new Tuple<bool, string>(true, "Updated successfully.");
            }
            else
            {
                return new Tuple<bool, string>(true, "Data not found.");
            }
        }

        public List<ShoppingCartItemsListModel> CartItemsListViaUserId(int userId)
        {
            return (from x in _context.ShoppingCartItems
                    join y in _context.ShoppingCart on x.ShoppingCardID equals y.Id
                    join z in _context.ProductItem on x.ProductItemId equals z.Id
                    join z1 in _context.Product on z.ProductId equals z1.Id
                    where x.IsDeleted == false && z.IsDeleted == false && z1.IsDeleted == false && z.Status == "Approved" && y.UserId == userId
                    select new ShoppingCartItemsListModel
                    {
                        ProductName = z.Name,
                        Price = z.Price,
                        DistributorPrice = z.DistributorPrice,
                        RetailPrice = z.RetailPrice,
                        SalePrice = z.SalePrice,
                        ShoppingCartID = x.Id,
                        GuidID = y.GuiD,
                        ProductId = x.ProductId,
                        ProductItemId = x.ProductItemId,
                        Quantity = x.Quantity,
                        DateAddedItems = x.DateAdded,
                        DateCreatedCart = y.DateCreated,
                        DateUpdatedCart = y.DateUpdated,
                        HSNNumber = "",
                        SKUNumber = z.SKU,
                        SoldBy = "",
                        COD = false,
                        Weight = z.Weight,
                        ProductQuantity = z.Quantity,
                        ImageURL1 = z.ImageURL1,
                        DeliveryCharge = z.DeliveryCharge,
                        Tax = !string.IsNullOrEmpty(z.Tax) ? (from a in _context.MasterTaxRates where z.Tax.Contains(a.Id.ToString()) select new TaxRatesModel { Id = a.Id, Name = a.Name, Amount = a.Amount, Type = a.Type }).ToList() : null,
                    }).ToList();
        }
    }
}
