﻿using Microsoft.Extensions.Configuration;
using Models;
using Newtonsoft.Json;
using Repository.Context;
using Repository.Interface;
using Repository.Model;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Repository.Helpers
{
    public class NotificationHelpers : INotificationHelpers
    {
        private readonly DataContext _context;
        private string ApplicationId;
        private string SenderID;
        /// <summary>
        /// Repository
        /// </summary>
        /// <param name="context"></param>
        public NotificationHelpers(DataContext context, IConfiguration configuration)
        {
            _context = context;
            ApplicationId = configuration.GetSection("NotificationApplicationId").Value;
            SenderID = configuration.GetSection("NotificationSenderId").Value;
        }

        public struct NotificationType
        {
            public static string UserRegistration { get { return "User Registration"; } }
            public static string ProductPurchase { get { return "Product Purchase"; } }
            public static string Invoice { get { return "Invoice"; } }
            public static string PasswordReset { get { return "Password Reset"; } }
            public static string ShippingUpdate { get { return "Shipping Update"; } }
        }

        public bool UserVerification(string mailID, string MobileNumber, string otp)
        {
            MasterSMSTemplates templateSMS = _context.MasterSMSTemplates.FirstOrDefault(x => x.Type.ToUpper() == "USER VERIFICATION");
            if (templateSMS != null)
            {
                string SMS_USER_REGISTRATION = templateSMS.SMSContent.Replace("#OTP", otp);
                SendSMS(MobileNumber, SMS_USER_REGISTRATION);
            }

            if (!string.IsNullOrEmpty(mailID))
            {
                MasterEmailTemplates templateEmail = _context.MasterEmailTemplates.FirstOrDefault(x => x.Type.ToUpper() == "USER VERIFICATION");
                if (templateEmail != null)
                {
                    string SUBJECT = templateEmail.Subject.Replace("#OTP", otp);
                    string BODY = templateEmail.Body.Replace("#OTP", otp);
                    SendEmail(BODY, SUBJECT, mailID);
                }
            }
            return true;
        }

        public bool UserRegistration(string mailID, string MobileNumber, string name)
        {
            MasterSMSTemplates templateSMS = _context.MasterSMSTemplates.FirstOrDefault(x => x.Type.ToUpper() == "USER REGISTRATION");
            if (templateSMS != null)
            {
                string SMS_USER_REGISTRATION = templateSMS.SMSContent.Replace("#NAME", name);
                SendSMS(MobileNumber, SMS_USER_REGISTRATION);
            }
            if (!string.IsNullOrEmpty(mailID))
            {
                MasterEmailTemplates templateEmail = _context.MasterEmailTemplates.FirstOrDefault(x => x.Type.ToUpper() == "USER REGISTRATION");
                if (templateEmail != null)
                {
                    string SUBJECT = templateEmail.Subject.Replace("#NAME", name);
                    string BODY = templateEmail.Body.Replace("#NAME", name);
                    SendEmail(BODY, SUBJECT, mailID);
                }
            }
            return true;
        }

        public bool UserKYCVerification(string mailID, string MobileNumber, string name, string status, string remarks)
        {
            MasterSMSTemplates templateSMS = _context.MasterSMSTemplates.FirstOrDefault(x => x.Type.ToUpper() == "USER KYC VERIFICATION");
            if (templateSMS != null)
            {
                string SMS_USER_REGISTRATION = templateSMS.SMSContent.Replace("#NAME", name).Replace("#STATUS", status).Replace("#REMARKS", remarks);
                SendSMS(MobileNumber, SMS_USER_REGISTRATION);
            }
            if (!string.IsNullOrEmpty(mailID))
            {
                MasterEmailTemplates templateEmail = _context.MasterEmailTemplates.FirstOrDefault(x => x.Type.ToUpper() == "USER KYC VERIFICATION");
                if (templateEmail != null)
                {
                    string SUBJECT = templateEmail.Subject.Replace("#NAME", name).Replace("#STATUS", status).Replace("#REMARKS", remarks);
                    string BODY = templateEmail.Body.Replace("#NAME", name).Replace("#STATUS", status).Replace("#REMARKS", remarks);
                    SendEmail(BODY, SUBJECT, mailID);
                }
            }
            return true;
        }

        public bool ProductPurchase(string mailID, string MobileNumber, string name, string orderContent, string orderID, string ShippingAddress)
        {
            MasterSMSTemplates templateSMS = _context.MasterSMSTemplates.FirstOrDefault(x => x.Type.ToUpper() == "PRODUCT PURCHASE");
            if (templateSMS != null)
            {
                string SMS_PRODUCT_PURCHASE = templateSMS.SMSContent.Replace("#NAME", name).Replace("#ORDERID", orderID);
                SendSMS(MobileNumber, SMS_PRODUCT_PURCHASE);
            }

            MasterEmailTemplates templateEmail = _context.MasterEmailTemplates.FirstOrDefault(x => x.Type.ToUpper() == "PRODUCT PURCHASE");
            if (templateEmail != null)
            {
                string SUBJECT = templateEmail.Subject.Replace("#NAME", name).Replace("#ORDERID", orderID);
                string BODY = templateEmail.Body.Replace("#NAME", name).Replace("#ORDERCONTENT", name).Replace("#ORDERID", orderID).Replace("#SHIPPINGADDRESS", ShippingAddress);
                SendEmail(BODY, SUBJECT, mailID);
            }
            return true;
        }

        public bool Invoice(string mailID, string MobileNumber, string name, string orderContent, string orderID, string BillingAddress)
        {
            MasterSMSTemplates templateSMS = _context.MasterSMSTemplates.FirstOrDefault(x => x.Type.ToUpper() == "INVOICE");
            if (templateSMS != null)
            {
                string SMS_INVOICE = templateSMS.SMSContent.Replace("#NAME", name).Replace("#ORDERID", orderID);
                SendSMS(MobileNumber, SMS_INVOICE);
            }

            MasterEmailTemplates templateEmail = _context.MasterEmailTemplates.FirstOrDefault(x => x.Type.ToUpper() == "INVOICE");
            if (templateEmail != null)
            {
                string SUBJECT = templateEmail.Subject.Replace("#NAME", name).Replace("#ORDERID", orderID);
                string BODY = templateEmail.Body.Replace("#NAME", name).Replace("#ORDERCONTENT", name).Replace("#ORDERID", orderID).Replace("#BILLINGADDRESS", BillingAddress);
                SendEmail(BODY, SUBJECT, mailID);
            }
            return true;
        }

        public bool PasswordReset(string mailID, string MobileNumber, string name, string NewPassword)
        {
            MasterSMSTemplates templateSMS = _context.MasterSMSTemplates.FirstOrDefault(x => x.Type.ToUpper() == "PASSWORD RESET");
            if (templateSMS != null)
            {
                string SMS_PASSWORD_RESET = templateSMS.SMSContent.Replace("#NAME", name).Replace("#PASSWORD", NewPassword);
                SendSMS(MobileNumber, SMS_PASSWORD_RESET);
            }

            MasterEmailTemplates templateEmail = _context.MasterEmailTemplates.FirstOrDefault(x => x.Type.ToUpper() == "PASSWORD RESET");
            if (templateEmail != null)
            {
                string SUBJECT = templateEmail.Subject.Replace("#NAME", name);
                string BODY = templateEmail.Body.Replace("#NAME", name).Replace("#PASSWORD", NewPassword);
                SendEmail(BODY, SUBJECT, mailID);
            }
            return true;
        }

        public bool ShippingUpdate(string mailID, string MobileNumber, string name, string status, string orderID, string ShippingAddress)
        {
            MasterSMSTemplates templateSMS = _context.MasterSMSTemplates.FirstOrDefault(x => x.Type.ToUpper() == "SHIPPING UPDATE");
            if (templateSMS != null)
            {
                string SMS_SHIPPING_UPDATE = templateSMS.SMSContent.Replace("#NAME", name).Replace("#STATUS", status).Replace("#ORDERID", orderID);
                SendSMS(MobileNumber, SMS_SHIPPING_UPDATE);
            }

            MasterEmailTemplates templateEmail = _context.MasterEmailTemplates.FirstOrDefault(x => x.Type.ToUpper() == "SHIPPING UPDATE");
            if (templateEmail != null)
            {
                string SUBJECT = templateEmail.Subject.Replace("#NAME", name).Replace("#ORDERID", orderID);
                string BODY = templateEmail.Body.Replace("#NAME", name).Replace("#ORDERID", orderID).Replace("#STATUS", status).Replace("#SHIPPINGADDRESS", ShippingAddress);
                SendEmail(BODY, SUBJECT, mailID);
            }
            return true;
        }

        public bool SendEmail(string mailBody, string mailSubject, string To)
        {
            MasterEmailSettings sett = _context.MasterEmailSettings.FirstOrDefault(x => x.Id == 1);
            if (Convert.ToBoolean(sett.IsMailActive))
            {
                try
                {
                    try
                    {
                        MailMessage mailMsg = new MailMessage();
                        SmtpClient mailObj = new SmtpClient(sett.HostName, Convert.ToInt32(sett.Port));

                        mailObj.Credentials = new NetworkCredential(sett.FromEmailAddress, sett.Password);

                        mailMsg.From = new MailAddress(sett.FromName);

                        string[] temp = To.Split(';');
                        for (int i = 0; i < temp.Length; i++)
                        {
                            if (temp[i].ToString().Length > 1)
                                mailMsg.To.Add(temp[i].ToString());
                        }
                        if (!string.IsNullOrEmpty(sett.CC))
                        {
                            temp = sett.CC.Split(';');
                            for (int i = 0; i < temp.Length; i++)
                            {
                                if (temp[i].ToString().Length > 1)
                                    mailMsg.CC.Add(temp[i].ToString());
                            }
                        }

                        mailMsg.Subject = mailSubject;
                        mailMsg.Body = mailBody;
                        mailMsg.IsBodyHtml = true;
                        mailObj.EnableSsl = sett.MailSSL;
                        int count = 0;
                        string oldChar = ExtractImages(mailBody, ref count);
                        Random RGen = new Random();
                        while (oldChar != "")
                        {
                            string imgPath = oldChar;
                            int startIndex = imgPath.ToLower().IndexOf("images/");
                            if (startIndex > 0)
                            {
                                imgPath = imgPath.Substring(startIndex);
                                imgPath = imgPath.Replace("/", "\\");
                                Attachment A = new Attachment(imgPath);
                                A.ContentId = RGen.Next(100000, 9999999).ToString();
                                mailBody = mailBody.Replace(oldChar, "cid:" + A.ContentId);
                                mailMsg.Attachments.Add(A);
                                oldChar = ExtractImages(mailBody, ref count);
                            }
                            else
                            {
                                oldChar = ExtractImages(mailBody, ref count);
                            }
                        }
                        mailMsg.Body = mailBody;
                        mailObj.Send(mailMsg);
                    }
                    catch (Exception ex)
                    {
                    }
                }
                catch { return false; }
            }
            return true;
        }

        public bool SendSMS(string number, string message)
        {
            MasterSMSApi sett = _context.MasterSMSApi.FirstOrDefault(x => x.Status == true);
            if (Convert.ToBoolean(sett.Status))
            {
                try
                {
                    string FinalURL = sett.Username.Replace("#MESSAGE", message).Replace("#NUMBER", number);
                    WebClient client = new WebClient();
                    string downloadString = client.DownloadString(FinalURL);
                }
                catch { return false; }
            }
            return true;
        }

        private static string ExtractImages(string body, ref int count)
        {
            int startIndex = body.ToLower().IndexOf("<img src=\"", count);
            int endIndex;
            if (startIndex >= 0)
            {
                endIndex = body.IndexOf("\"", startIndex + 10);
            }
            else
            {
                return "";
            }
            startIndex = startIndex + 10;
            string imgurl = body.Substring(startIndex, (endIndex - (startIndex)));
            count = startIndex;
            return imgurl;
        }

        public string PushNotification(string deviceId, PushNotificationModel model)
        {
            try
            {
                string jsonStr = string.Empty;
                if (!string.IsNullOrEmpty(model.NotificationImageURL))
                    jsonStr = "{\"notification\":{\"Title\":\"" + model.Title + "\",\"Messege\":\"" + model.Message + "\",\"ImageUrl\":\"" + model.NotificationImageURL + "\",\"Link\":\"" + model.Link + "\"}}";
                else
                    jsonStr = "{\"notification\":{\"Type\":\"Msg\",\"Title\":\"" + model.Title + "\",\"Messege\":\"" + model.Message + "\"}}";

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = deviceId,
                    data = new
                    {
                        body = jsonStr,
                        title = "This is the title of Message",
                        icon = "myicon"
                    },
                    priority = "high"

                };
                var json = JsonConvert.SerializeObject(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", ApplicationId));
                tRequest.Headers.Add(string.Format("Sender: id={0}", SenderID));
                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                jsonStr = sResponseFromServer;
                            }
                        }
                    }
                }
                return jsonStr;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}