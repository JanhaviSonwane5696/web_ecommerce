﻿using Microsoft.EntityFrameworkCore;
using Repository.Model;

namespace Repository.Context
{
    public class DataContext : DbContext
    {
        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<UsersType> UsersType { get; set; }
        public virtual DbSet<UsersVerification> UsersVerification { get; set; }
        public virtual DbSet<UserDocuments> UserDocuments { get; set; }
        public virtual DbSet<UserAddress> UserAddress { get; set; }
        public virtual DbSet<ProductCategoryGroup> ProductCategoryGroup { get; set; }
        public virtual DbSet<ProductCategory> ProductCategory { get; set; }
        public virtual DbSet<ProductSubCategory> ProductSubCategory { get; set; }
        public virtual DbSet<ProductTags> ProductTags { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<ProductItem> ProductItem { get; set; }
        public virtual DbSet<ProductRelated> ProductRelated { get; set; }
        public virtual DbSet<UsersBank> UsersBank { get; set; }
        public virtual DbSet<AllBankName> AllBankName { get; set; }
        public virtual DbSet<PinCodes> PinCodes { get; set; }
        public virtual DbSet<MasterEmailSettings> MasterEmailSettings { get; set; }
        public virtual DbSet<MasterEmailTemplates> MasterEmailTemplates { get; set; }
        public virtual DbSet<MasterSMSApi> MasterSMSApi { get; set; }
        public virtual DbSet<MasterSMSTemplates> MasterSMSTemplates { get; set; }
        public virtual DbSet<MasterAttribute> MasterAttribute { get; set; }
        public virtual DbSet<MasterAttributeItems> MasterAttributeItems { get; set; }
        public virtual DbSet<MasterTaxRates> MasterTaxRates { get; set; }
        public virtual DbSet<MasterBrands> MasterBrands { get; set; }
        public virtual DbSet<ShoppingCart> ShoppingCart { get; set; }
        public virtual DbSet<ShoppingCartItems> ShoppingCartItems { get; set; }
        public virtual DbSet<ShoppingOrder> ShoppingOrder { get; set; }
        public virtual DbSet<Payments> Payments { get; set; }
        public virtual DbSet<ProcessPay> ProcessPay { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
        }
    }
}