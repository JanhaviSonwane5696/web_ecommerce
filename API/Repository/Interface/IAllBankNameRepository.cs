﻿using Models;
using Repository.Model;
using System.Collections.Generic;

namespace Repository.Interface
{
    public interface IAllBankNameRepository
    {
        List<string> BankNameList();
        List<string> BankStateNameList(BankStateModel bankDetails);
        List<string> BankDistrictNameList(BankDistrictModel bankDetails);
        List<string> BankBranchNameList(BankBranchModel bankDetails);
        AllBankName BankBranchDetails(BankBranchDetailsModel bankDetail);
        AllBankName IFSCDetails(BankDetailsModel bankDetail);
    }
}
