﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models;
using Repository.Model;

namespace Repository.Interface
{
    public interface IMasterAttributeRepository
    {
        List<MasterAttribute> GetMasterAttribute(int attributeId);
        List<MasterAttribute> GetMasterAllAttribute();
        bool AddMasterAttribute(MasterAttribute attribute);
        bool UpdateMasterAttribute(MasterAttribute attribute);
        bool DeleteMasterAttribute(int id, string modifiedBy);
        bool UpdateStatusMasterAttribute(int id, bool status, string modifiedBy);
    }
}