﻿using System;
using System.Collections.Generic;
using Models;
using Repository.Model;

namespace Repository.Interface
{
    public interface IUserRepository
    {
        Tuple<bool, int> AuthenticateUser(UserLoginModel loginDetails);
        Tuple<bool, int> ResetPassword(ResetUserPasswordModel loginDetails);
        Tuple<bool, int> ForgotPassword(ResetUserPasswordModel loginDetails);
        Tuple<bool, string> ChangePassword(UserSettingModel userData);
        Tuple<bool, int, string> AddUser(AddUserModel addUserDetails, string status, string createdBy);
        Tuple<bool, int, string> UpdateUser(UpdateUserModel addUserDetails, string createdBy);
        Tuple<bool, int, string> UpdateUserStatus(UpdateUserStatusModel addUserDetails, string createdBy);
        Users GetUserById(int userId);
        List<Users> UserList();
        List<Users> UserByUserType(short userType);
        Tuple<bool, string> UpdateDeviceID(int userID, string deviceID, string modifiedBy);

        Tuple<bool,string > ValidateUserMobile(string mobileNumber, short userType);
        Tuple<bool, int, string> AddCustomer(AddCustomerConfirmRequestModel addUserDetails, string status, string createdBy);
        Tuple<bool,int, string> UpdateCustomerEmail(UpdateCustomerRequestModel updateUserDetails);


        Tuple<bool, int, string> AddVendor(AddVendorModel addVendorDetails, string status, string createdBy);
        Tuple<bool, int, string> UpdatePickUpAddress(UpdatePickupPinAddressModel addVendorDetails, string status, string modifyBy);
        Tuple<bool, int, string> UpdateGSTIN(UpdateGSTINModel addVendorDetails, string status, string modifyBy);
        PinCodes PinCodeDetails(PickUpAddressModel pickupDetails);
        Tuple<bool, int, string> UpdateStoreDetails(StoreDetailsModel addVendorStoreDetails, string status, string modifyBy);
        List<UsersList> VendorsList();
        Tuple<bool, int, string> UpdatedUserStatus(int userID, string status, string modifyBy);
        Tuple<bool, int, string> UpdateProfileDetails(UpdateProfileDetailsModel userDetails);
        Tuple<bool, int, string> UpdateUserDocument(int userID, string documentType, bool documentNumber, string documentURL, string docNumber, string createdBy);
        UserDocuments GetUserDocumentById(int userId, string documentType);
        List<UsersListModel> VendorsWithAdminList();
        List<UserDocuments> GetUserDocumentById(int userId);
    }
}