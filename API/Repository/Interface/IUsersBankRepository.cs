﻿using Models;
using Repository.Model;
using System;
using System.Collections.Generic;

namespace Repository.Interface
{
    public interface IUsersBankRepository
    {
        Tuple<bool, int, string> UpdateBankDetails(UpdateBankDetailsModel addVendorBankDetails);
        List<UsersBank> UserBankList(int userID);
    }
}
