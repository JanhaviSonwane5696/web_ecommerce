﻿using Models;
using Repository.Model;
using System;
using System.Collections.Generic;

namespace Repository.Interface
{
    public interface IShoppingOrderRepository
    {
        int AddPayment(int userId, string payment, decimal amount, int orderId);
        bool UpdatePayment(int paymentId, string status, string note, decimal walletAmount, decimal gatewayAmount, decimal discountAmount);
        ShoppingOrder AddOrder(ShoppingOrder model);
        long AddProcessPay(int userId, string emailId, string mobileNo, int paymentId, decimal amount, int orderId, string description);
        List<ShoppingOrderDetailsModel> UserOrders(int userId);
        ShoppingOrderDetailModel UserOrderDetails(int userId, int id);
        List<ShoppingOrdersDetailsModel> UsersOrders(OrderReportModel model);
        List<ShoppingOrdersDetailsModel> UsersAcceptedOrders(int userId);
        Tuple<bool, string, int, int, string, string> UpdateOrderStatus(UpdateOrderStatusModel model);
    }
}
