﻿using System.Collections.Generic;
using Repository.Model;

namespace Repository.Interface
{
    public interface IProductSubCategoryRepository
    {
        List<ProductSubCategory> GetProductSubCategory();
        List<ProductSubCategoryModel> GetAllProductSubCategory();
        bool AddProductSubCategory(ProductSubCategory ProductSubCategory);
        List<ProductSubCategory> GetProductSubCategoryByCategoryId(int productCategoryId);
        bool UpdateProductSubCategory(ProductSubCategory ProductSubCategory);
        bool DeleteProductSubCategory(int id, string modifiedBy);
        bool UpdateStatusProductSubCategory(int id, bool status, string modifiedBy);
    }
}