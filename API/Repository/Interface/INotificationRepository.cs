﻿using Models;
using Repository.Model;
using System;
using System.Collections.Generic;

namespace Repository.Interface
{
    public interface INotificationRepository
    {
        List<MasterSMSApi> SMSAPIList();
        Tuple<bool, int, string> AddSMSAPI(AddNotificationSMSAPIModel addSMS, string createdBy);
        Tuple<bool, int, string> UpdateSMSAPI(UpdateNotificationSMSAPIModel updateSMS, string createdBy);
        Tuple<bool, int, string> UpdateSMSAPIStatus(UpdateNotificationSMSAPIStatusModel updateSMS, string createdBy);

        List<MasterSMSTemplates> SMSAPITemplateList();
        Tuple<bool, int, string> AddSMS(AddNotificationSMSModel addSMS, string createdBy);
        Tuple<bool, int, string> UpdateSMS(UpdateNotificationSMSModel updateSMS, string createdBy);
        Tuple<bool, int, string> UpdateSMSStatus(UpdateStatusNotificationSMSModel updateSMS, string createdBy);
        Tuple<bool, int, string> DeleteSMS(DeleteNotificationSMSModel updateSMS, string createdBy);


        List<MasterEmailSettings> EmailAPIList();
        Tuple<bool, int, string> AddEmailAPI(AddNotificationEmailAPIModel addEmail, string createdBy);
        Tuple<bool, int, string> UpdateEmailAPI(UpdateNotificationEmailAPIModel updateEmail, string createdBy);
        Tuple<bool, int, string> UpdateEmailAPIStatus(UpdateNotificationEmailAPIStatusModel updateEmail, string createdBy);


        List<MasterEmailTemplates> EmailAPITemplateList();
        Tuple<bool, int, string> AddEmail(AddNotificationEmailModel addEmail, string createdBy);
        Tuple<bool, int, string> UpdateEmail(UpdateNotificationEmailModel updateEmail, string createdBy);
        Tuple<bool, int, string> UpdateEmailStatus(UpdateStatusNotificationEmailModel updateEmail, string createdBy);
    }
}
