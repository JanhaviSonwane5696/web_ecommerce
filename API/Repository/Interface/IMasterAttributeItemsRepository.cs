﻿using Repository.Model;
using System.Collections.Generic;

namespace Repository.Interface
{
    public interface IMasterAttributeItemsRepository
    {
        List<MasterAttributeItems> GetMasterAttributeItems(int attributeId);
        List<MasterAttributeItems> GetMasterAttributeItemsById(int attributeItemId);
        List<MasterAttributeItems> GetMasterAllAttributeItems();
        bool AddMasterAttributeItems(MasterAttributeItems attributeItem);
        bool UpdateMasterAttributeItems(MasterAttributeItems attributeItem);
        bool DeleteMasterAttributeItems(int id, string modifiedBy);
        bool UpdateStatusMasterAttributeItems(int id, bool status, string modifiedBy);
    }
}