﻿using Repository.Model;
using System;

namespace Repository.Interface
{
    public interface IUsersVerificationRepository
    {
        Tuple<bool> AddUpdateVerification(string phoneNumber, short userType, string otp);
        UsersVerification GetUserVerificationByMobile(string mobileNo, short userType);
    }
}
