﻿using Repository.Model;
using System.Collections.Generic;

namespace Repository.Interface
{
    public interface IProductRelatedRepository
    {
        List<ProductRelated> GetProductRelated();
        bool AddProductRelated(ProductRelated productRelated);
        bool UpdateProductRelated(ProductRelated productRelated);
        bool DeleteProductRelated(int id, string modifiedBy);
        bool UpdateStatusProductRelated(int id, bool status, string modifiedBy);
    }
}