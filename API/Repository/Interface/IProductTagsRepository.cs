﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models;
using Repository.Model;

namespace Repository.Interface
{
    public interface IProductTagsRepository
    {
        List<ProductTags> GetProductTags();
        bool AddProductTags(ProductTags productTags);
        bool UpdateProductTags(ProductTags productTags);
        bool DeleteProductTags(int id, string modifiedBy);
        bool UpdateStatusProductTags(int id, bool status, string modifiedBy);
    }
}