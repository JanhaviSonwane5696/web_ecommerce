﻿using Repository.Model;
using System;
using System.Collections.Generic;

namespace Repository.Interface
{
    public interface IProductRepository
    {
        List<Product> GetProduct(int userId);
        List<ProductsModel> GetAllProduct(int userId);
        Tuple<bool, int> AddProduct(Product product);
        bool UpdateProduct(Product product, int userId);
        bool DeleteProduct(int id, int userId, string modifiedBy);
        bool UpdateStatusProduct(int id, int userId, bool status, string modifiedBy);
        bool UpdateStatusProduct(int id, int userId, string status, string remarks, string modifiedBy);
        List<Product> GetProductByCategoryId(int categoryId, int userId);
        List<ProductsModel> GetAllProductByCategoryId(int userId, int categoryId);
        List<Product> GetProductBySubCategoryId(int subCategoryId, int userId);
        List<ProductsModel> GetAllProductBySubCategoryId(int userId, int subCategoryId);
        List<ProductsModel> GetProductByStatus(int userId, string status);
        ProductAttributesModel ProductAttributeList(int productId);
    }
}