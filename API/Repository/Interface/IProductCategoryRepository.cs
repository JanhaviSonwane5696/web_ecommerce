﻿using Repository.Model;
using System.Collections.Generic;

namespace Repository.Interface
{
    public interface IProductCategoryRepository
    {
        List<ProductCategoryModel> GetProductCategory();
        bool AddProductCategory(ProductCategory productCategory);
        bool UpdateProductCategory(ProductCategory productCategory);
        bool DeleteProductCategory(int id, string modifiedBy);
        bool UpdateStatusProductCategory(int id, bool status, string modifiedBy);
    }
}