﻿using Models;

namespace Repository.Interface
{
    public interface INotificationHelpers
    {
        bool UserVerification(string mailID, string MobileNumber, string otp);
        bool UserRegistration(string mailID, string MobileNumber, string name);
        bool UserKYCVerification(string mailID, string MobileNumber, string name, string status, string remarks);
        bool ProductPurchase(string mailID, string MobileNumber, string name, string orderContent, string orderID, string ShippingAddress);
        bool Invoice(string mailID, string MobileNumber, string name, string orderContent, string orderID, string BillingAddress);
        bool PasswordReset(string mailID, string MobileNumber, string name, string NewPassword);
        bool ShippingUpdate(string mailID, string MobileNumber, string name, string status, string orderID, string ShippingAddress);
        bool SendEmail(string mailBody, string mailSubject, string To);
        bool SendSMS(string number, string message);
        string PushNotification(string deviceId, PushNotificationModel model);
    }
}