﻿using Repository.Model;
using System.Collections.Generic;

namespace Repository.Interface
{
    public interface IProductCategoryGroupRepository
    {
        List<ProductCategoryGroup> GetProductCategoryGroup();
        bool AddProductCategoryGroup(ProductCategoryGroup productCategory);
        bool UpdateProductCategoryGroup(ProductCategoryGroup productCategory);
        bool DeleteProductCategoryGroup(int id, string modifiedBy);
        bool UpdateStatusProductCategoryGroup(int id, bool status, string modifiedBy);
    }
}