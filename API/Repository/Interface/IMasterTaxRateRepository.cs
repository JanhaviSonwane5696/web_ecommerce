﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models;
using Repository.Model;

namespace Repository.Interface
{
    public interface IMasterTaxRateRepository
    {
        List<MasterTaxRates> GetMasterTaxRate(int taxId);
        List<MasterTaxRates> GetAllMasterTaxRate();
        bool AddMasterTaxRate(MasterTaxRates taxRate);
        bool UpdateMasterTaxRate(MasterTaxRates taxRate);
        bool DeleteMasterTaxRate(int id, string modifiedBy);
        bool UpdateStatusMasterTaxRate(int id, bool status, string modifiedBy);
    }
}