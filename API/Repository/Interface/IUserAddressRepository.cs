﻿using Repository.Model;
using System;
using System.Collections.Generic;

namespace Repository.Interface
{
    public interface IUserAddressRepository
    {
        List<UserAddress> GetUserAddress(int userId);
        UserAddress GetUserAddress(int userId, int addressId);
        Tuple<bool, string> AddUserAddress(UserAddress model);
        Tuple<bool, string> Update(UserAddress model);
        Tuple<bool, string> DeleteUserAddress(int id, int userId, string modifiedBy);
    }
}
