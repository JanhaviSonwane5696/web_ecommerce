﻿using Repository.Model;
using System.Collections.Generic;

namespace Repository.Interface
{
    public interface IProductItemRepository
    {
        ProductItem GetProductItemById(int id);
        List<ProductItem> GetProductItems(int productId);
        List<ProductItemsModel> GetProductItem(int productItemId);
        List<ProductItem> GetProductItems(int productId, int productItemId);
        List<ProductItemsModel> GetProductItems(int productId, string status);
        List<ProductItemModel> GetProductItemsByStatus(int userId, int productId, string status);
        List<ProductItemAttributesModel> ProductItemAttributeList(int productId);
        List<ShortProductItemModel> ProductItemShortList(int productId);
        bool AddProductItem(ProductItem product);
        bool UpdateProductItem(ProductItem product);
        bool DeleteProductItem(int id, string modifiedBy);
        bool UpdateStatusProductItem(int id, bool status, string modifiedBy);
        bool UpdateStatusProductItem(int id, string status, string remarks, string modifiedBy);
    }
}