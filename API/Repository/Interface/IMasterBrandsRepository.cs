﻿using System.Collections.Generic;
using Repository.Model;

namespace Repository.Interface
{
    public interface IMasterBrandsRepository
    {
        List<MasterBrands> GetMasterBrands(int BrandsId);
        List<MasterBrands> GetMasterAllBrands();
        bool AddMasterBrands(MasterBrands Brands);
        bool UpdateMasterBrands(MasterBrands Brands);
        bool DeleteMasterBrands(int id, string modifiedBy);
        bool UpdateStatusMasterBrands(int id, bool status, string modifiedBy);
    }
}