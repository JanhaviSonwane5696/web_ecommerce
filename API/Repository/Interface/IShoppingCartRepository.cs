﻿using System;
using System.Collections.Generic;
using Repository.Model;

namespace Repository.Interface
{
    public interface IShoppingCartRepository
    {
        Tuple<bool, int, string> AddShoppingCart(ShoppingCart cart);
        ShoppingCart GetShoppingCart(string guid, int userId);
        bool AddShoppingCartItem(ShoppingCartItems cartItem);
        bool DeleteShoppingCartItem(int cartItemID);
        bool DeletesShoppingCartItem(int cartItemID);
        List<ShoppingCartItems> GetShoppingCartItem(int shoppingCardID);
        List<ShoppingCartItemsModel> GetShoppingCartItemWithProduct(int shoppingCardID);
        Tuple<bool, string> UpdateShoppingCart(string guid, int userId);
        List<ShoppingCartItemsListModel> CartItemsListViaUserId(int userId);
    }
}