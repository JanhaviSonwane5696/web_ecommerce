﻿using Repository.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Interface
{
   public interface IUserTypesRepository
    {
        List<UsersType> UserTypeList();
        UsersType GetUserTypeById(short typeId);
    }
}
